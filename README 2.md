# HomeArte

The HomeArte base repository that contains all artifacts related to the homearte product.


### Running the web app

1. Start the __MySQL__ server
2. Setup the __back end__
    - Follow the steps in the <a href="#setting-up-the-back-end">Setting up the back end</a> section ( one-time step )
3. Setup the __front end__
    - Follow the steps in the <a href="#setting-up-the-front-end">Setting up the front end</a> section ( one-time step )
    - Open a terminal and navigate to the directory containing this ReadMe file
    - Type `grunt` and press return
    - Let the command complete
4. Build the Maven project by using the following command
    ```mvn clean install```
5. Copy the `homearteweb.war` file present in the `HomeArte-Web/target/` directory into the `webapps/` directory of the Jetty server
6. Now navigate to the directory containing __Jetty__ and start the server using the following command
    ```	```
7. Access [https://localhost:8080](https://localhost:8080) in a web browser
8. Done


### Setting up the front end

__Install the following__

1. Node and npm
    - Go to [this](https://nodejs.org/) link and download Node
    - Install it ( npm comes bundled up with it )
    - Open terminal and type `node -v`
    - Open terminal and type `npm -v`
    - If versions are displayed for both the above commands, Node and npm have been successfully installed

2. [grunt-cli](http://gruntjs.com/getting-started#installing-the-cli "Grunt")
    - Open terminal and navigate to the directory containing this ReadMe file
    - Type `npm install -g grunt-cli` and hit return
    - Let the command complete ( the above step installs `grunt-cli` globally )

3. Grunt plugins
    - Open terminal and navigate to the directory containing this ReadMe file
    - Type `npm install` and hit return
    - Let the command complete ( the above step installs all the necessary Grunt plugins that the front end depends on )


### Setting up the back end

__Pre-requisites__

1. MySQL v5.0.0 or higher
2. JDK v1.7.0 or higher
3. Maven v3.3.3
4. Jetty v9.2.12

__Creating the DB__

1. Start the MySQL server
2. Connect to MySQL
3. Execute the file located in the `DB Configuration/Dumps/` directory
4. This will create the required database
5. Done

__Setting up the server__

1. Copy the contents of the `jetty.xml` file located in the `Server Configurations/JNDI configuration` directory to your `$JETTY_HOME/etc/jetty.xml` file at the end. Change the values of the properties to reflect according to your environment
2. Copy the file `homearteweb.xml` in your `$JETTY_HOME/webapps/` directory
3. First time of starting the server start with the below command:
    ```
    java -jar start.jar --add-to-startd=plus
    ```
4. Done


### Authors
- [Anurag Agrawal](mailto:agrawalanurag1988@gmail.com "agrawalanurag1988@gmail.com")
- [Navdeep Singh Bagga](mailto:navdeepb3191@gmail.com "navdeepb3191@gmail.com")