###
# @desc The Gruntfile responsible for automating all the tasks for the frontend
# @author Navdeep
###

module.exports = ( grunt ) ->

	# Load all the plugins:
	grunt.loadNpmTasks "grunt-bake"
	grunt.loadNpmTasks "grunt-contrib-clean"
	grunt.loadNpmTasks "grunt-contrib-concat"
	grunt.loadNpmTasks "grunt-contrib-cssmin"
	grunt.loadNpmTasks "grunt-contrib-handlebars"
	grunt.loadNpmTasks "grunt-contrib-stylus"
	grunt.loadNpmTasks "grunt-contrib-uglify"
	grunt.loadNpmTasks "grunt-contrib-watch"
	grunt.loadNpmTasks "grunt-text-replace"
	grunt.loadNpmTasks "grunt-usemin"

	# Config storing directory paths:
	dirConfig =
		assets: "HomeArte-Web/src/main/webapp/assets"
		styles:
			src: "frontend/styles"
			dest: "HomeArte-Web/src/main/webapp/assets/css"
		templates:
			src: "frontend/templates"
			dest: "HomeArte-Web/src/main/webapp/assets/js/build"
		views:
			src: "frontend/views"
			html:
				dest: ".tmp"
				assets: "../HomeArte-Web/src/main/webapp/assets"
			jsp:
				dest: "HomeArte-Web/src/main/webapp/WEB-INF/pages"
		usemin:
			dest: "HomeArte-Web/src/main/webapp"
			root: "HomeArte-Web/src/main/webapp"

	# Config storing logical paths:
	pathConfig =
		jspAssets:
			dev: "/assets"
			prod: "/assets"
		jspLinkPrefix:
			dev: ""
			prod: ""

	# Intialize the tasks to perform accordingly:
	grunt.initConfig

		# Store the `package.json` file in a nifty variable:
		pkg: grunt.file.readJSON "package.json"

		# Get the directory config inside this closure:
		dir: dirConfig

		# Get the path config inside this closure:
		path: pathConfig

		# Bake @task
		bake:
			html:
				options:
					content:
						isJSP: false
						assets: "<%= dir.views.html.assets %>"
				files: [
					expand: true
					cwd: "<%= dir.views.src %>/"
					src: [ "*.tpl.html" ]
					dest: "<%= dir.views.html.dest %>/"
					ext: ".html"
				]
			jsp:
				options:
					content:
						isJSP: true
						assets: "<%= path.jspAssets.dev %>"
						linkPrefix: "<%= path.jspLinkPrefix.dev %>"
				files: [
					expand: true
					cwd: "<%= dir.views.src %>/"
					src: [ "*.tpl.html" ]
					dest: "<%= dir.views.jsp.dest %>/"
					ext: ".jsp"
				]
			optimized:
				options:
					content:
						isJSP: true
						assets: "<%= path.jspAssets.prod %>"
						linkPrefix: "<%= path.jspLinkPrefix.dev %>"
				files: [
					expand: true
					cwd: "<%= dir.views.src %>/"
					src: [ "*.tpl.html" ]
					dest: "<%= dir.views.jsp.dest %>/"
					ext: ".jsp"
				]
			prod:
				options:
					content:
						isJSP: true
						assets: "<%= path.jspAssets.prod %>"
						linkPrefix: "<%= path.jspLinkPrefix.prod %>"
				files: [
					expand: true
					cwd: "<%= dir.views.src %>/"
					src: [ "*.tpl.html" ]
					dest: "<%= dir.views.jsp.dest %>/"
					ext: ".jsp"
				]

		# Clean @task
		clean:
			prod: [
				"<%= dir.assets %>/css/build.css"
				"<%= dir.assets %>/js/**/*"
				"!<%= dir.assets %>/js/dist/**"
			]

		# Handlebars @task
		handlebars:
			options:
				namespace: "Templates"
				processName: ( filePath ) ->
					filePath
						.replace( "frontend/templates/", "" )
						.replace( ".hbs", "" )
			templates:
				files:
					"<%= dir.templates.dest %>/hbs-templates.js" : "<%= dir.templates.src %>/{,*/}*.hbs"

		# Stylus @task
		stylus:
			options:
				compress: false
				banner: """
					/*!
					 * <%= pkg.name %>
					 * @ver <%= pkg.version %>
					 * @author <%= pkg.author %>
					 * NOTE: Do not modify this file!
					 */\n
					"""
			main:
				files:
					"<%= dir.styles.dest %>/build.css" : "<%= dir.styles.src %>/index.styl"

		# Text Replace @task
		replace:
			assetsLink:
				src: [ "<%= dir.views.jsp.dest %>/*.jsp" ]
				overwrite: true
				replacements: [{
					from: "<%= path.jspAssets.prod %>/"
					to: "<%= path.jspAssets.dev %>/"
				}]
			version:
				src: [ "<%= dir.views.jsp.dest %>/*.jsp" ]
				overwrite: true
				replacements: [{
					from: /(min\.css|min\.js)/g
					to: "$1?v=<%= pkg.version %>"
				}]

		# Usemin @task
		usemin:
			html: [ "<%= dir.views.jsp.dest %>/*.jsp" ]

		# UseminPrepare @task
		useminPrepare:
			html: "<%= dir.views.jsp.dest %>/*.jsp"
			options:
				root: "<%= dir.usemin.root %>"
				dest: "<%= dir.usemin.dest %>"
				staging: ".usemin"

		# Uglify task
		uglify:
			options:
				banner: """
					/*!
					 * <%= pkg.name %>
					 * @ver <%= pkg.version %>
					 * @author <%= pkg.author %>
					 */\n
					"""

		# Watch @task
		watch:
			styles:
				files: [ "<%= dir.styles.src %>/{,*/}*.styl" ]
				tasks: [ "stylus" ]
			templates:
				files: [ "<%= dir.templates.src %>/{,*/}*.hbs" ]
				tasks: [ "handlebars" ]
			views:
				files: [ "<%= dir.views.src %>/{,*/}*.tpl.html" ]
				tasks: [ "bake:html" ]

	# Register tasks:
	grunt.registerTask "default", "The default task", [
		"dev"
	]

	grunt.registerTask "dev", "Cooks up the front end assets and JSPs for development", [
		"stylus"
		"handlebars"
		"bake:jsp"
	]

	grunt.registerTask "testProd", "Cooks up the front end assets and JSPs for testing production-ready files", [
		"stylus"
		"handlebars"
		"bake:optimized"
		"useminPrepare"
		"concat:generated"
		"cssmin:generated"
		"uglify:generated"
		"usemin"
		"replace"
	]

	grunt.registerTask "prod", "Cooks up the front end assets and JSPs for production", [
		"stylus"
		"handlebars"
		"bake:prod"
		"useminPrepare"
		"concat:generated"
		"cssmin:generated"
		"uglify:generated"
		"usemin"
		"replace:version"
		"clean:prod"
	]