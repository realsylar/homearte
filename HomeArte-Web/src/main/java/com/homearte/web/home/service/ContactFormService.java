/**
 *
 */
package com.homearte.web.home.service;

import javax.mail.MessagingException;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.homearte.commons.aws.entity.SESMailingMessageParams;
import com.homearte.commons.aws.enums.EmailBodyType;
import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.commons.core.manager.EmailSenderManager;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.home.forms.dto.ContactUsFormDTO;
import com.homearte.web.home.persistence.dao.NewsletterSubscriberInfoDAO;
import com.homearte.web.home.persistence.entity.NewsletterSubscriberInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ContactFormService
{

	private NewsletterSubscriberInfoDAO	newsletterSubscriberInfoDAO;
	private EmailSenderManager			emailManager;

	/**
	 * @param newsletterSubscriberInfoDAO
	 * @param emailManager
	 */
	@Autowired
	public ContactFormService(
			NewsletterSubscriberInfoDAO newsletterSubscriberInfoDAO,
			EmailSenderManager emailManager)
	{
		super();
		this.newsletterSubscriberInfoDAO = newsletterSubscriberInfoDAO;
		this.emailManager = emailManager;
	}

	@Transactional
	public void subscribeUserToNewsletter(String data)
			throws PersistanceException
	{
		JSONObject dataObj = new JSONObject(data);
		String email = dataObj.getString("email");
		NewsletterSubscriberInfo info = new NewsletterSubscriberInfo(email);
		newsletterSubscriberInfoDAO.saveOrUpdate(info);
	}

	/**
	 * @param email
	 * @throws PersistanceException
	 */
	@Transactional
	public void unsubscribeUserFromNewsletter(String email)
			throws PersistanceException
	{
		NewsletterSubscriberInfo info = newsletterSubscriberInfoDAO
				.getByEmail(email);
		newsletterSubscriberInfoDAO.delete(info);
	}

	/**
	 * @param contactUsFormData
	 * @throws MessagingException
	 */
	public void handleContactUsFormSubmit(ContactUsFormDTO contactUsFormData)
			throws MessagingException
	{
		SESMailingMessageParams params = prepareEmailMessageForContactUs(contactUsFormData);
		emailManager.sendMail(params);
	}

	/**
	 * @param user
	 * @return
	 */
	private SESMailingMessageParams prepareEmailMessageForContactUs(
			ContactUsFormDTO cdto)
	{
		return new SESMailingMessageParams(
				ApplicationConstants.INFO_FROM_ADDRESS,
				ApplicationConstants.CUSTOMER_SUPPORT_ADDRESS,
				null,
				null,
				SpringContextPropertyLoader
				.getPropertyForKey(ApplicationConstants.CONTACT_US_FORM_SUBMIT_EMAIL_SUBJECT),
				cdto.toString(), EmailBodyType.TEXTHTML);
	}

}
