/**
 *
 */
package com.homearte.web.home.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "error")
public class ErrorPagesController
{

	@RequestMapping(value = "404")
	public ModelAndView load404Page()
	{
		return new ModelAndView("error-404");
	}

	@RequestMapping(value = "gen")
	public ModelAndView loadGeneralErrorPage()
	{
		return new ModelAndView("error-gen");
	}
}
