/**
 *
 */
package com.homearte.web.home.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.common.dto.AjaxResponse;
import com.homearte.web.home.forms.dto.ContactUsFormDTO;
import com.homearte.web.home.service.ContactFormService;
import com.homearte.web.user.controllers.SignupController;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
public class ContactFormsController
{

	private static final Logger	logger	= Logger.getLogger(SignupController.class);
	private ContactFormService	contactFormService;

	/**
	 * @param subsInfoService
	 */
	@Autowired
	public ContactFormsController(ContactFormService contactFormService)
	{
		super();
		this.contactFormService = contactFormService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/subscribe")
	@ResponseBody
	public String subscribeUserToNewsletter(HttpServletRequest request,
			HttpServletResponse response, @RequestBody String data)
	{
		String message = "";
		int status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		try
		{
			contactFormService.subscribeUserToNewsletter(data);
			message = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.THANK_YOU_FOR_SUBSCRIBING);
			status = HttpServletResponse.SC_OK;
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in subscribe to newsletter process",
					e);
			if (e.getCause() instanceof ConstraintViolationException)
				message = SpringContextPropertyLoader
				.getPropertyForKey(ApplicationConstants.ALREADY_SUBSCRIBED_TO_NEWSLETTER);
			else
				message = SpringContextPropertyLoader
				.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		}
		AjaxResponse subscribeUserToNewsletterResponse = new AjaxResponse(null,
				message, status);
		return new JSONObject().put("ajaxResponse",
				subscribeUserToNewsletterResponse).toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/unsubscribe")
	@ResponseBody
	public String unsubscribeUserFromNewsletter(HttpServletRequest request,
			HttpServletResponse response, @RequestParam("email") String email)
	{
		String message = "";
		try
		{
			contactFormService.unsubscribeUserFromNewsletter(email);
			message = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.SAD_FOR_UNSUBSCRIBING);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in subscribe to newsletter process",
					e);
			message = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		}
		return message;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/contact")
	public ModelAndView loadContactUsPage(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("contact-us");
		model.addObject("cdto", new ContactUsFormDTO());
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/contact/sub")
	public ModelAndView contactUsSubmit(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("cdto") ContactUsFormDTO contactUsFormData)
	{
		ModelAndView view = new ModelAndView("contact-us");
		String message = "";
		try
		{
			contactFormService.handleContactUsFormSubmit(contactUsFormData);
			message = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.CONTACT_US_SUCCESS);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in subscribe to newsletter process",
					e);
			message = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		}
		ContactUsFormDTO dto = new ContactUsFormDTO();
		dto.setMessage(message);
		view.addObject("cdto", dto);
		return view;
	}

}
