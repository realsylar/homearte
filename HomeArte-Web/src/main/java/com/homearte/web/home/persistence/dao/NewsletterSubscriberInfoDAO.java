package com.homearte.web.home.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.home.persistence.entity.NewsletterSubscriberInfo;

/**
 * DAO class for all DB operations on the newsletter_subscriber_info table.
 *
 * @author Anurag Agrawal
 */

@Repository
public class NewsletterSubscriberInfoDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */

	@Autowired
	public NewsletterSubscriberInfoDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public NewsletterSubscriberInfo saveOrUpdate(NewsletterSubscriberInfo entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(NewsletterSubscriberInfo entity)
			throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int entityId) throws PersistanceException
	{
		dataPersistance.delete(NewsletterSubscriberInfo.class, entityId);
	}

	public NewsletterSubscriberInfo getByEmail(String email)
			throws PersistanceException
	{
		String query = "from NewsletterSubscriberInfo where email = :email";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("email", new QueryParam(email, QueryParamTypes.STRING));
		return dataPersistance.executeHQLQueryUniqueResult(query, params);
	}

	public List<NewsletterSubscriberInfo> loadAllSubscribers()
			throws PersistanceException
			{
		String query = "from NewsletterSubscriberInfo where isActive = :isActive";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("isActive", new QueryParam(true, QueryParamTypes.BOOLEAN));
		List<Object> results = dataPersistance.executeHQLQueryListResult(query,
				params);
		List<NewsletterSubscriberInfo> listOfSubs = Lists.newArrayList();
		for (Object o : results)
		{
			listOfSubs.add((NewsletterSubscriberInfo) o);
		}
		results = null;
		return listOfSubs;
			}
}
