/**
 *
 */
package com.homearte.web.home.forms.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class ContactUsFormDTO
{

	private String	name;
	private String	email;
	private String	subject;
	private String	query;
	private String	message;

	/**
	 *
	 */
	public ContactUsFormDTO()
	{
		super();
	}

	/**
	 * @param name
	 * @param email
	 * @param subject
	 * @param message
	 */
	public ContactUsFormDTO(String name, String email, String subject,
			String query, String message)
	{
		super();
		this.name = name;
		this.email = email;
		this.subject = subject;
		this.query = query;
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "ContactUsFormDTO [name=" + name + ", email=" + email
				+ ", subject=" + subject + ", message=" + message + ", query="
				+ query + "]";
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the subject
	 */
	public String getSubject()
	{
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * @return the query
	 */
	public String getQuery()
	{
		return query;
	}

	/**
	 * @param query
	 *            the query to set
	 */
	public void setQuery(String query)
	{
		this.query = query;
	}

}
