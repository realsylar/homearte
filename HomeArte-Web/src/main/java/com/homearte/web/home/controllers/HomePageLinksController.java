/**
 *
 */
package com.homearte.web.home.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
public class HomePageLinksController
{

	@RequestMapping(method = RequestMethod.GET, value = "/about")
	public ModelAndView loadHomePage(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("about");
		return model;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/privacy")
	public ModelAndView loadPrivacyPage(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("privacy");
		return model;
	}

	/*
	 * @RequestMapping(method = RequestMethod.GET, value = "/team") public
	 * ModelAndView loadTeamPage(HttpServletRequest request, HttpServletResponse
	 * response) { ModelAndView model = new ModelAndView("team"); return model;
	 * }
	 */
}
