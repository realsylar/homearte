/**
 *
 */
package com.homearte.web.home.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "")
public class HomePageController
{

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView loadHomePage(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("index");
		return model;
	}

	/*
	 * @RequestMapping(method = RequestMethod.GET, value = "/home") public
	 * ModelAndView loadHomePageFromHomeLink(HttpServletRequest request,
	 * HttpServletResponse response) { ModelAndView model = new
	 * ModelAndView("index"); return model; }
	 */
}
