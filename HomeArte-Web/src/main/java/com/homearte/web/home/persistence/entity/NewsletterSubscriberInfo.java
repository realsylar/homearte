/**
 *
 */
package com.homearte.web.home.persistence.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "newsletter_subscriber_info")
public class NewsletterSubscriberInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "subs_id")
	private int			subsId;

	@Column(name = "email")
	private String		email;

	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;

	@Column(name = "last_sent")
	private Timestamp	lastSent;

	@Column(name = "is_active")
	private boolean		isActive;

	/**
	 * @param subsId
	 * @param email
	 * @param creationTimestamp
	 * @param lastSent
	 * @param isActive
	 */
	public NewsletterSubscriberInfo(String email)
	{
		super();
		this.email = email;
		this.isActive = true;
	}

	/**
	 *
	 */
	protected NewsletterSubscriberInfo()
	{
		super();
	}

	/**
	 * @return the subsId
	 */
	public int getSubsId()
	{
		return subsId;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the lastSent
	 */
	public Timestamp getLastSent()
	{
		return lastSent;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

}
