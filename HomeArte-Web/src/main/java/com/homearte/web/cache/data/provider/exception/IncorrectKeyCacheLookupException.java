/**
 *
 */
package com.homearte.web.cache.data.provider.exception;

/**
 * @author Anurag Agrawal
 *
 */
public class IncorrectKeyCacheLookupException extends Exception
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 *
	 */
	public IncorrectKeyCacheLookupException()
	{
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public IncorrectKeyCacheLookupException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public IncorrectKeyCacheLookupException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param arg0
	 */
	public IncorrectKeyCacheLookupException(String message)
	{
		super(message);
	}

	/**
	 * @param arg0
	 */
	public IncorrectKeyCacheLookupException(Throwable cause)
	{
		super(cause);
	}

}
