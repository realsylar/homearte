/**
 *
 */
package com.homearte.web.cache.data.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.display.dto.SubProjectDisplayDTO;
import com.homearte.web.builder.persistence.dao.ProjectInfoDAO;
import com.homearte.web.builder.persistence.dao.SubProjectInfoDAO;
import com.homearte.web.builder.persistence.entity.ProjectInfo;
import com.homearte.web.builder.persistence.entity.SubProjectInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class SubProjectDataProvider
{

	private SubProjectInfoDAO	subProjectInfoDAO;
	private ProjectInfoDAO		projectInfoDAO;

	/**
	 * @param projectInfoService
	 */
	@Autowired
	public SubProjectDataProvider(SubProjectInfoDAO subProjectInfoDAO,
			ProjectInfoDAO projectInfoDAO)
	{
		super();
		this.subProjectInfoDAO = subProjectInfoDAO;
		this.projectInfoDAO = projectInfoDAO;
	}

	/**
	 * @param userId
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional
	public SubProjectDisplayDTO getSubProjectDTOForRequest(int spid)
			throws PersistanceException
	{
		SubProjectInfo sp = subProjectInfoDAO.getById(spid);
		ProjectInfo p = projectInfoDAO.getById(sp.getProjectId());
		SubProjectDisplayDTO dto = new SubProjectDisplayDTO(sp,
				p.getProjectName(), p.getProjectId());
		return dto;
	}
}
