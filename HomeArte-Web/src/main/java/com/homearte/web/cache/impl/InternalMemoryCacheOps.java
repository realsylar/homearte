/**
 *
 */
package com.homearte.web.cache.impl;

import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homearte.commons.core.cache.HomearteCache;
import com.homearte.commons.core.cache.MemoryCacheOps;
import com.homearte.web.cache.data.provider.CacheDataProvider;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class InternalMemoryCacheOps implements MemoryCacheOps
{

	private static final Logger	logger	= Logger.getLogger(InternalMemoryCacheOps.class);
	private HomearteCache		hcache;

	/**
	 * @param cache
	 */
	@Autowired
	public InternalMemoryCacheOps(HomearteCache hcache)
	{
		super();
		this.hcache = hcache;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.homearte.commons.core.cache.MemoryCacheOps#addItemWithKey(java.lang
	 * .String, java.lang.Object)
	 */
	@Override
	public void addItemWithKey(String key, Object t)
	{
		hcache.getCache().put(key, t);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.homearte.commons.core.cache.MemoryCacheOps#removeItemWithKey(java
	 * .lang.String)
	 */
	@Override
	public void removeItemWithKey(String key)
	{
		hcache.getCache().invalidate(key);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.homearte.commons.core.cache.MemoryCacheOps#getItemWithKey(java.lang
	 * .String)
	 */
	@Override
	public Object getItemWithKey(String key) throws ExecutionException
	{
		logger.info("Cache lookup for key : " + key);
		return hcache.getCache().get(key, new CacheDataProvider(key));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.homearte.commons.core.cache.MemoryCacheOps#updateItemWithKey(java
	 * .lang.String, java.lang.Object)
	 */
	@Override
	public void updateItemWithKey(String key, Object t)
	{
		hcache.getCache().put(key, t);
	}

}
