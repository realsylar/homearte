/**
 *
 */
package com.homearte.web.cache.data.provider;

import java.util.concurrent.Callable;

import com.homearte.web.cache.constants.HomearteCacheConstants;
import com.homearte.web.cache.data.provider.exception.IncorrectKeyCacheLookupException;
import com.homearte.web.cache.data.provider.exception.NullKeyCacheLookupException;

/**
 * @author Anurag Agrawal
 *
 */
public class CacheDataProvider implements Callable<Object>
{

	private String					key;
	private DashboardDataProvider	dashboardDataProvider;
	private ProjectDataProvider		projectDataProvider;
	private SubProjectDataProvider	subProjectDataProvider;

	/**
	 * @param key
	 */
	public CacheDataProvider(String key)
	{
		super();
		this.key = key;
		init();
	}

	/**
	 *
	 */
	private void init()
	{
		dashboardDataProvider = ApplicationContextProvider
				.getBeanFromContext(DashboardDataProvider.class);
		projectDataProvider = ApplicationContextProvider
				.getBeanFromContext(ProjectDataProvider.class);
		subProjectDataProvider = ApplicationContextProvider
				.getBeanFromContext(SubProjectDataProvider.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Object call() throws Exception
	{
		if (key == null)
		{
			throw new NullKeyCacheLookupException();
		}
		String[] keyValues = key.split("_");
		if (keyValues == null || keyValues.length < 2)
		{
			throw new IncorrectKeyCacheLookupException();
		}
		if (key.startsWith(HomearteCacheConstants.USER_DASHBOARD_DISPLAY_DATA))
		{
			return dashboardDataProvider.getDashboardDTOForRequest(Integer
					.parseInt(keyValues[1]));

		}
		else if (key.startsWith(HomearteCacheConstants.BUILDER_ID_FOR_USER))
		{

			return dashboardDataProvider.getBuilderIdForRequest(Integer
					.parseInt(keyValues[1]));

		}
		else if (key
				.startsWith(HomearteCacheConstants.USER_PROJECT_DISPLAY_DATA))
		{

			return projectDataProvider.getProjectDTOForRequest(Integer
					.parseInt(keyValues[1]));

		}
		else if (key
				.startsWith(HomearteCacheConstants.USER_SUB_PROJECT_DISPLAY_DATA))
		{

			return subProjectDataProvider.getSubProjectDTOForRequest(Integer
					.parseInt(keyValues[1]));

		}
		return null;
	}
}
