/**
 *
 */
package com.homearte.web.cache.constants;

/**
 * @author Anurag Agrawal
 *
 */
public interface HomearteCacheConstants
{

	public static final String	USER_PROJECT_DISPLAY_DATA		= "up_";
	public static final String	USER_SUB_PROJECT_DISPLAY_DATA	= "usp_";
	public static final String	USER_DASHBOARD_DISPLAY_DATA		= "ud_";
	public static final String	BUILDER_ID_FOR_USER				= "um_";
}
