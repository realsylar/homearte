/**
 *
 */
package com.homearte.web.cache.data.provider;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.display.dto.DashboardDTO;
import com.homearte.web.builder.persistence.dao.UserBuilderMappingDAO;
import com.homearte.web.builder.persistence.entity.UserBuilderMapping;
import com.homearte.web.user.display.dto.UserNotificationsDTO;
import com.homearte.web.user.persistence.dao.UserInfoDAO;
import com.homearte.web.user.persistence.dao.UserNotificationsDAO;
import com.homearte.web.user.persistence.entity.UserInfo;
import com.homearte.web.user.persistence.entity.UserNotifications;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class DashboardDataProvider
{

	private UserInfoDAO				userInfoDAO;
	private UserBuilderMappingDAO	userBuilderMappingDAO;
	private UserNotificationsDAO	userNotificationDAO;

	/**
	 * @param userInfoDAO
	 * @param userBuilderMappingDAO
	 * @param userNotificationDAO
	 */
	@Autowired
	public DashboardDataProvider(UserInfoDAO userInfoDAO,
			UserBuilderMappingDAO userBuilderMappingDAO,
			UserNotificationsDAO userNotificationDAO)
	{
		super();
		this.userInfoDAO = userInfoDAO;
		this.userBuilderMappingDAO = userBuilderMappingDAO;
		this.userNotificationDAO = userNotificationDAO;
	}

	/**
	 * @param userId
	 * @return
	 * @throws PersistanceException
	 */
	public DashboardDTO getDashboardDTOForRequest(int userId)
			throws PersistanceException
	{
		DashboardDTO dto = new DashboardDTO();
		UserInfo user = userInfoDAO.getUser(userId);
		// Setting user data
		dto.setUserName(user.getFirstName() + " " + user.getLastName());
		dto.setUserImage(null);

		// Setting builder Id
		UserBuilderMapping mapping = userBuilderMappingDAO
				.getMappingByUserId(userId);
		dto.setBuilderId(mapping.getBuilderId());

		// Setting notifications data;
		dto.setUserNotifications(getUserNotificationDTOList(userNotificationDAO
				.getNotificationsByUserId(userId)));
		return dto;
	}

	public int getBuilderIdForRequest(int userId) throws PersistanceException
	{
		// Setting builder Id
		UserBuilderMapping mapping = userBuilderMappingDAO
				.getMappingByUserId(userId);
		if (mapping == null)
			return -1;
		return mapping.getBuilderId();
	}

	/**
	 * @param userNotifictions
	 * @return
	 */
	private List<UserNotificationsDTO> getUserNotificationDTOList(
			List<UserNotifications> userNotifictions)
	{
		List<UserNotificationsDTO> notificationDTOList = Lists.newArrayList();
		for (UserNotifications notification : userNotifictions)
		{
			notificationDTOList.add(new UserNotificationsDTO(notification));
		}
		return notificationDTOList;
	}
}
