/**
 *
 */
package com.homearte.web.cache.data.provider;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ApplicationContextProvider implements ApplicationContextAware
{

	private static ApplicationContext	ctx;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.context.ApplicationContextAware#setApplicationContext
	 * (org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException
	{
		ctx = applicationContext;
	}

	public static <T> T getBeanFromContext(Class<T> beanClass)
	{
		return ctx.getBean(beanClass);
	}
}
