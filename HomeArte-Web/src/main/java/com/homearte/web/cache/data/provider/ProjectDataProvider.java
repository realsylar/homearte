/**
 *
 */
package com.homearte.web.cache.data.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.display.dto.ProjectDisplayDTO;
import com.homearte.web.builder.persistence.dao.ProjectInfoDAO;
import com.homearte.web.builder.persistence.entity.ProjectInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ProjectDataProvider
{

	private ProjectInfoDAO	projectInfoDAO;

	/**
	 * @param projectInfoDAO
	 */
	@Autowired
	public ProjectDataProvider(ProjectInfoDAO projectInfoDAO)
	{
		super();
		this.projectInfoDAO = projectInfoDAO;
	}

	/**
	 * @param userId
	 * @return
	 * @throws PersistanceException
	 */
	public ProjectDisplayDTO getProjectDTOForRequest(int pid)
			throws PersistanceException
	{
		ProjectInfo p = projectInfoDAO.getById(pid);
		if (p != null)
		{
			ProjectDisplayDTO dto = new ProjectDisplayDTO(p);
			return dto;
		}
		return null;
	}
}
