/**
 *
 */
package com.homearte.web.cache.data.provider.exception;

/**
 * @author Anurag Agrawal
 *
 */
public class NullKeyCacheLookupException extends Exception
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 *
	 */
	public NullKeyCacheLookupException()
	{
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NullKeyCacheLookupException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NullKeyCacheLookupException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param arg0
	 */
	public NullKeyCacheLookupException(String message)
	{
		super(message);
	}

	/**
	 * @param arg0
	 */
	public NullKeyCacheLookupException(Throwable cause)
	{
		super(cause);
	}

}
