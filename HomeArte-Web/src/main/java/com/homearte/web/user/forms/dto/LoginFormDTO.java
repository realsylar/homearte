/**
 *
 */
package com.homearte.web.user.forms.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class LoginFormDTO
{

	private String	email;
	private String	password;
	private boolean	rememberMe;
	private String	error;

	/**
	 * @param email
	 * @param password
	 * @param rememberMe
	 */
	public LoginFormDTO(String email, String password, boolean rememberMe,
			String error)
	{
		super();
		this.email = email;
		this.password = password;
		this.rememberMe = rememberMe;
		this.error = error;
	}

	/**
	 *
	 */
	public LoginFormDTO()
	{
		super();
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @return the rememberMe
	 */
	public boolean isRememberMe()
	{
		return rememberMe;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @param rememberMe
	 *            the rememberMe to set
	 */
	public void setRememberMe(boolean rememberMe)
	{
		this.rememberMe = rememberMe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "LoginFormDTO [email=" + email + ", password=" + password
				+ ", rememberMe=" + rememberMe + ", error=" + error + "]";
	}

	/**
	 * @return the error
	 */
	public String getError()
	{
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error)
	{
		this.error = error;
	}

}
