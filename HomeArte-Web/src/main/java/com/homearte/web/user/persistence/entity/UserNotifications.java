/**
 *
 */
package com.homearte.web.user.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "user_notifications")
public class UserNotifications
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "notification_id")
	private int		notificationId;

	@Column(name = "user_id")
	private int		userId;

	@Column(name = "notification_text")
	private String	notificationText;

	@Column(name = "notification_summary")
	private String	notificationSummary;

	@Column(name = "creation_timestamp")
	private int		creationTimestamp;

	@Column(name = "modification_timestamp")
	private int		modificationTimestamp;

	@Column(name = "notification_type")
	private int		notificationType;

	@Column(name = "notification_status")
	private int		notificationStatus;

	/**
	 * @param notificationText
	 * @param notificationSummary
	 * @param notificationType
	 * @param notificationStatus
	 */
	public UserNotifications(int userId, String notificationText,
			String notificationSummary, int notificationType,
			int notificationStatus)
	{
		super();
		this.userId = userId;
		this.notificationText = notificationText;
		this.notificationSummary = notificationSummary;
		this.notificationType = notificationType;
		this.notificationStatus = notificationStatus;
	}

	/**
	 *
	 */
	protected UserNotifications()
	{
		super();
	}

	/**
	 * @return the notificationId
	 */
	public int getNotificationId()
	{
		return notificationId;
	}

	/**
	 * @return the notificationText
	 */
	public String getNotificationText()
	{
		return notificationText;
	}

	/**
	 * @return the notificationSummary
	 */
	public String getNotificationSummary()
	{
		return notificationSummary;
	}

	/**
	 * @return the creationTimestamp
	 */
	public int getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public int getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the notificationType
	 */
	public int getNotificationType()
	{
		return notificationType;
	}

	/**
	 * @return the notificationStatus
	 */
	public int getNotificationStatus()
	{
		return notificationStatus;
	}

}
