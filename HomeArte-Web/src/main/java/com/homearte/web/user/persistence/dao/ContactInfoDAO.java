package com.homearte.web.user.persistence.dao;
/**
 *
 */
/*
package com.homearte.web.persistence.dao.gen;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.homearte.web.persistence.entity.gen.ContactInfo;

 *//**
  * DAO class for all DB operations on the contact_info table.
  *
  * @author Anurag Agrawal
  */
/*
@Repository
public class ContactInfoDAO
{

private SessionFactory	sessionFactory;

 *//**
 * Autowired constructor to automatically load the session factory object
 * from the spring - hibernate configuration.
 *
 * @param sessionFactory
 */
/*
@Autowired
public ContactInfoDAO(SessionFactory sessionFactory)
{
super();
this.sessionFactory = sessionFactory;
}

 *//**
 * DAO method to get all the details of the Contacts which correspond to the
 * same phone number
 *
 * @param phoneNumber
 *            The phone number against which the contacts in the DB are
 *            searched
 * @return The list/set of the Contacts matched against the given phone
 *         number. Null if none
 */
/*
 * @SuppressWarnings("unchecked") public Collection<ContactInfo>
 * getAllContactInfoForPhoneNumber( String phoneNumber) { Session sess =
 * sessionFactory.getCurrentSession(); Criteria crit =
 * sess.createCriteria(ContactInfo.class); Collection<ContactInfo> contacts =
 * crit.add( Restrictions.eq("phoneNumber", phoneNumber)).list(); return
 * contacts; } }
 */