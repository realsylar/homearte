/**
 *
 */
package com.homearte.web.user.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.user.forms.dto.ForgotPasswordFormDTO;
import com.homearte.web.user.forms.dto.LoginFormDTO;
import com.homearte.web.user.service.ForgotPasswordService;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
public class ForgotPasswordController
{

	private static final Logger		logger	= Logger.getLogger(ForgotPasswordController.class);
	private ForgotPasswordService	forgotPasswordService;

	/**
	 * @param forgotPasswordService
	 */
	@Autowired
	public ForgotPasswordController(ForgotPasswordService forgotPasswordService)
	{
		super();
		this.forgotPasswordService = forgotPasswordService;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/fpwd")
	public ModelAndView loadForgotPasswordPage(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("forgot-pwd");
		String error = null;
		ForgotPasswordFormDTO fdto;
		if (request.getSession().getAttribute("fdto") == null)
			fdto = new ForgotPasswordFormDTO();
		else
		{
			fdto = (ForgotPasswordFormDTO) request.getSession().getAttribute(
					"fdto");
			request.getSession().removeAttribute("fdto");
		}
		if ((error = (String) request.getSession().getAttribute("error")) != null)
		{
			fdto.setError(error);
			request.getSession().removeAttribute("error");
		}
		model.addObject("fdto", fdto);
		return model;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/rpwd")
	public ModelAndView loadRecoverPasswordPage(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("recover-pwd");
		String error = null;
		ForgotPasswordFormDTO fdto;
		if (request.getSession().getAttribute("fdto") == null)
			fdto = new ForgotPasswordFormDTO();
		else
		{
			fdto = (ForgotPasswordFormDTO) request.getSession().getAttribute(
					"fdto");
			request.getSession().removeAttribute("fdto");
		}
		if ((error = (String) request.getSession().getAttribute("error")) != null)
		{
			fdto.setError(error);
			request.getSession().removeAttribute("error");
		}
		model.addObject("fdto", fdto);
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/fpwd/sub")
	public ModelAndView submitForgotPasswordPage(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("fdto") ForgotPasswordFormDTO forgotPasswordFormData)
	{
		ModelAndView model = new ModelAndView("forgot-pwd");
		try
		{
			boolean doesUserAlreadyExist = forgotPasswordService
					.checkIfUserAlreadyExists(forgotPasswordFormData);
			if (!doesUserAlreadyExist)
			{
				forgotPasswordFormData
				.setError(SpringContextPropertyLoader
						.getPropertyForKey(ApplicationConstants.USER_DOES_NOT_EXIST));
				forgotPasswordFormData.setEmail(null);
				request.getSession().setAttribute("fdto",
						forgotPasswordFormData);
				model = new ModelAndView("redirect:/fpwd");
			}
			else
			{
				forgotPasswordFormData = forgotPasswordService
						.processForgotPasswordMailInteractionForUser(
								forgotPasswordFormData, request);
				request.getSession().setAttribute("fdto",
						forgotPasswordFormData);
				model = new ModelAndView("redirect:/fpwd");
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in forgot password process", e);
			forgotPasswordFormData
					.setError(SpringContextPropertyLoader
							.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY));
			forgotPasswordFormData.setPassword(null);
			model = new ModelAndView("redirect:/fpwd");
			request.getSession().setAttribute("fdto", forgotPasswordFormData);
		}
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/rpwd/sub")
	public ModelAndView submitRecoverPasswordPage(
			HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("fdto") ForgotPasswordFormDTO recoverPasswordFormData)
	{
		String email = null;
		if ((email = (String) request.getSession().getAttribute("email")) != null)
		{
			recoverPasswordFormData.setEmail(email);
			request.getSession().removeAttribute("email");
		}
		ModelAndView model = new ModelAndView("redirect:/ulogin");
		try
		{
			forgotPasswordService.processPasswordRecoveryFormSubmit(
					recoverPasswordFormData, request);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in sign up confirmation process", e);
			request.getSession()
			.setAttribute(
					"error",
					SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY));
		}
		model.addObject("ldto", new LoginFormDTO());
		return model;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/fpwd/conf")
	public ModelAndView confirmLink(HttpServletRequest request,
			HttpServletResponse response, @RequestParam("i") String sId,
			@RequestParam("t") String token)
	{
		ModelAndView model = new ModelAndView("redirect:/rpwd");
		try
		{
			forgotPasswordService
					.processForgotPasswordConfirmationLinkClickForUser(sId,
							token, request);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in sign up confirmation process", e);
			request.getSession()
					.setAttribute(
							"error",
							SpringContextPropertyLoader
									.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY));
		}
		return model;
	}

}
