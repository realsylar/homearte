/**
 *
 */
package com.homearte.web.user.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "signup")
public class SignupController
{/*
 * 
 * private static final Logger logger =
 * Logger.getLogger(SignupController.class); private static final String
 * TOKEN_PARAM_NAME = "t"; private static final String ID_PARAM_NAME = "i";
 * private UserInfoService userInfoService; private EmailSenderManager
 * emailManager; private BasicTokenBasedUserInteractionService
 * userInteractionService; private RandomSecureTokenGenerator tokenGenerator;
 * private AESCBCEncryptor aesEncryptor; private SHA256Encryptor shaEncryptor;
 * private VelocityEngine velocityEngine; private WebSessionManager
 * webSessionManager;
 */
	/**
 * @param userInfoService
 * @param emailManager
 * @param userInteractionService
 * @param tokenGenerator
 * @param aesEncryptor
 * @param shaEncryptor
 * @param velocityEngine
 * @param webSessionManager
 */
	/*
	 * @Autowired public SignupController(UserInfoService userInfoService,
	 * EmailSenderManager emailManager, BasicTokenBasedUserInteractionService
	 * userInteractionService, RandomSecureTokenGenerator tokenGenerator,
	 * AESCBCEncryptor aesEncryptor, SHA256Encryptor shaEncryptor,
	 * VelocityEngine velocityEngine, WebSessionManager webSessionManager) {
	 * super(); this.userInfoService = userInfoService; this.emailManager =
	 * emailManager; this.userInteractionService = userInteractionService;
	 * this.tokenGenerator = tokenGenerator; this.aesEncryptor = aesEncryptor;
	 * this.shaEncryptor = shaEncryptor; this.velocityEngine = velocityEngine;
	 * this.webSessionManager = webSessionManager; }
	 * 
	 * @RequestMapping(method = RequestMethod.GET) public ModelAndView
	 * loadSignupPageBuilder(HttpServletRequest request, HttpServletResponse
	 * response) { ModelAndView model = new ModelAndView("signup"); if
	 * (request.getSession().getAttribute("sdto") == null)
	 * model.addObject("sdto", new
	 * SignupFormDTO(UserType.CLIENT_USER.getValue())); else {
	 * model.addObject("sdto", request.getSession().getAttribute("sdto"));
	 * request.getSession().removeAttribute("sdto"); }
	 * 
	 * return model; }
	 * 
	 * @RequestMapping(method = RequestMethod.GET, value = "c") public
	 * ModelAndView loadSignupPage(HttpServletRequest request,
	 * HttpServletResponse response) { ModelAndView model = new
	 * ModelAndView("signup"); model.addObject("sdto", new
	 * SignupFormDTO(UserType.BUILDER_ADMIN.getValue())); return model; }
	 * 
	 * @RequestMapping(method = RequestMethod.POST, value = "sub") public
	 * ModelAndView processSignupBuilder(HttpServletRequest request,
	 * HttpServletResponse response,
	 * 
	 * @ModelAttribute("sdto") SignupFormDTO dto) { ModelAndView model = new
	 * ModelAndView("signup"); try { boolean doesUserAlreadyExist =
	 * userInfoService.getUserByEmail(dto .getEmail()) != null; if
	 * (!doesUserAlreadyExist) { UserInfo user = userInfoService
	 * .saveUserInfo(getUserInfoEntityFromSignupFormDTO(dto)); String
	 * generatedConfirmationLink = generateConfirmationLink( user.getUserId(),
	 * request); SESMailingMessageParams params =
	 * prepareEmailMessageForNewUserSignUp( user, generatedConfirmationLink);
	 * emailManager.sendMail(params);
	 * request.getSession().removeAttribute("sdto");
	 * webSessionManager.addUserIdToSession(request.getSession(),
	 * user.getUserId()); model = new ModelAndView("redirect:/dashboard"); }
	 * else { dto.setError(SpringContextPropertyLoader
	 * .getPropertyForKey(ApplicationConstants.USER_EMAIL_ALREADY_EXISTS));
	 * dto.setPassword(null); model = new ModelAndView("redirect:/signup");
	 * request.getSession().setAttribute("sdto", dto); } } catch (Exception e) {
	 * e.printStackTrace();
	 * logger.error("Exception thrown in builder sign up process", e);
	 * dto.setError(SpringContextPropertyLoader
	 * .getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY));
	 * dto.setPassword(null); model = new ModelAndView("redirect:/signup");
	 * request.getSession().setAttribute("sdto", dto); } return model; }
	 * 
	 * @RequestMapping(method = RequestMethod.GET, value = "conf") public
	 * ModelAndView confirmUser(HttpServletRequest request, HttpServletResponse
	 * response,
	 * 
	 * @RequestParam(ID_PARAM_NAME) String sId,
	 * 
	 * @RequestParam(TOKEN_PARAM_NAME) String token) { ModelAndView model = new
	 * ModelAndView("redirect:/ulogin"); try { int interactionId =
	 * Integer.parseInt(aesEncryptor.decrypt(sId));
	 * BasicTokenBasedUserInteraction userInteraction = userInteractionService
	 * .getById(interactionId); int userId = userInteraction.getUserId(); String
	 * base64DecodedToken = new String(
	 * Base64.decodeBase64(URLDecoder.decode(token, "UTF-8"))); if
	 * (userInteraction.getToken().equals(
	 * shaEncryptor.encryptValue(base64DecodedToken,
	 * userInteraction.getSalt()))) { userInfoService.updateUserStatus(userId,
	 * true); userInteractionService.delete(userInteraction); } else {
	 * logger.error("User has clicked on an invalid confirmation link.");
	 * userInteractionService.delete(userInteraction); SESMailingMessageParams
	 * params = prepareEmailMessageForNewUserSignUp(
	 * userInfoService.getUser(userId), generateConfirmationLink(userId,
	 * request)); emailManager.sendMail(params); request.getSession()
	 * .setAttribute( "error", SpringContextPropertyLoader
	 * .getPropertyForKey(ApplicationConstants.INVALID_CONFIRMATION_LINK)); } }
	 * catch (Exception e) { e.printStackTrace();
	 * logger.error("Exception thrown in sign up confirmation process", e);
	 * request.getSession() .setAttribute( "error", SpringContextPropertyLoader
	 * .getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY)); } return
	 * model; }
	 *//**
  * @param dto
  * @return
  * @throws NoSuchAlgorithmException
  * @throws InvalidKeyException
  */
	/*
	 * private UserInfo getUserInfoEntityFromSignupFormDTO(SignupFormDTO dto)
	 * throws InvalidKeyException, NoSuchAlgorithmException { SHAEncryptedValue
	 * encVal = shaEncryptor.encryptValue(dto.getPassword()); return new
	 * UserInfo(dto.getEmail(), false, dto.getUserType(),
	 * encVal.getEncryptedValue(), encVal.getSalt(), dto.getFirstName(),
	 * dto.getLastName(), null); }
	 *//**
   * @param user
   * @return
   */
	/*
	 * private SESMailingMessageParams prepareEmailMessageForNewUserSignUp(
	 * UserInfo user, String link) { Template temp = velocityEngine
	 * .getTemplate(ApplicationConstants.SIGNUP_MAIL_TEMPLATE_NAME);
	 * VelocityContext cont = new VelocityContext(); cont.put("fName",
	 * user.getFirstName()); cont.put("lName", user.getLastName());
	 * cont.put("link", link); StringWriter writer = new StringWriter();
	 * temp.merge(cont, writer); return new SESMailingMessageParams(
	 * ApplicationConstants.INFO_FROM_ADDRESS, user.getEmail(), null, null,
	 * SpringContextPropertyLoader
	 * .getPropertyForKey(ApplicationConstants.NEW_USER_SIGNUP_EMAIL_SUBJECT),
	 * writer.toString(), EmailBodyType.TEXTHTML); }
	 *//**
    * @param userInt
    * @return
    * @throws EncryptorException
    * @throws NoSuchAlgorithmException
    * @throws InvalidKeyException
    * @throws PersistanceException
    * @throws UnsupportedEncodingException
    */
	/*
	 * private String generateConfirmationLink(int userId, HttpServletRequest
	 * request) throws EncryptorException, InvalidKeyException,
	 * NoSuchAlgorithmException, PersistanceException,
	 * UnsupportedEncodingException { String token =
	 * tokenGenerator.generateToken(); SHAEncryptedValue encVal =
	 * shaEncryptor.encryptValue(token); BasicTokenBasedUserInteraction userInt
	 * = new BasicTokenBasedUserInteraction( userId, encVal.getEncryptedValue(),
	 * BasicTokenBasedUserInteractionType.SIGNUP.getValue(), encVal.getSalt());
	 * userInt = userInteractionService.saveOrUpdate(userInt); StringBuffer url
	 * = request.getRequestURL(); String uri = request.getRequestURI(); String
	 * ctx = request.getContextPath(); String base = url.substring(0,
	 * url.length() - uri.length() + ctx.length()) + "/"; StringBuffer buf = new
	 * StringBuffer(); buf.append(base);
	 * buf.append(ApplicationConstants.SIGNUP_CONFIRMATION_LINK);
	 * buf.append(TOKEN_PARAM_NAME); buf.append("=");
	 * buf.append(URLEncoder.encode(
	 * Base64.encodeBase64String(String.valueOf(token).getBytes()), "UTF-8"));
	 * buf.append("&"); buf.append(ID_PARAM_NAME); buf.append("=");
	 * buf.append(URLEncoder.encode(
	 * aesEncryptor.encrypt(String.valueOf(userInt.getIntrxnId())), "UTF-8"));
	 * return buf.toString(); }
	 */
}
