package com.homearte.web.user.persistence.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.user.persistence.entity.UserInfo;

/**
 * DAO class for all DB operations on the user_info table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class UserInfoDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public UserInfoDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	/**
	 * DAO method to load the user info object from DB based on the identifier
	 * i.e. table primary key
	 *
	 * @param userId
	 *            The id of the user whose information has to be fetched
	 * @return The UserInfo object pertaining to the id. Null if not present
	 * @throws PersistanceException
	 */
	public UserInfo getUser(int userId) throws PersistanceException
	{
		UserInfo user = (UserInfo) dataPersistance.loadById(UserInfo.class,
				userId);
		return user;
	}

	/**
	 * DAO method to load the user info object from DB based on the email of the
	 * user
	 *
	 * @param email
	 *            The email id of the user whose information has to be fetched
	 * @return The UserInfo object pertaining to the email id. Null if not
	 *         present
	 * @throws PersistanceException
	 */
	public UserInfo getUserByEmail(String email) throws PersistanceException
	{
		String query = "from UserInfo where email = :email";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("email", new QueryParam(email, QueryParamTypes.STRING));
		return dataPersistance.executeHQLQueryUniqueResult(query, params);
	}

	/**
	 * DAO method to persist the UserInfo object
	 *
	 * @param user
	 *            The UserInfo object to be persisted
	 * @return The integer user id value used to identify the persisted user
	 *         here on
	 * @throws PersistanceException
	 */
	public UserInfo saveUserInfo(UserInfo user) throws PersistanceException
	{
		return dataPersistance.save(user);
	}

	/**
	 * DAO method to update an already saved user or persist a new user if no
	 * user already saved
	 *
	 * @param user
	 *            The user object to be updated or persisted
	 * @throws PersistanceException
	 */
	public void saveOrUpdateUserInfo(UserInfo user) throws PersistanceException
	{
		dataPersistance.saveOrUpdate(user);
	}

	/**
	 * @param userId
	 * @param status
	 * @throws PersistanceException
	 */
	public int updateUserStatus(int userId, boolean isActive)
			throws PersistanceException
	{
		String hqlUpdate = "update UserInfo u set u.isActive = :isActive where u.userId = :userId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("isActive",
				new QueryParam(isActive, QueryParamTypes.BOOLEAN));
		params.put("userId", new QueryParam(userId, QueryParamTypes.INT));
		int updatedEntities = dataPersistance.executeHQLQueryUpdateRecords(
				hqlUpdate, params);
		return updatedEntities;
	}
}
