package com.homearte.web.user.persistence.entity;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * DB Entity class for managing user details
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "user_info")
public class UserInfo
{

	/**
	 *
	 */
	private static final long			serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private int							userId;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_delivery_address", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "contact_info_id"))
	private Set<DeliveryContactInfo>	deliveryAddresses;

	@Column(name = "email")
	private String						email;

	@Column(name = "first_name")
	private String						firstName;

	@Column(name = "last_name")
	private String						lastName;

	@Column(name = "creation_timestamp")
	private Timestamp					creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp					modificationTimestamp;

	@Column(name = "is_active")
	private boolean						isActive;

	@Column(name = "user_type")
	private int							userType;

	@Column(name = "password")
	private String						password;

	@Column(name = "salt")
	private String						salt;

	@Column(name = "primary_phone_number")
	private String						primaryPhoneNumber;

	/**
	 * @param email
	 * @param isActive
	 * @param userType
	 * @param password
	 * @param firstName
	 * @param lastName
	 */
	public UserInfo(String email, boolean isActive, int userType,
			String password, String salt, String firstName, String lastName,
			String primaryPhoneNumber)
	{
		super();
		this.email = email;
		this.isActive = isActive;
		this.userType = userType;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.salt = salt;
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	/**
	 * @param primaryPhoneNumber
	 *            the primaryPhoneNumber to set
	 */
	public void setPrimaryPhoneNumber(String primaryPhoneNumber)
	{
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	/**
	 * @return the salt
	 */
	public String getSalt()
	{
		return salt;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

	/**
	 * @return the userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * @param salt
	 *            the salt to set
	 */
	public void setSalt(String salt)
	{
		this.salt = salt;
	}

	/**
	 * @return the contactInfo
	 */
	public Set<DeliveryContactInfo> getDeliveryAddresses()
	{
		return deliveryAddresses;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @return the userType
	 */
	public int getUserType()
	{
		return userType;
	}

	/**
	 *
	 */
	protected UserInfo()
	{
		super();
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

}
