/**
 *
 */
package com.homearte.web.user.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum BasicTokenBasedUserInteractionStatus
{

	STARTED(0), FINISHED(1), ONGOING(2);

	private int	value;

	/**
	 * @param value
	 */
	private BasicTokenBasedUserInteractionStatus(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static BasicTokenBasedUserInteractionStatus getEnumForValue(int value)
	{
		for (BasicTokenBasedUserInteractionStatus type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
