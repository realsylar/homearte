/**
 *
 */
package com.homearte.web.user.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum UserType
{

	SUPER_ADMIN(0), BUILDER_ADMIN(1), CLIENT_USER(2);

	private int	value;

	/**
	 * @param value
	 */
	private UserType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static UserType getEnumForValue(int value)
	{
		for (UserType type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}
}
