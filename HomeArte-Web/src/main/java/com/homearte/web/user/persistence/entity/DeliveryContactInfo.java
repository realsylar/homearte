/**
 *
 */
package com.homearte.web.user.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * General DB Entity class for managing contact delivery details
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "delivery_contact_info")
public class DeliveryContactInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "contact_info_id")
	private int		contactInfoId;

	@Column(name = "phone_number")
	private String	phoneNumber;

	@Column(name = "pincode")
	private int		pincode;

	@Column(name = "city")
	private String	city;

	@Column(name = "country")
	private String	country;

	@Column(name = "first_street")
	private String	firstStreet;

	@Column(name = "second_street")
	private String	secondStreet;

	@Column(name = "landmark")
	private String	landmark;

	@Column(name = "state")
	private String	state;

	/**
	 * @param contactInfoId
	 * @param phoneNumber
	 * @param address
	 * @param pincode
	 * @param city
	 * @param country
	 * @param firstStreet
	 * @param secondStreet
	 * @param landmark
	 * @param state
	 */
	public DeliveryContactInfo(int contactInfoId, String phoneNumber,
			int pincode, String city, String country, String firstStreet,
			String secondStreet, String landmark, String state)
	{
		super();
		this.contactInfoId = contactInfoId;
		this.phoneNumber = phoneNumber;
		this.pincode = pincode;
		this.city = city;
		this.country = country;
		this.firstStreet = firstStreet;
		this.secondStreet = secondStreet;
		this.landmark = landmark;
		this.state = state;
	}

	/**
	 *
	 */
	protected DeliveryContactInfo()
	{
		super();
	}

	/**
	 * @return the contactInfoId
	 */
	public int getContactInfoId()
	{
		return contactInfoId;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @return the pincode
	 */
	public int getPincode()
	{
		return pincode;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @return the firstStreet
	 */
	public String getFirstStreet()
	{
		return firstStreet;
	}

	/**
	 * @return the secondStreet
	 */
	public String getSecondStreet()
	{
		return secondStreet;
	}

	/**
	 * @return the landmark
	 */
	public String getLandmark()
	{
		return landmark;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + (city == null ? 0 : city.hashCode());
		result = prime * result + (country == null ? 0 : country.hashCode());
		result = prime * result
				+ (phoneNumber == null ? 0 : phoneNumber.hashCode());
		result = prime * result + pincode;
		result = prime * result + (state == null ? 0 : state.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryContactInfo other = (DeliveryContactInfo) obj;
		if (city == null)
		{
			if (other.city != null)
				return false;
		}
		else if (!city.equals(other.city))
			return false;
		if (country == null)
		{
			if (other.country != null)
				return false;
		}
		else if (!country.equals(other.country))
			return false;
		if (phoneNumber == null)
		{
			if (other.phoneNumber != null)
				return false;
		}
		else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (pincode != other.pincode)
			return false;
		if (state == null)
		{
			if (other.state != null)
				return false;
		}
		else if (!state.equals(other.state))
			return false;
		return true;
	}

}