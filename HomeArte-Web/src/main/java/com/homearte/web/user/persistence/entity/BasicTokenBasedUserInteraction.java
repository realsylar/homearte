/**
 *
 */
package com.homearte.web.user.persistence.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.homearte.web.user.enums.BasicTokenBasedUserInteractionStatus;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "basic_token_based_user_interaction")
public class BasicTokenBasedUserInteraction
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "intrxn_id")
	private int			intrxnId;
	@Column(name = "user_id")
	private int			userId;
	@Column(name = "token")
	private String		token;
	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;
	@Column(name = "intrxn_type")
	private int			intrxnType;
	@Column(name = "status")
	private int			status;
	@Column(name = "salt")
	private String		salt;

	/**
	 *
	 */
	protected BasicTokenBasedUserInteraction()
	{
		super();
	}

	/**
	 * @param userId
	 * @param token
	 * @param intrxnType
	 */
	public BasicTokenBasedUserInteraction(int userId, String token,
			int intrxnType, String salt)
	{
		super();
		this.userId = userId;
		this.token = token;
		this.intrxnType = intrxnType;
		this.status = BasicTokenBasedUserInteractionStatus.STARTED.getValue();
		this.salt = salt;
	}

	/**
	 * @return the intrxnId
	 */
	public int getIntrxnId()
	{
		return intrxnId;
	}

	/**
	 * @return the userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * @return the token
	 */
	public String getToken()
	{
		return token;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the intrxnType
	 */
	public int getIntrxnType()
	{
		return intrxnType;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/**
	 * @return the salt
	 */
	public String getSalt()
	{
		return salt;
	}

}
