/**
 *
 */
package com.homearte.web.user.forms.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class SignupFormDTO
{

	private String	firstName;
	private String	lastName;
	private String	email;
	private int		userType;
	private String	password;
	private String	error;
	private String	primaryPhoneNumber;

	/**
	 * @return the primaryPhoneNumber
	 */
	public String getPrimaryPhoneNumber()
	{
		return primaryPhoneNumber;
	}

	/**
	 * @param primaryPhoneNumber
	 *            the primaryPhoneNumber to set
	 */
	public void setPrimaryPhoneNumber(String primaryPhoneNumber)
	{
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	/**
	 * @param userType
	 */
	public SignupFormDTO(int userType)
	{
		super();
		this.userType = userType;
	}

	/**
	 *
	 */
	public SignupFormDTO()
	{
		super();
	}

	/**
	 * @return the error
	 */
	public String getError()
	{
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error)
	{
		this.error = error;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @return the userType
	 */
	public int getUserType()
	{
		return userType;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(int userType)
	{
		this.userType = userType;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "SignupFormDTO [firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", userType=" + userType
				+ ", password=" + password + ", error=" + error
				+ ", primaryPhoneNumber=" + primaryPhoneNumber + "]";
	}

}
