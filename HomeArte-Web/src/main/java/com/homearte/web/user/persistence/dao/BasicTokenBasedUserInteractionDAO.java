package com.homearte.web.user.persistence.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.user.persistence.entity.BasicTokenBasedUserInteraction;

/**
 * DAO class for all DB operations on the basic_token_based_user_interaction
 * table.
 *
 * @author Anurag Agrawal
 */

@Repository
public class BasicTokenBasedUserInteractionDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */

	@Autowired
	public BasicTokenBasedUserInteractionDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public BasicTokenBasedUserInteraction saveOrUpdate(
			BasicTokenBasedUserInteraction entity) throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(BasicTokenBasedUserInteraction entity)
			throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public BasicTokenBasedUserInteraction getById(int id)
			throws PersistanceException
	{
		return (BasicTokenBasedUserInteraction) dataPersistance.loadById(
				BasicTokenBasedUserInteraction.class, id);
	}

	/**
	 * @param id
	 * @param type
	 * @return
	 * @throws PersistanceException
	 */
	public BasicTokenBasedUserInteraction getByUserIdAndType(int id, int type)
			throws PersistanceException
	{
		String query = "from BasicTokenBasedUserInteraction where userId = :userId and intrxnType = :intrxnType";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("userId", new QueryParam(id, QueryParamTypes.INT));
		params.put("intrxnType", new QueryParam(type, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryUniqueResult(query, params);
	}
}
