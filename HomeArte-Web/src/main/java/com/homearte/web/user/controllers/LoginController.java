/**
 *
 */
package com.homearte.web.user.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.user.forms.dto.LoginFormDTO;
import com.homearte.web.user.service.LoginService;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
public class LoginController
{

	private static final Logger	logger	= Logger.getLogger(LoginController.class);
	private LoginService		loginService;

	/**
	 * @param loginService
	 * @param webSessionManager
	 */
	@Autowired
	public LoginController(LoginService loginService)
	{
		super();
		this.loginService = loginService;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ulogin")
	public ModelAndView loadLoginPage(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("login");
		String error = null;
		LoginFormDTO ldto;
		if (request.getSession().getAttribute("ldto") == null)
			ldto = new LoginFormDTO();
		else
		{
			ldto = (LoginFormDTO) request.getSession().getAttribute("ldto");
			request.getSession().removeAttribute("ldto");
		}
		if ((error = (String) request.getSession().getAttribute("error")) != null)
		{
			ldto.setError(error);
			request.getSession().removeAttribute("error");
		}
		model.addObject("ldto", ldto);
		logger.info("The ldto object is : " + ldto);
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ulogin/sub")
	public ModelAndView processLogin(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute(value = "ldto") LoginFormDTO loginFormData)
	{
		ModelAndView model = new ModelAndView("dashboard");
		try
		{
			boolean doesUserAlreadyExist = loginService
					.checkIfUserAlreadyExists(loginFormData);
			if (!doesUserAlreadyExist)
			{
				loginFormData
				.setError(SpringContextPropertyLoader
						.getPropertyForKey(ApplicationConstants.USER_DOES_NOT_EXIST));
				loginFormData = new LoginFormDTO();
				model = new ModelAndView("redirect:/ulogin");
				request.getSession().setAttribute("ldto", loginFormData);
			}
			else
			{
				model = loginService.processUserLogin(request, loginFormData);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in builder sign up process", e);
			loginFormData
			.setError(SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY));
			loginFormData.setPassword(null);
			model = new ModelAndView("redirect:/login");
			request.getSession().setAttribute("ldto", loginFormData);
		}
		return model;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/logout")
	public ModelAndView processLogout(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("redirect:/");
		loginService.processUserLogout(request);
		return model;
	}
}
