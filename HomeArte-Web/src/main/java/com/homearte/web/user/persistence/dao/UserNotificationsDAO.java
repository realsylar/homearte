/**
 *
 */
package com.homearte.web.user.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.user.persistence.entity.UserNotifications;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class UserNotificationsDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public UserNotificationsDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public UserNotifications saveOrUpdate(UserNotifications entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(UserNotifications entity) throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public UserNotifications getById(int id) throws PersistanceException
	{
		return dataPersistance.loadById(UserNotifications.class, id);
	}

	/**
	 * @param userId
	 * @return
	 * @throws PersistanceException
	 */
	public List<UserNotifications> getNotificationsByUserId(int userId)
			throws PersistanceException
			{
		String query = "from UserNotifications where userId = :userId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("userId", new QueryParam(userId, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryListResult(query, params);
			}
}
