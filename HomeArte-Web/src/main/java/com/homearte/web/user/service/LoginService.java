/**
 *
 */
package com.homearte.web.user.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.commons.core.security.encryption.SHA256Encryptor;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.session.WebSessionManager;
import com.homearte.web.user.forms.dto.LoginFormDTO;
import com.homearte.web.user.persistence.dao.UserInfoDAO;
import com.homearte.web.user.persistence.entity.UserInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class LoginService
{

	private UserInfoDAO			userInfoDAO;
	private SHA256Encryptor		shaEncryptor;
	private WebSessionManager	webSessionManager;

	/**
	 * @param userInfoDAO
	 * @param shaEncryptor
	 * @param webSessionManager
	 */
	@Autowired
	public LoginService(UserInfoDAO userInfoDAO, SHA256Encryptor shaEncryptor,
			WebSessionManager webSessionManager)
	{
		super();
		this.userInfoDAO = userInfoDAO;
		this.shaEncryptor = shaEncryptor;
		this.webSessionManager = webSessionManager;
	}

	/**
	 * @param loginFormData
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional(readOnly = true)
	public boolean checkIfUserAlreadyExists(LoginFormDTO loginFormData)
			throws PersistanceException
	{
		UserInfo user = userInfoDAO.getUserByEmail(loginFormData.getEmail());
		return user != null;
	}

	/**
	 * @param request
	 * @param loginFormData
	 * @return
	 * @throws PersistanceException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	@Transactional(readOnly = true)
	public ModelAndView processUserLogin(HttpServletRequest request,
			LoginFormDTO loginFormData) throws PersistanceException,
			InvalidKeyException, NoSuchAlgorithmException
	{
		ModelAndView model = null;
		UserInfo user = userInfoDAO.getUserByEmail(loginFormData.getEmail());
		String receivedPassword = loginFormData.getPassword();
		if (!user.getPassword().equalsIgnoreCase(
				shaEncryptor.encryptValue(receivedPassword, user.getSalt())))
		{
			loginFormData
			.setError(SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.PASSWORD_INCORRECT));
			loginFormData.setPassword(null);
			model = new ModelAndView("redirect:/ulogin");
			request.getSession().setAttribute("ldto", loginFormData);
		}
		else
		{
			request.getSession().removeAttribute("ldto");
			webSessionManager.addUserIdToSession(request.getSession(),
					user.getUserId());
			model = new ModelAndView("redirect:/dashboard");
		}
		return model;
	}

	/**
	 *
	 */
	@Transactional(readOnly = true)
	public void processUserLogout(HttpServletRequest request)
	{
		webSessionManager.removeUserFromSession(request.getSession());
	}
}
