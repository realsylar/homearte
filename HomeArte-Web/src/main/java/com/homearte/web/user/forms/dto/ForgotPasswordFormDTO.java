/**
 *
 */
package com.homearte.web.user.forms.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class ForgotPasswordFormDTO
{

	private String	email;
	private String	password;
	private String	error;
	private String	message;

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * @return the error
	 */
	public String getError()
	{
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error)
	{
		this.error = error;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 *
	 */
	public ForgotPasswordFormDTO()
	{
		super();
	}

	/**
	 * @param email
	 * @param password
	 * @param error
	 * @param message
	 */
	public ForgotPasswordFormDTO(String email, String password, String error,
			String message)
	{
		super();
		this.email = email;
		this.password = password;
		this.error = error;
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "ForgotPasswordFormDTO [email=" + email + ", password="
				+ password + ", error=" + error + ", message=" + message + "]";
	}

}
