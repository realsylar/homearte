/**
 *
 */
package com.homearte.web.user.display.dto;

import com.homearte.web.user.persistence.entity.UserNotifications;

/**
 * @author Anurag Agrawal
 *
 */
public class UserNotificationsDTO
{

	private int		notificationId;
	private String	notificationText;
	private int		status;
	private String	error;
	private String	message;

	/**
	 * @param notificationId
	 * @param notificationText
	 * @param status
	 * @param error
	 * @param message
	 */
	public UserNotificationsDTO(int notificationId, String notificationText,
			int status, String error, String message)
	{
		super();
		this.notificationId = notificationId;
		this.notificationText = notificationText;
		this.status = status;
		this.error = error;
		this.message = message;
	}

	/**
	 *
	 */
	public UserNotificationsDTO(UserNotifications userNotification)
	{
		super();
		this.notificationId = userNotification.getNotificationId();
		this.notificationText = userNotification.getNotificationText();
		this.status = userNotification.getNotificationStatus();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "UserNotificationsDTO [notificationId=" + notificationId
				+ ", notificationText=" + notificationText + ", status="
				+ status + ", error=" + error + ", message=" + message + "]";
	}

	/**
	 * @return the notificationId
	 */
	public int getNotificationId()
	{
		return notificationId;
	}

	/**
	 * @param notificationId
	 *            the notificationId to set
	 */
	public void setNotificationId(int notificationId)
	{
		this.notificationId = notificationId;
	}

	/**
	 * @return the notificationText
	 */
	public String getNotificationText()
	{
		return notificationText;
	}

	/**
	 * @param notificationText
	 *            the notificationText to set
	 */
	public void setNotificationText(String notificationText)
	{
		this.notificationText = notificationText;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/**
	 * @return the error
	 */
	public String getError()
	{
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error)
	{
		this.error = error;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

}
