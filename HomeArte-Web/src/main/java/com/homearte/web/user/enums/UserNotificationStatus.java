/**
 *
 */
package com.homearte.web.user.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum UserNotificationStatus
{
	UNREAD(0), READ(1);

	private int	value;

	/**
	 * @param value
	 */
	private UserNotificationStatus(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static UserNotificationStatus getEnumForValue(int value)
	{
		for (UserNotificationStatus type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
