/**
 *
 */
package com.homearte.web.user.service;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.homearte.commons.aws.entity.SESMailingMessageParams;
import com.homearte.commons.aws.enums.EmailBodyType;
import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.commons.core.manager.EmailSenderManager;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.commons.core.security.encryption.AESCBCEncryptor;
import com.homearte.commons.core.security.encryption.RandomSecureTokenGenerator;
import com.homearte.commons.core.security.encryption.SHA256Encryptor;
import com.homearte.commons.core.security.encryption.SHAEncryptedValue;
import com.homearte.commons.core.security.encryption.exception.EncryptorException;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.user.enums.BasicTokenBasedUserInteractionStatus;
import com.homearte.web.user.enums.BasicTokenBasedUserInteractionType;
import com.homearte.web.user.forms.dto.ForgotPasswordFormDTO;
import com.homearte.web.user.persistence.dao.BasicTokenBasedUserInteractionDAO;
import com.homearte.web.user.persistence.dao.UserInfoDAO;
import com.homearte.web.user.persistence.entity.BasicTokenBasedUserInteraction;
import com.homearte.web.user.persistence.entity.UserInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ForgotPasswordService
{

	private static final String					TOKEN_PARAM_NAME	= "t";
	private static final String					ID_PARAM_NAME		= "i";
	private UserInfoDAO							userInfoDAO;
	private EmailSenderManager					emailManager;
	private BasicTokenBasedUserInteractionDAO	userInteractionDAO;
	private RandomSecureTokenGenerator			tokenGenerator;
	private AESCBCEncryptor						aesEncryptor;
	private SHA256Encryptor						shaEncryptor;
	private VelocityEngine						velocityEngine;

	/**
	 * @param userInfoDAO
	 * @param emailManager
	 * @param userInteractionDAO
	 * @param tokenGenerator
	 * @param aesEncryptor
	 * @param shaEncryptor
	 * @param velocityEngine
	 */
	@Autowired
	public ForgotPasswordService(UserInfoDAO userInfoDAO,
			EmailSenderManager emailManager,
			BasicTokenBasedUserInteractionDAO userInteractionDAO,
			RandomSecureTokenGenerator tokenGenerator,
			AESCBCEncryptor aesEncryptor, SHA256Encryptor shaEncryptor,
			VelocityEngine velocityEngine)
	{
		super();
		this.userInfoDAO = userInfoDAO;
		this.emailManager = emailManager;
		this.userInteractionDAO = userInteractionDAO;
		this.tokenGenerator = tokenGenerator;
		this.aesEncryptor = aesEncryptor;
		this.shaEncryptor = shaEncryptor;
		this.velocityEngine = velocityEngine;
	}

	/**
	 * @param forgotPasswordFormData
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional(readOnly = true)
	public boolean checkIfUserAlreadyExists(
			ForgotPasswordFormDTO forgotPasswordFormData)
			throws PersistanceException
	{
		return userInfoDAO.getUserByEmail(forgotPasswordFormData.getEmail()) != null;
	}

	/**
	 * @param forgotPasswordFormData
	 * @return
	 * @throws PersistanceException
	 * @throws EncryptorException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws MessagingException
	 */
	@Transactional
	public ForgotPasswordFormDTO processForgotPasswordMailInteractionForUser(
			ForgotPasswordFormDTO forgotPasswordFormData,
			HttpServletRequest request) throws PersistanceException,
			InvalidKeyException, NoSuchAlgorithmException,
			UnsupportedEncodingException, EncryptorException,
			MessagingException
	{
		UserInfo user = userInfoDAO.getUserByEmail(forgotPasswordFormData
				.getEmail());
		String generatedConfirmationLink = generateConfirmationLink(
				user.getUserId(), request);
		SESMailingMessageParams params = prepareEmailMessageForForgotPassword(
				user, generatedConfirmationLink);
		emailManager.sendMail(params);
		forgotPasswordFormData
		.setMessage(SpringContextPropertyLoader
				.getPropertyForKey(ApplicationConstants.RECOVER_PASSWORD_MAIL_SENT));
		return forgotPasswordFormData;
	}

	/**
	 * @param recoverPasswordFormData
	 * @param request
	 * @throws PersistanceException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	@Transactional
	public void processPasswordRecoveryFormSubmit(
			ForgotPasswordFormDTO recoverPasswordFormData,
			HttpServletRequest request) throws PersistanceException,
			InvalidKeyException, NoSuchAlgorithmException
	{
		UserInfo user = userInfoDAO.getUserByEmail(recoverPasswordFormData
				.getEmail());
		BasicTokenBasedUserInteraction userInt = userInteractionDAO
				.getByUserIdAndType(user.getUserId(),
						BasicTokenBasedUserInteractionType.FORGOT_PASSWORD
						.getValue());
		if (userInt != null
				&& userInt.getStatus() == BasicTokenBasedUserInteractionStatus.ONGOING
				.getValue())
		{
			SHAEncryptedValue encVal = shaEncryptor
					.encryptValue(recoverPasswordFormData.getPassword());
			user.setPassword(encVal.getEncryptedValue());
			user.setSalt(encVal.getSalt());
			userInfoDAO.saveOrUpdateUserInfo(user);
			userInteractionDAO.delete(userInt);
		}
		else
		{
			request.getSession()
			.setAttribute(
					"error",
					SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.INVALID_LINK));
		}
	}

	/**
	 * @param sId
	 * @param token
	 * @param request
	 * @throws PersistanceException
	 * @throws MessagingException
	 * @throws EncryptorException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	@Transactional
	public void processForgotPasswordConfirmationLinkClickForUser(String sId,
			String token, HttpServletRequest request)
			throws PersistanceException, InvalidKeyException,
			NoSuchAlgorithmException, UnsupportedEncodingException,
			EncryptorException, MessagingException
	{
		int interactionId = Integer.parseInt(aesEncryptor.decrypt(sId));
		BasicTokenBasedUserInteraction userInteraction = userInteractionDAO
				.getById(interactionId);
		int userId = userInteraction.getUserId();
		String base64DecodedToken = new String(Base64.decodeBase64(URLDecoder
				.decode(token, "UTF-8")));
		if (userInteraction.getToken().equals(
				shaEncryptor.encryptValue(base64DecodedToken,
						userInteraction.getSalt()))
				&& userInteraction.getIntrxnType() == BasicTokenBasedUserInteractionType.FORGOT_PASSWORD
						.getValue()
				&& userInteraction.getStatus() == BasicTokenBasedUserInteractionStatus.STARTED
						.getValue())
		{
			userInteraction
					.setStatus(BasicTokenBasedUserInteractionStatus.ONGOING
							.getValue());
			userInteractionDAO.saveOrUpdate(userInteraction);
			UserInfo user = userInfoDAO.getUser(userId);
			request.getSession().setAttribute("email", user.getEmail());
		}
		else
		{
			userInteractionDAO.delete(userInteraction);
			SESMailingMessageParams params = prepareEmailMessageForForgotPassword(
					userInfoDAO.getUser(userId),
					generateConfirmationLink(userId, request));
			emailManager.sendMail(params);
			request.getSession()
					.setAttribute(
							"error",
							SpringContextPropertyLoader
									.getPropertyForKey(ApplicationConstants.INVALID_CONFIRMATION_LINK));
		}
	}

	private SESMailingMessageParams prepareEmailMessageForForgotPassword(
			UserInfo user, String link)
	{
		Template temp = velocityEngine
				.getTemplate(ApplicationConstants.RPWD_MAIL_TEMPLATE_NAME);
		VelocityContext cont = new VelocityContext();
		cont.put("fName", user.getFirstName());
		cont.put("lName", user.getLastName());
		cont.put("link", link);
		StringWriter writer = new StringWriter();
		temp.merge(cont, writer);
		return new SESMailingMessageParams(
				ApplicationConstants.INFO_FROM_ADDRESS,
				user.getEmail(),
				null,
				null,
				SpringContextPropertyLoader
				.getPropertyForKey(ApplicationConstants.RPWD_EMAIL_SUBJECT),
				writer.toString(), EmailBodyType.TEXTHTML);
	}

	/**
	 * @param userInt
	 * @return
	 * @throws EncryptorException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws PersistanceException
	 * @throws UnsupportedEncodingException
	 */
	private String generateConfirmationLink(int userId,
			HttpServletRequest request) throws EncryptorException,
			InvalidKeyException, NoSuchAlgorithmException,
			PersistanceException, UnsupportedEncodingException
	{
		String token = tokenGenerator.generateToken();
		SHAEncryptedValue encVal = shaEncryptor.encryptValue(token);
		BasicTokenBasedUserInteraction userInt = new BasicTokenBasedUserInteraction(
				userId, encVal.getEncryptedValue(),
				BasicTokenBasedUserInteractionType.FORGOT_PASSWORD.getValue(),
				encVal.getSalt());
		userInt = userInteractionDAO.saveOrUpdate(userInt);
		StringBuffer url = request.getRequestURL();
		String uri = request.getRequestURI();
		String ctx = request.getContextPath();
		String base = url.substring(0,
				url.length() - uri.length() + ctx.length())
				+ "/";
		StringBuffer buf = new StringBuffer();
		buf.append(base);
		buf.append(ApplicationConstants.RPWD_CONFIRMATION_LINK);
		buf.append(TOKEN_PARAM_NAME);
		buf.append("=");
		buf.append(URLEncoder.encode(
				Base64.encodeBase64String(String.valueOf(token).getBytes()),
				"UTF-8"));
		buf.append("&");
		buf.append(ID_PARAM_NAME);
		buf.append("=");
		buf.append(URLEncoder.encode(
				aesEncryptor.encrypt(String.valueOf(userInt.getIntrxnId())),
				"UTF-8"));
		return buf.toString();
	}

}
