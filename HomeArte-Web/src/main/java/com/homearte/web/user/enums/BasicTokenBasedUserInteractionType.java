/**
 *
 */
package com.homearte.web.user.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum BasicTokenBasedUserInteractionType
{

	SIGNUP(0), FORGOT_PASSWORD(1);

	private int	value;

	/**
	 * @param value
	 */
	private BasicTokenBasedUserInteractionType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static BasicTokenBasedUserInteractionType getEnumForValue(int value)
	{
		for (BasicTokenBasedUserInteractionType type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
