/**
 *
 */
package com.homearte.web.user.controllers.exception;

/**
 * @author Anurag Agrawal
 *
 */
public class UserIdNotValidException extends Exception
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	private Exception			e;

	/**
	 * @param e
	 */
	public UserIdNotValidException(Exception e)
	{
		super();
		this.e = e;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage()
	{
		return e.getMessage();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Throwable#getStackTrace()
	 */
	@Override
	public StackTraceElement[] getStackTrace()
	{
		return e.getStackTrace();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Throwable#getLocalizedMessage()
	 */
	@Override
	public String getLocalizedMessage()
	{
		return e.getLocalizedMessage();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Throwable#printStackTrace()
	 */
	@Override
	public void printStackTrace()
	{
		e.printStackTrace();
	}

}
