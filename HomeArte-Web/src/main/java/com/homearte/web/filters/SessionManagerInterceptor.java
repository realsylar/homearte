package com.homearte.web.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.homearte.commons.core.cache.manager.HomearteMemCacheManager;
import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.web.cache.constants.HomearteCacheConstants;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.session.WebSessionManager;

/**
 * Servlet Filter implementation class SessionManagerInterceptor
 */

public class SessionManagerInterceptor extends HandlerInterceptorAdapter
{

	private WebSessionManager		sessionManager;
	private HomearteMemCacheManager	memCacheManager;

	/**
	 * @param sessionManager
	 */
	@Autowired
	public SessionManagerInterceptor(WebSessionManager sessionManager,
			HomearteMemCacheManager memCacheManager)
	{
		super();
		this.sessionManager = sessionManager;
		this.memCacheManager = memCacheManager;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet
	 * .http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception
	{
		if (request.getSession() == null
				|| !sessionManager.isUserInSession(request.getSession()))
		{
			request.getSession()
			.setAttribute(
					"error",
					SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.UNAUTHORIZED_ACCESS_LOGIN_TO_CONTINUE));
			response.sendRedirect("/ulogin/");
			return false;
		}
		int userId = sessionManager.getUserIdFromSession(request.getSession());
		int bid = (Integer) memCacheManager
				.getItemWithKey(HomearteCacheConstants.BUILDER_ID_FOR_USER
						+ userId);
		if (bid == -1)
		{
			request.getSession()
			.setAttribute(
					"error",
					SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.UNAUTHORIZED_ACCESS_LOGIN_TO_CONTINUE));
			response.sendRedirect("redirect:/ulogin");
		}
		return true;
	}
}
