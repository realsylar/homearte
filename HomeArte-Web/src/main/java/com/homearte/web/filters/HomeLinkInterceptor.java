/**
 *
 */
package com.homearte.web.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.homearte.web.session.WebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
public class HomeLinkInterceptor extends HandlerInterceptorAdapter
{

	private WebSessionManager	sessionManager;

	/**
	 * @param sessionManager
	 */
	@Autowired
	public HomeLinkInterceptor(WebSessionManager sessionManager)
	{
		super();
		this.sessionManager = sessionManager;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle
	 * (javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception
	{
		if (sessionManager.isUserInSession(request.getSession()))
		{
			response.sendRedirect("/dashboard/");
			return false;
		}
		return true;
	}

}
