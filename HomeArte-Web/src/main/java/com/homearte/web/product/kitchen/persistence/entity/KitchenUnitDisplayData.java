/**
 *
 */
package com.homearte.web.product.kitchen.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "kitchen_unit_display_data")
public class KitchenUnitDisplayData
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "dd_id")
	private int		ddId;

	@Column(name = "kitchen_id")
	private int		kitchenId;

	@Column(name = "unit_type")
	private int		unitType;

	@Column(name = "unit_name")
	private String	unitName;

	@Column(name = "unit_dimension")
	private String	unitDimension;

	@Column(name = "unit_material")
	private String	unitMaterial;

	@Column(name = "shutter_material")
	private String	shutterMaterial;

	@Column(name = "shutter_color")
	private String	shutterColor;

	/**
	 * @return the ddId
	 */
	public int getDdId()
	{
		return ddId;
	}

	/**
	 * @return the kitchenId
	 */
	public int getKitchenId()
	{
		return kitchenId;
	}

	/**
	 * @return the unitName
	 */
	public String getUnitName()
	{
		return unitName;
	}

	/**
	 * @return the unitDimension
	 */
	public String getUnitDimension()
	{
		return unitDimension;
	}

	/**
	 * @return the unitMaterial
	 */
	public String getUnitMaterial()
	{
		return unitMaterial;
	}

	/**
	 * @return the shutterMaterial
	 */
	public String getShutterMaterial()
	{
		return shutterMaterial;
	}

	/**
	 * @return the shutterColor
	 */
	public String getShutterColor()
	{
		return shutterColor;
	}

	/**
	 * @return the unitType
	 */
	public int getUnitType()
	{
		return unitType;
	}

	/**
	 * @param kitchenId
	 * @param unitName
	 * @param unitDimension
	 * @param unitMaterial
	 * @param shutterMaterial
	 * @param shutterColor
	 */
	public KitchenUnitDisplayData(int kitchenId, String unitName,
			String unitDimension, String unitMaterial, String shutterMaterial,
			String shutterColor, int unitType)
	{
		super();
		this.kitchenId = kitchenId;
		this.unitName = unitName;
		this.unitDimension = unitDimension;
		this.unitMaterial = unitMaterial;
		this.shutterMaterial = shutterMaterial;
		this.shutterColor = shutterColor;
		this.unitType = unitType;
	}

	/**
	 *
	 */
	protected KitchenUnitDisplayData()
	{
		super();
	}

}
