/**
 *
 */
package com.homearte.web.product.display.dto;

import com.homearte.web.product.kitchen.persistence.entity.KitchenHardwareDisplayData;

/**
 * @author Anurag Agrawal
 *
 */
public class KitchenHardwareDisplayDataDTO
{

	private String	name;
	private String	brand;

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the brand
	 */
	public String getBrand()
	{
		return brand;
	}

	/**
	 * @param brand
	 *            the brand to set
	 */
	public void setBrand(String brand)
	{
		this.brand = brand;
	}

	/**
	 *
	 */
	public KitchenHardwareDisplayDataDTO()
	{
		super();
	}

	/**
	 * @param name
	 * @param brand
	 */
	public KitchenHardwareDisplayDataDTO(String name, String brand)
	{
		super();
		this.name = name;
		this.brand = brand;
	}

	public KitchenHardwareDisplayDataDTO(KitchenHardwareDisplayData data)
	{
		super();
		this.name = data.getHardwareName();
		this.brand = data.getHardwareBrandName();
	}
}
