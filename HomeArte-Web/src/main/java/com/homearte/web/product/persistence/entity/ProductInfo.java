/**
 *
 */
package com.homearte.web.product.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "product_info")
public class ProductInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "product_id")
	private int		productId;

	@Column(name = "base_price")
	private float	basePrice;

	@Column(name = "display_name")
	private String	displayName;

	@Column(name = "product_type")
	private int		productType;

	@Column(name = "creation_timestamp")
	private int		creationTimestamp;

	@Column(name = "modification_timestamp")
	private int		modificationTimestamp;

	@Column(name = "is_active")
	private boolean	isActive;

	/**
	 * @return the productId
	 */
	public int getProductId()
	{
		return productId;
	}

	/**
	 * @return the basePrice
	 */
	public float getBasePrice()
	{
		return basePrice;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @return the productType
	 */
	public int getProductType()
	{
		return productType;
	}

	/**
	 * @return the creationTimestamp
	 */
	public int getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public int getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param basePrice
	 * @param displayName
	 * @param productType
	 * @param isActive
	 */
	public ProductInfo(float basePrice, String displayName, int productType,
			boolean isActive)
	{
		super();
		this.basePrice = basePrice;
		this.displayName = displayName;
		this.productType = productType;
		this.isActive = isActive;
	}

	/**
	 *
	 */
	protected ProductInfo()
	{
		super();
	}

}
