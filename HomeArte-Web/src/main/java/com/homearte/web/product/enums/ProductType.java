/**
 *
 */
package com.homearte.web.product.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum ProductType
{
	BUILDER(1), BUILDER_AND_CUSTOMER(2), PUBLIC(3);

	private int	value;

	/**
	 * @param value
	 */
	private ProductType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static ProductType getEnumForValue(int value)
	{
		for (ProductType type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
