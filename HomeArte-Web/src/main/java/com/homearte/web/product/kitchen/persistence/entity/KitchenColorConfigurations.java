/**
 *
 */
package com.homearte.web.product.kitchen.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "kitchen_color_configuration")
public class KitchenColorConfigurations
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "config_id")
	private int		configId;

	@Column(name = "color_name")
	private String	colorName;

	@Column(name = "color_price")
	private float	colorPrice;

	@Column(name = "creation_timestamp")
	private int		creationTimestamp;

	@Column(name = "modification_timestamp")
	private int		modificationTimestamp;

	@Column(name = "is_active")
	private boolean	isActive;

	@Column(name = "uname")
	private String	uname;

	/**
	 * @return the uname
	 */
	public String getUname()
	{
		return uname;
	}

	/**
	 * @return the configId
	 */
	public int getConfigId()
	{
		return configId;
	}

	/**
	 * @return the colorName
	 */
	public String getColorName()
	{
		return colorName;
	}

	/**
	 * @return the colorPrice
	 */
	public float getColorPrice()
	{
		return colorPrice;
	}

	/**
	 * @return the creationTimestamp
	 */
	public int getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public int getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param colorName
	 * @param colorPrice
	 * @param isActive
	 */
	public KitchenColorConfigurations(String colorName, float colorPrice,
			boolean isActive, String uname)
	{
		super();
		this.colorName = colorName;
		this.colorPrice = colorPrice;
		this.isActive = isActive;
		this.uname = uname;
	}

	/**
	 *
	 */
	protected KitchenColorConfigurations()
	{
		super();
	}

}
