/**
 *
 */
package com.homearte.web.product.controllers.exception;

/**
 * @author Anurag Agrawal
 *
 */
public class ProductNotFoundException extends Exception
{

	private Exception	originalException;

	public ProductNotFoundException(Exception e)
	{
		originalException = e;
	}

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Throwable#getCause()
	 */
	@Override
	public synchronized Throwable getCause()
	{
		return originalException;
	}

}
