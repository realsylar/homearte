/**
 *
 */
package com.homearte.web.product.kichen.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.product.kitchen.persistence.entity.KitchenConfigurations;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class KitchenConfigurationsDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public KitchenConfigurationsDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public KitchenConfigurations saveOrUpdate(KitchenConfigurations entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(KitchenConfigurations entity)
			throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int id) throws PersistanceException
	{
		dataPersistance.delete(KitchenConfigurations.class, id);
	}

	public KitchenConfigurations getById(int id) throws PersistanceException
	{
		return dataPersistance.loadById(KitchenConfigurations.class, id);
	}

	public List<KitchenConfigurations> getByKitchenId(int kitchenId)
			throws PersistanceException
			{
		String query = "from KitchenConfigurations where kitchenId = :kitchenId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("kitchenId", new QueryParam(kitchenId, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryListResult(query, params);
			}
}
