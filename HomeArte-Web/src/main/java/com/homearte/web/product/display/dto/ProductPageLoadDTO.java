/**
 *
 */
package com.homearte.web.product.display.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class ProductPageLoadDTO
{

	private int		projectId;
	private int		subProjectId;
	private String	subProjectName;
	private int		hpid;

	/**
	 * @return the projectId
	 */
	public int getProjectId()
	{
		return projectId;
	}

	/**
	 * @param projectId
	 *            the projectId to set
	 */
	public void setProjectId(int projectId)
	{
		this.projectId = projectId;
	}

	/**
	 * @return the subProjectId
	 */
	public int getSubProjectId()
	{
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            the subProjectId to set
	 */
	public void setSubProjectId(int subProjectId)
	{
		this.subProjectId = subProjectId;
	}

	/**
	 * @return the subProjectName
	 */
	public String getSubProjectName()
	{
		return subProjectName;
	}

	/**
	 * @param subProjectName
	 *            the subProjectName to set
	 */
	public void setSubProjectName(String subProjectName)
	{
		this.subProjectName = subProjectName;
	}

	/**
	 * @param projectId
	 * @param subProjectId
	 * @param subProjectName
	 */
	public ProductPageLoadDTO(int projectId, int subProjectId,
			String subProjectName, int hpid)
	{
		super();
		this.projectId = projectId;
		this.subProjectId = subProjectId;
		this.subProjectName = subProjectName;
		this.hpid = hpid;
	}

	/**
	 *
	 */
	public ProductPageLoadDTO()
	{
		super();
	}

	/**
	 * @return the hpid
	 */
	public int getHpid()
	{
		return hpid;
	}

	/**
	 * @param hpid
	 *            the hpid to set
	 */
	public void setHpid(int hpid)
	{
		this.hpid = hpid;
	}

}
