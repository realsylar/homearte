/**
 *
 */
package com.homearte.web.product.display.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class ProductListDTO
{

	private int		productId;
	private String	productName;
	private float	unitPrice;
	private int		status;

	/**
	 * @return the productId
	 */
	public int getProductId()
	{
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(int productId)
	{
		this.productId = productId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * @return the unitPrice
	 */
	public float getUnitPrice()
	{
		return unitPrice;
	}

	/**
	 * @param unitPrice
	 *            the unitPrice to set
	 */
	public void setUnitPrice(float unitPrice)
	{
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/**
	 * @param productId
	 * @param productName
	 * @param unitPrice
	 * @param status
	 */
	public ProductListDTO(int productId, String productName, float unitPrice,
			int status)
	{
		super();
		this.productId = productId;
		this.productName = productName;
		this.unitPrice = unitPrice;
		this.status = status;
	}

	/**
	 *
	 */
	public ProductListDTO()
	{
		super();
	}

}
