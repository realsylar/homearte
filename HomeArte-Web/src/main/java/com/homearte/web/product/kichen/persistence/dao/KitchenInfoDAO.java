/**
 *
 */
package com.homearte.web.product.kichen.persistence.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.product.kitchen.persistence.entity.KitchenInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class KitchenInfoDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public KitchenInfoDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public KitchenInfo saveOrUpdate(KitchenInfo entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(KitchenInfo entity) throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int id) throws PersistanceException
	{
		dataPersistance.delete(KitchenInfo.class, id);
	}

	public KitchenInfo getById(int id) throws PersistanceException
	{
		return dataPersistance.loadById(KitchenInfo.class, id);
	}

	public KitchenInfo getByProductId(int hpid) throws PersistanceException
	{
		String query = "from KitchenInfo where productId = :productId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("productId", new QueryParam(hpid, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryUniqueResult(query, params);
	}
}
