/**
 *
 */
package com.homearte.web.product.kitchen.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "kitchen_configurations")
public class KitchenConfigurations
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "kitchen_config_id")
	private int		kitchenConfigId;

	@Column(name = "kitchen_id")
	private int		kitchenId;

	@Column(name = "config_id")
	private int		configId;

	@Column(name = "config_type")
	private int		configType;

	@Column(name = "creation_timestamp")
	private int		creationTimestamp;

	@Column(name = "modification_timestamp")
	private int		modificationTimestamp;

	@Column(name = "is_active")
	private boolean	isActive;

	@Column(name = "is_default")
	private boolean	isDefault;

	@Column(name = "configuration_price_multiplier")
	private int		configurationPriceMultiplier;

	/**
	 * @return the isDefault
	 */
	public boolean isDefault()
	{
		return isDefault;
	}

	/**
	 * @return the kitchenConfigId
	 */
	public int getKitchenConfigId()
	{
		return kitchenConfigId;
	}

	/**
	 * @return the kitchenId
	 */
	public int getKitchenId()
	{
		return kitchenId;
	}

	/**
	 * @return the configId
	 */
	public int getConfigId()
	{
		return configId;
	}

	/**
	 * @return the configType
	 */
	public int getConfigType()
	{
		return configType;
	}

	/**
	 * @return the creationTimestamp
	 */
	public int getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public int getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @return the configurationPriceMultiplier
	 */
	public int getConfigurationPriceMultiplier()
	{
		return configurationPriceMultiplier;
	}

	/**
	 * @param kitchenId
	 * @param configId
	 * @param configType
	 * @param isActive
	 */
	public KitchenConfigurations(int kitchenId, int configId, int configType,
			boolean isActive, boolean isDefault,
			int configurationPriceMultiplier)
	{
		super();
		this.kitchenId = kitchenId;
		this.configId = configId;
		this.configType = configType;
		this.isActive = isActive;
		this.isDefault = isDefault;
		this.configurationPriceMultiplier = configurationPriceMultiplier;
	}

	/**
	 *
	 */
	protected KitchenConfigurations()
	{
		super();
	}

}
