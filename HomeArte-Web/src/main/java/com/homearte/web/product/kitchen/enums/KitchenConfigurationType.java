/**
 *
 */
package com.homearte.web.product.kitchen.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum KitchenConfigurationType
{
	COLOR(1), MATERIAL(2);

	private int	value;

	/**
	 * @param value
	 */
	private KitchenConfigurationType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static KitchenConfigurationType getEnumForValue(int value)
	{
		for (KitchenConfigurationType type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
