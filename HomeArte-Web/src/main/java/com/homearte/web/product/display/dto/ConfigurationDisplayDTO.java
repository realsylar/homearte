/**
 *
 */
package com.homearte.web.product.display.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class ConfigurationDisplayDTO
{

	private String	uname;
	private String	dname;
	private float	price;
	private boolean	isDefault;

	/**
	 * @return the uname
	 */
	public String getUname()
	{
		return uname;
	}

	/**
	 * @param uname
	 *            the uname to set
	 */
	public void setUname(String uname)
	{
		this.uname = uname;
	}

	/**
	 * @return the dname
	 */
	public String getDname()
	{
		return dname;
	}

	/**
	 * @param dname
	 *            the dname to set
	 */
	public void setDname(String dname)
	{
		this.dname = dname;
	}

	/**
	 * @return the price
	 */
	public float getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(float price)
	{
		this.price = price;
	}

	/**
	 * @return the isDefault
	 */
	public boolean isDefault()
	{
		return isDefault;
	}

	/**
	 * @param isDefault
	 *            the isDefault to set
	 */
	public void setDefault(boolean isDefault)
	{
		this.isDefault = isDefault;
	}

	/**
	 * @param uname
	 * @param dname
	 * @param price
	 * @param isDefault
	 */
	public ConfigurationDisplayDTO(String uname, String dname, float price,
			boolean isDefault)
	{
		super();
		this.uname = uname;
		this.dname = dname;
		this.price = price;
		this.isDefault = isDefault;
	}

	/**
	 *
	 */
	public ConfigurationDisplayDTO()
	{
		super();
	}

}
