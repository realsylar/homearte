/**
 *
 */
package com.homearte.web.product.persistence.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.product.persistence.entity.ProductInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class ProductInfoDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public ProductInfoDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public ProductInfo saveOrUpdate(ProductInfo entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(ProductInfo entity) throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int id) throws PersistanceException
	{
		dataPersistance.delete(ProductInfo.class, id);
	}

	public ProductInfo getById(int id) throws PersistanceException
	{
		return dataPersistance.loadById(ProductInfo.class, id);
	}

}
