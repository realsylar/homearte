/**
 *
 */
package com.homearte.web.product.kichen.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.product.kitchen.persistence.entity.KitchenHardwareDisplayData;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class KitchenHardwareDisplayDataDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public KitchenHardwareDisplayDataDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public KitchenHardwareDisplayData saveOrUpdate(
			KitchenHardwareDisplayData entity) throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(KitchenHardwareDisplayData entity)
			throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int id) throws PersistanceException
	{
		dataPersistance.delete(KitchenHardwareDisplayData.class, id);
	}

	public KitchenHardwareDisplayData getById(int id)
			throws PersistanceException
	{
		return dataPersistance.loadById(KitchenHardwareDisplayData.class, id);
	}

	public List<KitchenHardwareDisplayData> getByKitchenId(int kitchenId)
			throws PersistanceException
	{
		String query = "from KitchenHardwareDisplayData where kitchenId = :kitchenId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("kitchenId", new QueryParam(kitchenId, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryListResult(query, params);
	}
}
