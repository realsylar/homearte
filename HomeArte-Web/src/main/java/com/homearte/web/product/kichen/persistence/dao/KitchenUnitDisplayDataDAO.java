/**
 *
 */
package com.homearte.web.product.kichen.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.product.kitchen.persistence.entity.KitchenUnitDisplayData;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class KitchenUnitDisplayDataDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public KitchenUnitDisplayDataDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public KitchenUnitDisplayData saveOrUpdate(KitchenUnitDisplayData entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(KitchenUnitDisplayData entity)
			throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int id) throws PersistanceException
	{
		dataPersistance.delete(KitchenUnitDisplayData.class, id);
	}

	public KitchenUnitDisplayData getById(int id) throws PersistanceException
	{
		return dataPersistance.loadById(KitchenUnitDisplayData.class, id);
	}

	public List<KitchenUnitDisplayData> getByKitchenId(int kitchenId)
			throws PersistanceException
	{
		String query = "from KitchenUnitDisplayData where kitchenId = :kitchenId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("kitchenId", new QueryParam(kitchenId, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryListResult(query, params);
	}
}
