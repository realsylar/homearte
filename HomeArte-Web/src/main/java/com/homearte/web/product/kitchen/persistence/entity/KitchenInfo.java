/**
 *
 */
package com.homearte.web.product.kitchen.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "kitchen_info")
public class KitchenInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "kitchen_id")
	private int		kitchenId;

	@Column(name = "product_id")
	private int		productId;

	@Column(name = "content_preview_id")
	private int		contentPreviewId;

	@Column(name = "content_id")
	private int		contentId;

	@Column(name = "kitchen_shape")
	private String	kitchenShape;

	@Column(name = "kitchen_dimensions")
	private String	kitchenDimensions;

	@Column(name = "creation_timestamp")
	private int		creationTimestamp;

	@Column(name = "modification_timestamp")
	private int		modificationTimestamp;

	@Column(name = "is_active")
	private boolean	isActive;

	/**
	 * @return the kitchenId
	 */
	public int getKitchenId()
	{
		return kitchenId;
	}

	/**
	 * @return the productId
	 */
	public int getProductId()
	{
		return productId;
	}

	/**
	 * @return the contentPreviewId
	 */
	public int getContentPreviewId()
	{
		return contentPreviewId;
	}

	/**
	 * @return the contentId
	 */
	public int getContentId()
	{
		return contentId;
	}

	/**
	 * @return the kitchenShape
	 */
	public String getKitchenShape()
	{
		return kitchenShape;
	}

	/**
	 * @return the kitchenDimensions
	 */
	public String getKitchenDimensions()
	{
		return kitchenDimensions;
	}

	/**
	 * @return the creationTimestamp
	 */
	public int getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public int getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param productId
	 * @param contentPreviewId
	 * @param contentId
	 * @param kitchenShape
	 * @param kitchenDimensions
	 * @param isActive
	 */
	public KitchenInfo(int productId, int contentPreviewId, int contentId,
			String kitchenShape, String kitchenDimensions, boolean isActive)
	{
		super();
		this.productId = productId;
		this.contentPreviewId = contentPreviewId;
		this.contentId = contentId;
		this.kitchenShape = kitchenShape;
		this.kitchenDimensions = kitchenDimensions;
		this.isActive = isActive;
	}

	/**
	 *
	 */
	protected KitchenInfo()
	{
		super();
	}

}
