/**
 *
 */
package com.homearte.web.product.kitchen.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "kitchen_material_configurations")
public class KitchenMaterialConfigurations
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "config_id")
	private int		configId;

	@Column(name = "material_name")
	private String	materialName;

	@Column(name = "uname")
	private String	uname;

	@Column(name = "material_price")
	private float	materialPrice;

	@Column(name = "material_image_content_id")
	private int		materialImageContentId;

	@Column(name = "creation_timestamp")
	private int		creationTimestamp;

	@Column(name = "modification_timestamp")
	private int		modificationTimestamp;

	@Column(name = "is_active")
	private boolean	isActive;

	/**
	 * @return the configId
	 */
	public int getConfigId()
	{
		return configId;
	}

	/**
	 * @return the materialName
	 */
	public String getMaterialName()
	{
		return materialName;
	}

	/**
	 * @return the uname
	 */
	public String getUname()
	{
		return uname;
	}

	/**
	 * @return the materialPrice
	 */
	public float getMaterialPrice()
	{
		return materialPrice;
	}

	/**
	 * @return the materialImageContentId
	 */
	public int getMaterialImageContentId()
	{
		return materialImageContentId;
	}

	/**
	 * @return the creationTimestamp
	 */
	public int getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public int getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param materialName
	 * @param materialPrice
	 * @param materialImageContentId
	 * @param isActive
	 */
	public KitchenMaterialConfigurations(String materialName,
			float materialPrice, int materialImageContentId, boolean isActive,
			String uname)
	{
		super();
		this.materialName = materialName;
		this.materialPrice = materialPrice;
		this.materialImageContentId = materialImageContentId;
		this.isActive = isActive;
		this.uname = uname;
	}

	/**
	 *
	 */
	protected KitchenMaterialConfigurations()
	{
		super();
	}

}
