/**
 *
 */
package com.homearte.web.product.kichen.persistence.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.product.kitchen.persistence.entity.KitchenMaterialConfigurations;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class KitchenMaterialConfigurationsDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public KitchenMaterialConfigurationsDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public KitchenMaterialConfigurations saveOrUpdate(
			KitchenMaterialConfigurations entity) throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(KitchenMaterialConfigurations entity)
			throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int id) throws PersistanceException
	{
		dataPersistance.delete(KitchenMaterialConfigurations.class, id);
	}

	public KitchenMaterialConfigurations getById(int id)
			throws PersistanceException
	{
		return dataPersistance
				.loadById(KitchenMaterialConfigurations.class, id);
	}

}
