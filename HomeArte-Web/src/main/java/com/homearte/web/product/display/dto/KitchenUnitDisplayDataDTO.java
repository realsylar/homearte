/**
 *
 */
package com.homearte.web.product.display.dto;

import com.homearte.web.product.kitchen.persistence.entity.KitchenUnitDisplayData;

/**
 * @author Anurag Agrawal
 *
 */
public class KitchenUnitDisplayDataDTO
{

	private String	name;
	private String	dimension;
	private String	unitMaterial;
	private String	shutterMaterial;
	private String	shutterColor;

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the dimension
	 */
	public String getDimension()
	{
		return dimension;
	}

	/**
	 * @param dimension
	 *            the dimension to set
	 */
	public void setDimension(String dimension)
	{
		this.dimension = dimension;
	}

	/**
	 * @return the unitMaterial
	 */
	public String getUnitMaterial()
	{
		return unitMaterial;
	}

	/**
	 * @param unitMaterial
	 *            the unitMaterial to set
	 */
	public void setUnitMaterial(String unitMaterial)
	{
		this.unitMaterial = unitMaterial;
	}

	/**
	 * @return the shutterMaterial
	 */
	public String getShutterMaterial()
	{
		return shutterMaterial;
	}

	/**
	 * @param shutterMaterial
	 *            the shutterMaterial to set
	 */
	public void setShutterMaterial(String shutterMaterial)
	{
		this.shutterMaterial = shutterMaterial;
	}

	/**
	 * @return the shutterColor
	 */
	public String getShutterColor()
	{
		return shutterColor;
	}

	/**
	 * @param shutterColor
	 *            the shutterColor to set
	 */
	public void setShutterColor(String shutterColor)
	{
		this.shutterColor = shutterColor;
	}

	/**
	 * @param name
	 * @param dimension
	 * @param unitMaterial
	 * @param shutterMaterial
	 * @param shutterColor
	 */
	public KitchenUnitDisplayDataDTO(String name, String dimension,
			String unitMaterial, String shutterMaterial, String shutterColor)
	{
		super();
		this.name = name;
		this.dimension = dimension;
		this.unitMaterial = unitMaterial;
		this.shutterMaterial = shutterMaterial;
		this.shutterColor = shutterColor;
	}

	public KitchenUnitDisplayDataDTO(KitchenUnitDisplayData data)
	{
		super();
		this.name = data.getUnitName();
		this.dimension = data.getUnitDimension();
		this.unitMaterial = data.getUnitMaterial();
		this.shutterMaterial = data.getShutterMaterial();
		this.shutterColor = data.getShutterColor();
	}

	/**
	 *
	 */
	public KitchenUnitDisplayDataDTO()
	{
		super();
	}

}
