/**
 *
 */
package com.homearte.web.product.display.dto;

import java.util.List;

/**
 * @author Anurag Agrawal
 *
 */
public class ProductSpecsDTO
{

	private String							name;
	private String							shape;
	private String							dimension;
	private List<ConfigurationDisplayDTO>	fin;
	private List<ConfigurationDisplayDTO>	col;
	private float							unitPrice;

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the shape
	 */
	public String getShape()
	{
		return shape;
	}

	/**
	 * @param shape
	 *            the shape to set
	 */
	public void setShape(String shape)
	{
		this.shape = shape;
	}

	/**
	 * @return the dimension
	 */
	public String getDimension()
	{
		return dimension;
	}

	/**
	 * @param dimension
	 *            the dimension to set
	 */
	public void setDimension(String dimension)
	{
		this.dimension = dimension;
	}

	/**
	 * @return the fin
	 */
	public List<ConfigurationDisplayDTO> getFin()
	{
		return fin;
	}

	/**
	 * @param fin
	 *            the fin to set
	 */
	public void setFin(List<ConfigurationDisplayDTO> fin)
	{
		this.fin = fin;
	}

	/**
	 * @return the col
	 */
	public List<ConfigurationDisplayDTO> getCol()
	{
		return col;
	}

	/**
	 * @param col
	 *            the col to set
	 */
	public void setCol(List<ConfigurationDisplayDTO> col)
	{
		this.col = col;
	}

	/**
	 * @return the unitPrice
	 */
	public float getUnitPrice()
	{
		return unitPrice;
	}

	/**
	 * @param unitPrice
	 *            the unitPrice to set
	 */
	public void setUnitPrice(float unitPrice)
	{
		this.unitPrice = unitPrice;
	}

	/**
	 * @param name
	 * @param shape
	 * @param dimension
	 * @param fin
	 * @param col
	 * @param unitPrice
	 */
	public ProductSpecsDTO(String name, String shape, String dimension,
			List<ConfigurationDisplayDTO> fin,
			List<ConfigurationDisplayDTO> col, float unitPrice)
	{
		super();
		this.name = name;
		this.shape = shape;
		this.dimension = dimension;
		this.fin = fin;
		this.col = col;
		this.unitPrice = unitPrice;
	}

	/**
	 *
	 */
	public ProductSpecsDTO()
	{
		super();
	}

}
