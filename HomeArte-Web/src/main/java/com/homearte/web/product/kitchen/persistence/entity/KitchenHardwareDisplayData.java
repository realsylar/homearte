/**
 *
 */
package com.homearte.web.product.kitchen.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "kitchen_hardware_display_data")
public class KitchenHardwareDisplayData
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "dd_id")
	private int		ddId;

	@Column(name = "kitchen_id")
	private int		kitchenId;

	@Column(name = "hardware_name")
	private String	hardwareName;

	@Column(name = "hardware_brand_name")
	private String	hardwareBrandName;

	/**
	 * @return the ddId
	 */
	public int getDdId()
	{
		return ddId;
	}

	/**
	 * @return the kitchenId
	 */
	public int getKitchenId()
	{
		return kitchenId;
	}

	/**
	 * @return the hardwareName
	 */
	public String getHardwareName()
	{
		return hardwareName;
	}

	/**
	 * @return the hardwareBrandName
	 */
	public String getHardwareBrandName()
	{
		return hardwareBrandName;
	}

	/**
	 * @param kitchenId
	 * @param hardwareName
	 * @param hardwareBrandName
	 */
	public KitchenHardwareDisplayData(int kitchenId, String hardwareName,
			String hardwareBrandName)
	{
		super();
		this.kitchenId = kitchenId;
		this.hardwareName = hardwareName;
		this.hardwareBrandName = hardwareBrandName;
	}

	/**
	 *
	 */
	protected KitchenHardwareDisplayData()
	{
		super();
	}

}
