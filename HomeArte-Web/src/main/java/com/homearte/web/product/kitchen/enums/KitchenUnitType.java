/**
 *
 */
package com.homearte.web.product.kitchen.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum KitchenUnitType
{
	BASE(0), WALL(1);

	private int	value;

	/**
	 * @param value
	 */
	private KitchenUnitType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static KitchenUnitType getEnumForValue(int value)
	{
		for (KitchenUnitType type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
