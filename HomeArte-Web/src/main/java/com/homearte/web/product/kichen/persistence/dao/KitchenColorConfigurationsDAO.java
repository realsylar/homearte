/**
 *
 */
package com.homearte.web.product.kichen.persistence.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.product.kitchen.persistence.entity.KitchenColorConfigurations;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class KitchenColorConfigurationsDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public KitchenColorConfigurationsDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public KitchenColorConfigurations saveOrUpdate(
			KitchenColorConfigurations entity) throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(KitchenColorConfigurations entity)
			throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int id) throws PersistanceException
	{
		dataPersistance.delete(KitchenColorConfigurations.class, id);
	}

	public KitchenColorConfigurations getById(int id)
			throws PersistanceException
	{
		return dataPersistance.loadById(KitchenColorConfigurations.class, id);
	}

}
