/**
 *
 */
package com.homearte.web.product.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum ProductStatus
{
	ACCEPT(0), REDESIGN(1), REJECT(2), UNDEFINED(3);

	private int	value;

	/**
	 * @param value
	 */
	private ProductStatus(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static ProductStatus getEnumForValue(int value)
	{
		for (ProductStatus type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
