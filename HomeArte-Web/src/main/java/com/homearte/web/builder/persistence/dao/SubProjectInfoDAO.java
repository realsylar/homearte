/**
 *
 */
package com.homearte.web.builder.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.persistence.entity.SubProjectInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class SubProjectInfoDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public SubProjectInfoDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public SubProjectInfo saveOrUpdate(SubProjectInfo entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(SubProjectInfo entity) throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public SubProjectInfo getById(int id) throws PersistanceException
	{
		return dataPersistance.loadById(SubProjectInfo.class, id);
	}

	public List<SubProjectInfo> getSubProjectsByProjectId(int projectId)
			throws PersistanceException
			{
		String query = "from SubProjectInfo where projectId = :projectId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("projectId", new QueryParam(projectId, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryListResult(query, params);
			}

	/**
	 * @param spid
	 * @throws PersistanceException
	 */
	public void delete(int spid) throws PersistanceException
	{
		dataPersistance.delete(SubProjectInfo.class, spid);
	}
}
