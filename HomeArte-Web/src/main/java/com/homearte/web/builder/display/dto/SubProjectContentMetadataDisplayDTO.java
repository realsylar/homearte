/**
 *
 */
package com.homearte.web.builder.display.dto;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.homearte.web.content.persistence.entity.MultimediaContentMetadata;
import com.homearte.web.content.utility.ContentTypeBindings;

/**
 * @author Anurag Agrawal
 *
 */
public class SubProjectContentMetadataDisplayDTO
{

	private int			contentId;
	private String		contentName;
	private String		contentType;
	private String		creationTimestamp;
	private DateFormat	fmt	= new SimpleDateFormat("MMMM dd, yyyy");

	/**
	 * @param contentId
	 * @param contentName
	 * @param contentType
	 * @param creationTimestamp
	 */
	public SubProjectContentMetadataDisplayDTO(int contentId,
			String contentName, String contentType, Timestamp creationTimestamp)
	{
		super();
		this.contentId = contentId;
		this.contentName = contentName;
		this.contentType = contentType;
		this.creationTimestamp = fmt.format(creationTimestamp);
	}

	/**
	 *
	 */
	public SubProjectContentMetadataDisplayDTO(
			MultimediaContentMetadata cMetadata)
	{
		this.contentId = cMetadata.getMetadataId();
		this.contentName = cMetadata.getName();
		this.contentType = ContentTypeBindings
				.getDisplayContentTypeForMimeTypeValue(cMetadata.getMimeType());
		this.creationTimestamp = fmt.format(cMetadata.getCreationTimestamp());
	}

	/**
	 * @param contentId
	 *            the contentId to set
	 */
	public void setContentId(int contentId)
	{
		this.contentId = contentId;
	}

	/**
	 * @param contentName
	 *            the contentName to set
	 */
	public void setContentName(String contentName)
	{
		this.contentName = contentName;
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(String creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the contentId
	 */
	public int getContentId()
	{
		return contentId;
	}

	/**
	 * @return the contentName
	 */
	public String getContentName()
	{
		return contentName;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType()
	{
		return contentType;
	}

	/**
	 * @return the creationTimestamp
	 */
	public String getCreationTimestamp()
	{
		return creationTimestamp;
	}

}
