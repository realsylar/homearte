/**
 *
 */
package com.homearte.web.builder.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.display.dto.ProjectEditDTO;
import com.homearte.web.builder.display.dto.SubProjectListDisplayDTO;
import com.homearte.web.builder.enums.ProjectStatus;
import com.homearte.web.builder.forms.dto.CreateProjectDTO;
import com.homearte.web.builder.persistence.dao.ProjectInfoDAO;
import com.homearte.web.builder.persistence.dao.SubProjectInfoDAO;
import com.homearte.web.builder.persistence.entity.ProjectInfo;
import com.homearte.web.builder.persistence.entity.SubProjectInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ProjectPageService {

	private ProjectInfoDAO projectInfoDAO;
	private ObjectMapper mapper;
	private SubProjectInfoDAO subProjectInfoDAO;

	/**
	 * @param projectInfoDAO
	 * @param mapper
	 * @param subProjectInfoDAO
	 */
	@Autowired
	public ProjectPageService(ProjectInfoDAO projectInfoDAO, SubProjectInfoDAO subProjectInfoDAO) {
		super();
		this.projectInfoDAO = projectInfoDAO;
		this.subProjectInfoDAO = subProjectInfoDAO;
		init_mapper();
	}

	//Initializing the mapper object.
	public void init_mapper() {
		mapper = new ObjectMapper();
	}

	/**
	 * @param builderId
	 * @param createProjectDTO
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional
	public ProjectInfo createProjectForBuilder(int builderId, CreateProjectDTO createProjectRequest)
			throws PersistanceException {
		ProjectInfo projectInfo = new ProjectInfo(builderId, createProjectRequest.getProjectName(),
				createProjectRequest.getProjectDescription(), ProjectStatus.CREATED.getValue());
		projectInfo = projectInfoDAO.saveOrUpdate(projectInfo);
		return projectInfo;
	}

	@Transactional
	public int editProjectForBuilder(ProjectInfo currentProject, String data)
			throws PersistanceException, JsonParseException, JsonMappingException, IOException {
		ProjectEditDTO dto = mapper.readValue(data, ProjectEditDTO.class);
		String pName = dto.getName() != null && !dto.getName().isEmpty() ? dto.getName()
				: currentProject.getProjectName();
		String pDesc = dto.getDescription() != null && !dto.getDescription().isEmpty() ? dto.getDescription()
				: currentProject.getProjectDescription();
		currentProject.setProjectName(pName);
		currentProject.setProjectDescription(pDesc);
		projectInfoDAO.saveOrUpdate(currentProject);
		return HttpServletResponse.SC_OK;
	}

	/**
	 * @param pid
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional(readOnly = true)
	public ProjectInfo loadProjectByProjectId(int projectId) throws PersistanceException {
		return projectInfoDAO.getById(projectId);
	}

	/**
	 * @param data
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional
	public int deleteProject(String data) throws PersistanceException {
		JSONObject deleteProjectRequestPayload = new JSONObject(data);
		int projectIdToBeDeleted = deleteProjectRequestPayload.getInt("pid");
		projectInfoDAO.delete(projectIdToBeDeleted);
		return HttpServletResponse.SC_OK;
	}

	/**
	 * @param data
	 * @return
	 * @throws PersistanceException
	 * @throws JSONException
	 */
	@Transactional(readOnly = true)
	public List<SubProjectListDisplayDTO> listSubProjectsForProject(String data)
			throws JSONException, PersistanceException {
		JSONObject listSubProjectRequestPayload = new JSONObject(data);
		List<SubProjectInfo> subProjects = subProjectInfoDAO
				.getSubProjectsByProjectId(listSubProjectRequestPayload.getInt("pid"));
		return getSubProjectListDTOs(subProjects);
	}

	/**
	 * @param projects
	 * @return
	 */
	private List<SubProjectListDisplayDTO> getSubProjectListDTOs(List<SubProjectInfo> subProjects) {
		List<SubProjectListDisplayDTO> subProjectDTOList = Lists.newArrayList();
		for (SubProjectInfo subProject : subProjects) {
			subProjectDTOList.add(new SubProjectListDisplayDTO(subProject));
		}
		return subProjectDTOList;
	}

}
