/**
 *
 */
package com.homearte.web.builder.forms.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class CreateProjectDTO
{

	private String	projectName;
	private String	projectDescription;
	private String	error;
	private String	message;

	/**
	 * @param projectName
	 * @param projectDescription
	 * @param builderId
	 */
	public CreateProjectDTO(String projectName, String projectDescription)
	{
		super();
		this.projectName = projectName;
		this.projectDescription = projectDescription;
	}

	/**
	 *
	 */
	protected CreateProjectDTO()
	{
		super();
	}

	/**
	 * @return the error
	 */
	public String getError()
	{
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error)
	{
		this.error = error;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName()
	{
		return projectName;
	}

	/**
	 * @param projectName
	 *            the projectName to set
	 */
	public void setProjectName(String projectName)
	{
		this.projectName = projectName;
	}

	/**
	 * @return the projectDescription
	 */
	public String getProjectDescription()
	{
		return projectDescription;
	}

	/**
	 * @param projectDescription
	 *            the projectDescription to set
	 */
	public void setProjectDescription(String projectDescription)
	{
		this.projectDescription = projectDescription;
	}

}
