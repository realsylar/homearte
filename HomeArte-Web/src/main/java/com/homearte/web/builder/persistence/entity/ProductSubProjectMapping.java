/**
 *
 */
package com.homearte.web.builder.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.homearte.web.product.persistence.entity.ProductInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "product_sub_project_mapping")
public class ProductSubProjectMapping
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int			id;

	@Column(name = "sub_project_id")
	private int			subProjectId;

	@OneToOne(fetch = FetchType.LAZY)
	private ProductInfo	product;

	@Column(name = "status")
	private int			status;

	@Column(name = "builder_id")
	private int			builderId;

	/**
	 * @return the builderId
	 */
	public int getBuilderId()
	{
		return builderId;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the subProjectId
	 */
	public int getSubProjectId()
	{
		return subProjectId;
	}

	/**
	 * @return the product
	 */
	public ProductInfo getProduct()
	{
		return product;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/**
	 * @param subProjectId
	 * @param product
	 * @param status
	 * @param builderId
	 */
	public ProductSubProjectMapping(int subProjectId, ProductInfo product,
			int status, int builderId)
	{
		super();
		this.subProjectId = subProjectId;
		this.product = product;
		this.status = status;
		this.builderId = builderId;
	}

	/**
	 *
	 */
	protected ProductSubProjectMapping()
	{
		super();
	}

}
