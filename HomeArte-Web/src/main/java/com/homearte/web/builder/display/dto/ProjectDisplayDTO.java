/**
 *
 */
package com.homearte.web.builder.display.dto;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.homearte.web.builder.persistence.entity.ProjectInfo;

/**
 * @author Anurag Agrawal
 *
 */
public class ProjectDisplayDTO
{

	private int			projectId;
	private String		projectName;
	private String		projectDescription;
	private String		creationTimestamp;
	private int			noOfSubProjects;
	private DateFormat	fmt	= new SimpleDateFormat("MMMM dd, yyyy");

	/**
	 * @param projectId
	 * @param projectName
	 * @param projectDescription
	 * @param creationTimestamp
	 * @param noOfSubProjects
	 */
	protected ProjectDisplayDTO(int projectId, String projectName,
			String projectDescription, Timestamp creationTimestamp,
			int noOfSubProjects)
	{
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.projectDescription = projectDescription;
		this.creationTimestamp = fmt.format(creationTimestamp);
		this.noOfSubProjects = noOfSubProjects;
	}

	/**
	 *
	 */
	public ProjectDisplayDTO(ProjectInfo p)
	{
		super();
		this.projectId = p.getProjectId();
		this.projectName = p.getProjectName();
		this.projectDescription = p.getProjectDescription();
		this.creationTimestamp = fmt.format(p.getCreationTimestamp());
		this.noOfSubProjects = p.getNoOfSubproject();
	}

	/**
	 * @return the creationTimestamp
	 */
	public String getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(String creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the noOfSubProjects
	 */
	public int getNoOfSubProjects()
	{
		return noOfSubProjects;
	}

	/**
	 * @param noOfSubProjects
	 *            the noOfSubProjects to set
	 */
	public void setNoOfSubProjects(int noOfSubProjects)
	{
		this.noOfSubProjects = noOfSubProjects;
	}

	/**
	 * @return the projectId
	 */
	public int getProjectId()
	{
		return projectId;
	}

	/**
	 * @param projectId
	 *            the projectId to set
	 */
	public void setProjectId(int projectId)
	{
		this.projectId = projectId;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName()
	{
		return projectName;
	}

	/**
	 * @param projectName
	 *            the projectName to set
	 */
	public void setProjectName(String projectName)
	{
		this.projectName = projectName;
	}

	/**
	 * @return the projectDescription
	 */
	public String getProjectDescription()
	{
		return projectDescription;
	}

	/**
	 * @param projectDescription
	 *            the projectDescription to set
	 */
	public void setProjectDescription(String projectDescription)
	{
		this.projectDescription = projectDescription;
	}

}
