/**
 *
 */
package com.homearte.web.builder.controllers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.homearte.commons.aws.entity.S3MultipartFileParams;
import com.homearte.commons.core.cache.manager.HomearteMemCacheManager;
import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.commons.core.manager.S3BucketManager;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.display.dto.DashboardDTO;
import com.homearte.web.builder.persistence.dao.SubProjectInfoDAO;
import com.homearte.web.builder.service.SubProjectProductPageService;
import com.homearte.web.cache.constants.HomearteCacheConstants;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.content.enums.ContentType;
import com.homearte.web.content.enums.ContentTypeToActualMimeType;
import com.homearte.web.content.persistence.entity.MultimediaContentMetadata;
import com.homearte.web.content.persistence.service.MultimediaContentMetadataService;
import com.homearte.web.product.controllers.exception.ProductNotFoundException;
import com.homearte.web.product.display.dto.ConfigurationDisplayDTO;
import com.homearte.web.product.display.dto.KitchenHardwareDisplayDataDTO;
import com.homearte.web.product.display.dto.KitchenUnitDisplayDataDTO;
import com.homearte.web.product.display.dto.ProductPageLoadDTO;
import com.homearte.web.product.display.dto.ProductSpecsDTO;
import com.homearte.web.product.enums.ProductStatus;
import com.homearte.web.product.kichen.persistence.dao.KitchenColorConfigurationsDAO;
import com.homearte.web.product.kichen.persistence.dao.KitchenConfigurationsDAO;
import com.homearte.web.product.kichen.persistence.dao.KitchenHardwareDisplayDataDAO;
import com.homearte.web.product.kichen.persistence.dao.KitchenInfoDAO;
import com.homearte.web.product.kichen.persistence.dao.KitchenMaterialConfigurationsDAO;
import com.homearte.web.product.kichen.persistence.dao.KitchenUnitDisplayDataDAO;
import com.homearte.web.product.kitchen.enums.KitchenConfigurationType;
import com.homearte.web.product.kitchen.enums.KitchenUnitType;
import com.homearte.web.product.kitchen.persistence.entity.KitchenColorConfigurations;
import com.homearte.web.product.kitchen.persistence.entity.KitchenConfigurations;
import com.homearte.web.product.kitchen.persistence.entity.KitchenHardwareDisplayData;
import com.homearte.web.product.kitchen.persistence.entity.KitchenInfo;
import com.homearte.web.product.kitchen.persistence.entity.KitchenMaterialConfigurations;
import com.homearte.web.product.kitchen.persistence.entity.KitchenUnitDisplayData;
import com.homearte.web.product.persistence.dao.ProductInfoDAO;
import com.homearte.web.product.persistence.entity.ProductInfo;
import com.homearte.web.session.WebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "/products/{hpid}")
public class SubProjectProductPageController
{

	private static final Logger					logger	= Logger.getLogger(SubProjectPageController.class);
	private WebSessionManager					webSessionManager;
	private HomearteMemCacheManager				memCache;
	private SubProjectProductPageService		productPageService;
	private SubProjectInfoDAO					subProjectInfoDAO;
	private ProductInfoDAO						productInfoDAO;
	private KitchenConfigurationsDAO			kitchenConfigurationsDAO;
	private KitchenInfoDAO						kitchenInfoDAO;
	private KitchenColorConfigurationsDAO		kitchenColorConfigurationsDAO;
	private KitchenMaterialConfigurationsDAO	kitchenMaterialConfigurationsDAO;
	private S3BucketManager						s3BucketManager;
	private KitchenUnitDisplayDataDAO			kitchenUnitDisplayDataDAO;
	private KitchenHardwareDisplayDataDAO		kitchenHardwareDisplayDataDAO;
	private MultimediaContentMetadataService	multimediaContentMetadataService;

	/**
	 * @param webSessionManager
	 * @param memCache
	 * @param productPageService
	 * @param subProjectInfoDAO
	 * @param productInfoDAO
	 * @param kitchenConfigurationsDAO
	 * @param kitchenInfoDAO
	 * @param kitchenColorConfigurationsDAO
	 * @param kitchenMaterialConfigurationsDAO
	 * @param s3BucketManager
	 * @param kitchenUnitDisplayDataDAO
	 * @param kitchenHardwareDisplayDataDAO
	 * @param multimediaContentMetadataService
	 */
	@Autowired
	public SubProjectProductPageController(WebSessionManager webSessionManager,
			HomearteMemCacheManager memCache,
			SubProjectProductPageService productPageService,
			SubProjectInfoDAO subProjectInfoDAO, ProductInfoDAO productInfoDAO,
			KitchenConfigurationsDAO kitchenConfigurationsDAO,
			KitchenInfoDAO kitchenInfoDAO,
			KitchenColorConfigurationsDAO kitchenColorConfigurationsDAO,
			KitchenMaterialConfigurationsDAO kitchenMaterialConfigurationsDAO,
			S3BucketManager s3BucketManager,
			KitchenUnitDisplayDataDAO kitchenUnitDisplayDataDAO,
			KitchenHardwareDisplayDataDAO kitchenHardwareDisplayDataDAO,
			MultimediaContentMetadataService multimediaContentMetadataService)
	{
		super();
		this.webSessionManager = webSessionManager;
		this.memCache = memCache;
		this.productPageService = productPageService;
		this.subProjectInfoDAO = subProjectInfoDAO;
		this.productInfoDAO = productInfoDAO;
		this.kitchenConfigurationsDAO = kitchenConfigurationsDAO;
		this.kitchenInfoDAO = kitchenInfoDAO;
		this.kitchenColorConfigurationsDAO = kitchenColorConfigurationsDAO;
		this.kitchenMaterialConfigurationsDAO = kitchenMaterialConfigurationsDAO;
		this.s3BucketManager = s3BucketManager;
		this.kitchenUnitDisplayDataDAO = kitchenUnitDisplayDataDAO;
		this.kitchenHardwareDisplayDataDAO = kitchenHardwareDisplayDataDAO;
		this.multimediaContentMetadataService = multimediaContentMetadataService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView loadProductPage(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int hpid)
					throws ProductNotFoundException
	{
		ModelAndView view = new ModelAndView("product");
		String error;
		DashboardDTO ddto = null;
		ProductPageLoadDTO hpdto = null;
		try
		{
			int userId = webSessionManager.getUserIdFromSession(request
					.getSession());
			ddto = (DashboardDTO) memCache
					.getItemWithKey(HomearteCacheConstants.USER_PROJECT_DISPLAY_DATA
							+ userId);
			int bid = (Integer) memCache
					.getItemWithKey(HomearteCacheConstants.BUILDER_ID_FOR_USER
							+ userId);
			ProductInfo productInfo = productInfoDAO.getById(hpid);
			/*
			 * ProductSubProjectMapping mapping = productSubProjectMappingDAO
			 * .getByProductId(hpid);
			 */
			/*
			 * SubProjectInfo subProjectInfo = subProjectInfoDAO.getById(mapping
			 * .getSubProjectId());
			 */
			hpdto = new ProductPageLoadDTO(0, 0, null, hpid);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			ddto.setError(error);
		}
		view.addObject("ddto", ddto);
		view.addObject("hpdto", hpdto);
		return view;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cspecs")
	@ResponseBody
	public String loadProductConfigurations(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int hpid)
					throws ProductNotFoundException
	{
		JSONObject responseJson = new JSONObject();
		int status;
		String error = null;
		ProductSpecsDTO cspecs = null;
		String cspecsValue = null;
		try
		{
			ProductInfo productInfo = productInfoDAO.getById(hpid);
			KitchenInfo kitchenInfo = kitchenInfoDAO.getByProductId(hpid);
			List<KitchenConfigurations> kConfigurations = kitchenConfigurationsDAO
					.getByKitchenId(kitchenInfo.getKitchenId());
			cspecs = new ProductSpecsDTO();
			cspecs.setDimension(kitchenInfo.getKitchenDimensions());
			cspecs.setName(productInfo.getDisplayName());
			cspecs.setShape(kitchenInfo.getKitchenShape());
			cspecs.setUnitPrice(productInfo.getBasePrice());
			cspecs = decorateConfigurations(kConfigurations, cspecs);
			ObjectMapper mapper = new ObjectMapper();
			cspecsValue = mapper.writeValueAsString(cspecs);
			status = HttpServletResponse.SC_OK;
		} catch (PersistanceException e)
		{
			e.printStackTrace();
			logger.error("Exception thrown", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
			logger.error("Exception thrown", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		responseJson.put("status", status);
		responseJson.put("error", error);
		responseJson.put("cspecs", cspecsValue);
		return responseJson.toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/hpstatus")
	@ResponseBody
	public String getProductStatus(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int hpid)
					throws ProductNotFoundException
	{
		JSONObject responseJson = new JSONObject();
		int status;
		int hpstatus = ProductStatus.UNDEFINED.getValue();
		String error = null;
		try
		{
			ProductInfo productInfo = productInfoDAO.getById(hpid);
			// hpstatus = mapping.getStatus();
			status = HttpServletResponse.SC_OK;
		} catch (PersistanceException e)
		{
			e.printStackTrace();
			logger.error("Exception thrown", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		responseJson.put("status", status);
		responseJson.put("error", error);
		responseJson.put("hpstatus", hpstatus);
		return responseJson.toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/hpupdatestatus")
	@ResponseBody
	public String getProductStatus(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int hpid,
			@RequestBody String payloadData) throws ProductNotFoundException
	{
		JSONObject payloadDataObj = new JSONObject(payloadData);
		JSONObject responseJson = new JSONObject();
		int status;
		int hpstatus = payloadDataObj.getInt("currentStatus");
		int newhpstatus = payloadDataObj.getInt("requestedStatus");
		String error = null;
		try
		{
			ProductInfo productInfo = productInfoDAO.getById(hpid);
			// mapping.setStatus(newhpstatus);
			// productSubProjectMappingDAO.saveOrUpdate(mapping);
			status = HttpServletResponse.SC_OK;
		} catch (PersistanceException e)
		{
			e.printStackTrace();
			logger.error("Exception thrown", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		responseJson.put("status", status);
		responseJson.put("error", error);
		return responseJson.toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/hpunits")
	@ResponseBody
	public String getProductUnitDetails(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int hpid)
					throws ProductNotFoundException
	{
		JSONObject responseJson = new JSONObject();
		int status;
		String error = null;
		try
		{
			ProductInfo productInfo = productInfoDAO.getById(hpid);
			KitchenInfo kitchen = kitchenInfoDAO.getByProductId(hpid);
			List<KitchenUnitDisplayData> unitDisplayData = kitchenUnitDisplayDataDAO
					.getByKitchenId(kitchen.getKitchenId());
			if (unitDisplayData != null)
				responseJson = getUpdatedResponsePayloadForKitchenUnits(
						responseJson, unitDisplayData);
			status = HttpServletResponse.SC_OK;
		} catch (PersistanceException e)
		{
			e.printStackTrace();
			logger.error("Exception thrown", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		responseJson.put("status", status);
		responseJson.put("error", error);
		return responseJson.toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/hphardware")
	@ResponseBody
	public String getProductHardwareDetails(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int hpid)
					throws ProductNotFoundException
	{
		JSONObject responseJson = new JSONObject();
		int status;
		String error = null;
		try
		{
			ProductInfo productInfo = productInfoDAO.getById(hpid);
			KitchenInfo kitchen = kitchenInfoDAO.getByProductId(hpid);
			List<KitchenHardwareDisplayData> hardwareDisplayData = kitchenHardwareDisplayDataDAO
					.getByKitchenId(kitchen.getKitchenId());
			if (hardwareDisplayData != null)
				responseJson = getUpdatedResponsePayloadForKitchenHardware(
						responseJson, hardwareDisplayData);
			status = HttpServletResponse.SC_OK;
		} catch (PersistanceException e)
		{
			e.printStackTrace();
			logger.error("Exception thrown", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		responseJson.put("status", status);
		responseJson.put("error", error);
		return responseJson.toString();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/hpiget")
	public void loadImageData(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int hpid,
			@RequestParam String fin, @RequestParam String col,
			@RequestParam boolean thumb)
	{
		BufferedInputStream input = null;
		BufferedOutputStream output = null;
		S3MultipartFileParams params = null;
		try
		{
			KitchenInfo kInfo = kitchenInfoDAO.getByProductId(hpid);
			int contentId = kInfo.getContentId();
			if (thumb)
			{
				contentId = kInfo.getContentPreviewId();
			}
			MultimediaContentMetadata cMetaData = multimediaContentMetadataService
					.readContentMetadata(contentId);
			StringBuffer contentPath = new StringBuffer();
			contentPath.append(cMetaData.getStorageKeyName() + "/");
			if (!thumb)
			{

				if (col != null && !col.isEmpty())
				{
					contentPath.append(col + "_");
				}
				if (fin != null && !col.isEmpty())
				{
					contentPath.append(fin + "_");
				}
			}
			contentPath.append(cMetaData.getName());
			params = new S3MultipartFileParams(null, contentPath.toString(), 0);
			params = s3BucketManager.recieveFile(params);
			response.setContentType(ContentTypeToActualMimeType
					.getValueForEnumName(ContentType.getEnumForValue(0).name()));
			response.setHeader("Content-Length",
					String.valueOf(params.getFileSize()));
			response.setHeader("Content-Disposition", "inline; filename=\""
					+ cMetaData.getName() + "\"");
			input = new BufferedInputStream(params.getFileData()
					.getObjectContent());
			output = new BufferedOutputStream(response.getOutputStream());
			byte[] buffer = new byte[8192];
			for (int length = 0; (length = input.read(buffer)) > 0;)
			{
				output.write(buffer, 0, length);
			}
		} catch (PersistanceException e)
		{
			e.printStackTrace();
			logger.error("Exception caught while dishing out content");
			return;
		} catch (IOException e)
		{
			e.printStackTrace();
			logger.error("Exception caught while dishing out content");
			return;
		} finally
		{
			if (output != null)
				try
			{
					output.close();
			} catch (IOException logOrIgnore)
			{
				logger.error("Exception caught while closing output stream.");
			}
			if (input != null)
				try
			{
					input.close();
			} catch (IOException logOrIgnore)
			{
				logger.error("Exception caught while closing input stream.");
			}
			if (params != null && params.getFileData() != null)
				try
			{
					params.getFileData().close();
			} catch (IOException logOrIgnore)
			{
				logger.error("Exception caught while closing data stream.");
			}
		}
	}

	/**
	 * @param responseJson
	 * @param hardwareDisplayData
	 * @return
	 */
	private JSONObject getUpdatedResponsePayloadForKitchenHardware(
			JSONObject responseJson,
			List<KitchenHardwareDisplayData> hardwareDisplayData)
	{
		List<KitchenHardwareDisplayDataDTO> hardware = Lists.newArrayList();
		for (KitchenHardwareDisplayData dispData : hardwareDisplayData)
		{
			KitchenHardwareDisplayDataDTO dto = new KitchenHardwareDisplayDataDTO(
					dispData);
			hardware.add(dto);
		}
		responseJson.put("hardwareList", hardware);
		return responseJson;
	}

	/**
	 * @param responseJson
	 * @param unitDisplayData
	 * @return
	 */
	private JSONObject getUpdatedResponsePayloadForKitchenUnits(
			JSONObject responseJson,
			List<KitchenUnitDisplayData> unitDisplayData)
	{
		List<KitchenUnitDisplayDataDTO> baseUnits = Lists.newArrayList();
		List<KitchenUnitDisplayDataDTO> wallUnits = Lists.newArrayList();
		for (KitchenUnitDisplayData dispData : unitDisplayData)
		{
			KitchenUnitDisplayDataDTO dto = new KitchenUnitDisplayDataDTO(
					dispData);
			if (dispData.getUnitType() == KitchenUnitType.BASE.getValue())
				baseUnits.add(dto);
			else if (dispData.getUnitType() == KitchenUnitType.WALL.getValue())
				wallUnits.add(dto);
		}
		responseJson.put("baseUnits", baseUnits);
		responseJson.put("wallUnits", wallUnits);
		return responseJson;
	}

	/**
	 * @param kConfigurations
	 * @param cspecs
	 * @return
	 * @throws PersistanceException
	 */
	private ProductSpecsDTO decorateConfigurations(
			List<KitchenConfigurations> kConfigurations, ProductSpecsDTO cspecs)
					throws PersistanceException
	{
		List<ConfigurationDisplayDTO> colList = Lists.newArrayList();
		List<ConfigurationDisplayDTO> matList = Lists.newArrayList();
		for (KitchenConfigurations config : kConfigurations)
		{

			if (config.getConfigType() == KitchenConfigurationType.COLOR
					.getValue())
			{
				KitchenColorConfigurations colorConfig = kitchenColorConfigurationsDAO
						.getById(config.getConfigId());
				ConfigurationDisplayDTO dto = new ConfigurationDisplayDTO(
						colorConfig.getUname(), colorConfig.getColorName(),
						config.getConfigurationPriceMultiplier()
						* colorConfig.getColorPrice(),
						config.isDefault());
				colList.add(dto);
			}
			if (config.getConfigType() == KitchenConfigurationType.MATERIAL
					.getValue())
			{
				KitchenMaterialConfigurations matConfig = kitchenMaterialConfigurationsDAO
						.getById(config.getConfigId());
				ConfigurationDisplayDTO dto = new ConfigurationDisplayDTO(
						matConfig.getUname(), matConfig.getMaterialName(),
						config.getConfigurationPriceMultiplier()
						* matConfig.getMaterialPrice(),
						config.isDefault());
				matList.add(dto);
			}
		}
		cspecs.setCol(colList);
		cspecs.setFin(matList);
		return cspecs;
	}

}
