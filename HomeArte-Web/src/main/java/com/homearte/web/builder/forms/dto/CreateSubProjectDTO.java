/**
 *
 */
package com.homearte.web.builder.forms.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class CreateSubProjectDTO
{

	private String	subProjectName;
	private String	subProjectDescription;
	private String	error;
	private String	message;

	/**
	 *
	 */
	protected CreateSubProjectDTO()
	{
		super();
	}

	/**
	 * @param subProjectName
	 * @param subProjectDescription
	 */
	public CreateSubProjectDTO(String subProjectName,
			String subProjectDescription)
	{
		super();
		this.subProjectName = subProjectName;
		this.subProjectDescription = subProjectDescription;
	}

	/**
	 * @return the subProjectName
	 */
	public String getSubProjectName()
	{
		return subProjectName;
	}

	/**
	 * @param subProjectName
	 *            the subProjectName to set
	 */
	public void setSubProjectName(String subProjectName)
	{
		this.subProjectName = subProjectName;
	}

	/**
	 * @return the subProjectDescription
	 */
	public String getSubProjectDescription()
	{
		return subProjectDescription;
	}

	/**
	 * @param subProjectDescription
	 *            the subProjectDescription to set
	 */
	public void setSubProjectDescription(String subProjectDescription)
	{
		this.subProjectDescription = subProjectDescription;
	}

	/**
	 * @return the error
	 */
	public String getError()
	{
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error)
	{
		this.error = error;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

}
