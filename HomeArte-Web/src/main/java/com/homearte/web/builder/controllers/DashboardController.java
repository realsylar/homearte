/**
 *
 */
package com.homearte.web.builder.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.homearte.commons.core.cache.manager.HomearteMemCacheManager;
import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.web.builder.display.dto.DashboardDTO;
import com.homearte.web.builder.display.dto.ProjectDisplayDTO;
import com.homearte.web.builder.service.DashboardService;
import com.homearte.web.cache.constants.HomearteCacheConstants;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.common.dto.AjaxResponse;
import com.homearte.web.session.WebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "dashboard")
public class DashboardController
{

	private static final Logger		logger	= Logger.getLogger(DashboardController.class);
	private WebSessionManager		webSessionManager;
	private DashboardService		dashboardService;
	private HomearteMemCacheManager	memCache;												;

	/**
	 * @param webSessionManager
	 * @param memCache
	 * @param dashboardService
	 */
	@Autowired
	public DashboardController(WebSessionManager webSessionManager,
			DashboardService dashboardService, HomearteMemCacheManager memCache)
	{
		super();
		this.webSessionManager = webSessionManager;
		this.dashboardService = dashboardService;
		this.memCache = memCache;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView loadDashboardPageForBuilder(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("dashboard");
		DashboardDTO dto = new DashboardDTO();
		try
		{
			int userId = webSessionManager.getUserIdFromSession(request
					.getSession());
			dto = (DashboardDTO) memCache
					.getItemWithKey(HomearteCacheConstants.USER_DASHBOARD_DISPLAY_DATA
							+ String.valueOf(userId));
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			dto.setError(SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY));
		}
		model.addObject("ddto", dto);
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/plist")
	@ResponseBody
	public String getBuilderProjects(HttpServletRequest request,
			HttpServletResponse response, @RequestBody String data)
	{
		int status;
		String error = null;
		List<ProjectDisplayDTO> projects = null;
		try
		{
			projects = dashboardService.getProjectsForBuilder(data);
			status = HttpServletResponse.SC_OK;
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.PROJECT_LIST_NOT_LOADED);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		AjaxResponse projectsData = new AjaxResponse(error, null, status);
		projectsData.addToValues("plist", projects);
		return new JSONObject().put("ajaxResponse", projectsData).toString();
	}

}
