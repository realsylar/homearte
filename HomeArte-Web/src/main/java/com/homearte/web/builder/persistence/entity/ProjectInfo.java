/**
 *
 */
package com.homearte.web.builder.persistence.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "project_info")
public class ProjectInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "project_id")
	private int			projectId;

	@Column(name = "builder_id")
	private int			builderId;

	@Column(name = "project_name")
	private String		projectName;

	@Column(name = "project_description")
	private String		projectDescription;

	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp	modificationTimestamp;

	@Column(name = "status")
	private int			status;

	@Column(name = "no_of_subproject")
	private int			noOfSubproject;

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/**
	 * @param projectName
	 *            the projectName to set
	 */
	public void setProjectName(String projectName)
	{
		this.projectName = projectName;
	}

	/**
	 * @param projectDescription
	 *            the projectDescription to set
	 */
	public void setProjectDescription(String projectDescription)
	{
		this.projectDescription = projectDescription;
	}

	/**
	 * @param noOfSubproject
	 *            the noOfSubproject to set
	 */
	public void setNoOfSubproject(int noOfSubproject)
	{
		this.noOfSubproject = noOfSubproject;
	}

	/**
	 * @return the projectId
	 */
	public int getProjectId()
	{
		return projectId;
	}

	/**
	 * @return the builderId
	 */
	public int getBuilderId()
	{
		return builderId;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName()
	{
		return projectName;
	}

	/**
	 * @return the projectDescription
	 */
	public String getProjectDescription()
	{
		return projectDescription;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param builderId
	 * @param projectName
	 * @param projectDescription
	 * @param status
	 */
	public ProjectInfo(int builderId, String projectName,
			String projectDescription, int status)
	{
		super();
		this.builderId = builderId;
		this.projectName = projectName;
		this.projectDescription = projectDescription;
		this.status = status;
	}

	/**
	 * @return the noOfSubproject
	 */
	public int getNoOfSubproject()
	{
		return noOfSubproject;
	}

	/**
	 *
	 */
	protected ProjectInfo()
	{
		super();
	}

}
