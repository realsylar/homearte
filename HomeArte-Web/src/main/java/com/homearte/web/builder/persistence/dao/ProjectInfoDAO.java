/**
 *
 */
package com.homearte.web.builder.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.QueryProjection;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.enums.QueryProjectionTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.persistence.entity.ProjectInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class ProjectInfoDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public ProjectInfoDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public ProjectInfo saveOrUpdate(ProjectInfo entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(ProjectInfo entity) throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int projectId) throws PersistanceException
	{
		dataPersistance.delete(ProjectInfo.class, projectId);
	}

	public ProjectInfo getById(int id) throws PersistanceException
	{
		return dataPersistance.loadById(ProjectInfo.class, id);
	}

	public List<ProjectInfo> getProjectsByBuilderId(int builderId,
			String sQuery, int skip, int limit) throws PersistanceException
			{
		String query = "from ProjectInfo where builderId = :builderId and projectName LIKE CONCAT('%', :sQuery, '%')";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("builderId", new QueryParam(builderId, QueryParamTypes.INT));
		params.put("sQuery", new QueryParam(sQuery, QueryParamTypes.STRING));
		List<QueryProjection> projections = Lists.newArrayList();
		projections.add(new QueryProjection(skip,
				QueryProjectionTypes.START_LIMIT));
		projections.add(new QueryProjection(limit,
				QueryProjectionTypes.END_LIMIT));
		return dataPersistance.executeHQLQueryListResult(query, params,
				projections);
			}
}
