/**
 *
 */
package com.homearte.web.builder.display.dto;

/**
 * @author Anurag Agrawal
 *
 */
public class ProjectEditDTO
{

	private String	name;
	private String	description;

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param name
	 * @param description
	 */
	public ProjectEditDTO(String name, String description)
	{
		super();
		this.name = name;
		this.description = description;
	}

	/**
	 *
	 */
	public ProjectEditDTO()
	{
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "ProjectEditDTO [name=" + name + ", description=" + description
				+ "]";
	}

}
