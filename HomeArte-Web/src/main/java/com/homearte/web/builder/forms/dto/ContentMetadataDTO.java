/**
 *
 */
package com.homearte.web.builder.forms.dto;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Anurag Agrawal
 *
 */
public class ContentMetadataDTO
{

	private MultipartFile	file;

	/**
	 * @return the file
	 */
	public MultipartFile getFile()
	{
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(MultipartFile file)
	{
		this.file = file;
	}

	/**
	 * @param file
	 */
	public ContentMetadataDTO(MultipartFile file)
	{
		super();
		this.file = file;
	}

	/**
	 *
	 */
	public ContentMetadataDTO()
	{
		super();
	}

}
