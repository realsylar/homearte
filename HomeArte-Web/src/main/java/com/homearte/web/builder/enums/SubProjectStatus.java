/**
 *
 */
package com.homearte.web.builder.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum SubProjectStatus
{
	CREATED(1), STARTED(1), ONGOING(2), COMPLETED(3), CLOSED(4);

	private int	value;

	/**
	 * @param value
	 */
	private SubProjectStatus(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static SubProjectStatus getEnumForValue(int value)
	{
		for (SubProjectStatus type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
