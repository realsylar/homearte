/**
 *
 */
package com.homearte.web.builder.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum ProjectStatus
{
	CREATED(1), STARTED(1), ONGOING(2), COMPLETED(3), CLOSED(4);

	private int	value;

	/**
	 * @param value
	 */
	private ProjectStatus(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static ProjectStatus getEnumForValue(int value)
	{
		for (ProjectStatus type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
