/**
 *
 */
package com.homearte.web.builder.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.homearte.commons.core.cache.manager.HomearteMemCacheManager;
import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.web.builder.display.dto.DashboardDTO;
import com.homearte.web.builder.display.dto.ProjectDisplayDTO;
import com.homearte.web.builder.display.dto.SubProjectListDisplayDTO;
import com.homearte.web.builder.forms.dto.CreateProjectDTO;
import com.homearte.web.builder.persistence.entity.ProjectInfo;
import com.homearte.web.builder.service.ProjectPageService;
import com.homearte.web.cache.constants.HomearteCacheConstants;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.common.dto.AjaxResponse;
import com.homearte.web.session.WebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping("/dashboard/project")
public class ProjectPageController
{

	private static final Logger		logger	= Logger.getLogger(ProjectPageController.class);
	private WebSessionManager		webSessionManager;
	private HomearteMemCacheManager	memCacheManager;
	private ProjectPageService		projectPageService;

	/**
	 * @param webSessionManager
	 * @param projectPageService
	 * @param memCacheManager
	 */
	@Autowired
	public ProjectPageController(WebSessionManager webSessionManager,
			HomearteMemCacheManager memCacheManager,
			ProjectPageService projectPageService)
	{
		super();
		this.webSessionManager = webSessionManager;
		this.memCacheManager = memCacheManager;
		this.projectPageService = projectPageService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/sub")
	public ModelAndView submitBuilderCreateProjectForm(
			HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("cpdto") CreateProjectDTO createProjectRequest)
	{
		ModelAndView model = null;
		int loggedInUser = webSessionManager.getUserIdFromSession(request
				.getSession());
		String error = null;
		DashboardDTO ddto = new DashboardDTO();
		try
		{
			ddto = (DashboardDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_DASHBOARD_DISPLAY_DATA
							+ String.valueOf(loggedInUser));
			int builderId = (Integer) memCacheManager
					.getItemWithKey(HomearteCacheConstants.BUILDER_ID_FOR_USER
							+ loggedInUser);
			ProjectInfo createdProject = projectPageService
					.createProjectForBuilder(builderId, createProjectRequest);
			model = new ModelAndView("redirect:/dashboard/project/"
					+ createdProject.getProjectId());
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			CreateProjectDTO dto = new CreateProjectDTO(null, null);
			dto.setError(error);
			model.addObject("cpdto", dto);
			model.addObject("ddto", ddto);
		}
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{pid}/edit/sub")
	@ResponseBody
	public String editBuilderProject(HttpServletRequest request,
			HttpServletResponse response, @RequestBody String data,
			@PathVariable int pid)
	{
		int status = -1;
		String error = null;
		try
		{
			ProjectInfo currentProject = projectPageService
					.loadProjectByProjectId(pid);
			if (currentProject != null)
			{
				status = projectPageService.editProjectForBuilder(
						currentProject, data);
			}
			else
			{
				status = HttpServletResponse.SC_BAD_REQUEST;
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		}
		AjaxResponse editProjectResponse = new AjaxResponse(error, null, status);
		return new JSONObject().put("ajaxResponse", editProjectResponse)
				.toString();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/create")
	public ModelAndView createBuilderProjectFormLoad(
			HttpServletRequest request, HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("create-project");
		int userId = webSessionManager.getUserIdFromSession(request
				.getSession());
		String error = null;
		DashboardDTO ddto = null;
		try
		{
			ddto = (DashboardDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_DASHBOARD_DISPLAY_DATA
							+ String.valueOf(userId));
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		}
		CreateProjectDTO dto = new CreateProjectDTO(null, null);
		dto.setError(error);
		model.addObject("cpdto", dto);
		model.addObject("ddto", ddto);
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete")
	@ResponseBody
	public String deleteBuilderProject(HttpServletRequest request,
			HttpServletResponse response, @RequestBody String data)
	{
		int status = -1;
		String error = null;
		try
		{
			status = projectPageService.deleteProject(data);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		}
		AjaxResponse deleteProjectResponse = new AjaxResponse(error, null,
				status);
		return new JSONObject().put("ajaxResponse", deleteProjectResponse)
				.toString();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{pid}")
	public ModelAndView loadProjectPageForBuilder(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int pid)
	{
		ModelAndView model = new ModelAndView("project");
		DashboardDTO dasboardDisplaydto = new DashboardDTO();
		try
		{
			int loggedInUser = webSessionManager.getUserIdFromSession(request
					.getSession());
			dasboardDisplaydto = (DashboardDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_DASHBOARD_DISPLAY_DATA
							+ String.valueOf(loggedInUser));
			ProjectDisplayDTO projectDisplaydto = (ProjectDisplayDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_PROJECT_DISPLAY_DATA
							+ pid);
			if (projectDisplaydto == null)
			{
				model = new ModelAndView("forward:/dashboard/project/invalid");
			}
			model.addObject("pdto", projectDisplaydto);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			dasboardDisplaydto
			.setError(SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY));
		}
		model.addObject("ddto", dasboardDisplaydto);
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/splist")
	@ResponseBody
	public String getSubprojectsForProject(HttpServletRequest request,
			HttpServletResponse response, @RequestBody String data)
	{
		int status;
		String error = null;
		List<SubProjectListDisplayDTO> subProjectsList = null;
		try
		{
			subProjectsList = projectPageService
					.listSubProjectsForProject(data);
			status = HttpServletResponse.SC_OK;
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.PROJECT_LIST_NOT_LOADED);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		AjaxResponse listSubProjectsResponse = new AjaxResponse(error, null,
				status);
		listSubProjectsResponse.addToValues("splist", subProjectsList);
		return new JSONObject().put("ajaxResponse", listSubProjectsResponse)
				.toString();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/invalid")
	public ModelAndView loadInavlidProjectPage(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("project-not-found");
		return model;
	}

}
