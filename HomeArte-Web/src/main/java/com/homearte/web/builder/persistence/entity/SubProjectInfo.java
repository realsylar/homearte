/**
 *
 */
package com.homearte.web.builder.persistence.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.homearte.web.content.persistence.entity.MultimediaContentMetadata;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "sub_project_info")
public class SubProjectInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "sub_project_id")
	private int								subProjectId;

	@Column(name = "project_id")
	private int								projectId;

	@Column(name = "sub_project_name")
	private String							subProjectName;

	@Column(name = "sub_project_description")
	private String							subProjectDescription;

	@Column(name = "creation_timestamp")
	private Timestamp						creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp						modificationTimestamp;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "floor_plan_content_metadata_id")
	private MultimediaContentMetadata		floorPlan;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "sub_project_supplementary_file_content_metadata_mapping", joinColumns = {@JoinColumn(name = "sub_project_id")}, inverseJoinColumns = {@JoinColumn(name = "content_metadata_id")})
	private List<MultimediaContentMetadata>	supplementaryFiles;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "product_sub_project_mapping", joinColumns = {@JoinColumn(name = "sub_project_id")})
	private List<ProductSubProjectMapping>	productMapping;

	@Column(name = "status")
	private int								status;

	/**
	 * @param projectId
	 * @param subProjectName
	 * @param subProjectDescription
	 * @param status
	 */
	public SubProjectInfo(int projectId, String subProjectName,
			String subProjectDescription, int status)
	{
		super();
		this.projectId = projectId;
		this.subProjectName = subProjectName;
		this.subProjectDescription = subProjectDescription;
		this.status = status;
	}

	/**
	 *
	 */
	protected SubProjectInfo()
	{
		super();
	}

	/**
	 * @return the subProjectId
	 */
	public int getSubProjectId()
	{
		return subProjectId;
	}

	/**
	 * @return the projectId
	 */
	public int getProjectId()
	{
		return projectId;
	}

	/**
	 * @return the subProjectName
	 */
	public String getSubProjectName()
	{
		return subProjectName;
	}

	/**
	 * @return the subProjectDescription
	 */
	public String getSubProjectDescription()
	{
		return subProjectDescription;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param subProjectName
	 *            the subProjectName to set
	 */
	public void setSubProjectName(String subProjectName)
	{
		this.subProjectName = subProjectName;
	}

	/**
	 * @param subProjectDescription
	 *            the subProjectDescription to set
	 */
	public void setSubProjectDescription(String subProjectDescription)
	{
		this.subProjectDescription = subProjectDescription;
	}

	/**
	 * @return the floorPlan
	 */
	public MultimediaContentMetadata getFloorPlan()
	{
		return floorPlan;
	}

	/**
	 * @param floorPlan
	 *            the floorPlan to set
	 */
	public void setFloorPlan(MultimediaContentMetadata floorPlan)
	{
		this.floorPlan = floorPlan;
	}

	/**
	 * @return the supplementaryFiles
	 */
	public List<MultimediaContentMetadata> getSupplementaryFiles()
	{
		return supplementaryFiles;
	}

	/**
	 * @param supplementaryFiles
	 *            the supplementaryFiles to set
	 */
	public void setSupplementaryFiles(
			List<MultimediaContentMetadata> supplementaryFiles)
	{
		this.supplementaryFiles = supplementaryFiles;
	}

	/**
	 * @return the productMapping
	 */
	public List<ProductSubProjectMapping> getProductMapping()
	{
		return productMapping;
	}

	/**
	 * @param productMapping
	 *            the productMapping to set
	 */
	public void setProductMapping(List<ProductSubProjectMapping> productMapping)
	{
		this.productMapping = productMapping;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

}
