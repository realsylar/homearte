/**
 *
 */
package com.homearte.web.builder.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "builder_info")
public class BuilderInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "builder_id")
	private int		builderId;

	@Column(name = "builder_name")
	private String	builderName;

	@Column(name = "builder_description")
	private String	builderDescription;

	@Column(name = "creation_timestamp")
	private int		creationTimestamp;

	@Column(name = "modification_timestamp")
	private int		modificationTimestamp;

	@Column(name = "is_active")
	private boolean	isActive;

	/**
	 * @param builderId
	 * @param builderName
	 * @param builderDescription
	 * @param creationTimestamp
	 * @param modificationTimestamp
	 * @param isActive
	 */
	public BuilderInfo(int builderId, String builderName,
			String builderDescription, int creationTimestamp,
			int modificationTimestamp, boolean isActive)
	{
		super();
		this.builderName = builderName;
		this.builderDescription = builderDescription;
		this.isActive = isActive;
	}

	/**
	 *
	 */
	public BuilderInfo()
	{
		super();
	}

	/**
	 * @return the builderId
	 */
	public int getBuilderId()
	{
		return builderId;
	}

	/**
	 * @return the builderName
	 */
	public String getBuilderName()
	{
		return builderName;
	}

	/**
	 * @return the builderDescription
	 */
	public String getBuilderDescription()
	{
		return builderDescription;
	}

	/**
	 * @return the creationTimestamp
	 */
	public int getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public int getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

}
