/**
 *
 */
package com.homearte.web.builder.persistence.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.persistence.entity.UserBuilderMapping;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class UserBuilderMappingDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public UserBuilderMappingDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public UserBuilderMapping saveOrUpdate(UserBuilderMapping entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(UserBuilderMapping entity) throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public UserBuilderMapping getById(int id) throws PersistanceException
	{
		return dataPersistance.loadById(UserBuilderMapping.class, id);
	}

	public UserBuilderMapping getMappingByUserIdAndBuilderId(int userId,
			int builderId) throws PersistanceException
	{
		String query = "from UserBuilderMapping where userId = :userId and builderId = :builderId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("userId", new QueryParam(userId, QueryParamTypes.INT));
		params.put("builderId", new QueryParam(builderId, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryUniqueResult(query, params);
	}

	public UserBuilderMapping getMappingByUserId(int userId)
			throws PersistanceException
	{
		String query = "from UserBuilderMapping where userId = :userId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("userId", new QueryParam(userId, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryUniqueResult(query, params);
	}

	public List<UserBuilderMapping> getMappingByBuilderId(int builderId)
			throws PersistanceException
	{
		String query = "from UserBuilderMapping where builderId = :builderId";
		Map<String, QueryParam> params = Maps.newHashMap();
		params.put("builderId", new QueryParam(builderId, QueryParamTypes.INT));
		return dataPersistance.executeHQLQueryListResult(query, params);
	}
}
