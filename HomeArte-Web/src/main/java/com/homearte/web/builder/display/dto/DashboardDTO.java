/**
 *
 */
package com.homearte.web.builder.display.dto;

import java.util.List;

import com.homearte.web.user.display.dto.UserNotificationsDTO;

/**
 * @author Anurag Agrawal
 *
 */
public class DashboardDTO
{

	private String						userName;
	private String						userImage;
	private int							builderId;
	private List<UserNotificationsDTO>	userNotifications;
	private String						error;
	private String						message;

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return the userImage
	 */
	public String getUserImage()
	{
		return userImage;
	}

	/**
	 * @param userImage
	 *            the userImage to set
	 */
	public void setUserImage(String userImage)
	{
		this.userImage = userImage;
	}

	/**
	 * @return the builderId
	 */
	public int getBuilderId()
	{
		return builderId;
	}

	/**
	 * @param builderId
	 *            the builderId to set
	 */
	public void setBuilderId(int builderId)
	{
		this.builderId = builderId;
	}

	/**
	 * @return the userNotifications
	 */
	public List<UserNotificationsDTO> getUserNotifications()
	{
		return userNotifications;
	}

	/**
	 * @param userNotifications
	 *            the userNotifications to set
	 */
	public void setUserNotifications(
			List<UserNotificationsDTO> userNotifications)
	{
		this.userNotifications = userNotifications;
	}

	/**
	 * @return the error
	 */
	public String getError()
	{
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error)
	{
		this.error = error;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "DashboardDTO [userName=" + userName + ", userImage="
				+ userImage + ", builderId=" + builderId
				+ ", userNotifications=" + userNotifications + ", error="
				+ error + ", message=" + message + "]";
	}

}
