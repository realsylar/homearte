/**
 *
 */
package com.homearte.web.builder.service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.homearte.commons.core.cache.manager.HomearteMemCacheManager;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.display.dto.SubProjectContentMetadataDisplayDTO;
import com.homearte.web.builder.display.dto.SubProjectEditDTO;
import com.homearte.web.builder.enums.SubProjectStatus;
import com.homearte.web.builder.forms.dto.CreateSubProjectDTO;
import com.homearte.web.builder.persistence.dao.ProjectInfoDAO;
import com.homearte.web.builder.persistence.dao.SubProjectInfoDAO;
import com.homearte.web.builder.persistence.entity.ProductSubProjectMapping;
import com.homearte.web.builder.persistence.entity.SubProjectInfo;
import com.homearte.web.cache.constants.HomearteCacheConstants;
import com.homearte.web.content.persistence.entity.MultimediaContentMetadata;
import com.homearte.web.content.persistence.service.MultimediaContentMetadataService;
import com.homearte.web.product.display.dto.ProductListDTO;
import com.homearte.web.product.persistence.entity.ProductInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class SubProjectPageService
{

	private SubProjectInfoDAO					subProjectInfoDAO;
	private MultimediaContentMetadataService	multimediaContentMetadataService;
	private ProjectInfoDAO						projectInfoDAO;
	private HomearteMemCacheManager				memCacheManager;

	/**
	 * @param subProjectInfoDAO
	 * @param multimediaContentMetadataService
	 * @param projectInfoDAO
	 */
	@Autowired
	public SubProjectPageService(SubProjectInfoDAO subProjectInfoDAO,
			MultimediaContentMetadataService multimediaContentMetadataService,
			ProjectInfoDAO projectInfoDAO,
			HomearteMemCacheManager memCacheManager)
	{
		super();
		this.subProjectInfoDAO = subProjectInfoDAO;
		this.multimediaContentMetadataService = multimediaContentMetadataService;
		this.projectInfoDAO = projectInfoDAO;
		this.memCacheManager = memCacheManager;
	}

	/**
	 * @param createSubProjectFormData
	 * @param projectId
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional
	public int createSubProjectForProject(
			CreateSubProjectDTO createSubProjectFormData, int projectId)
					throws PersistanceException
	{
		return subProjectInfoDAO.saveOrUpdate(
				new SubProjectInfo(projectId, createSubProjectFormData
						.getSubProjectName(), createSubProjectFormData
						.getSubProjectDescription(), SubProjectStatus.CREATED
						.getValue())).getSubProjectId();
	}

	@Transactional
	public int editSubProject(String data, int subProjectId, int projectId)
			throws PersistanceException, JsonParseException,
			JsonMappingException, IOException
	{
		int status;
		ObjectMapper mapper = new ObjectMapper();
		SubProjectEditDTO dto = mapper.readValue(data, SubProjectEditDTO.class);
		SubProjectInfo spInfo = subProjectInfoDAO.getById(subProjectId);
		if (spInfo != null && spInfo.getProjectId() == projectId)
		{
			String spName = dto.getName() != null && !dto.getName().isEmpty()
					? dto.getName()
					: spInfo.getSubProjectName();
			String spDesc = dto.getDescription() != null
					&& !dto.getDescription().isEmpty()
					? dto.getDescription()
					: spInfo.getSubProjectDescription();
			spInfo.setSubProjectName(spName);
			spInfo.setSubProjectDescription(spDesc);
			subProjectInfoDAO.saveOrUpdate(spInfo);
			status = HttpServletResponse.SC_OK;
		}
		else
		{
			status = HttpServletResponse.SC_BAD_REQUEST;
		}
		return status;
	}

	/**
	 * @param data
	 * @throws PersistanceException
	 */
	@Transactional
	public int deleteSubProject(String deleteSubProjectRequestPayload)
			throws PersistanceException
	{
		JSONObject dataObj = new JSONObject(deleteSubProjectRequestPayload);
		int spid = dataObj.getInt("spid");
		subProjectInfoDAO.delete(spid);
		return HttpServletResponse.SC_OK;
	}

	/**
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional(readOnly = true)
	public List<SubProjectContentMetadataDisplayDTO> getSupplementaryFileListForSubProject(
			int subProjectId) throws PersistanceException
			{
		List<MultimediaContentMetadata> cMetadataList = subProjectInfoDAO
				.getById(subProjectId).getSupplementaryFiles();
		if (cMetadataList != null && !cMetadataList.isEmpty())
			return getSupplementryFileListDTOs(cMetadataList);
		return null;
			}

	/**
	 * @param spid
	 * @return
	 * @throws PersistanceException
	 * @throws JsonProcessingException
	 */
	@Transactional(readOnly = true)
	public String getFloorPlanContentResponsePayload(int subProjectId)
			throws PersistanceException, JsonProcessingException
	{
		MultimediaContentMetadata floorPlanContentMetadata = subProjectInfoDAO
				.getById(subProjectId).getFloorPlan();
		if (floorPlanContentMetadata != null)
		{
			ObjectMapper mapper = new ObjectMapper();
			return mapper
					.writeValueAsString(new SubProjectContentMetadataDisplayDTO(
							floorPlanContentMetadata));
		}
		return null;
	}

	/**
	 * @param spid
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional(readOnly = true)
	public List<ProductListDTO> getProductsForProject(int spid)
			throws PersistanceException
			{
		List<ProductListDTO> productDTOList = null;
		List<ProductSubProjectMapping> products = subProjectInfoDAO.getById(
				spid).getProductMapping();
		if (products != null)
		{
			productDTOList = getProductListDTOs(products);
		}
		return productDTOList;
			}

	/**
	 * @param data
	 * @return
	 * @throws IOException
	 * @throws PersistanceException
	 */
	@Transactional
	public MultimediaContentMetadata deleteContentForSubProject(String data)
			throws PersistanceException, IOException
	{
		JSONObject dataObj = new JSONObject(data);
		int cid = dataObj.getInt("cid");
		MultimediaContentMetadata cMetadata = multimediaContentMetadataService
				.readContentMetadata(cid);
		multimediaContentMetadataService.deleteContent(cid);
		return cMetadata;
	}

	/*
	 * 
	 * private helper methods start here
	 */
	private List<SubProjectContentMetadataDisplayDTO> getSupplementryFileListDTOs(
			List<MultimediaContentMetadata> cMetadataList)
			{
		List<SubProjectContentMetadataDisplayDTO> cMetadataDTOList = Lists
				.newArrayList();
		for (MultimediaContentMetadata cMetadata : cMetadataList)
		{
			cMetadataDTOList.add(new SubProjectContentMetadataDisplayDTO(
					cMetadata));
		}
		return cMetadataDTOList;
			}

	/**
	 * @param products
	 * @return
	 * @throws PersistanceException
	 */
	private List<ProductListDTO> getProductListDTOs(
			List<ProductSubProjectMapping> productMappings)
					throws PersistanceException
	{
		List<ProductListDTO> productDTOList = Lists.newArrayList();
		for (ProductSubProjectMapping productMapping : productMappings)
		{
			ProductInfo product = productMapping.getProduct();
			ProductListDTO dto = new ProductListDTO(product.getProductId(),
					product.getDisplayName(), product.getBasePrice(),
					productMapping.getStatus());
			productDTOList.add(dto);
		}
		return productDTOList;
	}

	/**
	 * @param pid
	 * @param spid
	 * @param cid
	 * @return
	 * @throws PersistanceException
	 */
	public boolean isContentFetchRequestValid(int userId, int projectId,
			int subProjectId)
	{
		boolean isValidRequest = false;
		try
		{
			int builderId = (Integer) memCacheManager
					.getItemWithKey(HomearteCacheConstants.BUILDER_ID_FOR_USER
							+ String.valueOf(userId));
			isValidRequest = subProjectInfoDAO.getById(subProjectId)
					.getProjectId() == projectId
					&& projectInfoDAO.getById(projectId).getBuilderId() == builderId;
		} catch (PersistanceException e1)
		{
			e1.printStackTrace();
		} catch (ExecutionException e)
		{
			e.printStackTrace();
		}
		return isValidRequest;
	}
}
