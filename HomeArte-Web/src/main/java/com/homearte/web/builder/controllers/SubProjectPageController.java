/**
 *
 */
package com.homearte.web.builder.controllers;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.homearte.commons.core.cache.manager.HomearteMemCacheManager;
import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.web.builder.display.dto.DashboardDTO;
import com.homearte.web.builder.display.dto.ProjectDisplayDTO;
import com.homearte.web.builder.display.dto.SubProjectContentMetadataDisplayDTO;
import com.homearte.web.builder.display.dto.SubProjectDisplayDTO;
import com.homearte.web.builder.forms.dto.CreateSubProjectDTO;
import com.homearte.web.builder.service.SubProjectPageService;
import com.homearte.web.cache.constants.HomearteCacheConstants;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.common.dto.AjaxResponse;
import com.homearte.web.product.display.dto.ProductListDTO;
import com.homearte.web.session.WebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping("/dashboard/project/{pid}/sub-project")
public class SubProjectPageController
{

	private static final Logger		logger	= Logger.getLogger(SubProjectPageController.class);
	private WebSessionManager		webSessionManager;
	private SubProjectPageService	subProjectPageService;
	private HomearteMemCacheManager	memCacheManager;

	/**
	 * @param webSessionManager
	 * @param subProjectPageService
	 * @param memCacheManager
	 */
	@Autowired
	public SubProjectPageController(WebSessionManager webSessionManager,
			HomearteMemCacheManager memCacheManager,
			SubProjectPageService subProjectPageService)
	{
		super();
		this.webSessionManager = webSessionManager;
		this.memCacheManager = memCacheManager;
		this.subProjectPageService = subProjectPageService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/sub")
	public ModelAndView submitCreateSubProjectForm(
			HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("cspdto") CreateSubProjectDTO createSubProjectFormData,
			@PathVariable int projectId)
	{
		ModelAndView model = null;
		String error = null;
		try
		{
			int createdSubProjectId = subProjectPageService
					.createSubProjectForProject(createSubProjectFormData,
							projectId);
			model = new ModelAndView("redirect:/dashboard/project/" + projectId
					+ "/sub-project/" + createdSubProjectId);

		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			model = prepareModelForCreateSubProjectFormSubmitRequestErrorCase(
					request, model, error, projectId);
		}
		return model;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/create")
	public ModelAndView createBuilderProjectFormLoad(
			HttpServletRequest request, HttpServletResponse response,
			@PathVariable int pid)
	{
		ModelAndView model = new ModelAndView("create-sub-project");
		int userId = webSessionManager.getUserIdFromSession(request
				.getSession());
		String error = null;
		DashboardDTO ddto = null;
		ProjectDisplayDTO pdto = null;
		try
		{
			ddto = (DashboardDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_DASHBOARD_DISPLAY_DATA
							+ String.valueOf(userId));
			pdto = (ProjectDisplayDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_PROJECT_DISPLAY_DATA
							+ pid);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		}
		CreateSubProjectDTO dto = new CreateSubProjectDTO(null, null);
		dto.setError(error);
		model.addObject("cspdto", dto);
		model.addObject("pdto", pdto);
		model.addObject("ddto", ddto);
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{spid}/edit/sub")
	@ResponseBody
	public String editBuilderSubProject(HttpServletRequest request,
			HttpServletResponse response, @RequestBody String data,
			@PathVariable int subProjectId, @PathVariable int projectId)
	{
		int status = -1;
		String error = null;
		try
		{
			status = subProjectPageService.editSubProject(data, subProjectId,
					projectId);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		}
		AjaxResponse editSubProjectResponse = new AjaxResponse(error, null,
				status);
		return new JSONObject().put("ajaxResponse", editSubProjectResponse)
				.toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete")
	@ResponseBody
	public String deleteBuilderSubProject(HttpServletRequest request,
			HttpServletResponse response, @RequestBody String data)
	{
		int status = -1;
		String error = null;
		try
		{
			status = subProjectPageService.deleteSubProject(data);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		}
		AjaxResponse deleteSubProjectResponse = new AjaxResponse(error, null,
				status);
		return new JSONObject().put("ajaxResponse", deleteSubProjectResponse)
				.toString();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{spid}")
	public ModelAndView loadProjectPageForBuilder(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int spid,
			@PathVariable int pid)
	{
		ModelAndView model = new ModelAndView("sub-project");
		DashboardDTO ddto = null;
		ProjectDisplayDTO pdto = null;
		SubProjectDisplayDTO spdto = null;
		try
		{

			int userId = webSessionManager.getUserIdFromSession(request
					.getSession());
			ddto = (DashboardDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_DASHBOARD_DISPLAY_DATA
							+ String.valueOf(userId));
			pdto = (ProjectDisplayDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_PROJECT_DISPLAY_DATA
							+ pid);
			spdto = (SubProjectDisplayDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_SUB_PROJECT_DISPLAY_DATA
							+ spid);
			if (spdto == null)
			{
				model = new ModelAndView("forward:/dashboard/project/invalid");
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			ddto.setError(SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY));
		}
		model.addObject("sdto", spdto);
		model.addObject("pdto", pdto);
		model.addObject("ddto", ddto);
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{spid}/sflist")
	@ResponseBody
	public String getSupplymentryFileList(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int spid,
			@PathVariable int pid)
	{
		int status;
		String error = null;
		List<SubProjectContentMetadataDisplayDTO> supplementaryFilesList = null;
		try
		{
			supplementaryFilesList = subProjectPageService
					.getSupplementaryFileListForSubProject(spid);
			status = HttpServletResponse.SC_OK;
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.PROJECT_LIST_NOT_LOADED);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		AjaxResponse supplementaryFilesForSubProjectResponse = new AjaxResponse(
				error, null, status);
		supplementaryFilesForSubProjectResponse.addToValues("sflist",
				supplementaryFilesList);
		return new JSONObject().put("ajaxResponse",
				supplementaryFilesForSubProjectResponse).toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{spid}/gfp")
	@ResponseBody
	public String getFloorPlanContent(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int spid,
			@PathVariable int pid)
	{
		int status;
		String error = null;
		String floorPlanResponse = null;
		try
		{
			floorPlanResponse = subProjectPageService
					.getFloorPlanContentResponsePayload(spid);
			status = HttpServletResponse.SC_OK;
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.PROJECT_LIST_NOT_LOADED);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		AjaxResponse floorPlanForSubProjectResponse = new AjaxResponse(error,
				null, status);
		floorPlanForSubProjectResponse.addToValues("cdto", floorPlanResponse);
		return new JSONObject().put("ajaxResponse",
				floorPlanForSubProjectResponse).toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{spid}/hplist")
	@ResponseBody
	public String getSubprojectsForProject(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int pid,
			@PathVariable int spid)
	{
		int status;
		String error = null;
		List<ProductListDTO> productsForSubproject = null;
		try
		{
			productsForSubproject = subProjectPageService
					.getProductsForProject(spid);
			status = HttpServletResponse.SC_OK;
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in dashboard load up process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.PROJECT_LIST_NOT_LOADED);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		AjaxResponse productsForSubProjectResponse = new AjaxResponse(error,
				null, status);
		productsForSubProjectResponse.addToValues("hplist",
				productsForSubproject);
		return new JSONObject().put("ajaxResponse",
				productsForSubProjectResponse).toString();
	}

	/**
	 * @param model
	 * @param projectId
	 * @param error
	 * @return
	 */
	private ModelAndView prepareModelForCreateSubProjectFormSubmitRequestErrorCase(
			HttpServletRequest request, ModelAndView model, String error,
			int projectId)
	{
		try
		{
			int userId = webSessionManager.getUserIdFromSession(request
					.getSession());
			DashboardDTO ddto = (DashboardDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_DASHBOARD_DISPLAY_DATA
							+ String.valueOf(userId));
			ProjectDisplayDTO pdto = (ProjectDisplayDTO) memCacheManager
					.getItemWithKey(HomearteCacheConstants.USER_PROJECT_DISPLAY_DATA
							+ projectId);
			CreateSubProjectDTO dto = new CreateSubProjectDTO(null, null);
			dto.setError(error);
			model.addObject("cspdto", dto);
			model.addObject("pdto", pdto);
			model.addObject("ddto", ddto);
		} catch (ExecutionException e1)
		{
			logger.error("Error occured while handling error for create sub project request ");
			e1.printStackTrace();
		}
		return model;
	}
}
