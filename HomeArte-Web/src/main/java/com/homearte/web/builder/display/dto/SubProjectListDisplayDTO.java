/**
 *
 */
package com.homearte.web.builder.display.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.homearte.web.builder.persistence.entity.SubProjectInfo;

/**
 * @author Anurag Agrawal
 *
 */
public class SubProjectListDisplayDTO
{

	private int			subProjectId;
	private int			projectId;
	private String		subProjectName;
	private String		subProjectDescription;
	private String		creationTimestamp;
	private DateFormat	fmt	= new SimpleDateFormat("MMMM dd, yyyy");

	/**
	 *
	 */
	public SubProjectListDisplayDTO(SubProjectInfo p)
	{
		super();
		this.subProjectId = p.getSubProjectId();
		this.subProjectName = p.getSubProjectName();
		this.subProjectDescription = p.getSubProjectDescription();
		this.creationTimestamp = fmt.format(p.getCreationTimestamp());
		this.projectId = p.getProjectId();
	}

	/**
	 * @return the subProjectId
	 */
	public int getSubProjectId()
	{
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            the subProjectId to set
	 */
	public void setSubProjectId(int subProjectId)
	{
		this.subProjectId = subProjectId;
	}

	/**
	 * @return the subProjectName
	 */
	public String getSubProjectName()
	{
		return subProjectName;
	}

	/**
	 * @param subProjectName
	 *            the subProjectName to set
	 */
	public void setSubProjectName(String subProjectName)
	{
		this.subProjectName = subProjectName;
	}

	/**
	 * @return the subProjectDescription
	 */
	public String getSubProjectDescription()
	{
		return subProjectDescription;
	}

	/**
	 * @param subProjectDescription
	 *            the subProjectDescription to set
	 */
	public void setSubProjectDescription(String subProjectDescription)
	{
		this.subProjectDescription = subProjectDescription;
	}

	/**
	 * @return the creationTimestamp
	 */
	public String getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(String creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the projectId
	 */
	public int getProjectId()
	{
		return projectId;
	}

	/**
	 * @param projectId
	 *            the projectId to set
	 */
	public void setProjectId(int projectId)
	{
		this.projectId = projectId;
	}

}
