/**
 *
 */
package com.homearte.web.builder.persistence.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "user_builder_mapping")
public class UserBuilderMapping
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int			id;
	@Column(name = "user_id")
	private int			userId;
	@Column(name = "builder_id")
	private int			builderId;
	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;
	@Column(name = "is_active")
	private boolean		isActive;

	/**
	 * @param userId
	 * @param builderId
	 * @param isActive
	 */
	public UserBuilderMapping(int userId, int builderId, boolean isActive)
	{
		super();
		this.userId = userId;
		this.builderId = builderId;
		this.isActive = isActive;
	}

	/**
	 *
	 */
	protected UserBuilderMapping()
	{
		super();
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * @return the builderId
	 */
	public int getBuilderId()
	{
		return builderId;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

}
