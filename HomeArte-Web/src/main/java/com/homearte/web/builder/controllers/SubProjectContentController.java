/**
 *
 */
package com.homearte.web.builder.controllers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.homearte.commons.aws.entity.S3MultipartFileParams;
import com.homearte.commons.core.cache.manager.HomearteMemCacheManager;
import com.homearte.commons.core.config.loader.SpringContextPropertyLoader;
import com.homearte.web.builder.service.SubProjectPageService;
import com.homearte.web.common.constants.ApplicationConstants;
import com.homearte.web.common.dto.AjaxResponse;
import com.homearte.web.content.persistence.entity.MultimediaContentMetadata;
import com.homearte.web.content.persistence.service.MultimediaContentMetadataService;
import com.homearte.web.content.utility.ContentTypeBindings;
import com.homearte.web.content.utility.MultimediaContentParameters;
import com.homearte.web.content.utility.MultimediaContentUtilities;
import com.homearte.web.session.WebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "/dashboard/project/{pid}/sub-project/{spid}")
public class SubProjectContentController
{

	private static final Logger					logger	= Logger.getLogger(SubProjectContentController.class);
	private WebSessionManager					webSessionManager;
	//private HomearteMemCacheManager				memCacheManager;
	private SubProjectPageService				subProjectPageService;
	private MultimediaContentMetadataService	multimediaContentMetadataService;
	private MultimediaContentUtilities			multimediaContentUtilities;
	private DateFormat							fmt		= new SimpleDateFormat(
																"MMMM dd, yyyy");

	/**
	 * @param webSessionManager
	 * @param memCacheManager
	 * @param multimediaContentMetadataService
	 * @param s3BucketManager
	 * @param multimediaContentUtilities
	 */
	@Autowired
	public SubProjectContentController(WebSessionManager webSessionManager,
			HomearteMemCacheManager memCacheManager,
			SubProjectPageService subProjectPageService,
			MultimediaContentMetadataService multimediaContentMetadataService,
			MultimediaContentUtilities multimediaContentUtilities)
	{
		super();
		this.webSessionManager = webSessionManager;
		//this.memCacheManager = memCacheManager;
		this.subProjectPageService = subProjectPageService;
		this.multimediaContentMetadataService = multimediaContentMetadataService;
		this.multimediaContentUtilities = multimediaContentUtilities;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/fsub")
	@ResponseBody
	public String uploadFloorPlanForSubProject(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int pid,
			@PathVariable int spid, @RequestPart("file") MultipartFile mFile)
	{
		String error = null;
		int status = -1;
		MultimediaContentMetadata cMetadata = null;
		try
		{
			cMetadata = multimediaContentMetadataService.uploadContent(mFile,
					spid + "/");
			if (cMetadata != null)
			{
				status = HttpServletResponse.SC_OK;
				error = null;
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in upload file process", e);
			error = SpringContextPropertyLoader
					.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
			status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		AjaxResponse uploadFloorPlanResponse = new AjaxResponse(error, null,
				status);
		uploadFloorPlanResponse.addToValues("id", cMetadata.getMetadataId());
		return new JSONObject().put("ajaxResponse", uploadFloorPlanResponse)
				.toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ssub")
	@ResponseBody
	public String uploadASupplementaryFileForSubProject(
			HttpServletRequest request, HttpServletResponse response,
			@PathVariable int pid, @PathVariable int spid,
			@RequestPart("file") MultipartFile mFile)
	{
		String error = SpringContextPropertyLoader
				.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		int status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		MultimediaContentMetadata cMetadata = null;
		try
		{
			cMetadata = multimediaContentMetadataService.uploadContent(mFile,
					spid + "/");
			if (cMetadata != null)
			{
				status = HttpServletResponse.SC_OK;
				error = null;
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in upload file process", e);
		}
		AjaxResponse uploadsupplementryFileResponse = new AjaxResponse(error,
				null, status);
		uploadsupplementryFileResponse.addToValues("id",
				cMetadata.getMetadataId());
		uploadsupplementryFileResponse
				.addToValues("type", ContentTypeBindings
						.getDisplayContentTypeForMimeTypeValue(cMetadata
								.getMimeType()));
		uploadsupplementryFileResponse.addToValues("name", cMetadata.getName());
		uploadsupplementryFileResponse.addToValues("creationTimestamp",
				fmt.format(cMetadata.getCreationTimestamp()));
		return new JSONObject().put("ajaxResponse",
				uploadsupplementryFileResponse).toString();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/del")
	@ResponseBody
	public String deleteContentFile(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int pid,
			@PathVariable int spid, @RequestBody String data)
	{

		String error = SpringContextPropertyLoader
				.getPropertyForKey(ApplicationConstants.TECHNICAL_DIFFICULTY);
		int status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		MultimediaContentMetadata cMetadata = null;
		try
		{
			cMetadata = subProjectPageService.deleteContentForSubProject(data);
			error = null;
			status = HttpServletResponse.SC_OK;
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception thrown in upload file process", e);
		}
		AjaxResponse deleteFileResponse = new AjaxResponse(error, null, status);
		deleteFileResponse.addToValues("id", cMetadata.getMetadataId());
		deleteFileResponse
				.addToValues("type", ContentTypeBindings
						.getDisplayContentTypeForMimeTypeValue(cMetadata
								.getMimeType()));
		return new JSONObject().put("ajaxResponse", deleteFileResponse)
				.toString();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cget/{cid}")
	public void loadImageData(HttpServletRequest request,
			HttpServletResponse response, @PathVariable int pid,
			@PathVariable int spid, @PathVariable int cid)
	{
		int userId = webSessionManager.getUserIdFromSession(request
				.getSession());
		boolean isAValidContentRequest = subProjectPageService
				.isContentFetchRequestValid(userId, pid, spid);
		if (!isAValidContentRequest)
		{
			return;
		}
		else
		{
			MultimediaContentMetadata cMetaData;
			BufferedInputStream input = null;
			BufferedOutputStream output = null;
			S3MultipartFileParams params = null;
			try
			{
				cMetaData = multimediaContentMetadataService
						.readContentMetadata(cid);
				params = multimediaContentMetadataService.readContent(cid);
				response = multimediaContentUtilities
						.decorateResponseForContentStream(
								response,
								new MultimediaContentParameters(
										cMetaData.getName(),
										String.valueOf(params.getFileSize()),
										ContentTypeBindings
												.getRealMimeTypeForInternalMimeTypeValue(cMetaData
														.getMimeType())));
				input = new BufferedInputStream(params.getFileData()
						.getObjectContent());
				output = new BufferedOutputStream(response.getOutputStream());
				byte[] buffer = new byte[8192];
				for (int length = 0; (length = input.read(buffer)) > 0;)
				{
					output.write(buffer, 0, length);
				}
			} catch (Exception e)
			{
				e.printStackTrace();
				logger.error("Exception caught while dishing out content");
				return;
			} finally
			{
				handlePostContentWrite(input, output, params);
			}
		}
	}

	/**
	 * @param input
	 * @param output
	 * @param params
	 */
	private void handlePostContentWrite(BufferedInputStream input,
			BufferedOutputStream output, S3MultipartFileParams params)
	{
		if (output != null)
			try
		{
				output.close();
		} catch (IOException logOrIgnore)
		{
			logger.error("Exception caught while closing output stream.");
		}
		if (input != null)
			try
		{
				input.close();
		} catch (IOException logOrIgnore)
		{
			logger.error("Exception caught while closing input stream.");
		}
		if (params != null && params.getFileData() != null)
			try
		{
				multimediaContentMetadataService.handlePostDataRead(params);
		} catch (IOException logOrIgnore)
		{
			logger.error("Exception caught while closing data stream.");
		}
	}
}
