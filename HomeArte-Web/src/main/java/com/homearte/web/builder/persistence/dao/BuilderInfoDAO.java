/**
 *
 */
package com.homearte.web.builder.persistence.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.persistence.entity.BuilderInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class BuilderInfoDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public BuilderInfoDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public BuilderInfo saveOrUpdate(BuilderInfo entity)
			throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(BuilderInfo entity) throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public BuilderInfo getById(int id) throws PersistanceException
	{
		return dataPersistance.loadById(BuilderInfo.class, id);
	}
}
