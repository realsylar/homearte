/**
 *
 */
package com.homearte.web.builder.display.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import com.homearte.web.builder.persistence.entity.SubProjectInfo;

/**
 * @author Anurag Agrawal
 *
 */
public class SubProjectDisplayDTO
{

	private int				subProjectId;
	private int				projectId;
	private String			projectName;
	private String			subProjectName;
	private String			subProjectDescription;
	private String			creationTimestamp;
	private String			displayPlanURL;
	private List<String>	otherFileDownloadURLs;
	private DateFormat		fmt	= new SimpleDateFormat("MMMM dd, yyyy");

	/**
	 *
	 */
	public SubProjectDisplayDTO(SubProjectInfo spInfo, String pName, int pId)
	{
		super();
		this.subProjectId = spInfo.getSubProjectId();
		this.subProjectName = spInfo.getSubProjectName();
		this.subProjectDescription = spInfo.getSubProjectDescription();
		this.creationTimestamp = fmt.format(spInfo.getCreationTimestamp());
		this.projectName = pName;
		this.projectId = pId;
	}

	/**
	 * @return the subProjectId
	 */
	public int getSubProjectId()
	{
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            the subProjectId to set
	 */
	public void setSubProjectId(int subProjectId)
	{
		this.subProjectId = subProjectId;
	}

	/**
	 * @return the projetId
	 */
	public int getProjectId()
	{
		return projectId;
	}

	/**
	 * @param projetId
	 *            the projetId to set
	 */
	public void setProjectId(int projectId)
	{
		this.projectId = projectId;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName()
	{
		return projectName;
	}

	/**
	 * @param projectName
	 *            the projectName to set
	 */
	public void setProjectName(String projectName)
	{
		this.projectName = projectName;
	}

	/**
	 * @return the subProjectName
	 */
	public String getSubProjectName()
	{
		return subProjectName;
	}

	/**
	 * @param subProjectName
	 *            the subProjectName to set
	 */
	public void setSubProjectName(String subProjectName)
	{
		this.subProjectName = subProjectName;
	}

	/**
	 * @return the subProjectDescription
	 */
	public String getSubProjectDescription()
	{
		return subProjectDescription;
	}

	/**
	 * @param subProjectDescription
	 *            the subProjectDescription to set
	 */
	public void setSubProjectDescription(String subProjectDescription)
	{
		this.subProjectDescription = subProjectDescription;
	}

	/**
	 * @return the creationTimestamp
	 */
	public String getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(String creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the displayPlanURL
	 */
	public String getDisplayPlanURL()
	{
		return displayPlanURL;
	}

	/**
	 * @param displayPlanURL
	 *            the displayPlanURL to set
	 */
	public void setDisplayPlanURL(String displayPlanURL)
	{
		this.displayPlanURL = displayPlanURL;
	}

	/**
	 * @return the otherFileDownloadURLs
	 */
	public List<String> getOtherFileDownloadURLs()
	{
		return otherFileDownloadURLs;
	}

	/**
	 * @param otherFileDownloadURLs
	 *            the otherFileDownloadURLs to set
	 */
	public void setOtherFileDownloadURLs(List<String> otherFileDownloadURLs)
	{
		this.otherFileDownloadURLs = otherFileDownloadURLs;
	}

	/**
	 * @return the fmt
	 */
	public DateFormat getFmt()
	{
		return fmt;
	}

	/**
	 * @param fmt
	 *            the fmt to set
	 */
	public void setFmt(DateFormat fmt)
	{
		this.fmt = fmt;
	}

}
