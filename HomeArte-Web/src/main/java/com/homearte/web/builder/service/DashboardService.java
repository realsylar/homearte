/**
 *
 */
package com.homearte.web.builder.service;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.builder.display.dto.ProjectDisplayDTO;
import com.homearte.web.builder.persistence.dao.ProjectInfoDAO;
import com.homearte.web.builder.persistence.entity.ProjectInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class DashboardService
{

	private ProjectInfoDAO	projectInfoDAO;

	/**
	 * @param projectInfoDAO
	 */
	@Autowired
	public DashboardService(ProjectInfoDAO projectInfoDAO)
	{
		super();
		this.projectInfoDAO = projectInfoDAO;
	}

	/**
	 * @param data
	 * @return
	 * @throws PersistanceException
	 */
	@Transactional(readOnly = true)
	public List<ProjectDisplayDTO> getProjectsForBuilder(String data)
			throws PersistanceException
	{
		JSONObject dataObj = new JSONObject(data);
		int builderId = dataObj.getInt("bid");
		int skip = dataObj.getInt("skip");
		int limit = dataObj.getInt("limit");
		String query = dataObj.getString("query");
		return getProjectListDTOs(projectInfoDAO.getProjectsByBuilderId(
				builderId, query, skip, limit));
	}

	/**
	 * @param projects
	 * @return
	 */
	private List<ProjectDisplayDTO> getProjectListDTOs(
			List<ProjectInfo> projects)
			{
		List<ProjectDisplayDTO> projectDTOList = Lists.newArrayList();
		for (ProjectInfo project : projects)
		{
			projectDTOList.add(new ProjectDisplayDTO(project));
		}
		return projectDTOList;
			}
}
