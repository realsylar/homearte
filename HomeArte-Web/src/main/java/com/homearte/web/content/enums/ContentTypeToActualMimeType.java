/**
 *
 */
package com.homearte.web.content.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum ContentTypeToActualMimeType
{
	IMAGE_JPEG("image/jpeg"), IMAGE_PNG("image/png"), DOC("application/msword"), XLS(
			"application/excel"), PDF("application/pdf"), AUTOCAD_DWG(
			"image/x-dwg"), IMAGE_BMP("image/bmp");

	private String	value;

	/**
	 * @param value
	 */
	private ContentTypeToActualMimeType(String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}

	public static ContentTypeToActualMimeType getEnumForValue(String value)
	{
		for (ContentTypeToActualMimeType type : values())
		{
			if (type.getValue().equals(value))
			{
				return type;
			}
		}
		return null;
	}

	public static String getValueForEnumName(String enumName)
	{
		for (ContentTypeToActualMimeType type : values())
		{
			if (type.name().equals(enumName))
			{
				return type.getValue();
			}
		}
		return null;
	}
}
