/**
 *
 */
package com.homearte.web.content.persistence.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.content.persistence.entity.MultimediaContentMetadata;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class MultimediaContentMetadataDAO
{

	private DataPersistance	dataPersistance;

	/**
	 * Autowired constructor to automatically load the data persistance object
	 * from the spring - hibernate configuration.
	 *
	 * @param dataPersistance
	 */
	@Autowired
	public MultimediaContentMetadataDAO(DataPersistance dataPersistance)
	{
		super();
		this.dataPersistance = dataPersistance;
	}

	public MultimediaContentMetadata saveOrUpdate(
			MultimediaContentMetadata entity) throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(entity);
	}

	public void delete(MultimediaContentMetadata entity)
			throws PersistanceException
	{
		dataPersistance.delete(entity);
	}

	public void delete(int id) throws PersistanceException
	{
		dataPersistance.delete(MultimediaContentMetadata.class, id);
	}

	public MultimediaContentMetadata getById(int id)
			throws PersistanceException
	{
		return dataPersistance.loadById(MultimediaContentMetadata.class, id);
	}

}
