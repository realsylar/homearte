/**
 *
 */
package com.homearte.web.content.utility;

/**
 * @author Anurag Agrawal
 *
 */
public class MultimediaContentParameters
{

	private String	fileName;
	private String	fileSize;
	private String	fileMimeType;

	/**
	 * @param fileName
	 * @param fileSize
	 * @param fileMimeType
	 */
	public MultimediaContentParameters(String fileName, String fileSize,
			String fileMimeType)
	{
		super();
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.fileMimeType = fileMimeType;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName()
	{
		return fileName;
	}

	/**
	 * @return the fileSize
	 */
	public String getFileSize()
	{
		return fileSize;
	}

	/**
	 * @return the fileMimeType
	 */
	public String getFileMimeType()
	{
		return fileMimeType;
	}

}
