/**
 *
 */
package com.homearte.web.content.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum ApplicationContentType
{
	PROJECT(0), PRODUCT(1);

	private int	value;

	/**
	 * @param value
	 */
	private ApplicationContentType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static ApplicationContentType getEnumForValue(int value)
	{
		for (ApplicationContentType type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
