/**
 *
 */
package com.homearte.web.content.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum DisplayContentType
{
	IMAGE_JPEG("Image"), IMAGE_PNG("Image"), DOC("Document"), XLS("Worksheet"), PDF(
			"PDF"), AUTOCAD_DWG("Drawing"), IMAGE_BMP("Image");

	private String	value;

	/**
	 * @param value
	 */
	private DisplayContentType(String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}

	public static DisplayContentType getEnumForValue(String value)
	{
		for (DisplayContentType type : values())
		{
			if (type.getValue().equals(value))
			{
				return type;
			}
		}
		return null;
	}

}
