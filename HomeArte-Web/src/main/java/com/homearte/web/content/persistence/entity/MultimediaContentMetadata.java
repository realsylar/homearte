/**
 *
 */
package com.homearte.web.content.persistence.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "multimedia_content_metadata")
public class MultimediaContentMetadata
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "metadata_id")
	private int			metadataId;

	@Column(name = "name")
	private String		name;

	@Column(name = "mime_type")
	private int			mimeType;

	@Column(name = "storage_key_name")
	private String		storageKeyName;

	@Column(name = "is_active")
	private boolean		isActive;

	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp	modificationTimestamp;

	/**
	 * @param name
	 * @param mimeType
	 * @param storageKeyName
	 * @param isActive
	 */
	public MultimediaContentMetadata(String name, int mimeType,
			String storageKeyName, boolean isActive)
	{
		super();
		this.name = name;
		this.mimeType = mimeType;
		this.storageKeyName = storageKeyName;
		this.isActive = isActive;
	}

	/**
	 *
	 */
	protected MultimediaContentMetadata()
	{
		super();
	}

	/**
	 * @return the metadataId
	 */
	public int getMetadataId()
	{
		return metadataId;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the mimeType
	 */
	public int getMimeType()
	{
		return mimeType;
	}

	/**
	 * @return the storageKeyName
	 */
	public String getStorageKeyName()
	{
		return storageKeyName;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

}
