/**
 *
 */
package com.homearte.web.content.utility;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.dao.MimeTypeBindingsDAO;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class MimeTypeBindings
{

	private static final Logger	logger									= Logger.getLogger(MimeTypeBindings.class);

	private Map<String, String>	mimeTypeToApplicationContentTypeBinding	= Maps.newHashMap();
	private MimeTypeBindingsDAO	bindingdDAO;

	@Autowired
	public MimeTypeBindings(MimeTypeBindingsDAO bindingdDAO)
	{
		this.bindingdDAO = bindingdDAO;
	}

	@PostConstruct
	@Transactional(propagation = Propagation.REQUIRED)
	public void loadProperties()
	{
		List<Object[]> list = bindingdDAO.loadBindingsFromDB();
		Map<String, String> propertiesFromDB = Maps.newHashMap();
		for (Object[] keyValue : list)
		{
			propertiesFromDB.put((String) keyValue[0], (String) keyValue[1]);
		}
		mimeTypeToApplicationContentTypeBinding.putAll(propertiesFromDB);
		logger.info("The map loaded from DB is : "
				+ mimeTypeToApplicationContentTypeBinding);
		propertiesFromDB = null;
	}

	public String getProperty(String key)
	{
		return mimeTypeToApplicationContentTypeBinding.get(key);
	}
}
