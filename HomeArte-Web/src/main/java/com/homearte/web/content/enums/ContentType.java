/**
 *
 */
package com.homearte.web.content.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum ContentType
{
	IMAGE_JPEG(0), IMAGE_PNG(1), DOC(2), XLS(3), PDF(4), AUTOCAD_DWG(5), IMAGE_BMP(
			6);

	private int	value;

	/**
	 * @param value
	 */
	private ContentType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static ContentType getEnumForValue(int value)
	{
		for (ContentType type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
