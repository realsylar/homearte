/**
 *
 */
package com.homearte.web.content.utility;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class MultimediaContentUtilities
{

	public HttpServletResponse decorateResponseForContentStream(
			HttpServletResponse response, MultimediaContentParameters fileParams)
	{
		response.setContentType(fileParams.getFileMimeType());
		response.setHeader("Content-Length",
				String.valueOf(fileParams.getFileSize()));
		response.setHeader("Content-Disposition", "inline; filename=\""
				+ fileParams.getFileName() + "\"");
		return response;
	}
}
