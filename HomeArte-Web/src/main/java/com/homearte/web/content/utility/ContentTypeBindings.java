/**
 *
 */
package com.homearte.web.content.utility;

import com.homearte.web.cache.data.provider.ApplicationContextProvider;
import com.homearte.web.content.enums.ContentType;
import com.homearte.web.content.enums.ContentTypeToActualMimeType;
import com.homearte.web.content.enums.DisplayContentType;

/**
 * @author Anurag Agrawal
 *
 */
public class ContentTypeBindings
{

	private static MimeTypeBindings	mimeTypeBindings;

	private static void init()
	{
		if (mimeTypeBindings == null)
			mimeTypeBindings = ApplicationContextProvider
					.getBeanFromContext(MimeTypeBindings.class);
	}

	public static int getContentTypeForMimeType(String mimeType)
	{
		init();
		return ContentType.valueOf(mimeTypeBindings.getProperty(mimeType))
				.getValue();
	}

	public static String getDisplayContentTypeForMimeTypeValue(int mimeType)
	{
		init();
		return DisplayContentType.valueOf(
				ContentType.getEnumForValue(mimeType).name()).getValue();
	}

	public static String getRealMimeTypeForInternalMimeTypeValue(int mimeType)
	{
		init();
		return ContentTypeToActualMimeType.getValueForEnumName(ContentType
				.getEnumForValue(mimeType).name());
	}
}
