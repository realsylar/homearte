/**
 *
 */
package com.homearte.web.content.persistence.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.homearte.commons.aws.entity.S3MultipartFileParams;
import com.homearte.commons.core.manager.S3BucketManager;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.web.content.persistence.dao.MultimediaContentMetadataDAO;
import com.homearte.web.content.persistence.entity.MultimediaContentMetadata;
import com.homearte.web.content.utility.ContentTypeBindings;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class MultimediaContentMetadataService
{

	private MultimediaContentMetadataDAO	dao;
	private S3BucketManager					s3BucketManager;

	/**
	 * @param dao
	 */
	@Autowired
	public MultimediaContentMetadataService(MultimediaContentMetadataDAO dao,
			S3BucketManager s3BucketManager)
	{
		super();
		this.dao = dao;
		this.s3BucketManager = s3BucketManager;
	}

	@Transactional
	public MultimediaContentMetadata uploadContent(MultipartFile mFile,
			String storageKeyName) throws PersistanceException, IOException
	{
		MultimediaContentMetadata cMetadata = new MultimediaContentMetadata(
				mFile.getName(),
				ContentTypeBindings.getContentTypeForMimeType(mFile
						.getContentType()), storageKeyName, true);
		cMetadata = dao.saveOrUpdate(cMetadata);
		s3BucketManager.uploadFile(new S3MultipartFileParams(mFile
				.getInputStream(), storageKeyName + "/"
						+ cMetadata.getMetadataId(), mFile.getSize()));
		return cMetadata;
	}

	@Transactional(readOnly = true)
	public S3MultipartFileParams readContent(int contentMetadataId)
			throws PersistanceException, IOException
	{
		MultimediaContentMetadata cMetadata = dao.getById(contentMetadataId);
		S3MultipartFileParams fileParams = new S3MultipartFileParams(
				null,
				cMetadata.getStorageKeyName() + "/" + cMetadata.getMetadataId(),
				0);
		fileParams = s3BucketManager.recieveFile(fileParams);
		return fileParams;
	}

	@Transactional(readOnly = true)
	public MultimediaContentMetadata readContentMetadata(int contentMetadataId)
			throws PersistanceException, IOException
	{
		MultimediaContentMetadata cMetadata = dao.getById(contentMetadataId);
		return cMetadata;
	}

	@Transactional
	public void deleteContent(int contentMetadataId)
			throws PersistanceException, IOException
	{
		MultimediaContentMetadata cMetadata = dao.getById(contentMetadataId);
		s3BucketManager.deleteFile(new S3MultipartFileParams(null, cMetadata
				.getStorageKeyName() + "/" + contentMetadataId, 0));
		dao.delete(contentMetadataId);
	}

	public void handlePostDataRead(S3MultipartFileParams fileParams)
			throws IOException
	{
		s3BucketManager.handlePostDataRead(fileParams);
	}
}
