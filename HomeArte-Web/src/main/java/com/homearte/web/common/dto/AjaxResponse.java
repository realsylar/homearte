/**
 *
 */
package com.homearte.web.common.dto;

import java.util.Map;

import com.google.common.collect.Maps;

/**
 * @author Anurag Agrawal
 *
 */
public class AjaxResponse
{

	private Map<String, Object>	values;
	private String				error;
	private String				message;
	private int					status;

	/**
	 * @return the values
	 */
	public Map<String, Object> getValues()
	{
		return Maps.newHashMap(values);
	}

	public void addToValues(String key, Object data)
	{
		values.put(key, data);
	}

	/**
	 * @return the error
	 */
	public String getError()
	{
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error)
	{
		this.error = error;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/**
	 * @param error
	 * @param message
	 * @param status
	 */
	public AjaxResponse(String error, String message, int status)
	{
		super();
		this.values = Maps.newHashMap();
		this.error = error;
		this.message = message;
		this.status = status;
	}
}
