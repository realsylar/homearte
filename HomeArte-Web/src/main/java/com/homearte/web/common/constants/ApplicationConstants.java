/**
 *
 */
package com.homearte.web.common.constants;

/**
 * @author Anurag Agrawal
 *
 */
public interface ApplicationConstants
{

	public static final String	USER_EMAIL_ALREADY_EXISTS				= "user.email.already.exists";
	public static final String	SIGNUP_MAIL_TEMPLATE_NAME				= "new_user_sign_up.vm";
	public static final String	RPWD_MAIL_TEMPLATE_NAME					= "recover_pwd.vm";
	public static final String	SIGNUP_CONFIRMATION_LINK				= "signup/conf?";
	public static final String	RPWD_CONFIRMATION_LINK					= "fpwd/conf?";
	public static final String	TECHNICAL_DIFFICULTY					= "tech.diff";
	public static final String	INFO_FROM_ADDRESS						= "info@homearte.in";
	public static final String	INVALID_CONFIRMATION_LINK				= "invalid.confirmation.link";
	public static final String	INVALID_LINK							= "invalid.link";
	public static final String	NEW_USER_SIGNUP_EMAIL_SUBJECT			= "new.user.signup.email.subject";
	public static final String	RPWD_EMAIL_SUBJECT						= "rpwd.email.subject";
	public static final String	THANK_YOU_FOR_SUBSCRIBING				= "thank.you.for.subscribing";
	public static final String	ALREADY_SUBSCRIBED_TO_NEWSLETTER		= "already.subscribed.to.newsletter";
	public static final String	SAD_FOR_UNSUBSCRIBING					= "sad.for.unsubscribing";
	public static final String	USER_DOES_NOT_EXIST						= "user.does.not.exist";
	public static final String	PASSWORD_INCORRECT						= "password.incorrect";
	public static final String	CUSTOMER_SUPPORT_ADDRESS				= "anurag@homearte.in";
	public static final String	CONTACT_US_FORM_SUBMIT_EMAIL_SUBJECT	= "contact.us.form.submit.email.subject";
	public static final String	CONTACT_US_SUCCESS						= "contact.us.success";
	public static final String	RECOVER_PASSWORD_MAIL_SENT				= "recover.password.mail.sent";
	public static final String	PROJECT_LIST_NOT_LOADED					= "project.list.not.loaded";
	public static final String	UNAUTHORIZED_ACCESS_LOGIN_TO_CONTINUE	= "unauthorized.access.login.to.continue";

}
