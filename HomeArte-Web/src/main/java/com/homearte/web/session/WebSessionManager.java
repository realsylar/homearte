/**
 *
 */
package com.homearte.web.session;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class WebSessionManager
{

	private static final String	USER_SESSION_NAME	= "user";

	public void addUserIdToSession(HttpSession session, int userId)
	{
		session.setAttribute(USER_SESSION_NAME, userId);
		session.setMaxInactiveInterval(30 * 60);
	}

	public int getUserIdFromSession(HttpSession session)
	{
		return (Integer) session.getAttribute(USER_SESSION_NAME);
	}

	public boolean isUserInSession(HttpSession session)
	{
		return session.getAttribute(USER_SESSION_NAME) != null;
	}

	public void removeUserFromSession(HttpSession session)
	{
		session.removeAttribute(USER_SESSION_NAME);
	}
}
