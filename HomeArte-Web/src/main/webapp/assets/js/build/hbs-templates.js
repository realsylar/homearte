this["Templates"] = this["Templates"] || {};

this["Templates"]["hardwareUnit"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2=this.escapeExpression, alias3="function";

  return "<tr class=\"block\">\n	<td class=\"inline-block col-2\">\n		"
    + alias2((helpers.incr || (depth0 && depth0.incr) || alias1).call(depth0,(depth0 != null ? depth0.index : depth0),{"name":"incr","hash":{},"data":data}))
    + ")\n	</td><td class=\"inline-block col-5\">\n		"
    + alias2(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias3 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\n	</td><td class=\"inline-block col-5\">\n		"
    + alias2(((helper = (helper = helpers.brand || (depth0 != null ? depth0.brand : depth0)) != null ? helper : alias1),(typeof helper === alias3 ? helper.call(depth0,{"name":"brand","hash":{},"data":data}) : helper)))
    + "\n	</td>\n</tr>";
},"useData":true});

this["Templates"]["modal"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div id=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"modal hide\">\n	<div class=\"modal__header big center\">\n		Please confirm\n	</div><!-- .modal__header -->\n	<div class=\"modal__body\">\n		<p class=\"center line-height js-modal-text\">"
    + alias3(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"text","hash":{},"data":data}) : helper)))
    + "</p>\n		<p class=\"small center line-height js-modal-subtext\"></p>\n		<p class=\"center\">\n			<a href=\"#confirm\" class=\"btn btn--success inline-block no-states js-modal-hide js-modal-confirm\">\n				<span class=\"fa fa-check-circle fa-fw\"></span> Confirm\n			</a>\n			<a href=\"#cancel\" class=\"btn btn--error inline-block no-states js-modal-hide\">\n				<span class=\"fa fa-times-circle fa-fw\"></span> Cancel\n			</a>\n		</p>\n	</div><!-- .modal__body -->\n</div><!-- .modal -->";
},"useData":true});

this["Templates"]["product"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div id=\"prod-"
    + alias3(((helper = (helper = helpers.productId || (depth0 != null ? depth0.productId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"productId","hash":{},"data":data}) : helper)))
    + "\" class=\"sub-proj__wrapper col-4 inline-block\">\n	<div class=\"card product-card\">\n		<a href=\""
    + alias3(((helper = (helper = helpers.linkPrefix || (depth0 != null ? depth0.linkPrefix : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"linkPrefix","hash":{},"data":data}) : helper)))
    + "/products/"
    + alias3(((helper = (helper = helpers.productId || (depth0 != null ? depth0.productId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"productId","hash":{},"data":data}) : helper)))
    + "\" class=\"product-card__inner block\">\n			<div class=\"product-card__header\">\n				<img src=\""
    + alias3(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"url","hash":{},"data":data}) : helper)))
    + "\" alt=\""
    + alias3(((helper = (helper = helpers.productName || (depth0 != null ? depth0.productName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"productName","hash":{},"data":data}) : helper)))
    + "\" class=\"img--centered js-prod-thumb-img\">\n			</div>\n			<div class=\"product-card__content\">\n				<p class=\"big center no-margin ellipsis\" title=\""
    + alias3(((helper = (helper = helpers.productName || (depth0 != null ? depth0.productName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"productName","hash":{},"data":data}) : helper)))
    + "\">\n					"
    + alias3(((helper = (helper = helpers.productName || (depth0 != null ? depth0.productName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"productName","hash":{},"data":data}) : helper)))
    + "\n				</p>\n				<p class=\"center no-margin\">\n					<span class=\"fa fa-rupee fa-fw big\"></span>\n					<span class=\"big\">"
    + alias3(((helper = (helper = helpers.unitPrice || (depth0 != null ? depth0.unitPrice : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"unitPrice","hash":{},"data":data}) : helper)))
    + "</span><span class=\"small\">.00</span>\n				</p>\n			</div>\n			<div class=\"product-card__controls white center\">\n				<span class=\"fa fa-fw big\"></span> <span class=\"js-status-txt\"></span>\n			</div>\n		</a>\n	</div>\n</div>";
},"useData":true});

this["Templates"]["productUnit"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2=this.escapeExpression, alias3="function";

  return "<tr class=\"block\">\n	<td class=\"inline-block col-2\">\n		"
    + alias2((helpers.incr || (depth0 && depth0.incr) || alias1).call(depth0,(depth0 != null ? depth0.index : depth0),{"name":"incr","hash":{},"data":data}))
    + ")\n	</td><td class=\"inline-block col-2\">\n		"
    + alias2(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias3 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\n	</td><td class=\"inline-block col-2\">\n		"
    + alias2(((helper = (helper = helpers.dimension || (depth0 != null ? depth0.dimension : depth0)) != null ? helper : alias1),(typeof helper === alias3 ? helper.call(depth0,{"name":"dimension","hash":{},"data":data}) : helper)))
    + "\n	</td><td class=\"inline-block col-2\">\n		"
    + alias2(((helper = (helper = helpers.unitMaterial || (depth0 != null ? depth0.unitMaterial : depth0)) != null ? helper : alias1),(typeof helper === alias3 ? helper.call(depth0,{"name":"unitMaterial","hash":{},"data":data}) : helper)))
    + "\n	</td><td class=\"inline-block col-2\">\n		"
    + alias2(((helper = (helper = helpers.shutterMaterial || (depth0 != null ? depth0.shutterMaterial : depth0)) != null ? helper : alias1),(typeof helper === alias3 ? helper.call(depth0,{"name":"shutterMaterial","hash":{},"data":data}) : helper)))
    + "\n	</td><td class=\"inline-block col-2\">\n		"
    + alias2(((helper = (helper = helpers.shutterColor || (depth0 != null ? depth0.shutterColor : depth0)) != null ? helper : alias1),(typeof helper === alias3 ? helper.call(depth0,{"name":"shutterColor","hash":{},"data":data}) : helper)))
    + "\n	</td>\n</tr>";
},"useData":true});

this["Templates"]["project"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "		"
    + this.escapeExpression(((helper = (helper = helpers.projectDescription || (depth0 != null ? depth0.projectDescription : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"projectDescription","hash":{},"data":data}) : helper)))
    + "\n";
},"3":function(depth0,helpers,partials,data) {
    return "		No description provided\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div id=\"proj-"
    + alias3(((helper = (helper = helpers.projectId || (depth0 != null ? depth0.projectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"projectId","hash":{},"data":data}) : helper)))
    + "\" class=\"card\">\n	<span class=\"bigger\">"
    + alias3(((helper = (helper = helpers.projectName || (depth0 != null ? depth0.projectName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"projectName","hash":{},"data":data}) : helper)))
    + "</span>\n	<span class=\"small\">( created on "
    + alias3(((helper = (helper = helpers.creationTimestamp || (depth0 != null ? depth0.creationTimestamp : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"creationTimestamp","hash":{},"data":data}) : helper)))
    + " )</span>\n	<p class=\"big\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.projectDescription : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "	</p>\n	<p class=\"small\">\n		No. of sub-projects: "
    + alias3(((helper = (helper = helpers.noOfSubProjects || (depth0 != null ? depth0.noOfSubProjects : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"noOfSubProjects","hash":{},"data":data}) : helper)))
    + "\n	</p>\n	<div class=\"grid block\" style=\"position: relative\">\n		<div class=\"col col-6\">\n			<div style=\"width: 200px; position: absolute; left: 0; bottom: 0\">\n				<a href=\""
    + alias3(((helper = (helper = helpers.linkPrefix || (depth0 != null ? depth0.linkPrefix : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"linkPrefix","hash":{},"data":data}) : helper)))
    + "/dashboard/project/"
    + alias3(((helper = (helper = helpers.projectId || (depth0 != null ? depth0.projectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"projectId","hash":{},"data":data}) : helper)))
    + "/edit\">edit</a>\n				&nbsp;\n				<a href=\"#delete\" class=\"mdl-cfrm-del\" data-modal-subjective-data='{\"_id\":\""
    + alias3(((helper = (helper = helpers.projectId || (depth0 != null ? depth0.projectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"projectId","hash":{},"data":data}) : helper)))
    + "\",\"name\":\""
    + alias3(((helper = (helper = helpers.projectName || (depth0 != null ? depth0.projectName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"projectName","hash":{},"data":data}) : helper)))
    + "\"}'>delete</a>\n			</div>\n		</div>\n		<div class=\"col col-6 no-gutter align-right\">\n			<a href=\""
    + alias3(((helper = (helper = helpers.linkPrefix || (depth0 != null ? depth0.linkPrefix : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"linkPrefix","hash":{},"data":data}) : helper)))
    + "/dashboard/project/"
    + alias3(((helper = (helper = helpers.projectId || (depth0 != null ? depth0.projectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"projectId","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn--large inline-block no-states\">\n				View this project <span class=\"fa fa-angle-right fa-fw\"></span>\n			</a>\n		</div>\n	</div>\n</div>";
},"useData":true});

this["Templates"]["subProject"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div id=\"subProj-"
    + alias3(((helper = (helper = helpers.subProjectId || (depth0 != null ? depth0.subProjectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"subProjectId","hash":{},"data":data}) : helper)))
    + "\" class=\"sub-proj__wrapper col-4 inline-block\">\n	<div class=\"sub-proj card\">\n		<div class=\"sub-proj__ico center\">\n			<span class=\"fa fa-file-text-o fa-fw fa-3x\"></span>\n		</div>\n		<div class=\"sub-proj__controls grid\">\n			<div class=\"col col-6 no-gutter\">\n				<a href=\""
    + alias3(((helper = (helper = helpers.linkPrefix || (depth0 != null ? depth0.linkPrefix : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"linkPrefix","hash":{},"data":data}) : helper)))
    + "/dashboard/project/"
    + alias3(((helper = (helper = helpers.projectId || (depth0 != null ? depth0.projectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"projectId","hash":{},"data":data}) : helper)))
    + "/sub-project/"
    + alias3(((helper = (helper = helpers.subProjectId || (depth0 != null ? depth0.subProjectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"subProjectId","hash":{},"data":data}) : helper)))
    + "/edit\" class=\"small\" title=\"Edit\">\n					<span class=\"fa fa-pencil-square fa-fw\"></span>\n				</a>\n			</div>\n			<div class=\"col col-6 no-gutter align-right\">\n				<a href=\"#\" class=\"small error mdl-cfrm-del\" data-modal-subjective-data='{\"_id\":\""
    + alias3(((helper = (helper = helpers.subProjectId || (depth0 != null ? depth0.subProjectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"subProjectId","hash":{},"data":data}) : helper)))
    + "\",\"name\":\""
    + alias3(((helper = (helper = helpers.subProjectName || (depth0 != null ? depth0.subProjectName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"subProjectName","hash":{},"data":data}) : helper)))
    + "\"}' title=\"Delete\">\n					<span class=\"fa fa-times-circle fa-fw\"></span>\n				</a>\n			</div>\n		</div>\n		<div class=\"big center\">\n			"
    + alias3(((helper = (helper = helpers.subProjectName || (depth0 != null ? depth0.subProjectName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"subProjectName","hash":{},"data":data}) : helper)))
    + "\n		</div>\n		<p class=\"smaller center\">\n			Created on "
    + alias3(((helper = (helper = helpers.creationTimestamp || (depth0 != null ? depth0.creationTimestamp : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"creationTimestamp","hash":{},"data":data}) : helper)))
    + "\n		</p>\n		<div class=\"center\">\n			<a href=\""
    + alias3(((helper = (helper = helpers.linkPrefix || (depth0 != null ? depth0.linkPrefix : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"linkPrefix","hash":{},"data":data}) : helper)))
    + "/dashboard/project/"
    + alias3(((helper = (helper = helpers.projectId || (depth0 != null ? depth0.projectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"projectId","hash":{},"data":data}) : helper)))
    + "/sub-project/"
    + alias3(((helper = (helper = helpers.subProjectId || (depth0 != null ? depth0.subProjectId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"subProjectId","hash":{},"data":data}) : helper)))
    + "\">\n				view in detail <span class=\"fa fa-angle-right fa-fw\"></span>\n			</a>\n		</div>\n	</div>\n</div>";
},"useData":true});

this["Templates"]["suppFileUploaded"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<td class=\"inline-block col-3\">\n	<a href=\""
    + alias3(((helper = (helper = helpers.cLink || (depth0 != null ? depth0.cLink : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"cLink","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</a>\n</td><td class=\"inline-block col-4 center\">\n	"
    + alias3(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"type","hash":{},"data":data}) : helper)))
    + "\n</td><td class=\"inline-block col-4 center\">\n	Added on "
    + alias3(((helper = (helper = helpers.createdOn || (depth0 != null ? depth0.createdOn : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"createdOn","hash":{},"data":data}) : helper)))
    + "\n</td><td class=\"inline-block col-1 align-right\">\n	<a href=\"#\" class=\"error mdl-cfrm-del-fp-files\" data-modal-subjective-data='{\"_id\":\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\",\"name\":\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\"}'>remove</a>\n</td>";
},"useData":true});

this["Templates"]["suppFileUploading"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<tr id=\""
    + alias3(((helper = (helper = helpers.tmpId || (depth0 != null ? depth0.tmpId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"tmpId","hash":{},"data":data}) : helper)))
    + "\" class=\"block js-supp-file-uploading\">\n	<td class=\"inline-block col-4\">\n		<span class=\"fa fa-spinner fa-spin\"></span> "
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\n	</td><td class=\"inline-block col-4 center\">\n	</td><td class=\"inline-block col-4 align-right\">\n		<a href=\"#\" class=\"error js-remove-supp-file\">remove</a>\n	</td>\n</tr>";
},"useData":true});