/**
 * @desc Web app's client-side common methods
 * @author Navdeep
 * @dependsOn null
 */


// A global common helper methods namespace object:
var CommonHelpers = {};

/**
 * @desc
 * Returns the value of the content attribute of a "meta" tag
 *
 * @param
 * - name {string} The name of the "meta" tag
 */
CommonHelpers.getMetaValue = function( name ) {
	var meta = document.querySelector( "meta[name='" + name + "']" );
	return meta.content || meta.getAttribute( "content" ) || "";
};

/**
 * @desc
 * Executes a callback after a delay:
 *
 * @param
 * - callback {function} The callback to execute
 * - delay    {number}   Delay in milliseconds
 */
CommonHelpers.executeCallbackAfterDelay = (function(){
	var timer = null;
	return function( callback, delay ) {
		clearTimeout( timer );
		timer = setTimeout( callback, delay );
	};
})();
