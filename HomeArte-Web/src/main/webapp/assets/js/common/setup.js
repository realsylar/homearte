/**
 * @desc Web app's client-side setup
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 *   + common/methods.js
 */


// Attach necessary headers for every subsequent AJAX request
$.ajaxSetup({
	headers: {
		"X-CSRF-TOKEN" : CommonHelpers.getMetaValue( "_csrf" ),
		"Accept"       : "application/json",
		"Content-Type" : "application/json"
	}
});