/**
 * @desc Web app's footer interaction
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 *   + common/methods.js
 *   + validators/email.js
 */


$( document ).ready( function() {

	// Get the link/API prefix:
	var linkPrefix = CommonHelpers.getMetaValue( "link-prefix" );

	$( "#newsLetterForm" ).on( "submit", function( e ) {
		e.preventDefault();

		// Get the email from the form:
		var $email = $( this ).find( "input" );
		var $msg   = $( ".js-newsletter-msg" );
		var email  = $email.val().trim();

		// If "email" is truthy and valid, proceed to submit it:
		if( email && EmailValidator.isValid( email ) ) {

			// Reset the message text:
			$msg.text( "" );

			// And proceed for AJAX request:
			$.ajax({
				url    : linkPrefix + "/subscribe",
				method : "POST",
				data   : JSON.stringify({ email: email }),
				success: function( response ) {
					var ajaxResponse = response.ajaxResponse;
					if( ajaxResponse.status === 200 ) {
						// Reset the input field:
						$email.val( "" );

						// And show the message on the DOM:
						$msg
							.removeClass( "error" )
							.addClass( "success" )
							.text( ajaxResponse.message );
						console.log( ajaxResponse.message );
					}
				}
			});
		}
		else {
			// Show the error message on the DOM:
			$msg
				.removeClass( "success" )
				.addClass( "error" )
				.text( "Invalid email address, please try again..." );
			console.log( "Invalide email: {" + email + "}" );
		}
	});

});