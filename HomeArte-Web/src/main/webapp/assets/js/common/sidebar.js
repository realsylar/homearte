/**
 * @desc Web app's sidebar interaction
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 */


$( document ).ready( function() {

	// Constants:
	var TRANSITION_INTERVAL = 0.3; // in `seconds`
	var DATA_ATTR_VISIBLE   = "visible";

	// Variables containing jQuery elements:
	var $sidebar        = $( ".sidebar" );
	var $sidebarOverlay = $( ".sidebar__overlay" );
	var $sidebarToggle  = $( ".js-toggle-sidebar" );

	// Other variables:
	var sidebarWidth = $sidebar.outerWidth();

	// The main toggle function:
	var toggle = function() {
		// Find the current visible state of the sidebar:
		var isVisible = $sidebar.data( DATA_ATTR_VISIBLE );

		// Calculate the `left` property value based on whether
		// the sidebar is visible or not:
		// If it is visible, we need to take it out of the viewport
		// by giving it a negative value.
		// If it is visible, we need to show it by setting the left
		// property to 0 ( which is relative to the document ).
		var leftVal = isVisible ? -1 * sidebarWidth : 0;

		$sidebar
			// Animate the sidebar's `left` property:
			.animate({
				"left" : leftVal + "px"
			}, TRANSITION_INTERVAL * 1000 )

			// Toggle the "visible" state of the sidebar:
			.data( DATA_ATTR_VISIBLE, !isVisible );

		// And finally, toggle the overlay visibility too:
		$sidebarOverlay.toggleClass( "hide" );
	};

	// Handle the click event on the toggles:
	$sidebarToggle.on( "click", function( event ) {
		event.preventDefault();
		toggle();
	});

	// Handle the click event on the overlay:
	$sidebarOverlay.on( "click", function() {
		toggle();
	});

});