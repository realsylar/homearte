/**
 * @desc Email validator
 * @author Navdeep
 * @depenedsOn null
 */


var EmailValidator = {

	// The email regex:
	filter: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,

	// This function returns `true` if email is valid
	// and otherwise `false`:
	isValid: function( email ) {
		if ( this.filter.test( email ) ) {
			return true;
		}
		return false;
	}

};