/**
 * @desc Web app's template utils
 * @author Navdeep
 * @dependsOn
 *   + common/methods.js
 *   + build/hbs-templates.js
 */


// Get the link/API prefix:
var linkPrefix = CommonHelpers.getMetaValue( "link-prefix" );

// The template utils hash:
var TemplateUtils = {

	render: function( tplName, domNode, data ) {
		var toRender = "";

		if( Array.isArray( data ) ) {
			data.forEach( function( datum, idx ) {
				// Add mandatory properties:
				datum.linkPrefix = linkPrefix;

				// Add index property for arrays:
				datum.index = idx;

				toRender += Templates[ tplName ]( datum );
			});
		}
		else {
			// Add mandatory properties:
			data.linkPrefix = linkPrefix;

			toRender = Templates[ tplName ]( data );
		}

		domNode.innerHTML += toRender;
	}

};


// Register helpers:
Handlebars.registerHelper( "incr", function( num ) {
	return ++num;
});