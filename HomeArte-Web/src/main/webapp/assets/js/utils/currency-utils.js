/**
 * @desc Web app's currency utils
 * @author Navdeep
 */


var CurrencyUtils = {

	// The separator to be used in the currency:
	// If it is "," then price will be "20,000"
	// If it is "-" then price will be "20-000"
	// and so on...
	priceSeparator: ",",

	/**
	 * @desc Returns formated currency in "00,00,000" format
	 *
	 * @example  20000 ->   20,000
	 * @example 200000 -> 2,00,000
	 *
	 */
	formatNumber: function( number ) {
		var retValue = "";
		number = number.toString();

		if( number.length <= 3 ) return number;

		retValue += this.priceSeparator + number.substr( number.length - 3 );

		var k = ( number.length - 3 ) - 1;
		for( var idx = k; idx >= 0; idx-- ) {
			var sep = ( idx - k ) % 2 !== 0 ? this.priceSeparator : "";
			retValue = sep + number[ idx ] + retValue;
		}

		if( retValue.startsWith( this.priceSeparator ) ) {
			retValue = retValue.substr( 1 );
		}

		return retValue;
	}

};