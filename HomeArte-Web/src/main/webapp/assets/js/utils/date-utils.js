/**
 * @desc Web app's date utils
 * @author Navdeep
 */


var DateUtils = {

	// Array containing full names of all the months:
	months: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],

	/**
	 * @desc Returns date in "<month> <d>, <year>" format
	 * @param dateInLong {long}
	 */
	getReadableDate: function( dateInLong ) {
		var date = new Date( dateInLong );
		return this.months[ date.getMonth() ] + " " + date.getDate() + ", " + date.getFullYear() ;
	},

	/**
	 * @desc Sets the date on the DOM
	 * @param domNode {DOM}
	 */
	setCurrDate: function( domNode ) {
		domNode.innerText = this.getReadableDate( Date.now() );
	}

};