/**
 * @desc Web app's sub-project page
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 *   + common/methods.js
 *   + modules/modal.js
 *   + utils/template-utils.js
 *   + utils/currency-utils.js
 */


$( document ).ready( function() {

	// Get the meta values:
	var linkPrefix   = CommonHelpers.getMetaValue( "link-prefix" );
	var projectId    = CommonHelpers.getMetaValue( "proj-id" );
	var subProjectId = CommonHelpers.getMetaValue( "sub-proj-id" );

	// Constants:
	var SUPP_FILE_ID_PREFIX   = "sfile-";
	var PRODUCT_ID_PREFIX     = "prod-";
	var TPL_SUPP_FILE_UPLDING = "suppFileUploading";
	var TPL_SUPP_FILE_UPLDED  = "suppFileUploaded";
	var TPL_PRODUCT_CARD      = "product";
	var DATA_ATTR_FP_ID       = "data-fp-id";
	var URL_PREFIX            = linkPrefix + "/dashboard/project/" + projectId + "/sub-project/" + subProjectId;
	var PRODUCT_STATUS = {
		ACCEPT    : 0,
		REDESIGN  : 1,
		REJECT    : 2,
		UNDEFINED : 3
	};
	var COLORS = {
		GREEN  : "#28c684",
		ORANGE : "#f7b944",
		RED    : "#fe4365",
	};

	// Variables containing jQuery elements:
	var $floorPlan        = $( "#floorPlan" );
	var $floorPlanLoading = $( "#floorPlanLoading" );
	var $floorPlanToAdd   = $( "#floorPlanToAdd" );
	var $floorPlanAdded   = $( "#floorPlanAdded" );
	var $subProjName      = $( "#subProjName" );
	var $subProjDesc      = $( "#subProjDesc" );

	// Variables:
	var suppFileDomNode = document.getElementById( "suppFiles" );
	var prodDomNode     = document.getElementById( "prods" );
	var prodCardWidth   = Math.round( $( ".content--dashboard" ).width() / 3 );
	var jsonHeader      = {
		"Accept"       : "application/json",
		"Content-Type" : "application/json"
	};

	/**
	 * @desc Sends a request to delete a specified file via AJAX
	 *
	 * @param id        {string}   The ID of the file to delete
	 * @param onSuccess {function} The callback to execute on success
	 */
	var deleteFile = function( id, onSuccess ) {
		console.log( "ID of file to delete: [" + id + "]" );

		// If ID is emtpy, do nothing:
		if( !id ) return;

		// Proceed with AJAX:
		$.ajax({
			headers: jsonHeader,
			url    : URL_PREFIX + "/del",
			method : "POST",
			data   : JSON.stringify({ cid: id }),
			success: function( response ) {
				console.log( "Response for deleting file:", response, "; id:", id );
				var ajaxResponse = response.ajaxResponse;

				// Check for successful response:
				if( ajaxResponse.status === 200 ) {
					// Check the type of the callback:
					if( typeof onSuccess === "function" ) {
						// Call the callback:
						onSuccess.call( this, ajaxResponse.values );
					}
					else {
						console.log( "onSuccess is not a function - ", onSuccess );
					}
				}
			},
			error: function( response ) {
				console.log( "Error occurred for deleting file:", response, "; id:", id );
			}
		});
	};

	/**
	 * @desc Sets the background color card's status band
	 *       according to the status code
	 *
	 * @param $card      {element} The jQuery ref. to the card
	 * @param statusCode {number}  The status code
	 */
	var coloriseCard = function( $card, statusCode ) {
		var bkgColor   = "black";
		var statusIcon = "fa-circle";
		var statusText = "Unknown";
		var $controls  = $card.find( ".product-card__controls" );

		switch( statusCode ) {
			case PRODUCT_STATUS.ACCEPT:
				bkgColor   = COLORS.GREEN;
				statusIcon = "fa-check-circle";
				statusText = "Accepted";
				break;
			case PRODUCT_STATUS.REDESIGN:
				bkgColor   = COLORS.ORANGE;
				statusIcon = "fa-refresh";
				statusText = "Redesign";
				break;
			case PRODUCT_STATUS.REJECT:
				bkgColor   = COLORS.RED;
				statusIcon = "fa-times-circle";
				statusText = "Rejected";
				break;
			default:
				return;
		}

		$controls.css({ "background": bkgColor });
		$controls.find( ".fa" ).addClass( statusIcon );
		$controls.find( ".js-status-txt" ).text( statusText );
	};

	// Configure the floor plan uploader:
	var floorPlanUploader = new Uploader({
		facadeId: "floorPlanUploaderFacade",
		realId  : "floorPlanUploaderReal",
		onUpload: function( event, file ) {
			var image = new Image();
			image.src = event.target.result;
			image.onload = function() {
				// Do DOM changes:
				$floorPlanToAdd.hide();
				$floorPlanAdded.show().find( "img" ).attr({
					src   : this.src,
					width : Math.min( $floorPlan.width(), this.width )
				}).parent( "a" ).attr({
					href  : this.src
				});

				// Hide the overlay saying 'uploading':
				$floorPlanAdded.find( ".hero__overlay" ).hide();

				// Fill the form data:
				var form = new FormData();
				form.append( "file", file );

				// And proceed to submit data via AJAX:
				$.ajax({
					url         : URL_PREFIX + "/fsub",
					method      : "POST",
					data        : form,
					dataType    : "json",
					processData : false,
					contentType : false,
					success: function( response ) {
						console.log( "Floor plan upload response:", response );
						var ajaxResponse = response.ajaxResponse;

						// Check for successful response:
						if( ajaxResponse.status === 200 ) {
							// Hide the overlay saying 'uploading':
							$floorPlanAdded.find( ".hero__overlay" ).hide();

							// And set the id in the remove link:
							$floorPlanAdded.find( ".mdl-cfrm-del-fp" ).attr( DATA_ATTR_FP_ID, ajaxResponse.values.id );
						}
						else {
							$floorPlanAdded.hide();
							$floorPlanToAdd.show();
						}
					},
					error: function( response ) {
						console.log( "Floor plan upload error:", response );
						$floorPlanAdded.hide();
						$floorPlanToAdd.show();
					}
				});
			};
		}
	});

	// Configure the supp. file uploader:
	var supFileUploader = new Uploader({
		facadeId: "supFileUploaderFacade",
		realId  : "supFileUploaderReal",
		onUpload: function( event, file ) {
			// Make a temporary id to get a selector to the
			// current element:
			var tmpId = SUPP_FILE_ID_PREFIX + Date.now();

			// Render the 'uploading' element for supp. file:
			TemplateUtils.render( TPL_SUPP_FILE_UPLDING, suppFileDomNode, {
				tmpId : tmpId,
				name  : file.name
			});

			// Initialize the modal because suppFileDomNode has been recreated
			// and all the events have been reset:
			delSuppFile.init();

			// Fill the form data:
			var form = new FormData();
			form.append( "file", file );

			// And proceed to submit data via AJAX:
			$.ajax({
				url         : URL_PREFIX + "/ssub",
				method      : "POST",
				data        : form,
				dataType    : "json",
				processData : false,
				contentType : false,
				success: function( response ) {
					console.log( "Supp. file upload response:", response );
					var ajaxResponse = response.ajaxResponse;

					// Check for successful response:
					if( ajaxResponse.status === 200 ) {
						// Strategy:
						// Obtain an element ref. to the 'uploading' element
						// and replace its contents with the equivalend
						// 'uploaded' element...

						// Obtain the ref.
						var fileNode = suppFileDomNode.querySelector( "#" + tmpId );

						// Remove the 'uploading' contents:
						fileNode.innerHTML = "";

						// Append a row with the 'uploaded' file data:
						TemplateUtils.render( TPL_SUPP_FILE_UPLDED, fileNode, {
							id        : ajaxResponse.values.id,
							name      : ajaxResponse.values.name,
							type      : ajaxResponse.values.type,
							createdOn : ajaxResponse.values.creationTimestamp,
							cLink     : URL_PREFIX + "/cget/" + ajaxResponse.values.id
						});

						// Initialize the delete supp. file modal:
						delSuppFile.init();
					}
					else {
						console.log( "Supp. file upload error in success() cb:", ajaxResponse.error, "; tmpId:", tmpId );
						$( "#" + tmpId ).find( ".fa" ).toggleClass( "fa-spinner fa-spin fa-times-circle" );
					}
				},
				error: function( response ) {
					console.log( "Supp. file upload error:", response, "; tmpId:", tmpId );
					$( "#" + tmpId ).find( ".fa" ).toggleClass( "fa-spinner fa-spin fa-times-circle" );
				}
			});
		}
	});

	// Configure the 'delete floor plan' modal:
	var delFloorPlan = new Modal({
		text          : "Are you sure you want to remove the floor plan?",
		groupSelector : ".mdl-cfrm-del-fp",
		onConfirm     : function() {
			deleteFile( $( ".mdl-cfrm-del-fp" ).attr( DATA_ATTR_FP_ID ), function( data ) {
				// Do DOM changes:
				$floorPlanAdded.hide();
				$floorPlanToAdd.show();

				// And reset the id in the remove link:
				$( ".mdl-cfrm-del-fp" ).attr( DATA_ATTR_FP_ID, "" );

				// Show the overlay:
				$floorPlanAdded.find( ".hero__overlay" ).show();
			});
		}
	}).init();

	// Configure the 'delete supp. file' modal:
	var delSuppFile = new Modal({
		text          : "Are you sure you want to remove this file?",
		groupSelector : ".mdl-cfrm-del-fp-files",
		onConfirm     : function( data ) {
			deleteFile( data._id, function() {
				// Do DOM changes:
				$( "#" + SUPP_FILE_ID_PREFIX + data._id ).remove();
			});
		}
	});

	// Add event listener to remove the supp. file uploading element:
	$( document ).on( "click", ".js-remove-supp-file", function( e ) {
		e.preventDefault();
		$( this ).closest( ".js-supp-file-uploading" ).remove();
	});

	// Get the floor plan via AJAX:
	$.ajax({
		headers: jsonHeader,
		url    : URL_PREFIX + "/gfp",
		method : "POST",
		data   : JSON.stringify({}),
		success: function( response ) {
			console.log( "Response for floor plan get:", response );
			var ajaxResponse = response.ajaxResponse;

			// Check for successful response:
			if( ajaxResponse.status === 200 ) {
				$floorPlanLoading.hide();

				// Check if a floor plan was already uploaded:
				if( ajaxResponse.values.cdto ) {
					var cdto  = JSON.parse( ajaxResponse.values.cdto );
					var fpSrc = URL_PREFIX + "/cget/" + cdto.contentId;

					console.log( "Floor plan image get src:", fpSrc );

					// Make a pseudo image to get the stored image's width:
					var pseudoImage = new Image();
					pseudoImage.src = fpSrc;

					// When the image has loaded, change the DOM:
					pseudoImage.onload = function() {
						// Set the image attributes:
						$floorPlanAdded.show().find( "img" ).attr({
							src   : this.src,
							width : Math.min( $floorPlan.width(), this.width )
						}).parent( "a" ).attr({
							href  : this.src
						});

						// Hide the overlay saying 'uploading':
						$floorPlanAdded.find( ".hero__overlay" ).hide();
					};

					// Set the id in the remove link:
					$floorPlanAdded.find( ".mdl-cfrm-del-fp" ).attr( DATA_ATTR_FP_ID, cdto.contentId );
				}
				else {
					$floorPlanToAdd.show();
				}
			}
		},
		error: function( response ) {
			console.log( "Error occurred in floor plan get:", response );
		}
	});

	// Get the supp. files via AJAX:
	$.ajax({
		headers: jsonHeader,
		url    : URL_PREFIX + "/sflist",
		method : "POST",
		data   : JSON.stringify({}),
		success: function( response ) {
			console.log( "Response for supp. files. get:", response );
			var ajaxResponse = response.ajaxResponse;

			// Check for successful response:
			if( ajaxResponse.status === 200 ) {
				// Check if files exist:
				if( ajaxResponse.values.slist && ajaxResponse.values.slist.length ) {
					ajaxResponse.values.slist.forEach( function( file ) {
						var fileNode = document.createElement( "tr" );

						// Add necessary classes and attributes:
						fileNode.classList.add( "block" );
						fileNode.setAttribute( "id", SUPP_FILE_ID_PREFIX + file.contentId );

						// Append a row with the 'uploaded' file data:
						TemplateUtils.render( TPL_SUPP_FILE_UPLDED, suppFileDomNode.appendChild( fileNode ), {
							id        : file.contentId,
							name      : file.contentName,
							type      : file.contentType,
							createdOn : file.creationTimestamp,
							cLink     : URL_PREFIX + "/cget/" + file.contentId
						});

						// Initialize the delete supp. file modal:
						delSuppFile.init();
					});
				}
			}
		}
	});

	// Get the products via AJAX:
	$.ajax({
		url    : URL_PREFIX + "/hplist",
		method : "POST",
		data   : JSON.stringify({}),
		success: function( response ) {
			console.log( "Response for hplist:", response );

			if( typeof response === "string" ) {
				response = JSON.parse( response );
			}

			var ajaxResponse = response.ajaxResponse;

			if( ajaxResponse.status === 200 && ajaxResponse.values.hplist ) {
				ajaxResponse.values.hplist.forEach( function( item ) {
					// Make the image url:
					item.url = linkPrefix + "/products/" + item.productId + "/hpiget?fin=&col=&thumb=true";

					// Format the price:
					item.unitPrice = CurrencyUtils.formatNumber( item.unitPrice );

					// Render product on DOM:
					TemplateUtils.render( TPL_PRODUCT_CARD, prodDomNode, item );

					// Borderise it:
					coloriseCard( $( "#" + PRODUCT_ID_PREFIX + item.productId ), item.status );

					// And set proper widths to images:
					$( "img.js-prod-thumb-img" ).on( "load", function( e ) {
						var $this = $( this );
						$this.width( Math.min( prodCardWidth, $this.width() ) );
					});
				});
			}
		},
		error: function( response ) {
			console.log( "Error occurred for hplist:", response );
		}
	});

	// Define the "on editable out of focus" function that will
	// be executed whenever an editable element goes out of focus:
	window.onEditableBlur = function( oldText, newText ) {
		var json = JSON.stringify({
			name        : $subProjName.text(),
			description : $subProjDesc.text()
		});

		console.log( "Sending JSON: " + json );

		$.ajax({
			url    : URL_PREFIX + "/edit/sub",
			method : "POST",
			data   : json,
			success: function( response ) {
				console.log( "Success:", response );
			},
			error: function( response ) {
				console.log( "Error occurred:", response );
			}
		});
	};

});