/**
 * @desc Web app's login page
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 */


$( document ).ready( function() {

	// Login form submit:
	$( ".js-login-form" ).on( "submit", function( e ) {
		// Get the form values:
		var email    = $.trim( $( this ).find( "input[name='email']" ).val() );
		var password = $.trim( $( this ).find( "input[name='password']" ).val() );

		// Get the jquery element to show validation messages:
		var $msg = $( this ).find( ".js-notif" );

		// Validate the fields:
		if( !email ) {
			$msg.text( "Please provide an email address" );
			e.preventDefault();
		}
		else if( !password ) {
			$msg.text( "Please provide a password" );
			e.preventDefault();
		}
	});

});