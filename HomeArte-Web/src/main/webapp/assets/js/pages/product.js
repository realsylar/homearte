/**
 * @desc Web app's product page
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 *   + common/methods.js
 *   + utils/template-utils.js
 *   + utils/currency-utils.js
 */


$( document ).ready( function() {

	// Get the meta values:
	var linkPrefix = CommonHelpers.getMetaValue( "link-prefix" );
	var productId  = CommonHelpers.getMetaValue( "prod-id" );

	// Constants:
	var PRODUCT_STATUS = {
		ACCEPT    : 0,
		REDESIGN  : 1,
		REJECT    : 2,
		UNDEFINED : 3
	};
	var DATA_ATTR_PRICE = "price";
	var DATA_ATTR_STATUS_CODE = "status-code";
	var TPL_PRODUCT_UNIT  = "productUnit";
	var TPL_HARDWARE_UNIT = "hardwareUnit";
	var URL_PREFIX = linkPrefix + "/products/" + productId;

	// Variables containing jQuery elements:
	var $finDropdown = $( "#fin" );
	var $colDropdown = $( "#col" );
	var $unitPrice   = $( "#unitPrice" );
	var $prodImg     = $( "#prodImg" );
	var $prodName    = $( "#prodName" );
	var $prodShape   = $( "#prodShape" );
	var $prodDim     = $( "#prodDim" );
	var $prodStatus  = $( "#prodStatus" );
	var $prodStatusLoader = $( "#prodStatusLoader" );

	// Other variables:
	var basePrice = 0;
	var currentStatus = -1;
	var isStatusUpdateInProgress = true;
	var baseUnitsDomNode = document.getElementById( "baseUnits" );
	var wallUnitsDomNode = document.getElementById( "wallUnits" );
	var hardwareDomNode  = document.getElementById( "hardware" );

	/**
	 * @desc Updates the product info like unit price and the
     *       display picture on DOM on the basis of the currently
	 *       selected product specifications such as finish,
	 *       color, etc.
	 */
	var updateProductInfo = function() {
		var $fin = $finDropdown.find( "option:selected" );
		var $col = $colDropdown.find( "option:selected" );

		// Update price:
		var newPrice = basePrice + $fin.data( DATA_ATTR_PRICE ) + $col.data( DATA_ATTR_PRICE );
		console.log( "New price:", newPrice );
		$unitPrice.text( CurrencyUtils.formatNumber( newPrice ) );

		// Update picture:
		var imgUrl = URL_PREFIX + "/hpiget?fin=" + $fin.val() + "&col=" + $col.val() + "&thumb=false";
		console.log( "New product image url:", imgUrl );
		$prodImg.attr( "src", imgUrl );
	};

	/**
	 * @desc Updates the product status on the DOM
	 *
	 * @param status {number} The status code. Its value
	 *                        belongs to the PRODUCT_STATUS
	 *                        enum.
	 */
	var updateProductStatus = function( status ) {
		var idx = -1;

		// Reset the status controls:
		$prodStatus.find( ".col" ).each( function() {
			$( this ).find( "a" ).removeClass( "hide" );
			$( this ).find( ".js-static-status" ).addClass( "hide" );
		});

		switch( status ) {
			case PRODUCT_STATUS.ACCEPT:
				idx = 0;
				currentStatus = status;
				break;
			case PRODUCT_STATUS.REDESIGN:
				idx = 1;
				currentStatus = status;
				break;
			case PRODUCT_STATUS.REJECT:
				idx = 2;
				currentStatus = status;
				break;
			default:
				console.log( "Invalid status: [" + status + "]" );
				return;
		}

		// Set the status on UI:
		var $col = $prodStatus.find( ".col" ).eq( idx );
		$col.find( "a" ).toggleClass( "hide" );
		$col.find( ".js-static-status" ).toggleClass( "hide" );
	};

	// Handle dropdown changes:
	$finDropdown.on( "change", function() {
		updateProductInfo();
	});
	$colDropdown.on( "change", function() {
		updateProductInfo();
	});

	// Handle status change requests:
	$prodStatus.find( "a" ).on( "click", function( e ) {
		e.preventDefault();

		// If a status update is in progress, don't
		// proceed further:
		if( isStatusUpdateInProgress ) return;

		// Get the requested status:
		var requestedStatus = $( this ).data( DATA_ATTR_STATUS_CODE );

		// Set the progress flag:
		isStatusUpdateInProgress = true;
		$prodStatusLoader.toggleClass( "hide" );

		// Send status change request to backend:
		$.ajax({
			url    : URL_PREFIX + "/hpupdatestatus",
			method : "POST",
			data   : JSON.stringify({
				currentStatus   : currentStatus,
				requestedStatus : requestedStatus
			}),
			complete: function( response ) {
				// Unset the progress flag:
				isStatusUpdateInProgress = false;
				$prodStatusLoader.toggleClass( "hide" );
			},
			success: function( response ) {
				console.log( "Response for status update:", response );
				var ajaxResponse = response.ajaxResponse;

				if( ajaxResponse.status === 200 ) {
					// Change status:
					updateProductStatus( requestedStatus );
				}
			},
			error: function( response ) {
				console.log( "Error occurred for status update:", response );
			}
		});
	});

	// Get the product specifications:
	$.ajax({
		url    : URL_PREFIX + "/cspecs",
		method : "POST",
		success: function( response ) {
			console.log( "Response for getting product specs:", response );
			var ajaxResponse = response.ajaxResponse;

			if( ajaxResponse.status === 200 ) {
				var cspecs = JSON.parse( ajaxResponse.values.cspecs );

				// Render product finishes:
				( cspecs.fin || [] ).forEach( function( item ) {
					$( "<option>" )
						.val( item.uname )
						.text( item.dname )
						.data( DATA_ATTR_PRICE, item.price )
						.prop( "selected", item.isDefault )
						.appendTo( $finDropdown );
				});

				// Render product colors:
				( cspecs.col || [] ).forEach( function( item ) {
					$( "<option>" )
						.val( item.uname )
						.text( item.dname )
						.data( DATA_ATTR_PRICE, item.price )
						.prop( "selected", item.isDefault )
						.appendTo( $colDropdown );
				});

				// Set and render the unit price:
				basePrice = cspecs.unitPrice;
				$unitPrice.text( cspecs.unitPrice );

				// Set name, shape and dimension:
				$prodName.text( cspecs.name );
				$prodShape.text( cspecs.shape );
				$prodDim.text( cspecs.dimension );

				// Update product info:
				updateProductInfo();
			}
		},
		error: function( response ) {
			console.log( "Error occurred for getting product specs:", response );
		}
	});

	// Get the product status:
	$.ajax({
		url    : URL_PREFIX + "/hpstatus",
		method : "POST",
		complete: function( response ) {
			// Unset the progress flag:
			isStatusUpdateInProgress = false;
			$prodStatusLoader.toggleClass( "hide" );
		},
		success: function( response ) {
			console.log( "Response for product status:", response );
			var ajaxResponse = response.ajaxResponse;

			if( ajaxResponse.status === 200 ) {
				updateProductStatus( ajaxResponse.values.hpstatus );
			}
		},
		error: function( response ) {
			console.log( "Error occurred for product status:", response );
		}
	});

	// Get the product units:
	$.ajax({
		url    : URL_PREFIX + "/hpunits",
		method : "POST",
		success: function( response ) {
			console.log( "Response for product units:", response );
			var ajaxResponse = response.ajaxResponse;

			if( ajaxResponse.status === 200 ) {
				// Render base units:
				TemplateUtils.render( TPL_PRODUCT_UNIT, baseUnitsDomNode, ajaxResponse.values.baseUnits || [] );

				// Render wall units:
				TemplateUtils.render( TPL_PRODUCT_UNIT, wallUnitsDomNode, ajaxResponse.values.wallUnits || [] );
			}
		},
		error: function( response ) {
			console.log( "Error occurred for product units:", response );
		}
	});

	// Get the product hardware units:
	$.ajax({
		url    : URL_PREFIX + "/hphardware",
		method : "POST",
		success: function( response ) {
			console.log( "Response for product hardware:", response );
			var ajaxResponse = response.ajaxResponse;

			if( ajaxResponse.status === 200 ) {
				// Render hardware list:
				TemplateUtils.render( TPL_HARDWARE_UNIT, hardwareDomNode, ajaxResponse.values.hardwareList );
			}
		},
		error: function( response ) {
			console.log( "Error occurred for product hardware:", response );
		}
	});

});