/**
 * @desc Web app's contact us page
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 *   + EmailValidator
 */


$( document ).ready( function() {

	// Contact us form submit:
	$( ".js-contact-form" ).on( "submit", function( e ) {
		// Get the form values:
		var name    = $.trim( $( this ).find( "input[name='name']" ).val() );
		var email   = $.trim( $( this ).find( "input[name='email']" ).val() );
		var subject = $.trim( $( this ).find( "input[name='subject']" ).val() );
		var query   = $.trim( $( this ).find( "input[name='query']" ).val() );

		// Get the jquery element to show validation messages:
		var $msg = $( this ).find( ".js-notif" );

		// Validate the fields:
		if( !name ) {
			$msg.text( "Please provide your name" );
			e.preventDefault();
		}
		else if( !email ) {
			$msg.text( "Please provide an email address" );
			e.preventDefault();
		}
		else if( !EmailValidator.isValid( email ) ) {
			$msg.text( "Please provide a valid email address" );
			e.preventDefault();
		}
	});

});