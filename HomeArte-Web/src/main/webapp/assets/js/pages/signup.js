/**
 * @desc Web app's signup page
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 *   + EmailValidator
 */


$( document ).ready( function() {

	// Signup form submit:
	$( ".js-signup-form" ).on( "submit", function( e ) {
		// Get the form values:
		var fname     = $.trim( $( this ).find( "input[name='firstName']" ).val() );
		var lname     = $.trim( $( this ).find( "input[name='lastName']" ).val() );
		var email     = $.trim( $( this ).find( "input[name='email']" ).val() );
		var password  = $.trim( $( this ).find( "input[name='password']" ).val() );
		var password2 = $.trim( $( this ).find( "input[name='confirmPassword']" ).val() );

		// Get the jquery element to show validation messages:
		var $msg = $( this ).find( ".js-notif" );

		// Validate the fields:
		if( !fname ) {
			$msg.text( "Please provide a first name" );
			e.preventDefault();
		}
		else if( !lname ) {
			$msg.text( "Please provide a last name" );
			e.preventDefault();
		}
		else if( !email ) {
			$msg.text( "Please provide an email address" );
			e.preventDefault();
		}
		else if( !password ) {
			$msg.text( "Please provide a password" );
			e.preventDefault();
		}
		else if( !password2 ) {
			$msg.text( "Please confirm the password" );
			e.preventDefault();
		}
		else if( !EmailValidator.isValid( email ) ) {
			$msg.text( "Please provide a valid email address" );
			e.preventDefault();
		}
		else if( password !== password2 ) {
			$msg.text( "Passwords don't match" );
			e.preventDefault();
		}
		else if( !PasswordValidator.isValid( password ) ) {
			$msg.text( "The password must be at least 8 characters long and have an alphabet, a special character and a number" );
			e.preventDefault();
		}
	});

});