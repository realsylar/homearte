/**
 * @desc Web app's project page
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 *   + common/methods.js
 *   + utils/template-utils.js
 *   + modules/modal.js
 */


$( document ).ready( function() {

	// Get the meta values:
	var linkPrefix = CommonHelpers.getMetaValue( "link-prefix" );
	var builderId  = CommonHelpers.getMetaValue( "builder-id" );
	var projectId  = CommonHelpers.getMetaValue( "proj-id" );

	// Constants:
	var TPL_SUB_PROJ = "subProject";
	var URL_PREFIX   = linkPrefix + "/dashboard/project/" + projectId;

	// Variables:
	var subprojsDomNode = document.getElementById( "subProjs" );

	// Variables containing jQuery elements:
	var $projName = $( "#projName" );
	var $projDesc = $( "#projDesc" );

	// Configure modals:
	var delSubProjectModal = new Modal({
		text          : "Are you sure you want to delete this sub-project?",
		groupSelector : ".mdl-cfrm-del",
		onConfirm     : function( subProjData ) {
			console.log( "You just confirmed to delete:", subProjData.name );

			// Proceed for deleting via AJAX:
			$.ajax({
				url    : URL_PREFIX + "/sub-project/delete",
				method : "POST",
				data   : JSON.stringify({ spid : subProjData._id }),
				success: function( response ) {
					console.log( "Response for delete sub project:", response, "; Sub proj data:", subProjData );
					var ajaxResponse = response.ajaxResponse;
					if( ajaxResponse.status === 200 ) {
						$( "#subProj-" + subProjData._id ).remove();
					}
				}
			});
		}
	});

	// Proceed for fetching project list via AJAX:
	$.ajax({
		url    : linkPrefix + "/dashboard/project/splist",
		method : "POST",
		data   : JSON.stringify({
			bid : builderId,
			pid : projectId
		}),
		success: function( response ) {
			console.log( "Got data:", response );
			var ajaxResponse = response.ajaxResponse;

			// Check for successful response:
			if( ajaxResponse.status === 200 ) {
				var spList = ajaxResponse.values.splist || [];

				// Clear the DOM container:
				subprojsDomNode.innerHTML = "";

				if( spList.length ) {
					// Render the records on DOM:
					TemplateUtils.render( TPL_SUB_PROJ, subprojsDomNode, spList );

					// Initialize the modal:
					delSubProjectModal.init();
				}
				else {
					// Inform user that no sub-projects have been created yet:
					subprojsDomNode.innerHTML = "<p class='small'>No sub-projects yet.</p>";
				}
			}
		},
		error: function( response ) {
			console.log( "Error occurred:", response );
		}
	});

	// Define the "on editable out of focus" function that will
	// be executed whenever an editable element goes out of focus:
	window.onEditableBlur = function( oldText, newText ) {
		$.ajax({
			url    : URL_PREFIX + "/edit/sub",
			method : "POST",
			data   : JSON.stringify({
				name        : $projName.text(),
				description : $projDesc.text()
			}),
			success: function( response ) {
				console.log( "Success:", response );
			},
			error: function( response ) {
				console.log( "Error occurred:", response );
			}
		});
	};

});