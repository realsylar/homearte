/**
 * @desc Web app's dashboard page
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 *   + common/methods.js
 *   + utils/template-utils.js
 *   + modules/modal.js
 */


$( document ).ready( function() {

	// Get the meta values:
	var linkPrefix = CommonHelpers.getMetaValue( "link-prefix" );
	var builderId  = CommonHelpers.getMetaValue( "builder-id" );

	// Constants:
	var NUM_PROJ = 5;
	var TPL_PROJ = "project";
	var QUERY_TYPE_DELAY = 1000; // time ( in `milliseconds` ) to wait before sending user query

	// Variables containing jQuery elements:
	var $window         = $( window );
	var $document       = $( document );
	var $projQuery      = $( "#projQuery" );
	var $projQueryForm  = $( ".js-proj-query" );
	var $recordsLoader  = $( "#records-loader" );
	var $recordsFetched = $( "#records-fetched" );

	// Other variables:
	var projsDomNode      = document.getElementById( "projs" ); // The DOM node where projects will be appended
	var userQuery         = ""; // Variable containing user's project query
	var currSkip          = 0;  // Variable containing the current skip value
	var isAllFetched      = false; // Whether all projects have been fetched or not, this will contain the end condition for infinite scroll
	var isFetchInProgress = false; // Whether the fetching of projects is in progress or not

	// Function to delete a project associated with the `projData` data:
	var deleteProject = function( projData ) {
		console.log( "You just confirmed to delete:", projData.name );

		// Proceed for deleting via AJAX:
		$.ajax({
			url    : linkPrefix + "/dashboard/project/delete",
			method : "POST",
			data   : JSON.stringify({ pid : projData._id }),
			success: function( response ) {
				console.log( "Response for delete project:", response, "; Proj data:", projData );
				var ajaxResponse = response.ajaxResponse;
				if( ajaxResponse.status === 200 ) {
					$( "#proj-" + projData._id ).slideUp( "fast" );
				}
			}
		});
	};

	// Configure the modals:
	var delModal = new Modal({
		text          : "Are you sure you want to delete this project?",
		groupSelector : ".mdl-cfrm-del",
		onConfirm     : deleteProject
	});

	// Function to fetch records skipped by `_skip` and matching the `_qry` string:
	var fetchRecords = function( _skip, _qry ) {
		// Proceed if:
		// 1. Current "fetching" is not in process
		// 2. All records have not been fetched yet
		if( !isFetchInProgress && !isAllFetched ) {
			// Prepare DOM to show that records are being fetched:
			$recordsLoader.show();

			// Set the flag regarding "fetch in progress":
			isFetchInProgress = true;

			// Hide the final message anyways:
			$recordsFetched.hide();

			// Proceed for fetching project list via AJAX:
			$.ajax({
				url    : linkPrefix + "/dashboard/plist",
				method : "POST",
				data   : JSON.stringify({
					bid   : builderId,
					skip  : _skip,
					limit : NUM_PROJ,
					query : _qry
				}),
				success: function( response ) {
					console.log( "Got data:", response, "; skip:", _skip, ", query:", _qry );
					var ajaxResponse = response.ajaxResponse;

					// Hide the loader:
					$recordsLoader.hide();

					// Unset the flag regarding "fetch in progress":
					isFetchInProgress = false;

					// Check for successful response:
					if( ajaxResponse.status === 200 ) {
						var projList = ajaxResponse.values.plist || [];

						// Check whether all records have been fetched or not:
						if( projList.length < NUM_PROJ ) {
							// Show on DOM that all records have been fetched:
							$recordsFetched.show();

							// And set the flag regarding "all records fetched":
							isAllFetched = true;
						}

						// Render the records on DOM:
						TemplateUtils.render( TPL_PROJ, projsDomNode, projList );

						// Initialize the modal:
						delModal.init();
					}
				},
				error: function( response ) {
					console.log( "Error occurred:", response, "; skip:", _skip, ", query:", _qry );
				}
			});
		}
	};

	// Handle user's project query:
	$projQuery.on( "keyup", function() {
		// Get the current value:
		var val = $projQuery.val().trim();

		// Don't make AJAX request if:
		// 1. User has typed the last-sent query
		if( val === userQuery ) return;

		CommonHelpers.executeCallbackAfterDelay( function() {
			// Set this value as the last-sent query:
			userQuery = val;

			// Clear the current projects list on the DOM:
			projsDomNode.innerHTML = "";

			// Unset the flag regarding "all records fetched" because
			// user has typed a new query:
			isAllFetched = false;

			// Reset the skip value too for the same reason:
			currSkip = 0;

			// Proceed to fetch records:
			fetchRecords( currSkip, userQuery );

			console.log( "You typed:", userQuery );
		}, QUERY_TYPE_DELAY );
	});

	// Handle the project query form submit event:
	// (prevent it)
	$projQueryForm.on( "submit", function( e ) {
		e.preventDefault();
	});

	// Attach a scroll event to the curret window to detect when
	// the page bottom is hit:
	$window.on( "scroll", function() {
		if( $window.scrollTop() + $window.height() >= $document.height() ) {
			// Page bottom hit, indeed...
			console.log("Bottom hit indeed...");
			// Increment the skip so that the next batch of records can
			// be fetched:
			currSkip += NUM_PROJ;

			// Proceed to fetch records:
			fetchRecords( currSkip, userQuery );
		}
	});

	// Fetch records ( projects ) on page load:
	fetchRecords( currSkip, userQuery );

});