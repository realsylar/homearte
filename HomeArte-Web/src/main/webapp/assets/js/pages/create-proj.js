/**
 * @desc Web app's create project and sub-project page
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 */


$( document ).ready( function() {

	// Create project form submit:
	$( ".js-creat-proj-form" ).on( "submit", function( e ) {
		// Get the name element:
		var $name = $( this ).find( "input[name='projectName']" );
		if( $name.length === 0 ) {
			$name = $( this ).find( "input[name='subProjectName']" );
		}

		// Get the form values:
		var name = $.trim( $name.val() );

		// Get the jquery element to show validation messages:
		var $msg = $( this ).find( ".js-notif" );

		// Validate the fields:
		if( !name ) {
			$msg.text( "Please provide a name" );
			e.preventDefault();
		}
	});

});