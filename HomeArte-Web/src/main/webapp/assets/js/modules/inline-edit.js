/**
 * @desc Inline editing
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 */


$( document ).ready( function() {

	// Constants:
	var FOCUSSED_CLASS      = "editable__text--focussed";
	var EDITABLE_TEXT_CLASS = "editable__text";

	// Variables containing jQuery elemenets:
	var $editableLink = $( ".editable__link" );
	var $editableText = $( "." + EDITABLE_TEXT_CLASS );

	// Handle the edit click:
	$editableLink.on("click", function( e ) {
		e.preventDefault();

		var $editable = $( this ).prev( "." + EDITABLE_TEXT_CLASS );

		if( $editable.prop( "contenteditable" ) !== "true" ) {
			$editable
				.prop( "contenteditable", "true" )
				.addClass( FOCUSSED_CLASS )
				.focus();
		}
	});

	// Handle out-of-focus:
	$editableText.on( "blur", function( e ) {
		var $this   = $( this );
		var newText = $this.text().trim();
		var oldText = $this.data( "oldtext" );

		$this
			.prop( "contenteditable", "inherit" )
			.removeClass( FOCUSSED_CLASS );

		if( newText !== oldText ) {
			$this.data( "oldtext", newText );
			// And send newText to server...
			if( window && typeof window.onEditableBlur === "function" ) {
				window.onEditableBlur( oldText, newText );
			}
		}
	});
});