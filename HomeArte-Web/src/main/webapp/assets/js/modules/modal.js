/**
 * @desc Modal
 * @author Navdeep
 * @dependsOn
 *   + utils/template-utils.js
 */


var Modal = function( config ) {
	this.config  = config;
	this.modalId = "modal-" + Date.now();
	this.entityData = {};
	return this;
};

// Class variables:
Modal.templateName           = "modal";
Modal.overlayClassName       = "modal__overlay";
Modal.subTextClassName       = "js-modal-subtext";
Modal.subjectiveDataAttrName = "data-modal-subjective-data";
Modal.hideModalClassName     = "js-modal-hide";
Modal.confirmClassName       = "js-modal-confirm";
Modal.textClassName          = "js-modal-text";

/**
 * @desc
 * Append modal and add event listeners
 */
Modal.prototype.init = function() {

	// A singleton modal overlay:
	if( document.querySelectorAll( "." + Modal.overlayClassName ).length === 0 ) {
		// Create the overlay element:
		var overlayEl = document.createElement( "div" );

		// Add the appropriate class(es):
		overlayEl.classList.add(
			Modal.overlayClassName,
			Modal.hideModalClassName,
			"hide"
		);

		// And then append this overlay to the body:
		document.querySelector( "body" ).appendChild( overlayEl );
	}

	// Prevent the following on multiple calls to the `init` API:
	// 1. Making redundant modals on the DOM
	// 2. Attaching redundant event listener to the "confirm" button
	if( !document.querySelector( "#" + this.modalId ) ) {
		// Render the hidden modal on the DOM:
		TemplateUtils.render(
			Modal.templateName,
			document.querySelector( "." + Modal.overlayClassName ),
			{
				id   : this.modalId,
				text : this.config.text
			}
		);

		// Add event listeners to execute the `onConfirm` callback on the
		// "confirm" button of the modal:
		[].forEach.call( document.querySelector( "#" + this.modalId ).querySelectorAll( "." + Modal.confirmClassName ), function( el ) {
			el.addEventListener( "click", function( e ) {
				e.preventDefault();

				// Call the callback:
				this.config.onConfirm.call( this, this.entityData );
			}.bind( this ), false );
		}.bind( this ));
	}

	// Iterate over all the specified elements on the DOM and:
	// 1. Get the subjective data; and
	// 2. Add event listener to show the modal
	[].forEach.call( document.querySelectorAll( this.config.groupSelector ), function( el ) {
		// Get the subjective data from the attribute:
		var subjectiveData = el.getAttribute( Modal.subjectiveDataAttrName ) || "{}";

		// Convert this string data into an object:
		if( typeof subjectiveData === "string" ) {
			try {
				subjectiveData = JSON.parse( subjectiveData );
			}
			catch( e ) {
				// Data was not properly provided!
				subjectiveData = {};
			}
		}

		// Finally, add the event listener to show the modal:
		el.addEventListener( "click", function( e ) {
			e.preventDefault();

			// Call the `show` API with the subjective data:
			this.show( subjectiveData );
		}.bind( this ), false );
	}.bind( this ));

	// Add event listeners to close / hide the modal:
	[].forEach.call( document.querySelectorAll( "." + Modal.hideModalClassName ), function( el ) {
		// Preserve context:
		var that = this;

		el.addEventListener( "click", function( e ) {
			e.preventDefault();

			// Call the `hide` API:
			that.hide( e.target, this );
		}, false );
	}.bind( this ));
};

/**
 * @desc
 * Shows the modal
 *
 * @param subjectiveData {object} Additional info specific to the entity
 *                                in context i.e. the entity for which the
 *                                modal is opened.
 */
Modal.prototype.show = function( subjectiveData ) {
	// Get the modal element:
	var modalEl = document.querySelector( "#" + this.modalId );

	// And get the modal subtext element too:
	var modalSubTextEl = modalEl.querySelector( "." + Modal.subTextClassName );

	// Iterate over the subjective data and convert the key-value
	// combinations into a human readable string:
	var subtext = Object.keys( subjectiveData ).map( function( key ) {
		if( key.startsWith( "_" ) ) return;
		return key.charAt( 0 ).toUpperCase() + key.slice( 1 ) + " - " + subjectiveData[ key ];
	}).filter( function( item ) {
		return item;
	}).join( ", " );

	// Reset the sub text:
	modalSubTextEl.innerText = "";
	modalSubTextEl.innerText = subtext || "";

	// Change the modal text:
	modalEl.querySelector( "." + Modal.textClassName ).innerText = this.config.text;

	// Set the entity data for the current modal. This data
	// will be used to get the necessary info specific to
	// the entity for which the modal was opened in the
	// `onConfirm` callback.
	this.entityData = subjectiveData;

	// Show the overlay:
	document.querySelector( "." + Modal.overlayClassName ).classList.remove( "hide" );

	// And then show the modal:
	modalEl.classList.remove( "hide" );
};

/**
 * @desc
 * Hides the modal
 *
 * @param currTarget {object} The event target which caused this
 *                            funtion to execute in the "click"
 *                            event handler.
 * @param realTarget {object} The event target on which the "click"
 *                            event handler was attached.
 */
Modal.prototype.hide = function( currTarget, realTarget ) {
	if( currTarget.className === realTarget.className ) {
		// Hide the overlay:
		document.querySelector( "." + Modal.overlayClassName ).classList.add( "hide" );

		// And also hide the modal:
		document.querySelector( "#" + this.modalId ).classList.add( "hide" );
	}
};