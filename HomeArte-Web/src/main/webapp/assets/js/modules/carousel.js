/**
 * @desc Carousel
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 */


$( document ).ready( function() {

	// Constants:
	var SLIDE_CLASS             = "carousel__slide";
	var SLIDE_ACTIVE_CLASS      = "carousel__slide--active";
	var SLIDE_TRANSITION_INTRVL = 0.3; // in `seconds`
	var SLIDE_STAY_INTRVL       = 4.0; // in `seconds`

	// Variables containing jQuery elemenets:
	var $carousel         = $( ".carousel" ).find( "ul" );
	var $slideStore       = $( "." + SLIDE_CLASS );
	var $carouselNav      = $( ".carousel__nav" );
	var $carouselNavLeft  = $( ".carousel__nav--left" );
	var $carouselNavRight = $( ".carousel__nav--right" );

	// Variables:
	var idxActive     = $( "." + SLIDE_ACTIVE_CLASS ).index();
	var wCarousel     = $carousel.parent().width();
	var hCarousel     = $carousel.parent().height();
	var doSlideshow   = $carousel.data( "slideshow" );
	var slideInterval = null;

	// Quick functions:
	var startSlideshow = function() {
		slideInterval  = setInterval( function() {
			$carouselNavRight.click();
		}, SLIDE_STAY_INTRVL * 1000 );
	};

	// Adjust the active slide index:
	if( idxActive === $slideStore.length - 1 ) idxActive = -1;

	// DOM adjustment #1)
	// Remove the original slides
	$slideStore.remove();

	// DOM adjustment #2)
	// Form the slide buffer by appending 3 slides to the DOM:
	//   i) the slide before the active slide
	//  ii) the active slide
	// iii) the slide after the active slide
	for( var idx = 0; idx < 3; idx++ ) {
		$slideStore
			.eq( idxActive - 1 + idx )
			.css( "left", ( wCarousel * ( idx - 1 ) ) + "px" )
			.appendTo( $carousel );
	}

	// DOM adjustment #3)
	// Vertically align the carousel navigation buttons
	$carouselNav.each( function(){
		$( this ).css({
			"margin-top" : ( hCarousel - $( this ).height() ) / 2 + "px"
		});
	});

	// Bind callback to the click of the "move left" button
	$carouselNavLeft.on( "click", function( e ) {
		e.preventDefault();

		var $this = null;
		var $currSlides = $( "." + SLIDE_CLASS ).removeClass( SLIDE_ACTIVE_CLASS );
		for( var idx = 0; idx < 3; idx++ ) {
			$this = $currSlides.eq( idx );
			$this.animate({
				"left" : ( $this.position().left + wCarousel ) + "px"
			}, SLIDE_TRANSITION_INTRVL * 1000 );
		}
		$currSlides.last().remove();

		if( --idxActive === -1 * $slideStore.length )
			idxActive = 0;

		$slideStore
			.eq( idxActive - 1 )
			.css( "left", ( -1 * wCarousel ) + "px" )
			.prependTo( $carousel );

		$( "." + SLIDE_CLASS ).eq( 1 ).addClass( SLIDE_ACTIVE_CLASS );
	});

	// Bind callback to the click of the "move right" button
	$carouselNavRight.on("click", function( e ) {
		e.preventDefault();

		var $this = null;
		var $currSlides = $( "." + SLIDE_CLASS ).removeClass( SLIDE_ACTIVE_CLASS );
		for( var idx = 0; idx < 3; idx++ ) {
			$this = $currSlides.eq( idx );
			$this.animate({
				"left" : ( $this.position().left - wCarousel ) + "px"
			}, SLIDE_TRANSITION_INTRVL * 1000 );
		}
		$currSlides.first().remove();

		if( ++idxActive === $slideStore.length - 1 )
			idxActive = -1;

		$slideStore
			.eq( idxActive + 1 )
			.css( "left", wCarousel + "px" )
			.appendTo( $carousel );

		$( "." + SLIDE_CLASS ).eq( 1 ).addClass( SLIDE_ACTIVE_CLASS );
	});

	// Pause slideshow when mouse hovered over the carousel:
	$carousel.parent().on( "mouseover", function() {
		if( doSlideshow ) {
			clearInterval( slideInterval );
		}
	});

	// Resume slideshow when mouse hovered over the carousel:
	$carousel.parent().on( "mouseout", function() {
		if( doSlideshow ) {
			startSlideshow();
		}
	});

	// Start slide show if needed:
	if( doSlideshow ) {
		startSlideshow();
	}

});