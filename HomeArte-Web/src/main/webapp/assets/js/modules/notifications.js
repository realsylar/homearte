/**
 * @desc Web app's dashboard notification drawer
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 */


$( document ).ready( function() {

	// Variables containing jQuery elements:
	var $document       = $( document );
	var $notifBell      = $( ".notif__bell > a" );
	var $notifIndicator = $( ".notif__indicator" );
	var $notifDrawer    = $( ".notif__drawer" );

	// Handle the click events on the toggle:
	$notifBell.on( "click", function( e ) {
		e.preventDefault();

		$notifBell.toggleClass( "notif__bell--active" );
		$notifIndicator.hide();
		$notifDrawer.toggleClass( "hide" );
	});

	$document.on( "click", function( e ) {
		var className = e.target.className;
		// console.log( "You clicked on:", className );
	});

});