/**
 * @desc Web app's file uploader
 * @author Navdeep
 * @dependsOn
 *   + jQuery
 */


// Constants:
var MAX_FILE_SIZE = 5; // in `Mb`
var UPLOADER_FACADE_SELECTOR = ".js-uploader-facade";
var UPLOADER_REAL_SELECTOR   = ".js-uploader-real";
var UPLOADER_REMOVE_SELECTOR = ".js-uploader-remove";

// Variables containing jQuery elements:
var $document = $( document );

var Uploader = function( config ) {
	this.config = config;

	this.init();
};

Uploader.prototype.init = function() {
	// Preserve context:
	var that = this;

	// Handle the facade click that is visible to the user:
	$document.on( "click", "#" + this.config.facadeId, function( e ) {
		e.preventDefault();

		// Trigger the real uploader control click:
		$( "#" + this.config.realId ).click();
	}.bind( this ));

	// Handle the real uploader link:
	$document.on( "change", "#" + this.config.realId, function( e ) {
		var input = e.target;

		if ( input.files && input.files[ 0 ] ) {
			var file = input.files[ 0 ];

			if( file.size <= MAX_FILE_SIZE * 1024 * 1024 ) {
				var reader = new FileReader();

				reader.onload = function( e ) {
					that.config.onUpload.call( that, e, file );
				};

				reader.readAsDataURL( file );
			}
			else {
				console.error( "File size greater than " + MAX_FILE_SIZE + " Mb" );
			}
		}
	});

	// Handle the remove file link click:
	$document.on( "click", "#" + this.config.removeId, function( e ) {
		e.preventDefault();

		this.remove.call( this );
	}.bind( this ));
};

Uploader.prototype.remove = function() {
	this.config.onRemove.call( this );
};