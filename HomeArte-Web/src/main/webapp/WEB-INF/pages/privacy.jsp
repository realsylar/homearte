<!-- Put JSP headers like tag libs here... -->
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- All the meta tags -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="link-prefix" content="">
	<meta name="_csrf" content="${_csrf.token}">

	<!-- Page title -->
	<title>Privacy Policy | HomeArte</title>

	<!-- Import any and all stylesheets here -->
	<link rel="stylesheet" href="/assets/css/lib/font-awesome.min.css">
	<!-- build:css /assets/css/style.min.css -->
	<link rel="stylesheet" href="/assets/css/build.css">
	<!-- endbuild -->
	<!-- NOTE: Do not put script tags here! -->
</head>
<body>

	<!-- The main page content is encapsulated in this div -->
	<div class="page">
		<!-- Page header -->
		<div class="header grid">
			<div class="col col-4 no-gutter">
				<a href="#show-sidebar" class="js-toggle-sidebar" tabindex="-1">
					<span class="fa fa-bars fa-fw"></span> Menu
				</a>
			</div><!-- .col -->
			<div class="col col-4 no-gutter center">
				<a href="/" class="biggest upper print-clearly loose-text dark" tabindex="-1">HomeArte</a>
			</div><!-- .col -->
			<div class="col col-4 no-gutter align-right">
				<a href="/ulogin" tabindex="-1">Login</a>
				&nbsp;
				<a href="/signup" class="btn btn--medium no-states hide" tabindex="-1">Signup</a>
			</div><!-- .col -->
		</div><!-- .header.grid -->

		<!-- Page content -->
		<div class="content section">
			<div class="container">
				<h1 class="bigger">Privacy Policy</h1>
				We do not sell or rent your personal information to third parties for their marketing purposes without your explicit consent, and only use your information as described below. We view protection of users' privacy as a very important community principle. We understand clearly that you and your information is one of our most important assets. We store and process your information on computers located in various geographies including the United States. The servers are protected by physical as well as technological security devices offered by third party vendors. HomeArte.in does not guarantee the nature and extent of security offered by such vendors. This Privacy Policy describes the manner in which your data is collected and used by HomeArte.in. You are advised to please read this Privacy Policy carefully. By accessing the services provided by HomeArte.in you agree to the collection and use of your data by HomeArte.in in the manner provided in this Privacy Policy.
				<p class="bold">What information is, or may be, collected from you?</p>
				We will automatically receive and collect certain anonymous information in standard usage logs through our Web server, including computer-identification information obtained from "cookies," sent to your browser from:
				<ul>
					<li>web server cookie stored on your hard drive</li>
					<li>an IP address, assigned to the computer which you use</li>
					<li>the domain server through which you access our service</li>
					<li>the type of computer you're using</li>
					<li>the type of web browser you're using</li>
				</ul>
				<p class="bold">We may collect the following personally identifiable information about you:</p>
				<ul>
					<li>name including first and last name</li>
					<li>alternate email address</li>
					<li>mobile phone number and contact details</li>
					<li>ZIP/Postal code</li>
					<li>demographic profile (like your age, gender, occupation, education, address and durables owned)</li>
					<li>preferences and interests (such as news, sports, travel and so on)</li>
					<li>financial information (like account or credit card numbers) and</li>
				</ul>
				<p class="bold">We may also collect the following information:</p>
				<ul>
					<li>pages on our site you visit/access</li>
					<li>links you click on the Sites</li>
					<li>number of times you access pages</li>
				</ul>
				HomeArte.in only displays the product catalogue. At checkout, your order information is forwarded to our in-country retail partner for payment and fulfillment. You can terminate your account at any time. However, your information may remain stored in archive on our servers even after deletion or termination of your account.
				<p class="bold">Who collects the information?</p>
				<ul>
					<li>We will collect anonymous traffic information from you when you visit the Sites.</li>
					<li>We will collect personally identifiable information about you only as part of a voluntary registration process, on-line survey, or contest, or any combination thereof.</li>
					<li>We are not responsible for the privacy practices of Linked Sites.</li>
				</ul>
				<p class="big bold">How is the information used?</p>
				<p class="bold">We use your personal information to:</p>
				<ul>
					<li>help us provide personalized features;</li>
					<li>tailor the Sites to your need or interest;</li>
					<li>promote offers through our business associates and partners;</li>
					<li>get in touch with you when necessary;</li>
					<li>provide the services requested by you; and</li>
					<li>preserve social history as governed by existing law or policy.</li>
				</ul>
				<p class="bold">We use contact information internally to:</p>
				<ul>
					<li>direct our efforts for product improvement;</li>
					<li>contact you as a survey respondent;</li>
					<li>notify you if you win any contest; and</li>
					<li>send you promotional materials from our contest sponsors or advertisers.</li>
				</ul>
				Generally, we use anonymous traffic information to:
				<ul>
					<li>remind us of who you are in order to deliver to you a better and more personalized service from both an advertising and an editorial perspective;</li>
					<li>recognize your access privileges to our the Sites;</li>
					<li>track your entries in some of our promotions, sweepstakes and contests to indicate a player's progress through the promotion and to track entries, submissions, and status in prize drawings;</li>
					<li>make sure that you don't see the same ad repeatedly;</li>
					<li>help diagnose problems with our server;</li>
					<li>administer the Sites; and</li>
					<li>track your session so that we can understand better how people use the Sites.</li>
				</ul>
				<p class="bold">With whom will your information be shared?</p>
				We will not use your financial information for any purpose other than to complete a transaction with you. - We do not rent, sell or share your personal information other than as provided herein, and we will not disclose any of your personally identifiable information to third parties unless: - we have your permission to provide products or services you've requested for; - is shared with our business associates and partners for promotional offers; - to help investigate, prevent or take action regarding unlawful and illegal activities, suspected fraud, potential threat to the safety or security of any person, - violations of HomeArte.in's terms of use, or defend against legal claims; - in special circumstances such as compliance with subpoenas, court orders, requests/order from legal authorities or law enforcement agencies requiring such disclosure; Please do note that personal information is stored on third party servers. We may, however, distribute or license aggregated data that does not contain any reference to you. We share your information with advertisers on an aggregate basis only.
				<p class="bold">What choices are available to you regarding collection, use and distribution of your information?</p>
				Supplying personally identifiable information is entirely voluntary. You are not required to register with us in order to use our sites. However, we offer some services only to visitors who do register. You may change your interests at any time and may opt-in or opt-out of any marketing/promotional/newsletters mailings. HomeArte.in reserves the right to send you certain service related communication, considered to be a part of your HomeArte account, without offering you the facility to opt-out. You may update your information and change your account settings at any time. Upon request, we will remove/block your personally identifiable information from our database, thereby canceling your registration. See Contact Information below. However, your information may remain stored in archive on our servers even after deletion or termination of your account. If we plan to use your personally identifiable information for any commercial purposes, we will notify you at the time we collect that information and allow you to opt-out of having your information used for those purposes. You can accept or decline the cookies. All sites that are customizable require that you accept cookies. You also must accept cookies to register as someone who can access some of our services. For information on how to set your browser to alert you to cookies, or to reject cookies, go to cookie central.
				<p class="bold">What security procedures are in place to protect information from loss, misuse or alteration?</p>
				To protect against the loss, misuse and alteration of the information under our control, we have in place physical, electronic and managerial procedures. For example, our servers are accessible only to authorized personnel, and your information is shared with respective personnel on need to know basis to complete the transaction and provide the services requested by you. Although we will endeavor to safeguard the confidentiality of your personally identifiable information, transmissions made by means of the Internet cannot be made absolutely secure. By using the Sites, you agree that we will have no liability for disclosure of your information due to errors in transmission or unauthorized acts of third parties.
				<p class="bold">How can you correct inaccuracies in the information?</p>
				Our sites allow you to correct or update any information you have provided online. In the event of loss of access details you can send an e-mail to support@HomeArte.in.
				<p class="bold">Policy Updates</p>
				We reserve the right to change or update this policy at any time. Such changes shall be effective immediately upon posting on this site.
			</div><!-- .container -->
		</div><!-- .content -->

		<!-- Page footer -->
		<div class="footer">
			<div class="container">
				<div class="grid">
					<div class="col col-2">
						<p class="small bold hide"><a href="/team" tabindex="-1">Team</a></p>
						<p class="small bold"><a href="/about" tabindex="-1">About Us</a></p>
						<p class="small bold"><a href="/contact" tabindex="-1">Contact Us</a></p>
					</div><!-- .col -->
					<div class="col col-3"></div><!-- .col -->
					<div class="col col-7">
						<!-- Form for submitting an email for newsletter subscription -->
						<form id="newsLetterForm" class="grid">
							<div class="col col-9 no-gutter">
								<input type="text" name="email" class="txt txt--white block" spellcheck="false" placeholder="Enter your email address..." tabindex="-1">
							</div><!-- .col -->
							<div class="col col-3 no-gutter">
								<button class="btn block" tabindex="-1">Subscribe</button>
							</div><!-- .col -->
						</form><!-- .grid -->
						<p class="smaller">Subscribe to our newsletter for all the latest updates about HomeArte</p>
						<!-- Paragraph to show form validation messages -->
						<p class="small js-newsletter-msg"></p>
					</div><!-- .col -->
				</div><!-- .grid -->
			</div><!-- .container -->
			<hr />
			<div class="grid">
				<div class="col col-4 no-gutter">
					<div>
						<span class="smaller">Follow us on</span>
						&nbsp;
						<a href="https://www.facebook.com/Homeartein-1608384559416314/timeline/" class="social--facebook big" target="_blank" title="Facebook" tabindex="-1">
							<span class="fa fa-facebook-square fa-fw"></span>
						</a>
						<a href="http://www.twitter.com/home_arte" class="social--twitter big" target="_blank" title="Twitter" tabindex="-1">
							<span class="fa fa-twitter-square fa-fw"></span>
						</a>
						<a href="https://www.linkedin.com/company/homearte-in?report%2Esuccess=KJ_KkFGTDCfMt-A7wV3Fn9Yvgwr02Kd6AZHGx4bQCDiP6-2rfP2oxyVoEQiPrcAQ7Bf" class="social--linkedin big" target="_blank" title="LinkedIn" tabindex="-1">
							<span class="fa fa-linkedin-square fa-fw"></span>
						</a>
					</div>
				</div><!-- .col -->
				<div class="col col-4 no-gutter center">
					&copy; 2015 HomeArte
				</div><!-- .col -->
				<div class="col col-4 no-gutter align-right">
					<a href="/privacy" class="smaller">Privacy Policy</a>
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .footer -->
		
	</div>

	<!-- Page sidebar -->
	<div class="sidebar" data-visible="false">
		<p class="pad align-right">
			<a href="#hide-sidebar" class="white no-states js-toggle-sidebar" tabindex="-1">
				<span class="fa fa-times fa-fw"></span>
			</a>
		</p>
		<p class="pad">
			<a href="/signup" class="btn btn--accent block center no-states hide" tabindex="-1">Signup</a>
		</p>
		<ul class="sidebar__nav big">
			<li>
				<a href="/" class="white block no-states">
					<span class="fa fa-home fa-fw"></span>&nbsp;&nbsp; Home
				</a>
			</li>
			<li>
				<a href="/ulogin" class="white block no-states">
					<span class="fa fa-sign-in fa-fw"></span>&nbsp;&nbsp; Login
				</a>
			</li>
			<li>
				<a href="/contact" class="white block no-states">
					<span class="fa fa-phone-square fa-fw"></span>&nbsp;&nbsp; Contact Us
				</a>
			</li>
			<li>
				<a href="/about" class="white block no-states">
					<span class="fa fa-info-circle fa-fw"></span>&nbsp;&nbsp; About
				</a>
			</li>
		</ul>
	</div><!-- .sidebar -->
	<div class="sidebar__overlay hide"></div><!-- .sidebar__overlay -->

	<!-- Import any and all scripts here -->
	<!-- build:js /assets/js/dist/privacy.min.js -->
	<script src="/assets/js/lib/jquery.min.js"></script>
	<script src="/assets/js/validators/email.js"></script>
	<script src="/assets/js/common/methods.js"></script>
	<script src="/assets/js/common/setup.js"></script>
	<script src="/assets/js/common/sidebar.js"></script>
	<script src="/assets/js/common/footer.js"></script>
	<!-- endbuild -->
</body>
</html>

<!-- @author Navdeep -->