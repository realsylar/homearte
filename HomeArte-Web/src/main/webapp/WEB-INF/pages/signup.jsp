<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- All the meta tags -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="link-prefix" content="">
	<meta name="_csrf" content="${_csrf.token}">

	<!-- Page title -->
	<title>Signup | HomeArte</title>

	<!-- Import any and all stylesheets here -->
	<link rel="stylesheet" href="/assets/css/lib/font-awesome.min.css">
	<!-- build:css /assets/css/style.min.css -->
	<link rel="stylesheet" href="/assets/css/build.css">
	<!-- endbuild -->
	<!-- NOTE: Do not put script tags here! -->
</head>
<body>

	<!-- The main page content is encapsulated in this div -->
	<div class="page">
		<!-- Page header -->
		<div class="header grid">
			<div class="col col-4 no-gutter">
				<a href="#show-sidebar" class="js-toggle-sidebar" tabindex="-1">
					<span class="fa fa-bars fa-fw"></span> Menu
				</a>
			</div><!-- .col -->
			<div class="col col-4 no-gutter center">
				<a href="/" class="biggest upper print-clearly loose-text dark" tabindex="-1">HomeArte</a>
			</div><!-- .col -->
			<div class="col col-4 no-gutter align-right">
				<a href="/ulogin" tabindex="-1">Login</a>
				&nbsp;
				<a href="/signup" class="btn btn--medium no-states hide" tabindex="-1">Signup</a>
			</div><!-- .col -->
		</div><!-- .header.grid -->

		<!-- Page content -->
		<div class="content section">
			<form:form class="form js-signup-form" method="POST" action="/signup/sub" commandName="sdto">
				<!-- Input field for first name -->
				<div class="grid">
					<div class="col col-2 no-gutter">
						<input type="text" name="firstNameIco" class="block txt center" disabled="disabled" placeholder="&#xf044;">
					</div><!-- .col -->
					<div class="col col-10 no-gutter">
						<form:input type="text" path="firstName" class="block txt" spellcheck="false" placeholder="First name" />

					</div><!-- .col -->
				</div><!-- .grid -->
				<!-- Input field for last name -->
				<div class="grid">
					<div class="col col-2 no-gutter">
						<input type="text" name="lastNameIco" class="block txt center" disabled="disabled" placeholder="&#xf044;">
					</div><!-- .col -->
					<div class="col col-10 no-gutter">
						<form:input type="text" path="lastName" class="block txt" spellcheck="false" placeholder="Last name" />

					</div><!-- .col -->
				</div><!-- .grid -->
				<!-- Input field for username / email address -->
				<div class="grid">
					<div class="col col-2 no-gutter">
						<input type="text" name="emailIco" class="block txt center" disabled="disabled" placeholder="&#xf003;">
					</div><!-- .col -->
					<div class="col col-10 no-gutter">
						<form:input type="text" path="email" class="block txt" spellcheck="false" placeholder="Email address" />

					</div><!-- .col -->
				</div><!-- .grid -->
				<!-- Input field for password -->
				<div class="grid">
					<div class="col col-2 no-gutter">
						<input type="text" name="passwordIco" class="block txt center" disabled="disabled" placeholder="&#xf023;">
					</div><!-- .col -->
					<div class="col col-10 no-gutter">
						<form:input type="password" path="password" class="block txt" spellcheck="false" placeholder="Password" />

					</div><!-- .col -->
				</div><!-- .grid -->
				<!-- Input field for confirming password -->
				<div class="grid">
					<div class="col col-2 no-gutter">
						<input type="text" name="confirmPasswordIco" class="block txt center" disabled="disabled" placeholder="&#xf023;">
					</div><!-- .col -->
					<div class="col col-10 no-gutter">
						<input type="password" name="confirmPassword" class="block txt" spellcheck="false" placeholder="Confirm password">
					</div><!-- .col -->
				</div><!-- .grid -->
				<!-- Paragraph to show form validation messages -->
				<p class="small error js-notif">${sdto.error}</p>

				<!-- Button to submit the form -->
				<button class="block btn btn--large">Signup</button>
				<!-- Google signup -->
				<p class="center small">or</p>
				<button class="block btn btn--large btn--google">
					<span class="fa fa-google-plus fa-fw"></span>
					&nbsp;
					Signup with Google
				</button>
				<!-- Login link -->
				<p class="align-right small">
					Already a member? <a href="/ulogin">Login</a>
				</p>
			</form:form><!-- .form -->
		</div><!-- .content -->

		<!-- Page footer -->
		<div class="footer">
			<div class="container">
				<div class="grid">
					<div class="col col-2">
						<p class="small bold hide"><a href="/team" tabindex="-1">Team</a></p>
						<p class="small bold"><a href="/about" tabindex="-1">About Us</a></p>
						<p class="small bold"><a href="/contact" tabindex="-1">Contact Us</a></p>
					</div><!-- .col -->
					<div class="col col-3"></div><!-- .col -->
					<div class="col col-7">
						<!-- Form for submitting an email for newsletter subscription -->
						<form id="newsLetterForm" class="grid">
							<div class="col col-9 no-gutter">
								<input type="text" name="email" class="txt txt--white block" spellcheck="false" placeholder="Enter your email address..." tabindex="-1">
							</div><!-- .col -->
							<div class="col col-3 no-gutter">
								<button class="btn block" tabindex="-1">Subscribe</button>
							</div><!-- .col -->
						</form><!-- .grid -->
						<p class="smaller">Subscribe to our newsletter for all the latest updates about HomeArte</p>
						<!-- Paragraph to show form validation messages -->
						<p class="small js-newsletter-msg"></p>
					</div><!-- .col -->
				</div><!-- .grid -->
			</div><!-- .container -->
			<hr />
			<div class="grid">
				<div class="col col-4 no-gutter">
					<div>
						<span class="smaller">Follow us on</span>
						&nbsp;
						<a href="https://www.facebook.com/Homeartein-1608384559416314/timeline/" class="social--facebook big" target="_blank" title="Facebook" tabindex="-1">
							<span class="fa fa-facebook-square fa-fw"></span>
						</a>
						<a href="http://www.twitter.com/home_arte" class="social--twitter big" target="_blank" title="Twitter" tabindex="-1">
							<span class="fa fa-twitter-square fa-fw"></span>
						</a>
						<a href="https://www.linkedin.com/company/homearte-in?report%2Esuccess=KJ_KkFGTDCfMt-A7wV3Fn9Yvgwr02Kd6AZHGx4bQCDiP6-2rfP2oxyVoEQiPrcAQ7Bf" class="social--linkedin big" target="_blank" title="LinkedIn" tabindex="-1">
							<span class="fa fa-linkedin-square fa-fw"></span>
						</a>
					</div>
				</div><!-- .col -->
				<div class="col col-4 no-gutter center">
					&copy; 2015 HomeArte
				</div><!-- .col -->
				<div class="col col-4 no-gutter align-right">
					<a href="/privacy" class="smaller">Privacy Policy</a>
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .footer -->
		
	</div>

	<!-- Page sidebar -->
	<div class="sidebar" data-visible="false">
		<p class="pad align-right">
			<a href="#hide-sidebar" class="white no-states js-toggle-sidebar" tabindex="-1">
				<span class="fa fa-times fa-fw"></span>
			</a>
		</p>
		<p class="pad">
			<a href="/signup" class="btn btn--accent block center no-states hide" tabindex="-1">Signup</a>
		</p>
		<ul class="sidebar__nav big">
			<li>
				<a href="/" class="white block no-states">
					<span class="fa fa-home fa-fw"></span>&nbsp;&nbsp; Home
				</a>
			</li>
			<li>
				<a href="/ulogin" class="white block no-states">
					<span class="fa fa-sign-in fa-fw"></span>&nbsp;&nbsp; Login
				</a>
			</li>
			<li>
				<a href="/contact" class="white block no-states">
					<span class="fa fa-phone-square fa-fw"></span>&nbsp;&nbsp; Contact Us
				</a>
			</li>
			<li>
				<a href="/about" class="white block no-states">
					<span class="fa fa-info-circle fa-fw"></span>&nbsp;&nbsp; About
				</a>
			</li>
		</ul>
	</div><!-- .sidebar -->
	<div class="sidebar__overlay hide"></div><!-- .sidebar__overlay -->

	<!-- Import any and all scripts here -->
	<!-- build:js /assets/js/dist/signup.min.js -->
	<script src="/assets/js/lib/jquery.min.js"></script>
	<script src="/assets/js/validators/email.js"></script>
	<script src="/assets/js/validators/password.js"></script>
	<script src="/assets/js/common/methods.js"></script>
	<script src="/assets/js/common/setup.js"></script>
	<script src="/assets/js/common/sidebar.js"></script>
	<script src="/assets/js/common/footer.js"></script>
	<script src="/assets/js/pages/signup.js"></script>
	<!-- endbuild -->
</body>
</html>

<!-- @author Navdeep -->
