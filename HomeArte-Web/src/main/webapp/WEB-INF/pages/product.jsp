<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- All the meta tags -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="link-prefix" content="">
	<meta name="_csrf" content="${_csrf.token}">
	<meta name="prod-id" content="${hpdto.hpid}">

	<!-- Page title -->
	<title>Product | HomeArte</title>

	<!-- Import any and all stylesheets here -->
	<link rel="stylesheet" href="/assets/css/lib/font-awesome.min.css">
	<!-- build:css /assets/css/style.min.css -->
	<link rel="stylesheet" href="/assets/css/build.css">
	<!-- endbuild -->
	<!-- NOTE: Do not put script tags here! -->
</head>
<body>

	<!-- The main page content is encapsulated in this div -->
	<div class="page">
		<!-- Page header -->
		<div class="header header--dashboard grid">
			<div class="col col-4 no-gutter">
				<span id="today" class="small white" style="padding-left: 10px"></span>
			</div><!-- .col -->
			<div class="col col-4 no-gutter center">
				<a href="/" class="bigger upper print-clearly loose-text white" tabindex="-1">HomeArte</a>
			</div><!-- .col -->
			<div class="col col-4 no-gutter align-right">
				<div class="notif__bell inline-block" style="width: 100px">
					<a href="#show-notif" class="white no-states" tabindex="-1">
						<span class="fa fa-bell-o fa-fw small"></span>
						<span class="notif__indicator inline-block"></span>
					</a>
					<div class="notif__drawer hide">
						<div class="notif__drawer__main scroll-vert">
							<div class="white center">
								<span class="underline">Notifications</span>
							</div>
							<ul class="align-left">
								<c:forEach var="notif" items="${ddto.userNotifications}">
									<c:set var="unreadClass" scope="session" value="notif--read " />
									<c:if test="${notif.status == 0}">
										<c:set var="unreadClass" scope="session" value="notif--unread " />
									</c:if>
									<li>
										<a href="#" class="notif ${unreadClass} small white block no-states">${notif.notificationText}</a>
									</li>
								</c:forEach>
							</ul>
						</div><!-- .notif__drawer__main -->
						<div class="notif__drawer__footer center">
							<a href="#" class="small white center block no-states">
								view all <span class="fa fa-angle-right fa-fw"></span>
							</a>
						</div><!-- .notif__drawer__footer -->
					</div><!-- .notif__drawer -->
				</div><!-- .notif__bell -->
				<span class="white">|</span>
				<a href="/logout" class="small white no-states" tabindex="-1">Logout</a>
			</div><!-- .col -->
		</div><!-- .header.grid -->

		<!-- Page content -->
		<div class="content content--dashboard content--product">
			<p class="small">
				<span class="fa fa-angle-left fa-fw"></span> Go back to <a href="/dashboard/project/${hpdto.projectId}/sub-project/${hpdto.subProjectId}">${hpdto.subProjectName}</a>
			</p>
			<div class="grid bkg--light">
				<div class="col col-9">
					<div class="product__img">
						<img id="prodImg" src="" alt="Product Image" class="img--centered">
					</div>
				</div><!-- .col -->
				<div class="col col-3">
					<p class="no-margin" style="margin-bottom: 25px">
						<span id="prodName" class="dark" title="Name" style="color: black">...</span>
					</p>
					<p class="no-margin" style="margin-bottom: 25px">
						<span id="prodShape" class="dark" title="Shape" style="color: black">...</span>
					</p>
					<p class="no-margin" style="margin-bottom: 25px">
						<span id="prodDim" class="dark" title="Dimension" style="color: black">...</span>
					</p>
					<span class="small upper">Material</span>
					<select id="fin" class="txt block" style="margin-bottom: 5px"></select>
					<span class="small upper">Color</span>
					<select id="col" class="txt block" style="margin-bottom: 5px"></select>
					<span class="small upper">Price</span>
					<p>
						<span class="fa fa-rupee"></span>
						<span id="unitPrice" class="biggest">...</span>
						<span class="small">.00</span>
					</p>
				</div><!-- .col -->
			</div><!-- .grid -->
			<div class="container">
				<div id="prodStatus" class="grid center">
					<div class="col col-4">
						<a href="#" class="btn btn--large btn--success block no-states" data-status-code="0">
							<span class="fa fa-check-circle fa-fw"></span>
							<span class="big">Accept</span>
						</a>
						<div class="center success bkg--light hide js-static-status" style="padding: 23px">
							<span class="fa fa-check-circle fa-fw"></span> Accepted
						</div>
					</div><!-- .col -->
					<div class="col col-4">
						<a href="#" class="btn btn--large btn--warn block no-states" data-status-code="1">
							<span class="fa fa-refresh fa-fw"></span>
							<span class="big">Redesign</span>
						</a>
						<div class="center warn bkg--light hide js-static-status" style="padding: 23px">
							<span class="fa fa-refresh fa-fw"></span> Redesign
						</div>
					</div><!-- .col -->
					<div class="col col-4">
						<a href="#" class="btn btn--large btn--error block no-states" data-status-code="2">
							<span class="fa fa-times-circle fa-fw"></span>
							<span class="big">Reject</span>
						</a>
						<div class="center error bkg--light hide js-static-status" style="padding: 23px">
							<span class="fa fa-times-circle fa-fw"></span> Rejected
						</div>
					</div><!-- .col -->
				</div><!-- .grid -->
				<p id="prodStatusLoader" class="small center">Updating status... Please wait...</p>
				<div class="grid">
					<div class="col col-12">
						<h2>Product specifications</h2>
						<p class="big">
							<span class="underline">Base units</span>
						</p>
						<table width="100%" class="big product__specs">
							<thead class="bold">
								<tr class="block">
									<td class="inline-block col-2">S.No.</td><!--
									--><td class="inline-block col-2">Name</td><!--
									--><td class="inline-block col-2">W x D x H</td><!--
									--><td class="inline-block col-2">Unit Material</td><!--
									--><td class="inline-block col-2">Shutter Material</td><!--
									--><td class="inline-block col-2">Shutter Color</td>
								</tr>
							</thead>
							<tbody id="baseUnits" class="block"></tbody>
						</table>
						<p class="big">
							<span class="underline">Wall units</span>
						</p>
						<table width="100%" class="big product__specs">
							<thead class="bold">
								<tr class="block">
									<td class="inline-block col-2">S.No.</td><!--
									--><td class="inline-block col-2">Name</td><!--
									--><td class="inline-block col-2">W x D x H</td><!--
									--><td class="inline-block col-2">Unit Material</td><!--
									--><td class="inline-block col-2">Shutter Material</td><!--
									--><td class="inline-block col-2">Shutter Color</td>
								</tr>
							</thead>
							<tbody id="wallUnits" class="block"></tbody>
						</table>
						<p class="big">
							<span class="underline">Hardware</span>
						</p>
						<table width="100%" class="big product__specs">
							<thead class="bold">
								<tr class="block">
									<td class="inline-block col-2">S.No.</td><!--
									--><td class="inline-block col-5">Name</td><!--
									--><td class="inline-block col-5">Brand</td>
								</tr>
							</thead>
							<tbody id="hardware" class="block"></tbody>
						</table>
					</div><!-- .col -->
				</div><!-- .grid -->
			</div><!-- .container -->
		</div><!-- .content -->

		<!-- Page footer -->
		<div class="footer">
			<div class="container">
				<div class="grid">
					<div class="col col-2">
						<p class="small bold hide"><a href="/team" tabindex="-1">Team</a></p>
						<p class="small bold"><a href="/about" tabindex="-1">About Us</a></p>
						<p class="small bold"><a href="/contact" tabindex="-1">Contact Us</a></p>
					</div><!-- .col -->
					<div class="col col-3"></div><!-- .col -->
					<div class="col col-7">
						<!-- Form for submitting an email for newsletter subscription -->
						<form id="newsLetterForm" class="grid">
							<div class="col col-9 no-gutter">
								<input type="text" name="email" class="txt txt--white block" spellcheck="false" placeholder="Enter your email address..." tabindex="-1">
							</div><!-- .col -->
							<div class="col col-3 no-gutter">
								<button class="btn block" tabindex="-1">Subscribe</button>
							</div><!-- .col -->
						</form><!-- .grid -->
						<p class="smaller">Subscribe to our newsletter for all the latest updates about HomeArte</p>
						<!-- Paragraph to show form validation messages -->
						<p class="small js-newsletter-msg"></p>
					</div><!-- .col -->
				</div><!-- .grid -->
			</div><!-- .container -->
			<hr />
			<div class="grid">
				<div class="col col-4 no-gutter">
					<div>
						<span class="smaller">Follow us on</span>
						&nbsp;
						<a href="https://www.facebook.com/Homeartein-1608384559416314/timeline/" class="social--facebook big" target="_blank" title="Facebook" tabindex="-1">
							<span class="fa fa-facebook-square fa-fw"></span>
						</a>
						<a href="http://www.twitter.com/home_arte" class="social--twitter big" target="_blank" title="Twitter" tabindex="-1">
							<span class="fa fa-twitter-square fa-fw"></span>
						</a>
						<a href="https://www.linkedin.com/company/homearte-in?report%2Esuccess=KJ_KkFGTDCfMt-A7wV3Fn9Yvgwr02Kd6AZHGx4bQCDiP6-2rfP2oxyVoEQiPrcAQ7Bf" class="social--linkedin big" target="_blank" title="LinkedIn" tabindex="-1">
							<span class="fa fa-linkedin-square fa-fw"></span>
						</a>
					</div>
				</div><!-- .col -->
				<div class="col col-4 no-gutter center">
					&copy; 2015 HomeArte
				</div><!-- .col -->
				<div class="col col-4 no-gutter align-right">
					<a href="/privacy" class="smaller">Privacy Policy</a>
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .footer -->
		
	</div>

	<!-- Import any and all scripts here -->
	<!-- build:js /assets/js/dist/product.min.js -->
	<script src="/assets/js/lib/jquery.min.js"></script>
	<script src="/assets/js/lib/handlebars.runtime.min.js"></script>
	<script src="/assets/js/common/methods.js"></script>
	<script src="/assets/js/common/setup.js"></script>
	<script src="/assets/js/common/sidebar.js"></script>
	<script src="/assets/js/common/footer.js"></script>
	<script src="/assets/js/utils/date-utils.js"></script>
	<script src="/assets/js/utils/currency-utils.js"></script>
	<script src="/assets/js/utils/template-utils.js"></script>
	<script src="/assets/js/build/hbs-templates.js"></script>
	<script src="/assets/js/modules/notifications.js"></script>
	<script src="/assets/js/pages/product.js"></script>
	<!-- endbuild -->
	<script>
		// Set the current date:
		DateUtils.setCurrDate( document.getElementById( "today" ) );
	</script>
</body>
</html>

<!-- @author Navdeep -->