<!-- Put JSP headers like tag libs here... -->
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- All the meta tags -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="link-prefix" content="">
	<meta name="_csrf" content="${_csrf.token}">

	<!-- Page title -->
	<title>HomeArte</title>

	<!-- Import any and all stylesheets here -->
	<link rel="stylesheet" href="/assets/css/lib/font-awesome.min.css">
	<link rel="stylesheet" href="/assets/css/lib/swipebox.min.css">
	<!-- build:css /assets/css/style.min.css -->
	<link rel="stylesheet" href="/assets/css/build.css">
	<!-- endbuild -->
	<!-- NOTE: Do not put script tags here! -->
</head>
<body>

	<!-- The main page content is encapsulated in this div -->
	<div class="page">
		<!-- Page header -->
		<div class="header grid">
			<div class="col col-4 no-gutter">
				<a href="#show-sidebar" class="js-toggle-sidebar" tabindex="-1">
					<span class="fa fa-bars fa-fw"></span> Menu
				</a>
			</div><!-- .col -->
			<div class="col col-4 no-gutter center">
				<a href="/" class="biggest upper print-clearly loose-text dark" tabindex="-1">HomeArte</a>
			</div><!-- .col -->
			<div class="col col-4 no-gutter align-right">
				<a href="/ulogin" tabindex="-1">Login</a>
				&nbsp;
				<a href="/signup" class="btn btn--medium no-states hide" tabindex="-1">Signup</a>
			</div><!-- .col -->
		</div><!-- .header.grid -->

		<!-- Page content -->
		<div class="content">
			<div class="hero carousel">
				<ul data-slideshow="true">
					<li id="hero-slide-1" class="carousel__slide carousel__slide--active">
						<div class="hero__overlay"></div>
						<div class="hero__content">
							<p class="white center print-clearly">
								Make your dream kitchen a reality
							</p>
						</div><!-- .hero__content -->
					</li><!-- .carousel__slide -->
					<li id="hero-slide-2" class="carousel__slide">
						<div class="hero__overlay"></div>
						<div class="hero__content">
							<p class="white center print-clearly">
								Select from exquisite designs
							</p>
						</div><!-- .hero__content -->
					</li><!-- .carousel__slide -->
					<li id="hero-slide-3" class="carousel__slide">
						<div class="hero__overlay"></div>
						<div class="hero__content">
							<p class="white center print-clearly">
								Hand picked by professionals
							</p>
						</div><!-- .hero__content -->
					</li><!-- .carousel__slide -->
					<li id="hero-slide-4" class="carousel__slide">
						<div class="hero__overlay"></div>
						<div class="hero__content">
							<p class="white center print-clearly">
								Customize your selected design
							</p>
						</div><!-- .hero__content -->
					</li><!-- .carousel__slide -->
				</ul>

				<button class="carousel__nav carousel__nav--left" tabindex="-1">
					<span class="fa fa-angle-left fa-fw"></span>
				</button>
				<button class="carousel__nav carousel__nav--right" tabindex="-1">
					<span class="fa fa-angle-right fa-fw"></span>
				</button>
			</div><!-- .hero.carousel -->

			<div class="section">
				<div class="container">
					<h1 class="biggest center print-clearly">Features</h1>
					<div class="grid center">
						<div class="col col-4 no-gutter">
							<div class="feature__card">
								<span class="fa fa-pencil-square-o fa-fw fa-5x"></span>
								<p class="bigger">Designed by Experts</p>
								<p>Professional designers work closely with you to create your personalised kitchen.</p>
							</div><!-- .feature__card -->
						</div><!-- .col -->
						<div class="col col-4 no-gutter">
							<div class="feature__card">
								<span class="fa fa-magic fa-fw fa-5x"></span>
								<p class="bigger">Customize</p>
								<p>Customize your selected design to suit your taste and budget.</p>
							</div><!-- .feature__card -->
						</div><!-- .col -->
						<div class="col col-4 no-gutter">
							<div class="feature__card">
								<span class="fa fa-diamond fa-fw fa-5x"></span>
								<p class="bigger">Premium Quality</p>
								<p>State-of-the-art manufacturing process.</p>
							</div><!-- .feature__card -->
						</div><!-- .col -->
					</div><!-- .grid -->
					<div class="grid center">
						<div class="col col-4 no-gutter">
							<div class="feature__card">
								<span class="fa fa-truck fa-fw fa-5x"></span>
								<p class="bigger">30-day Delivery</p>
								<p>30 day deleivery guaranteed.</p>
							</div><!-- .feature__card -->
						</div><!-- .col -->
						<div class="col col-4 no-gutter">
							<div class="feature__card">
								<span class="fa fa-wrench fa-fw fa-5x"></span>
								<p class="bigger">Hassle-free Installation</p>
								<p>Schedule kitchen installation as per your convenience.</p>
							</div><!-- .feature__card -->
						</div><!-- .col -->
						<div class="col col-4 no-gutter">
							<div class="feature__card">
								<span class="fa fa-smile-o fa-fw fa-5x"></span>
								<p class="bigger">After-Sales Services</p>
								<p>A team of specialised agents to meet all your after-sales servicing requirements.</p>
							</div><!-- .feature__card -->
						</div><!-- .col -->
					</div><!-- .grid -->
				</div><!-- .container -->
			</div><!-- .section #features -->

			<div class="section">
				<div class="container">
					<h1 class="biggest center print-clearly">Our catalogue sneak peek</h1>
					<div class="grid center">
						<div class="col col-3">
							<a href="#" id="catalogue-1" class="catalogue__card block" data-imgs='[{"href":"/assets/img/catalogue/1-1.jpg"},{"href":"/assets/img/catalogue/1-2.jpg"},{"href":"/assets/img/catalogue/1-3.jpg"},{"href":"/assets/img/catalogue/1-4.jpg"}]' tabindex="-1">
								<div class="catalogue__text big">Mediterranean Blue</div>
							</a><!-- .catalogue__card -->
						</div><!-- .col -->
						<div class="col col-3">
							<a href="#" id="catalogue-2" class="catalogue__card block" data-imgs='[{"href":"/assets/img/catalogue/2-1.jpg"},{"href":"/assets/img/catalogue/2-2.jpg"},{"href":"/assets/img/catalogue/2-3.jpg"},{"href":"/assets/img/catalogue/2-4.jpg"}]' tabindex="-1">
								<div class="catalogue__text big">Modern Touch</div>
							</a><!-- .catalogue__card -->
						</div><!-- .col -->
						<div class="col col-3">
							<a href="#" id="catalogue-3" class="catalogue__card block" data-imgs='[{"href":"/assets/img/catalogue/3-1.jpg"},{"href":"/assets/img/catalogue/3-2.jpg"},{"href":"/assets/img/catalogue/3-3.jpg"}]' tabindex="-1">
								<div class="catalogue__text big">Pink Lady</div>
							</a><!-- .catalogue__card -->
						</div><!-- .col -->
						<div class="col col-3">
							<a href="#" id="catalogue-4" class="catalogue__card block" data-imgs='[{"href":"/assets/img/catalogue/4-1.jpg"},{"href":"/assets/img/catalogue/4-2.jpg"}]' tabindex="-1">
								<div class="catalogue__text big">Neo-classic</div>
							</a><!-- .catalogue__card -->
						</div><!-- .col -->
					</div><!-- .grid -->
				</div><!-- .container -->
			</div><!-- .section #catalogue -->
		</div><!-- .content -->

		<!-- Page footer -->
		<div class="footer">
			<div class="container">
				<div class="grid">
					<div class="col col-2">
						<p class="small bold hide"><a href="/team" tabindex="-1">Team</a></p>
						<p class="small bold"><a href="/about" tabindex="-1">About Us</a></p>
						<p class="small bold"><a href="/contact" tabindex="-1">Contact Us</a></p>
					</div><!-- .col -->
					<div class="col col-3"></div><!-- .col -->
					<div class="col col-7">
						<!-- Form for submitting an email for newsletter subscription -->
						<form id="newsLetterForm" class="grid">
							<div class="col col-9 no-gutter">
								<input type="text" name="email" class="txt txt--white block" spellcheck="false" placeholder="Enter your email address..." tabindex="-1">
							</div><!-- .col -->
							<div class="col col-3 no-gutter">
								<button class="btn block" tabindex="-1">Subscribe</button>
							</div><!-- .col -->
						</form><!-- .grid -->
						<p class="smaller">Subscribe to our newsletter for all the latest updates about HomeArte</p>
						<!-- Paragraph to show form validation messages -->
						<p class="small js-newsletter-msg"></p>
					</div><!-- .col -->
				</div><!-- .grid -->
			</div><!-- .container -->
			<hr />
			<div class="grid">
				<div class="col col-4 no-gutter">
					<div>
						<span class="smaller">Follow us on</span>
						&nbsp;
						<a href="https://www.facebook.com/Homeartein-1608384559416314/timeline/" class="social--facebook big" target="_blank" title="Facebook" tabindex="-1">
							<span class="fa fa-facebook-square fa-fw"></span>
						</a>
						<a href="http://www.twitter.com/home_arte" class="social--twitter big" target="_blank" title="Twitter" tabindex="-1">
							<span class="fa fa-twitter-square fa-fw"></span>
						</a>
						<a href="https://www.linkedin.com/company/homearte-in?report%2Esuccess=KJ_KkFGTDCfMt-A7wV3Fn9Yvgwr02Kd6AZHGx4bQCDiP6-2rfP2oxyVoEQiPrcAQ7Bf" class="social--linkedin big" target="_blank" title="LinkedIn" tabindex="-1">
							<span class="fa fa-linkedin-square fa-fw"></span>
						</a>
					</div>
				</div><!-- .col -->
				<div class="col col-4 no-gutter center">
					&copy; 2015 HomeArte
				</div><!-- .col -->
				<div class="col col-4 no-gutter align-right">
					<a href="/privacy" class="smaller">Privacy Policy</a>
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .footer -->
		
	</div>

	<!-- Page sidebar -->
	<div class="sidebar" data-visible="false">
		<p class="pad align-right">
			<a href="#hide-sidebar" class="white no-states js-toggle-sidebar" tabindex="-1">
				<span class="fa fa-times fa-fw"></span>
			</a>
		</p>
		<p class="pad">
			<a href="/signup" class="btn btn--accent block center no-states hide" tabindex="-1">Signup</a>
		</p>
		<ul class="sidebar__nav big">
			<li>
				<a href="/" class="white block no-states">
					<span class="fa fa-home fa-fw"></span>&nbsp;&nbsp; Home
				</a>
			</li>
			<li>
				<a href="/ulogin" class="white block no-states">
					<span class="fa fa-sign-in fa-fw"></span>&nbsp;&nbsp; Login
				</a>
			</li>
			<li>
				<a href="/contact" class="white block no-states">
					<span class="fa fa-phone-square fa-fw"></span>&nbsp;&nbsp; Contact Us
				</a>
			</li>
			<li>
				<a href="/about" class="white block no-states">
					<span class="fa fa-info-circle fa-fw"></span>&nbsp;&nbsp; About
				</a>
			</li>
		</ul>
	</div><!-- .sidebar -->
	<div class="sidebar__overlay hide"></div><!-- .sidebar__overlay -->

	<!-- Import any and all scripts here -->
	<!-- build:js /assets/js/dist/index.min.js -->
	<script src="/assets/js/lib/jquery.min.js"></script>
	<script src="/assets/js/lib/jquery.swipebox.min.js"></script>
	<script src="/assets/js/validators/email.js"></script>
	<script src="/assets/js/common/methods.js"></script>
	<script src="/assets/js/common/setup.js"></script>
	<script src="/assets/js/common/sidebar.js"></script>
	<script src="/assets/js/common/footer.js"></script>
	<script src="/assets/js/modules/carousel.js"></script>
	<!-- endbuild -->
	<script>
		$( document ).ready( function() {
			$( ".catalogue__card" ).on( "click", function( e ) {
				e.preventDefault();
				$.swipebox( $( this ).data( "imgs" ) || [] );
			});
		});
	</script>
</body>
</html>

<!-- @author Navdeep -->