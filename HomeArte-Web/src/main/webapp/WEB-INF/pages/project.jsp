<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- All the meta tags -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="link-prefix" content="">
	<meta name="_csrf" content="${_csrf.token}">
	<meta name="builder-id" content="${ddto.builderId}">
	<meta name="proj-id" content="${pdto.projectId}">

	<!-- Page title -->
	<title>Project | HomeArte</title>

	<!-- Import any and all stylesheets here -->
	<link rel="stylesheet" href="/assets/css/lib/font-awesome.min.css">
	<!-- build:css /assets/css/style.min.css -->
	<link rel="stylesheet" href="/assets/css/build.css">
	<!-- endbuild -->
	<!-- NOTE: Do not put script tags here! -->
</head>
<body>

	<!-- The main page content is encapsulated in this div -->
	<div class="page">
		<!-- Page header -->
		<div class="header header--dashboard grid">
			<div class="col col-4 no-gutter">
				<span id="today" class="small white" style="padding-left: 10px"></span>
			</div><!-- .col -->
			<div class="col col-4 no-gutter center">
				<a href="/" class="bigger upper print-clearly loose-text white" tabindex="-1">HomeArte</a>
			</div><!-- .col -->
			<div class="col col-4 no-gutter align-right">
				<div class="notif__bell inline-block" style="width: 100px">
					<a href="#show-notif" class="white no-states" tabindex="-1">
						<span class="fa fa-bell-o fa-fw small"></span>
						<span class="notif__indicator inline-block"></span>
					</a>
					<div class="notif__drawer hide">
						<div class="notif__drawer__main scroll-vert">
							<div class="white center">
								<span class="underline">Notifications</span>
							</div>
							<ul class="align-left">
								<c:forEach var="notif" items="${ddto.userNotifications}">
									<c:set var="unreadClass" scope="session" value="notif--read " />
									<c:if test="${notif.status == 0}">
										<c:set var="unreadClass" scope="session" value="notif--unread " />
									</c:if>
									<li>
										<a href="#" class="notif ${unreadClass} small white block no-states">${notif.notificationText}</a>
									</li>
								</c:forEach>
							</ul>
						</div><!-- .notif__drawer__main -->
						<div class="notif__drawer__footer center">
							<a href="#" class="small white center block no-states">
								view all <span class="fa fa-angle-right fa-fw"></span>
							</a>
						</div><!-- .notif__drawer__footer -->
					</div><!-- .notif__drawer -->
				</div><!-- .notif__bell -->
				<span class="white">|</span>
				<a href="/logout" class="small white no-states" tabindex="-1">Logout</a>
			</div><!-- .col -->
		</div><!-- .header.grid -->

		<!-- Page content -->
		<div class="content content--dashboard">
			<div class="grid">
				<div class="col col-12">
					<div class="bigger editable">
						<span id="projName" class="editable__text">${pdto.projectName}</span>
						<a href="#edit" class="editable__link">
							<span class="fa fa-pencil fa-fw"></span>
						</a>
					</div>
					<p class="small">Created on ${pdto.creationTimestamp}</p>
					<p class="big editable">
						<span id="projDesc" class="editable__text">${pdto.projectDescription}</span>
						<a href="#edit" class="editable__link">
							<span class="fa fa-pencil fa-fw"></span>
						</a>
					</p>
					<p class="small">
						<a href="/dashboard/project/${pdto.projectId}/sub-project/create">create a sub-project</a>
					</p>
					<div id="subProjs">
						<p class="small">Loading sub-projects...</p>
					</div>
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .content -->

		<!-- Page sidebar -->
		<div class="footer footer--dashboard footer--dashboard--sidebar">
			<p class="center no-margin">&copy; 2015 HomeArte</p>
		</div><!-- .footer -->
	</div>

	<!-- Page sidebar -->
	<div class="sidebar sidebar--dashboard">
		<div class="center">
			<div class="img--circle">
				<img src="/assets/img/user-placeholder.png" alt="User Image">
			</div>
		</div>
		<p class="big center">${ddto.userName}</p>
		<hr />
		<ul class="sidebar__nav">
			<li>
				<a href="/dashboard" class="small block">
					<span class="fa fa-folder-open fa-fw"></span>&nbsp;&nbsp; Projects
				</a>
			</li>
			<li>
				<a href="/builder-info" class="small block">
					<span class="fa fa-user fa-fw"></span>&nbsp;&nbsp; View builder info
				</a>
			</li>
			<li>
				<a href="/account" class="small block">
					<span class="fa fa-cogs fa-fw"></span>&nbsp;&nbsp; Account settings
				</a>
			</li>
		</ul>
	</div><!-- .sidebar -->

	<!-- Import any and all scripts here -->
	<!-- build:js /assets/js/dist/project.min.js -->
	<script src="/assets/js/lib/jquery.min.js"></script>
	<script src="/assets/js/lib/handlebars.runtime.min.js"></script>
	<script src="/assets/js/common/methods.js"></script>
	<script src="/assets/js/common/setup.js"></script>
	<script src="/assets/js/common/sidebar.js"></script>
	<script src="/assets/js/utils/date-utils.js"></script>
	<script src="/assets/js/utils/template-utils.js"></script>
	<script src="/assets/js/build/hbs-templates.js"></script>
	<script src="/assets/js/modules/notifications.js"></script>
	<script src="/assets/js/modules/modal.js"></script>
	<script src="/assets/js/modules/inline-edit.js"></script>
	<script src="/assets/js/pages/project.js"></script>
	<!-- endbuild -->
	<script>
		// Set the current date:
		DateUtils.setCurrDate( document.getElementById( "today" ) );
	</script>
</body>
</html>

<!-- @author Navdeep -->