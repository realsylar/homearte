CREATE DATABASE  IF NOT EXISTS `homearte` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `homearte`;
-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: homearte
-- ------------------------------------------------------
-- Server version	5.6.25-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appliances`
--

DROP TABLE IF EXISTS `appliances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appliances` (
  `appliance_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `display_name` varchar(45) DEFAULT NULL,
  `company` varchar(45) NOT NULL,
  `specifications` varchar(45) DEFAULT NULL,
  `dimensions` varchar(45) DEFAULT NULL,
  `description` varchar(155) DEFAULT NULL,
  `photo_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`appliance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appliances`
--

LOCK TABLES `appliances` WRITE;
/*!40000 ALTER TABLE `appliances` DISABLE KEYS */;
/*!40000 ALTER TABLE `appliances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authority` varchar(85) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES (1,'VIEW_OWNER_DASHBOARD'),(2,'VIEW_AND_MANAGE_SUBSCRIPTION_DETAILS'),(3,'ALTER_OWNER_DATA'),(4,'DELETE_OWNER_RECORD');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `basic_property`
--

DROP TABLE IF EXISTS `basic_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basic_property` (
  `key` varchar(45) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `basic_property`
--

LOCK TABLES `basic_property` WRITE;
/*!40000 ALTER TABLE `basic_property` DISABLE KEYS */;
INSERT INTO `basic_property` VALUES ('aes_encrypt_2nd_key','aep29cm38fk10xmr'),('aes_encrypt_main_key','ap49sk2hd948ale2'),('ses_auth_required','true'),('ses_host','email-smtp.us-west-2.amazonaws.com'),('ses_port','25'),('ses_protocol','smtp'),('ses_smtp_password','AmZTHR9VD+vtM+JkjeRlg7/uKR4gyydJBBGXGkQ/cj0f'),('ses_smtp_username','AKIAJVU6MZKQ5SHNXNCA'),('ses_starttls_enable','true'),('ses_starttls_required','true'),('ses_user_name','ses-smtp-user-homearte');
/*!40000 ALTER TABLE `basic_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `basic_token_based_user_interaction`
--

DROP TABLE IF EXISTS `basic_token_based_user_interaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basic_token_based_user_interaction` (
  `intrxn_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(256) NOT NULL,
  `intrxn_type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `salt` varchar(256) NOT NULL,
  PRIMARY KEY (`intrxn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `basic_token_based_user_interaction`
--

LOCK TABLES `basic_token_based_user_interaction` WRITE;
/*!40000 ALTER TABLE `basic_token_based_user_interaction` DISABLE KEYS */;
INSERT INTO `basic_token_based_user_interaction` VALUES (1,33,'NzAzYjM0OTRiNDI2NzU4YzI4NjdmNTFkMjQzMWQ0ZThmNDM2YzRiNGU0YjdlM2IwNDYxNTM5YjA2M2ViMzg0Zg==',1,0,'2015-09-04 12:34:37','[-4, 45, -95, -13, 17, -20, 125, -80, -24, -43, -20, 83, 92, 101, -18, 63, 60, 25, -96, 44, -48, -64, 91, 50, -86, 24, 59, -39, 84, -44, -60, 69]');
/*!40000 ALTER TABLE `basic_token_based_user_interaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `builder_info`
--

DROP TABLE IF EXISTS `builder_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `builder_info` (
  `builder_id` int(11) NOT NULL,
  `builder_name` varchar(45) NOT NULL,
  `builder_description` varchar(215) DEFAULT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`builder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `builder_info`
--

LOCK TABLES `builder_info` WRITE;
/*!40000 ALTER TABLE `builder_info` DISABLE KEYS */;
INSERT INTO `builder_info` VALUES (1,'anurag_test_builder','This is a test builder created to check the data flow','2015-09-02 14:57:47','2015-09-02 14:57:47',1);
/*!40000 ALTER TABLE `builder_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_contact_info`
--

DROP TABLE IF EXISTS `delivery_contact_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_contact_info` (
  `contact_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `pincode` int(10) NOT NULL,
  `first_street` varchar(95) NOT NULL,
  `country` varchar(95) NOT NULL,
  `second_street` varchar(95) NOT NULL,
  `landmark` varchar(45) DEFAULT NULL,
  `state` varchar(95) NOT NULL,
  PRIMARY KEY (`contact_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_contact_info`
--

LOCK TABLES `delivery_contact_info` WRITE;
/*!40000 ALTER TABLE `delivery_contact_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_contact_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design_unit`
--

DROP TABLE IF EXISTS `design_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design_unit` (
  `design_unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `display_name` varchar(45) DEFAULT NULL,
  `raw_dimensions` varchar(45) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `metadata_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`design_unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `design_unit`
--

LOCK TABLES `design_unit` WRITE;
/*!40000 ALTER TABLE `design_unit` DISABLE KEYS */;
/*!40000 ALTER TABLE `design_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design_unit_metadata`
--

DROP TABLE IF EXISTS `design_unit_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design_unit_metadata` (
  `design_unit_metadata_id` int(11) NOT NULL AUTO_INCREMENT,
  `specifications` varchar(255) DEFAULT NULL,
  `unit_photo_id` varchar(45) DEFAULT NULL,
  `cabinet_material_id` int(11) NOT NULL,
  `cabinet_finish_id` int(11) NOT NULL,
  `cabinet_color_scheme_id` int(11) NOT NULL,
  `shutter_material_id` int(11) NOT NULL,
  `shutter_finish_id` int(11) NOT NULL,
  `shutter_color_scheme_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`design_unit_metadata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `design_unit_metadata`
--

LOCK TABLES `design_unit_metadata` WRITE;
/*!40000 ALTER TABLE `design_unit_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `design_unit_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designer_info`
--

DROP TABLE IF EXISTS `designer_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designer_info` (
  `designer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `display_name` varchar(45) DEFAULT NULL,
  `email_id` varchar(45) NOT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`designer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designer_info`
--

LOCK TABLES `designer_info` WRITE;
/*!40000 ALTER TABLE `designer_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `designer_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `display_design`
--

DROP TABLE IF EXISTS `display_design`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `display_design` (
  `display_design_id` int(11) NOT NULL AUTO_INCREMENT,
  `display_name` varchar(45) NOT NULL,
  `kitchen_size` varchar(45) NOT NULL,
  `display_description` varchar(45) DEFAULT NULL,
  `designer_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `design_price` varchar(45) NOT NULL,
  `design_metadata_id` int(11) NOT NULL,
  PRIMARY KEY (`display_design_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `display_design`
--

LOCK TABLES `display_design` WRITE;
/*!40000 ALTER TABLE `display_design` DISABLE KEYS */;
/*!40000 ALTER TABLE `display_design` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `display_design_metadata`
--

DROP TABLE IF EXISTS `display_design_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `display_design_metadata` (
  `display_design_metadata_id` int(11) NOT NULL AUTO_INCREMENT,
  `specifications` varchar(225) DEFAULT NULL,
  `cabinet_material_id` int(11) NOT NULL,
  `cabinet_finish_id` int(11) NOT NULL,
  `cabinet_color_scheme_id` int(11) NOT NULL,
  `shutter_material_id` int(11) NOT NULL,
  `shutter_finish_id` int(11) NOT NULL,
  `shutter_color_scheme_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`display_design_metadata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `display_design_metadata`
--

LOCK TABLES `display_design_metadata` WRITE;
/*!40000 ALTER TABLE `display_design_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `display_design_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_subscriber_info`
--

DROP TABLE IF EXISTS `newsletter_subscriber_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_subscriber_info` (
  `subs_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`subs_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_subscriber_info`
--

LOCK TABLES `newsletter_subscriber_info` WRITE;
/*!40000 ALTER TABLE `newsletter_subscriber_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_subscriber_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_info`
--

DROP TABLE IF EXISTS `project_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_info` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `builder_id` varchar(45) NOT NULL,
  `project_name` varchar(45) NOT NULL,
  `project_description` varchar(256) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  `no_of_subproject` int(11) DEFAULT '0',
  PRIMARY KEY (`project_id`),
  KEY `s_index_1` (`builder_id`),
  FULLTEXT KEY `search_index_1` (`project_name`,`project_description`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_info`
--

LOCK TABLES `project_info` WRITE;
/*!40000 ALTER TABLE `project_info` DISABLE KEYS */;
INSERT INTO `project_info` VALUES (1,'1','anurag_test_project','This is a test project created to check the flow of data','2015-09-02 14:56:41','2015-09-03 15:55:11',1,3),(2,'1','anurag_test_project_2','This is another test project created to check the flow of data','2015-09-02 14:58:34','2015-09-03 15:55:11',1,5),(3,'1','test_project_3','This is another test project created to check the flow of data','2015-09-04 16:20:44','2015-09-04 16:20:44',1,7),(4,'1','test_project_4','This is another test project created to check the flow of data','2015-09-04 16:20:44','2015-09-04 16:20:44',1,7),(5,'1','test_project_5','This is another test project created to check the flow of data','2015-09-04 16:20:44','2015-09-04 16:20:44',1,7),(6,'1','test_project_6','This is another test project created to check the flow of data','2015-09-04 16:20:44','2015-09-04 16:20:44',1,7),(7,'1','test_project_7','This is another test project created to check the flow of data','2015-09-04 16:20:44','2015-09-04 16:20:44',1,7),(8,'1','anurag_test_8','This is a test.','2015-09-04 18:06:08','2015-09-04 18:06:08',1,0);
/*!40000 ALTER TABLE `project_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_units`
--

DROP TABLE IF EXISTS `project_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_units` (
  `project_unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_unit_name` varchar(45) NOT NULL,
  `p_unit_display_name` varchar(45) DEFAULT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`project_unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_units`
--

LOCK TABLES `project_units` WRITE;
/*!40000 ALTER TABLE `project_units` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rm_cookie_details`
--

DROP TABLE IF EXISTS `rm_cookie_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rm_cookie_details` (
  `cookie_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(256) NOT NULL,
  `salt` varchar(256) NOT NULL,
  `user_agent` varchar(155) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cookie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rm_cookie_details`
--

LOCK TABLES `rm_cookie_details` WRITE;
/*!40000 ALTER TABLE `rm_cookie_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `rm_cookie_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_authorities`
--

DROP TABLE IF EXISTS `role_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_authorities` (
  `role_id` int(11) NOT NULL,
  `auth_id` int(11) NOT NULL,
  KEY `role_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_authorities`
--

LOCK TABLES `role_authorities` WRITE;
/*!40000 ALTER TABLE `role_authorities` DISABLE KEYS */;
INSERT INTO `role_authorities` VALUES (1,1),(1,2),(2,3),(2,4);
/*!40000 ALTER TABLE `role_authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(85) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'SUPER_ADMIN'),(2,'SYSTEM_ADMIN');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_builder_mapping`
--

DROP TABLE IF EXISTS `user_builder_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_builder_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `builder_id` int(11) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_builder_mapping`
--

LOCK TABLES `user_builder_mapping` WRITE;
/*!40000 ALTER TABLE `user_builder_mapping` DISABLE KEYS */;
INSERT INTO `user_builder_mapping` VALUES (1,33,1,'2015-09-02 14:53:22',1),(2,18,1,'2015-09-03 17:52:25',1);
/*!40000 ALTER TABLE `user_builder_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_delivery_address`
--

DROP TABLE IF EXISTS `user_delivery_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_delivery_address` (
  `user_id` int(11) NOT NULL,
  `contact_info_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_delivery_address`
--

LOCK TABLES `user_delivery_address` WRITE;
/*!40000 ALTER TABLE `user_delivery_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_delivery_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(115) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  `password` varchar(256) NOT NULL,
  `user_type` int(11) NOT NULL,
  `salt` varchar(256) NOT NULL,
  `primary_phone_number` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `primary_phone_number_UNIQUE` (`primary_phone_number`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` VALUES (18,'Navdeep','Singh Bagga','navdeep@homearte.in','2015-08-24 14:25:49','2015-08-24 14:25:49',0,'NGM2YTVhNmIxM2JjMzFkYWM0MWE0NmU3YmY1ZTU0NWNhMjUwOGE1ZmYxNTRkZjE5Njc1YzhmZjFkYjc5YzIwZg==',0,'[65, 5, -74, -3, -105, 88, -23, -28, 13, -109, 29, -36, 24, -43, 114, 7, -122, 118, -54, 77, 33, 12, 22, 116, 21, 119, -56, 61, -127, -22, -56, -4]',NULL),(33,'Anurag','Agrawal','anurag@homearte.in','2015-08-31 09:59:37','2015-08-31 10:00:11',1,'YzE4MDg0MWU1NjZlYjg4ODQwYWMxNWQ2YzI3YTIwNDFlNDM2MzcwMTY2YjAwZTk0YjcwMzgyOGMzZjcyMjQwNw==',0,'[-57, 53, -74, -7, 6, 112, -73, -53, 47, 21, -92, -57, 55, 37, 97, 58, -23, -59, 61, -1, -92, 94, 72, 76, 29, -101, 69, 121, -37, -74, 17, 6]',NULL);
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_notifications`
--

DROP TABLE IF EXISTS `user_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `notification_text` varchar(115) NOT NULL,
  `notification_summary` varchar(215) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notification_type` int(11) NOT NULL,
  `notification_status` int(11) NOT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_notifications`
--

LOCK TABLES `user_notifications` WRITE;
/*!40000 ALTER TABLE `user_notifications` DISABLE KEYS */;
INSERT INTO `user_notifications` VALUES (1,33,'setup_completed','test notification to make sure data flow is working fine','2015-09-03 09:44:18','2015-09-03 09:44:18',0,0),(2,33,'verfiy email','test notification to verify email','2015-09-03 09:44:18','2015-09-03 09:44:18',0,0),(3,18,'verfiy email','test notification to verify email','2015-09-03 09:44:18','2015-09-03 09:44:18',0,0),(4,18,'setup_completed','test notification to make sure data flow is working fine','2015-09-03 09:44:18','2015-09-03 09:44:18',0,0);
/*!40000 ALTER TABLE `user_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `user_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1),(1,2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor_info`
--

DROP TABLE IF EXISTS `vendor_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendor_info` (
  `vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(45) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT NULL,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor_info`
--

LOCK TABLES `vendor_info` WRITE;
/*!40000 ALTER TABLE `vendor_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-04 23:38:36
