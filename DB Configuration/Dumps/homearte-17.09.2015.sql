CREATE DATABASE  IF NOT EXISTS `homearte` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `homearte`;
-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: homearte
-- ------------------------------------------------------
-- Server version	5.6.25-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `basic_property`
--

DROP TABLE IF EXISTS `basic_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basic_property` (
  `key` varchar(45) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `basic_property`
--

LOCK TABLES `basic_property` WRITE;
/*!40000 ALTER TABLE `basic_property` DISABLE KEYS */;
INSERT INTO `basic_property` VALUES ('aes_encrypt_2nd_key','aep29cm38fk10xmr'),('aes_encrypt_main_key','ap49sk2hd948ale2'),('s3_access_key','AKIAJCE7UAJJB2VOU47Q'),('s3_access_secret_key','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'),('s3_bucket_name_project_data','homearte-projectmedia'),('ses_auth_required','true'),('ses_host','email-smtp.us-west-2.amazonaws.com'),('ses_port','25'),('ses_protocol','smtp'),('ses_smtp_password','AmZTHR9VD+vtM+JkjeRlg7/uKR4gyydJBBGXGkQ/cj0f'),('ses_smtp_username','AKIAJVU6MZKQ5SHNXNCA'),('ses_starttls_enable','true'),('ses_starttls_required','true'),('ses_user_name','ses-smtp-user-homearte');
/*!40000 ALTER TABLE `basic_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `basic_token_based_user_interaction`
--

DROP TABLE IF EXISTS `basic_token_based_user_interaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basic_token_based_user_interaction` (
  `intrxn_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(256) NOT NULL,
  `intrxn_type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `salt` varchar(256) NOT NULL,
  PRIMARY KEY (`intrxn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `basic_token_based_user_interaction`
--

LOCK TABLES `basic_token_based_user_interaction` WRITE;
/*!40000 ALTER TABLE `basic_token_based_user_interaction` DISABLE KEYS */;
INSERT INTO `basic_token_based_user_interaction` VALUES (1,33,'NzAzYjM0OTRiNDI2NzU4YzI4NjdmNTFkMjQzMWQ0ZThmNDM2YzRiNGU0YjdlM2IwNDYxNTM5YjA2M2ViMzg0Zg==',1,0,'2015-09-04 12:34:37','[-4, 45, -95, -13, 17, -20, 125, -80, -24, -43, -20, 83, 92, 101, -18, 63, 60, 25, -96, 44, -48, -64, 91, 50, -86, 24, 59, -39, 84, -44, -60, 69]'),(2,34,'NTgzYTMwOGFhNjM1N2ZlZGIyODAyZWI0Yzg0NzdkNzY4NjFkMjk2NmFmOWM4YjRkZWY0M2Y1MDdiYmFkY2YxOA==',0,0,'2015-09-17 13:18:36','[99, -98, 36, -99, -40, 62, 99, -25, -6, 7, 96, -6, -106, -107, -22, -42, -89, 29, -96, -73, -76, -43, -39, 18, -100, 52, -110, 109, 74, -89, -127, 118]');
/*!40000 ALTER TABLE `basic_token_based_user_interaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `builder_info`
--

DROP TABLE IF EXISTS `builder_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `builder_info` (
  `builder_id` int(11) NOT NULL,
  `builder_name` varchar(45) NOT NULL,
  `builder_description` varchar(215) DEFAULT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`builder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `builder_info`
--

LOCK TABLES `builder_info` WRITE;
/*!40000 ALTER TABLE `builder_info` DISABLE KEYS */;
INSERT INTO `builder_info` VALUES (1,'Bhawna Housing','Bhawna Housing is the Hallmark of perfect living. Bhawan housing since insecption created unmatched excellence in real estate development & executed numberous residential & commercial projects, located in Agra,the c','2015-09-02 14:57:47','2015-09-16 13:10:21',1);
/*!40000 ALTER TABLE `builder_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kitchen_color_configuration`
--

DROP TABLE IF EXISTS `kitchen_color_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kitchen_color_configuration` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `color_name` varchar(45) NOT NULL,
  `color_price` float NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  `uname` varchar(45) NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kitchen_color_configuration`
--

LOCK TABLES `kitchen_color_configuration` WRITE;
/*!40000 ALTER TABLE `kitchen_color_configuration` DISABLE KEYS */;
INSERT INTO `kitchen_color_configuration` VALUES (1,'Blue',15,'2015-09-14 15:10:03','2015-09-14 15:10:03',1,'cblue'),(2,'Red',25,'2015-09-14 15:10:03','2015-09-14 15:10:03',1,'cred');
/*!40000 ALTER TABLE `kitchen_color_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kitchen_configurations`
--

DROP TABLE IF EXISTS `kitchen_configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kitchen_configurations` (
  `kitchen_config_id` int(11) NOT NULL AUTO_INCREMENT,
  `kitchen_id` int(11) NOT NULL,
  `config_id` int(11) NOT NULL,
  `config_type` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_default` tinyint(1) NOT NULL,
  `configuration_price_multiplier` int(11) NOT NULL,
  PRIMARY KEY (`kitchen_config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kitchen_configurations`
--

LOCK TABLES `kitchen_configurations` WRITE;
/*!40000 ALTER TABLE `kitchen_configurations` DISABLE KEYS */;
INSERT INTO `kitchen_configurations` VALUES (1,1,1,1,1,'2015-09-14 15:28:13','2015-09-15 11:36:33',1,0),(2,1,2,1,1,'2015-09-14 15:28:57','2015-09-15 11:35:55',0,2),(3,1,1,2,1,'2015-09-14 15:28:57','2015-09-15 11:36:33',1,0),(4,1,2,2,1,'2015-09-14 15:28:57','2015-09-15 11:35:55',0,3);
/*!40000 ALTER TABLE `kitchen_configurations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kitchen_hardware_display_data`
--

DROP TABLE IF EXISTS `kitchen_hardware_display_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kitchen_hardware_display_data` (
  `dd_id` int(11) NOT NULL AUTO_INCREMENT,
  `kitchen_id` int(11) NOT NULL,
  `hardware_name` varchar(45) NOT NULL,
  `hardware_brand_name` varchar(45) NOT NULL,
  PRIMARY KEY (`dd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kitchen_hardware_display_data`
--

LOCK TABLES `kitchen_hardware_display_data` WRITE;
/*!40000 ALTER TABLE `kitchen_hardware_display_data` DISABLE KEYS */;
INSERT INTO `kitchen_hardware_display_data` VALUES (1,1,'Hinges','Hafelle'),(2,1,'Coupling','Hafelle');
/*!40000 ALTER TABLE `kitchen_hardware_display_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kitchen_info`
--

DROP TABLE IF EXISTS `kitchen_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kitchen_info` (
  `kitchen_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `content_preview_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `kitchen_shape` varchar(45) NOT NULL COMMENT '	',
  `kitchen_dimensions` varchar(45) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`kitchen_id`),
  UNIQUE KEY `product_id_UNIQUE` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kitchen_info`
--

LOCK TABLES `kitchen_info` WRITE;
/*!40000 ALTER TABLE `kitchen_info` DISABLE KEYS */;
INSERT INTO `kitchen_info` VALUES (1,1,13,12,'2015-09-14 15:12:38','2015-09-14 17:36:56','U Shaped Kitchen','7\" X 6\"',1);
/*!40000 ALTER TABLE `kitchen_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kitchen_material_configurations`
--

DROP TABLE IF EXISTS `kitchen_material_configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kitchen_material_configurations` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(45) NOT NULL,
  `material_price` float NOT NULL,
  `material_image_content_id` int(11) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  `uname` varchar(45) NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kitchen_material_configurations`
--

LOCK TABLES `kitchen_material_configurations` WRITE;
/*!40000 ALTER TABLE `kitchen_material_configurations` DISABLE KEYS */;
INSERT INTO `kitchen_material_configurations` VALUES (1,'MDF',240,0,'2015-09-14 15:11:07','2015-09-14 15:11:07',1,'mmdf'),(2,'Ply Wood',320,0,'2015-09-14 15:11:07','2015-09-15 16:26:07',1,'mplywood');
/*!40000 ALTER TABLE `kitchen_material_configurations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kitchen_unit_display_data`
--

DROP TABLE IF EXISTS `kitchen_unit_display_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kitchen_unit_display_data` (
  `dd_id` int(11) NOT NULL AUTO_INCREMENT,
  `kitchen_id` int(11) NOT NULL,
  `unit_name` varchar(45) NOT NULL,
  `unit_dimension` varchar(45) NOT NULL,
  `unit_material` varchar(45) NOT NULL,
  `shutter_material` varchar(45) NOT NULL,
  `shutter_color` varchar(45) NOT NULL,
  `unit_type` int(11) NOT NULL,
  PRIMARY KEY (`dd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kitchen_unit_display_data`
--

LOCK TABLES `kitchen_unit_display_data` WRITE;
/*!40000 ALTER TABLE `kitchen_unit_display_data` DISABLE KEYS */;
INSERT INTO `kitchen_unit_display_data` VALUES (1,1,'Unit 1','5\" x 4\" x 2\"','MDF','MDF','Blue',0),(2,1,'Unit 2','5\" x 4\" x 2\"','MDF','MDF','Blue',0),(3,1,'Unit 3','5\" x 3\" x 2\"','PLY','PLY','Red',1),(4,1,'Unit 4','5\" x 3\" x 2\"','PLY','PLY','Red',1);
/*!40000 ALTER TABLE `kitchen_unit_display_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mime_type_mapping`
--

DROP TABLE IF EXISTS `mime_type_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mime_type_mapping` (
  `key` varchar(45) NOT NULL,
  `value` varchar(45) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mime_type_mapping`
--

LOCK TABLES `mime_type_mapping` WRITE;
/*!40000 ALTER TABLE `mime_type_mapping` DISABLE KEYS */;
INSERT INTO `mime_type_mapping` VALUES ('application/excel','XLS'),('application/msword','DOC'),('application/pdf','PDF'),('application/vnd.ms-excel','XLS'),('application/x-excel','XLS'),('application/x-msexcel','XLS'),('image/bmp','IMAGE_BMP'),('image/jpeg','IMAGE_JPEG'),('image/pjpeg','IMAGE_JPEG'),('image/png','IMAGE_PNG'),('image/vnd.dwg','AUTOCAD_DWG'),('image/x-dwg','AUTOCAD_DWG'),('image/x-windows-bmp','IMAGE_BMP');
/*!40000 ALTER TABLE `mime_type_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_subscriber_info`
--

DROP TABLE IF EXISTS `newsletter_subscriber_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_subscriber_info` (
  `subs_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`subs_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_subscriber_info`
--

LOCK TABLES `newsletter_subscriber_info` WRITE;
/*!40000 ALTER TABLE `newsletter_subscriber_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_subscriber_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_info`
--

DROP TABLE IF EXISTS `product_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_info` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `base_price` float NOT NULL,
  `display_name` varchar(145) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `product_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_info`
--

LOCK TABLES `product_info` WRITE;
/*!40000 ALTER TABLE `product_info` DISABLE KEYS */;
INSERT INTO `product_info` VALUES (1,150000,'Sample Kitchen',1,'2015-09-13 16:46:00','2015-09-13 16:46:00',0);
/*!40000 ALTER TABLE `product_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_sub_project_mapping`
--

DROP TABLE IF EXISTS `product_sub_project_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_sub_project_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_project_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  `builder_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_sub_project_mapping`
--

LOCK TABLES `product_sub_project_mapping` WRITE;
/*!40000 ALTER TABLE `product_sub_project_mapping` DISABLE KEYS */;
INSERT INTO `product_sub_project_mapping` VALUES (1,1,1,1,'2015-09-13 17:35:17','2015-09-15 11:01:13',0,1);
/*!40000 ALTER TABLE `product_sub_project_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_info`
--

DROP TABLE IF EXISTS `project_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_info` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `builder_id` varchar(45) NOT NULL,
  `project_name` varchar(45) NOT NULL,
  `project_description` varchar(256) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  `no_of_subproject` int(11) DEFAULT '0',
  PRIMARY KEY (`project_id`),
  KEY `s_index_1` (`builder_id`),
  FULLTEXT KEY `search_index_1` (`project_name`,`project_description`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_info`
--

LOCK TABLES `project_info` WRITE;
/*!40000 ALTER TABLE `project_info` DISABLE KEYS */;
INSERT INTO `project_info` VALUES (1,'1','Bhawna Estate, Sikandra','Bhawna Housing is the Hallmark of perfect living. Bhawan housing since insecption created unmatched excellence in real estate development & executed numberous residential & commercial projects, located in Agra,the c','2015-09-02 14:56:41','2015-09-16 13:28:40',1,1);
/*!40000 ALTER TABLE `project_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_units`
--

DROP TABLE IF EXISTS `project_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_units` (
  `project_unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_unit_name` varchar(45) NOT NULL,
  `p_unit_display_name` varchar(45) DEFAULT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`project_unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_units`
--

LOCK TABLES `project_units` WRITE;
/*!40000 ALTER TABLE `project_units` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rm_cookie_details`
--

DROP TABLE IF EXISTS `rm_cookie_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rm_cookie_details` (
  `cookie_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(256) NOT NULL,
  `salt` varchar(256) NOT NULL,
  `user_agent` varchar(155) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cookie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rm_cookie_details`
--

LOCK TABLES `rm_cookie_details` WRITE;
/*!40000 ALTER TABLE `rm_cookie_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `rm_cookie_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_project_content_metadata`
--

DROP TABLE IF EXISTS `sub_project_content_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_project_content_metadata` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_name` varchar(145) NOT NULL,
  `content_mime_type` int(11) NOT NULL,
  `sub_project_id` int(11) NOT NULL,
  `is_display_image` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bucket_key_name` varchar(45) NOT NULL,
  `content_type` int(11) NOT NULL,
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_project_content_metadata`
--

LOCK TABLES `sub_project_content_metadata` WRITE;
/*!40000 ALTER TABLE `sub_project_content_metadata` DISABLE KEYS */;
INSERT INTO `sub_project_content_metadata` VALUES (12,'xyz.jpg',0,1,0,1,'2015-09-15 14:57:39','2015-09-15 18:32:24','products/1',1),(13,'abc.jpg',0,1,0,1,'2015-09-15 14:58:49','2015-09-15 18:32:24','products/1',1);
/*!40000 ALTER TABLE `sub_project_content_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_project_info`
--

DROP TABLE IF EXISTS `sub_project_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_project_info` (
  `sub_project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `sub_project_name` varchar(45) NOT NULL,
  `sub_project_description` varchar(256) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`sub_project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_project_info`
--

LOCK TABLES `sub_project_info` WRITE;
/*!40000 ALTER TABLE `sub_project_info` DISABLE KEYS */;
INSERT INTO `sub_project_info` VALUES (1,1,'Bhawna Estate Executive Apartments I Block','3BHK : 8ft by 10ft : Standard','2015-09-07 11:09:41','2015-09-16 13:12:51',1);
/*!40000 ALTER TABLE `sub_project_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_builder_mapping`
--

DROP TABLE IF EXISTS `user_builder_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_builder_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `builder_id` int(11) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_builder_mapping`
--

LOCK TABLES `user_builder_mapping` WRITE;
/*!40000 ALTER TABLE `user_builder_mapping` DISABLE KEYS */;
INSERT INTO `user_builder_mapping` VALUES (1,33,1,'2015-09-02 14:53:22',1),(2,18,1,'2015-09-03 17:52:25',1),(3,34,1,'2015-09-03 17:52:25',1),(4,35,1,'2015-09-03 17:52:25',1),(5,36,1,'2015-09-03 17:52:25',1);
/*!40000 ALTER TABLE `user_builder_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_delivery_address`
--

DROP TABLE IF EXISTS `user_delivery_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_delivery_address` (
  `user_id` int(11) NOT NULL,
  `contact_info_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_delivery_address`
--

LOCK TABLES `user_delivery_address` WRITE;
/*!40000 ALTER TABLE `user_delivery_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_delivery_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(115) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL,
  `password` varchar(256) NOT NULL,
  `user_type` int(11) NOT NULL,
  `salt` varchar(256) NOT NULL,
  `primary_phone_number` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `primary_phone_number_UNIQUE` (`primary_phone_number`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` VALUES (18,'Navdeep','Singh Bagga','navdeep@homearte.in','2015-08-24 14:25:49','2015-09-17 13:21:12',1,'NGM2YTVhNmIxM2JjMzFkYWM0MWE0NmU3YmY1ZTU0NWNhMjUwOGE1ZmYxNTRkZjE5Njc1YzhmZjFkYjc5YzIwZg==',0,'[65, 5, -74, -3, -105, 88, -23, -28, 13, -109, 29, -36, 24, -43, 114, 7, -122, 118, -54, 77, 33, 12, 22, 116, 21, 119, -56, 61, -127, -22, -56, -4]',NULL),(33,'Anurag','Agrawal','anurag@homearte.in','2015-08-31 09:59:37','2015-08-31 10:00:11',1,'YzE4MDg0MWU1NjZlYjg4ODQwYWMxNWQ2YzI3YTIwNDFlNDM2MzcwMTY2YjAwZTk0YjcwMzgyOGMzZjcyMjQwNw==',0,'[-57, 53, -74, -7, 6, 112, -73, -53, 47, 21, -92, -57, 55, 37, 97, 58, -23, -59, 61, -1, -92, 94, 72, 76, 29, -101, 69, 121, -37, -74, 17, 6]',NULL),(34,'Ronak','Gupta','ronak@homearte.in','2015-09-17 13:18:36','2015-09-17 13:21:12',1,'MGU3NmRlNjBkOTI2ODg4N2YxMGU2ZWRkOWJjOGVmYzk0MWIyYmVlYTljM2NiM2IwOGRkMmU2NjBmZmRmYzEzYQ==',0,'[-25, -89, 40, 29, 8, 22, -117, -65, 45, -6, 123, -83, -71, 42, 0, 10, -47, -103, -56, 10, 111, 15, -97, -51, -37, -84, 51, -83, -96, 17, -89, 39]',NULL),(35,'Saransh','Mahajan','saransh@homearte.in','2015-09-17 13:18:36','2015-09-17 13:21:12',1,'MGU3NmRlNjBkOTI2ODg4N2YxMGU2ZWRkOWJjOGVmYzk0MWIyYmVlYTljM2NiM2IwOGRkMmU2NjBmZmRmYzEzYQ==',0,'[-25, -89, 40, 29, 8, 22, -117, -65, 45, -6, 123, -83, -71, 42, 0, 10, -47, -103, -56, 10, 111, 15, -97, -51, -37, -84, 51, -83, -96, 17, -89, 39]',NULL),(36,'Vishal','Luniya','vishal@homearte.in','2015-09-17 13:18:36','2015-09-17 13:21:12',1,'MGU3NmRlNjBkOTI2ODg4N2YxMGU2ZWRkOWJjOGVmYzk0MWIyYmVlYTljM2NiM2IwOGRkMmU2NjBmZmRmYzEzYQ==',0,'[-25, -89, 40, 29, 8, 22, -117, -65, 45, -6, 123, -83, -71, 42, 0, 10, -47, -103, -56, 10, 111, 15, -97, -51, -37, -84, 51, -83, -96, 17, -89, 39]',NULL);
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_notifications`
--

DROP TABLE IF EXISTS `user_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `notification_text` varchar(115) NOT NULL,
  `notification_summary` varchar(215) NOT NULL,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notification_type` int(11) NOT NULL,
  `notification_status` int(11) NOT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_notifications`
--

LOCK TABLES `user_notifications` WRITE;
/*!40000 ALTER TABLE `user_notifications` DISABLE KEYS */;
INSERT INTO `user_notifications` VALUES (1,33,'setup_completed','test notification to make sure data flow is working fine','2015-09-03 09:44:18','2015-09-03 09:44:18',0,0),(2,33,'verfiy email','test notification to verify email','2015-09-03 09:44:18','2015-09-03 09:44:18',0,0),(3,18,'verfiy email','test notification to verify email','2015-09-03 09:44:18','2015-09-03 09:44:18',0,0),(4,18,'setup_completed','test notification to make sure data flow is working fine','2015-09-03 09:44:18','2015-09-03 09:44:18',0,0);
/*!40000 ALTER TABLE `user_notifications` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-17 18:54:36
