/**
 *
 */
package com.homearte.commons.aws.main;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.homearte.commons.aws.entity.SESMailingConfiguration;
import com.homearte.commons.aws.entity.SESMailingCredentialParams;
import com.homearte.commons.aws.entity.SESMailingMessageParams;

/**
 * @author Anurag Agrawal
 *
 */
public class SESEmailSender
{

	private SESMailingConfiguration	mailingConfiguration;

	/**
	 * @param mailingConfiguration
	 */
	public SESEmailSender(SESMailingConfiguration mailingConfiguration)
	{
		super();
		this.mailingConfiguration = mailingConfiguration;
	}

	public void sendEmail(SESMailingMessageParams message,
			SESMailingCredentialParams credentials) throws MessagingException
	{
		Properties props = System.getProperties();
		props = fillProperties(props);
		Session session = Session.getDefaultInstance(props);
		MimeMessage mimeMsg = new MimeMessage(session);
		mimeMsg = fillMessage(mimeMsg, message);
		Transport transport = null;
		try
		{
			transport = session.getTransport();

			System.out
			.println("Attempting to send an email through the Amazon SES SMTP interface...");
			transport.connect(credentials.getHost(),
					credentials.getSmtpUsername(),
					credentials.getSmtpPassword());
			transport.sendMessage(mimeMsg, mimeMsg.getAllRecipients());
			System.out.println("Email sent!");
		} catch (Exception ex)
		{
			System.out.println("The email was not sent.");
			System.out.println("Error message: " + ex.getMessage());
		} finally
		{
			transport.close();
		}
	}

	/**
	 * @param msg
	 * @param message
	 * @return
	 * @throws MessagingException
	 * @throws AddressException
	 */
	private MimeMessage fillMessage(MimeMessage msg,
			SESMailingMessageParams message) throws AddressException,
			MessagingException
	{
		msg.setFrom(new InternetAddress(message.getFromAddress()));
		for (String str : message.getToAddresses())
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(str));
		for (String str : message.getCcAddresses())
			msg.setRecipient(Message.RecipientType.CC, new InternetAddress(str));
		for (String str : message.getBccAddresses())
			msg.setRecipient(Message.RecipientType.BCC,
					new InternetAddress(str));
		msg.setSubject(message.getSubject());
		msg.setContent(message.getBody(), message.getBodyType().getValue());
		return msg;
	}

	/**
	 * @param props
	 * @return
	 */
	private Properties fillProperties(Properties props)
	{
		props.put("mail.transport.protocol", mailingConfiguration.getProtocol());
		props.put("mail.smtp.port", mailingConfiguration.getPort());
		props.put("mail.smtp.auth",
				mailingConfiguration.getAuthenticationRequired());
		props.put("mail.smtp.starttls.enable",
				mailingConfiguration.getStarttlsEnable());
		props.put("mail.smtp.starttls.required",
				mailingConfiguration.getStarttlsRequired());
		return props;
	}
}
