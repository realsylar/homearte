/**
 *
 */
package com.homearte.commons.aws.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum EmailBodyType
{

	TEXTPLAIN("text/plain"), TEXTHTML("text/html");

	private String	value;

	/**
	 * @param value
	 */
	private EmailBodyType(String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}

}
