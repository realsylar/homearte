/**
 *
 */
package com.homearte.commons.aws.entity;

/**
 * @author Anurag Agrawal
 *
 */
public class S3AccessCredentialParams
{

	private String	bucketName;
	private String	accessKey;
	private String	accessSecretKey;

	/**
	 * @param bucketName
	 * @param accessKey
	 * @param accessSecretKey
	 */
	public S3AccessCredentialParams(String bucketName, String accessKey,
			String accessSecretKey)
	{
		super();
		this.bucketName = bucketName;
		this.accessKey = accessKey;
		this.accessSecretKey = accessSecretKey;
	}

	/**
	 * @return the bucketName
	 */
	public String getBucketName()
	{
		return bucketName;
	}

	/**
	 * @return the accessKey
	 */
	public String getAccessKey()
	{
		return accessKey;
	}

	/**
	 * @return the accessSecretKey
	 */
	public String getAccessSecretKey()
	{
		return accessSecretKey;
	}

}
