/**
 *
 */
package com.homearte.commons.aws.entity;

/**
 * @author Anurag Agrawal
 *
 */
public class SESMailingConfiguration
{

	private String	protocol;
	private String	authenticationRequired;
	private String	starttlsEnable;
	private String	starttlsRequired;
	private int		port;

	/**
	 * @param protocol
	 * @param authenticationRequired
	 * @param starttlsEnable
	 * @param starttlsRequired
	 * @param port
	 */
	public SESMailingConfiguration(String protocol,
			String authenticationRequired, String starttlsEnable,
			String starttlsRequired, int port)
	{
		super();
		this.protocol = protocol;
		this.authenticationRequired = authenticationRequired;
		this.starttlsEnable = starttlsEnable;
		this.starttlsRequired = starttlsRequired;
		this.port = port;
	}

	/**
	 * @return the protocol
	 */
	public String getProtocol()
	{
		return protocol;
	}

	/**
	 * @return the authenticationRequired
	 */
	public String getAuthenticationRequired()
	{
		return authenticationRequired;
	}

	/**
	 * @return the starttlsEnable
	 */
	public String getStarttlsEnable()
	{
		return starttlsEnable;
	}

	/**
	 * @return the starttlsRequired
	 */
	public String getStarttlsRequired()
	{
		return starttlsRequired;
	}

	/**
	 * @return the port
	 */
	public int getPort()
	{
		return port;
	}

}
