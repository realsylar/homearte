/**
 *
 */
package com.homearte.commons.aws.entity;

/**
 * @author Anurag Agrawal
 *
 */
public class SESMailingCredentialParams
{

	private String	host;
	private String	smtpUsername;
	private String	smtpPassword;

	/**
	 * @return the host
	 */
	public String getHost()
	{
		return host;
	}

	/**
	 * @return the smtpUsername
	 */
	public String getSmtpUsername()
	{
		return smtpUsername;
	}

	/**
	 * @return the smtpPassword
	 */
	public String getSmtpPassword()
	{
		return smtpPassword;
	}

	/**
	 * @param host
	 * @param smtpUsername
	 * @param smtpPassword
	 */
	public SESMailingCredentialParams(String host, String smtpUsername,
			String smtpPassword)
	{
		super();
		this.host = host;
		this.smtpUsername = smtpUsername;
		this.smtpPassword = smtpPassword;
	}

}
