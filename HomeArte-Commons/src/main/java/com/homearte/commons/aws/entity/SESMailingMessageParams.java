/**
 *
 */
package com.homearte.commons.aws.entity;

import java.util.ArrayList;
import java.util.List;

import com.homearte.commons.aws.enums.EmailBodyType;

/**
 * @author Anurag Agrawal
 *
 */
public class SESMailingMessageParams
{

	private String			fromAddress;
	private List<String>	toAddresses;
	private List<String>	ccAddresses;
	private List<String>	bccAddresses;
	private String			subject;
	private String			body;
	private EmailBodyType	bodyType;

	/**
	 * @param fromAddress
	 * @param toAddresses
	 * @param ccAddresses
	 * @param bccAddresses
	 * @param subject
	 * @param body
	 * @param bodyType
	 */
	public SESMailingMessageParams(String fromAddress,
			List<String> toAddresses, List<String> ccAddresses,
			List<String> bccAddresses, String subject, String body,
			EmailBodyType bodyType)
	{
		super();
		this.fromAddress = fromAddress;
		this.toAddresses = toAddresses;
		if (ccAddresses == null)
			this.ccAddresses = new ArrayList<String>();
		else
			this.ccAddresses = ccAddresses;
		if (bccAddresses == null)
			this.bccAddresses = new ArrayList<String>();
		else
			this.bccAddresses = bccAddresses;
		this.subject = subject;
		this.body = body;
		this.bodyType = bodyType;
	}

	public SESMailingMessageParams(String fromAddress, String toAddress,
			String ccAddress, String bccAddress, String subject, String body,
			EmailBodyType bodyType)
	{
		super();
		this.fromAddress = fromAddress;
		this.toAddresses = new ArrayList<String>();
		this.toAddresses.add(toAddress);
		if (ccAddresses == null)
			this.ccAddresses = new ArrayList<String>();
		else
		{
			this.ccAddresses = new ArrayList<String>();
			this.ccAddresses.add(ccAddress);
		}
		if (bccAddresses == null)
			this.bccAddresses = new ArrayList<String>();
		else
		{
			this.bccAddresses = new ArrayList<String>();
			this.bccAddresses.add(bccAddress);
		}
		this.subject = subject;
		this.body = body;
		this.bodyType = bodyType;
	}

	/**
	 * @return the fromAddress
	 */
	public String getFromAddress()
	{
		return fromAddress;
	}

	/**
	 * @return the toAddresses
	 */
	public List<String> getToAddresses()
	{
		return toAddresses;
	}

	/**
	 * @return the ccAddresses
	 */
	public List<String> getCcAddresses()
	{
		return ccAddresses;
	}

	/**
	 * @return the bccAddresses
	 */
	public List<String> getBccAddresses()
	{
		return bccAddresses;
	}

	/**
	 * @return the subject
	 */
	public String getSubject()
	{
		return subject;
	}

	/**
	 * @return the body
	 */
	public String getBody()
	{
		return body;
	}

	/**
	 * @return the bodyType
	 */
	public EmailBodyType getBodyType()
	{
		return bodyType;
	}

}
