/**
 *
 */
package com.homearte.commons.aws.entity;

import java.io.InputStream;

import com.amazonaws.services.s3.model.S3Object;

/**
 * @author Anurag Agrawal
 *
 */
public class S3MultipartFileParams
{

	private S3Object	fileData;
	private InputStream	multipartFileInputStream;
	private String		fileKeyName;
	private long		fileSize;

	/**
	 * @return the fileData
	 */
	public S3Object getFileData()
	{
		return fileData;
	}

	/**
	 * @param fileData
	 *            the fileData to set
	 */
	public void setFileData(S3Object fileData)
	{
		this.fileData = fileData;
	}

	/**
	 * @return the multipartFileInputStream
	 */
	public InputStream getMultipartFileInputStream()
	{
		return multipartFileInputStream;
	}

	/**
	 * @return the fileKeyName
	 */
	public String getFileKeyName()
	{
		return fileKeyName;
	}

	/**
	 * @return the fileSize
	 */
	public long getFileSize()
	{
		return fileSize;
	}

	/**
	 * @param multipartFileInputStream
	 * @param fileKeyName
	 * @param fileSize
	 */
	public S3MultipartFileParams(InputStream multipartFileInputStream,
			String fileKeyName, long fileSize)
	{
		super();
		this.multipartFileInputStream = multipartFileInputStream;
		this.fileKeyName = fileKeyName;
		this.fileSize = fileSize;
	}

	/**
	 * @param multipartFileInputStream
	 *            the multipartFileInputStream to set
	 */
	public void setMultipartFileInputStream(InputStream multipartFileInputStream)
	{
		this.multipartFileInputStream = multipartFileInputStream;
	}

	/**
	 * @param fileKeyName
	 *            the fileKeyName to set
	 */
	public void setFileKeyName(String fileKeyName)
	{
		this.fileKeyName = fileKeyName;
	}

	/**
	 * @param fileSize
	 *            the fileSize to set
	 */
	public void setFileSize(long fileSize)
	{
		this.fileSize = fileSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "S3MultipartFileParams [multipartFileInputStream="
				+ multipartFileInputStream + ", fileKeyName=" + fileKeyName
				+ ", fileSize=" + fileSize + "]";
	}

}
