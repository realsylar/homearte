/**
 *
 */
package com.homearte.commons.cookie.exception;

/**
 * @author Anurag Agrawal
 *
 */
public class CookieNotUpdatedException extends Exception
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 *
	 */
	public CookieNotUpdatedException()
	{
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CookieNotUpdatedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CookieNotUpdatedException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public CookieNotUpdatedException(String message)
	{
		super(message);
	}

	/**
	 * @param cause
	 */
	public CookieNotUpdatedException(Throwable cause)
	{
		super(cause);
	}

}
