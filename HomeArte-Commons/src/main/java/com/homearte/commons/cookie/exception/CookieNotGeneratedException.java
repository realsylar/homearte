/**
 *
 */
package com.homearte.commons.cookie.exception;

/**
 * @author Anurag Agrawal
 *
 */
public class CookieNotGeneratedException extends Exception
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 *
	 */
	public CookieNotGeneratedException()
	{
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CookieNotGeneratedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CookieNotGeneratedException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public CookieNotGeneratedException(String message)
	{
		super(message);
	}

	/**
	 * @param cause
	 */
	public CookieNotGeneratedException(Throwable cause)
	{
		super(cause);
	}

}
