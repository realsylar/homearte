/**
 *
 */
package com.homearte.commons.cookie.manager;

import com.homearte.commons.cookie.enums.RMCookieVerificationStatus;
import com.homearte.commons.cookie.persistance.entity.RMCookie;

/**
 * @author Anurag Agrawal
 *
 */
public class RMCookieDTO
{

	private RMCookie					cookie;
	private RMCookieVerificationStatus	status;
	private String						userCookieValue;

	/**
	 * @return the userCookieValue
	 */
	public String getUserCookieValue()
	{
		return userCookieValue;
	}

	/**
	 * @return the cookie
	 */
	public RMCookie getCookie()
	{
		return cookie;
	}

	/**
	 * @return the status
	 */
	public RMCookieVerificationStatus getStatus()
	{
		return status;
	}

	/**
	 * @param userId
	 * @param cookie
	 * @param status
	 */
	public RMCookieDTO(RMCookie cookie, RMCookieVerificationStatus status,
			String userCookieValue)
	{
		super();
		this.cookie = cookie;
		this.status = status;
		this.userCookieValue = userCookieValue;
	}

}
