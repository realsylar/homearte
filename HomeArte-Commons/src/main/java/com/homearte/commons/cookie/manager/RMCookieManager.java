/**
 *
 */

package com.homearte.commons.cookie.manager;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.homearte.commons.cookie.enums.RMCookieVerificationStatus;
import com.homearte.commons.cookie.exception.CookieNotGeneratedException;
import com.homearte.commons.cookie.exception.CookieNotUpdatedException;
import com.homearte.commons.cookie.exception.CookieNotVerifiedException;
import com.homearte.commons.cookie.persistance.dao.RMCookieDAO;
import com.homearte.commons.cookie.persistance.entity.RMCookie;
import com.homearte.commons.core.persistance.exception.PersistanceException;
import com.homearte.commons.core.security.encryption.AESCBCEncryptor;
import com.homearte.commons.core.security.encryption.RandomSecureTokenGenerator;
import com.homearte.commons.core.security.encryption.SHA256Encryptor;
import com.homearte.commons.core.security.encryption.SHAEncryptedValue;
import com.homearte.commons.core.security.encryption.exception.EncryptorException;

/**
 * @author Anurag Agrawal
 *
 */

@Service
public class RMCookieManager
{

	private static Logger				logger		= Logger.getLogger(RMCookieDAO.class);
	private static final String			DELIMITER	= "$$";
	private RMCookieDAO					dao;
	private RandomSecureTokenGenerator	tokenGenerator;
	private AESCBCEncryptor				aesEncryptor;
	private SHA256Encryptor				shaEncryptor;

	/**
	 * @param dao
	 */

	@Autowired
	public RMCookieManager(RMCookieDAO dao,
			RandomSecureTokenGenerator tokenGenerator,
			AESCBCEncryptor aesEncryptor, SHA256Encryptor shaEncryptor)
	{
		super();
		this.dao = dao;
		this.tokenGenerator = tokenGenerator;
		this.aesEncryptor = aesEncryptor;
		this.shaEncryptor = shaEncryptor;
	}

	@Transactional
	public RMCookieDTO createNewRMCookieValue(int userId, String userAgent)
			throws CookieNotGeneratedException
	{
		String base64EncodedToken = generateEncryptedToken();
		String tokenHash;
		String tokenSalt;
		try
		{
			SHAEncryptedValue encValue = generateTokenHash(base64EncodedToken);
			tokenHash = encValue.getEncryptedValue();
			tokenSalt = encValue.getSalt();
		} catch (InvalidKeyException | NoSuchAlgorithmException e1)
		{
			e1.printStackTrace();
			throw new CookieNotGeneratedException("Token hash not generated",
					e1);
		}
		RMCookie rmCookie = new RMCookie(tokenHash, userAgent, userId,
				tokenSalt);
		try
		{
			rmCookie = dao.saveCookie(rmCookie);
		} catch (PersistanceException e)
		{
			e.printStackTrace();
			throw new CookieNotGeneratedException("Cookie could not be saved",
					e);
		}
		try
		{
			String cookieValue = encryptCookieValue(rmCookie.getCookieId()
					+ DELIMITER + base64EncodedToken);
			return new RMCookieDTO(rmCookie, null, cookieValue);
		} catch (EncryptorException e)
		{
			e.printStackTrace();
			try
			{
				dao.deleteCookie(rmCookie);
			} catch (PersistanceException e1)
			{
				e1.printStackTrace();
				logger.error(
						"RMCookie:Deletion:Could not delete the non issued cookie from DB:Cookie Id:"
								+ rmCookie.getCookieId(), e1);

			}
			throw new CookieNotGeneratedException(
					"Cookie value could not be generated", e);
		}
	}

	@Transactional
	public RMCookieDTO checkAndVerifyCookieValue(String userCookieValue,
			String userAgent) throws CookieNotVerifiedException
	{
		String decodedCookieValue;
		try
		{
			decodedCookieValue = decodeCookieValue(userCookieValue);
		} catch (EncryptorException e)
		{
			e.printStackTrace();
			throw new CookieNotVerifiedException(
					"Cookie value could not be decrypted so could not be verified",
					e);
		}
		try
		{
			String[] cookieValues = decodedCookieValue.split(DELIMITER);
			int cookieId = Integer.parseInt(cookieValues[0]);
			RMCookie cookie = dao.loadCookie(cookieId);
			if (cookie == null)
			{
				return new RMCookieDTO(null,
						RMCookieVerificationStatus.INVALID, userCookieValue);
			}
			String cookieToken = cookieValues[1];
			if (cookie.getToken().equals(
					shaEncryptor.encryptValue(cookieToken, cookie.getSalt()))
					&& cookie.getUserAgent().equals(userAgent))
			{
				return new RMCookieDTO(cookie,
						RMCookieVerificationStatus.CORRECT, userCookieValue);
			}
			return new RMCookieDTO(null, RMCookieVerificationStatus.STOLEN,
					userCookieValue);
		} catch (PersistanceException e)
		{
			e.printStackTrace();
			throw new CookieNotVerifiedException();
		} catch (InvalidKeyException | NoSuchAlgorithmException e)
		{
			e.printStackTrace();
			throw new CookieNotVerifiedException(
					"Cookie could not be verified beacuse encryption failed.",
					e);
		}
	}

	@Transactional
	public void deleteAllCookiesForUser(RMCookieDTO userCookieDTO)
			throws PersistanceException
	{
		dao.deleteCookieByUserId(userCookieDTO.getCookie().getUserId());
	}

	@Transactional
	public RMCookieDTO updateCookieForUser(RMCookieDTO userCookieDTO)
			throws CookieNotUpdatedException
	{
		String decodedCookieValue;
		try
		{
			decodedCookieValue = decodeCookieValue(userCookieDTO
					.getUserCookieValue());
		} catch (EncryptorException e)
		{
			e.printStackTrace();
			throw new CookieNotUpdatedException(
					"Cookie value could not be decrypted so could not be verified",
					e);
		}
		try
		{
			String[] cookieValues = decodedCookieValue.split(DELIMITER);
			int cookieId = Integer.parseInt(cookieValues[0]);
			RMCookie cookie = dao.loadCookie(cookieId);
			String newToken = generateEncryptedToken();
			SHAEncryptedValue encValue = generateTokenHash(newToken);
			cookie.setToken(encValue.getEncryptedValue());
			cookie.setSalt(encValue.getSalt());
			String cookieValue = encryptCookieValue(cookie.getCookieId()
					+ DELIMITER + newToken);
			return new RMCookieDTO(cookie, null, cookieValue);
		} catch (PersistanceException e)
		{
			e.printStackTrace();
			throw new CookieNotUpdatedException();
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| EncryptorException e)
		{
			e.printStackTrace();
			throw new CookieNotUpdatedException(
					"Cookie could not be verified beacuse encryption failed.",
					e);
		}
	}

	/**
	 * @param userCookieValue
	 * @return
	 * @throws EncryptorException
	 */

	private String decodeCookieValue(String userCookieValue)
			throws EncryptorException
	{
		return aesEncryptor.decrypt(userCookieValue);
	}

	/**
	 * @param cookieId
	 * @param base64EncodedToken
	 * @return
	 * @throws EncryptorException
	 */

	private String encryptCookieValue(String finalToken)
			throws EncryptorException
	{
		return aesEncryptor.encrypt(finalToken);
	}

	/**
	 * @param base64EncodedToken
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */

	private SHAEncryptedValue generateTokenHash(String base64EncodedToken)
			throws InvalidKeyException, NoSuchAlgorithmException
	{
		return shaEncryptor.encryptValue(base64EncodedToken);
	}

	/**
	 * @return
	 */

	private String generateEncryptedToken()
	{
		String token = tokenGenerator.generateToken();
		token = Base64.encodeBase64(token.getBytes()).toString();
		return token;
	}
}
