/**
 *
 */
package com.homearte.commons.cookie.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum RMCookieVerificationStatus
{
	CORRECT, STOLEN, INVALID;
}
