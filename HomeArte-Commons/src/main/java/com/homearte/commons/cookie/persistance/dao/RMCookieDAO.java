/**
 *
 */
package com.homearte.commons.cookie.persistance.dao;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.homearte.commons.cookie.persistance.entity.RMCookie;
import com.homearte.commons.core.persistance.DataPersistance;
import com.homearte.commons.core.persistance.exception.PersistanceException;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class RMCookieDAO
{

	private static Logger	logger	= Logger.getLogger(RMCookieDAO.class);
	private DataPersistance	dataPersistance;
	private SessionFactory	sessionFactory;

	/**
	 * @param dataPersistance
	 */
	@Autowired
	public RMCookieDAO(DataPersistance dataPersistance,
			SessionFactory sessionFactory)
	{
		super();
		this.dataPersistance = dataPersistance;
		this.sessionFactory = sessionFactory;
	}

	public RMCookie saveCookie(RMCookie cookie) throws PersistanceException
	{
		return dataPersistance.save(cookie);
	}

	public RMCookie updateCookie(RMCookie cookie) throws PersistanceException
	{
		return dataPersistance.saveOrUpdate(cookie);
	}

	public void deleteCookie(RMCookie cookie) throws PersistanceException
	{
		dataPersistance.delete(cookie);
	}

	public void deleteCookie(int cookieId) throws PersistanceException
	{
		dataPersistance.delete(cookieId);
	}

	public RMCookie loadCookie(int cookieId) throws PersistanceException
	{
		return (RMCookie) dataPersistance.loadById(RMCookie.class, cookieId);
	}

	public void deleteCookieByUserId(int userId) throws PersistanceException
	{
		try
		{
			Session session = sessionFactory.getCurrentSession();
			Query query = session
					.createQuery("delete RMCookie where userId = :userId");
			query.setParameter("userId", userId);
			query.executeUpdate();
		} catch (Exception e)
		{
			logger.error("Unable to save/update entity", e);
			throw new PersistanceException(e);
		}
	}
}
