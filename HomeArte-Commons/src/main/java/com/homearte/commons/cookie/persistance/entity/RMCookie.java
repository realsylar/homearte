/**
 *
 */
package com.homearte.commons.cookie.persistance.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "rm_cookie_details")
public class RMCookie
{

	@Transient
	public static final String	RM_COOKIE_NAME	= "hrp";

	@Id
	@Column(name = "cookie_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int					cookieId;

	@Column(name = "user_id")
	private int					userId;

	@Column(name = "token")
	private String				token;

	@Column(name = "salt")
	private String				salt;

	@Column(name = "user_agent")
	private String				userAgent;

	@Column(name = "creation_timestamp")
	private Timestamp			creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp			modificationTimestamp;

	/**
	 * @return the userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @return the cookieId
	 */
	public int getCookieId()
	{
		return cookieId;
	}

	/**
	 * @return the token
	 */
	public String getToken()
	{
		return token;
	}

	/**
	 * @return the userAgent
	 */
	public String getUserAgent()
	{
		return userAgent;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	public void setToken(String token)
	{
		this.token = token;
	}

	/**
	 * @return the salt
	 */
	public String getSalt()
	{
		return salt;
	}

	/**
	 * @param salt
	 *            the salt to set
	 */
	public void setSalt(String salt)
	{
		this.salt = salt;
	}

	/**
	 *
	 */
	protected RMCookie()
	{
		super();
	}

	/**
	 * @param token
	 * @param userAgent
	 */
	public RMCookie(String token, String userAgent, int userId, String salt)
	{
		super();
		this.userId = userId;
		this.token = token;
		this.userAgent = userAgent;
		this.salt = salt;
	}
}
