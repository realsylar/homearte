/**
 *
 */
package com.homearte.commons.cookie.exception;

/**
 * @author Anurag Agrawal
 *
 */
public class CookieNotVerifiedException extends Exception
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 *
	 */
	public CookieNotVerifiedException()
	{
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CookieNotVerifiedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CookieNotVerifiedException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public CookieNotVerifiedException(String message)
	{
		super(message);
	}

	/**
	 * @param cause
	 */
	public CookieNotVerifiedException(Throwable cause)
	{
		super(cause);
	}

}
