/**
 *
 */
package com.homearte.commons.core.persistance.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum QueryProjectionTypes
{
	START_LIMIT(0), END_LIMIT(1);

	private int	value;

	/**
	 * @param value
	 */
	private QueryProjectionTypes(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static QueryProjectionTypes getEnumForValue(int value)
	{
		for (QueryProjectionTypes type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
