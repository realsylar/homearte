package com.homearte.commons.core.persistance;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.homearte.commons.core.persistance.decorator.HQLQueryParameterDecorator;
import com.homearte.commons.core.persistance.decorator.HQLQueryProjectionDecorator;
import com.homearte.commons.core.persistance.exception.PersistanceException;

/**
 * @author Anurag Agrawal
 *
 */
@Transactional
@Repository
public class DataPersistance
{

	private static Logger				logger	= Logger.getLogger(DataPersistance.class);
	private SessionFactory				sessionFactory;
	private HQLQueryParameterDecorator	queryParamDecorator;
	private HQLQueryProjectionDecorator	queryProjectionDecorator;

	/**
	 * @param sessionFactory
	 */
	@Autowired
	public DataPersistance(SessionFactory sessionFactory,
			HQLQueryParameterDecorator queryParamDecorator,
			HQLQueryProjectionDecorator queryProjectionDecorator)
	{
		super();
		this.sessionFactory = sessionFactory;
		this.queryParamDecorator = queryParamDecorator;
		this.queryProjectionDecorator = queryProjectionDecorator;
	}

	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession()
	{
		return this.sessionFactory.getCurrentSession();
	}

	/**
	 * @param t
	 * @return saved entity
	 * @throws PersistanceException
	 */
	public <T> T save(T t) throws PersistanceException
	{
		try
		{
			Session session = getCurrentSession();
			session.save(t);
		} catch (Exception e)
		{
			logger.error("Unable to save entity", e);
			throw new PersistanceException(e);
		}
		return t;
	}

	public <T> List<T> save(List<T> t) throws PersistanceException
	{
		try
		{
			Session session = getCurrentSession();
			for (T entity : t)
			{
				session.save(entity);
			}
		} catch (Exception e)
		{
			logger.error("Unable to save entity", e);
			throw new PersistanceException(e);
		}
		return t;
	}

	/**
	 * @param t
	 * @return saved/updated entity
	 * @throws PersistanceException
	 */
	public <T> T saveOrUpdate(T t) throws PersistanceException
	{
		try
		{
			Session session = getCurrentSession();
			session.saveOrUpdate(t);
		} catch (Exception e)
		{
			logger.error("Unable to save/update entity", e);
			throw new PersistanceException(e);
		}
		return t;
	}

	/**
	 * @param t
	 * @return update entity
	 * @throws PersistanceException
	 */
	public <T> T update(T t) throws PersistanceException
	{
		try
		{
			Session session = getCurrentSession();
			session.update(t);
		} catch (Exception e)
		{
			logger.error("Unable to save/update entity", e);
			throw new PersistanceException(e);
		}
		return t;
	}

	public <T> void delete(T t) throws PersistanceException
	{
		try
		{
			Session session = getCurrentSession();
			session.delete(t);
		} catch (Exception e)
		{
			logger.error("Unable to save/update entity", e);
			throw new PersistanceException(e);
		}
	}

	public <T> void delete(Class<T> t, int identifier)
			throws PersistanceException
	{
		try
		{
			Session session = getCurrentSession();
			T entity = loadById(t, identifier);
			if (entity != null)
				session.delete(entity);
		} catch (Exception e)
		{
			logger.error("Unable to save/update entity", e);
			throw new PersistanceException(e);
		}
	}

	public <T> List<T> saveOrUpdateInBulk(List<T> t)
			throws PersistanceException
			{
		logger.info("Start time for bulk:" + System.currentTimeMillis() + ":"
				+ t.size());
		try
		{
			for (T entity : t)
				saveOrUpdate(entity);
		} catch (Exception e)
		{
			logger.error(e);
			throw new PersistanceException(e);
		}
		logger.info("End time for bulk:" + System.currentTimeMillis() + ":"
				+ t.size());
		return t;
			}

	public <T> T loadById(Class<T> t, String identifier)
			throws PersistanceException
	{
		logger.info("Loading object by id:" + identifier);
		Session session = getCurrentSession();
		T obj;
		try
		{
			obj = (T) session.get(t, identifier);
		} catch (Exception e)
		{
			logger.error("Error while getting object by id", e);
			throw new PersistanceException(e);
		}
		logger.info("Loaded object by id:" + identifier);
		return obj;
	}

	public <T> T loadById(Class<T> t, int identifier)
			throws PersistanceException
	{
		logger.info("Loading object by id:" + identifier);
		Session session = getCurrentSession();
		T obj;
		try
		{
			obj = (T) session.get(t, identifier);
		} catch (Exception e)
		{
			logger.error("Error while getting object by id", e);
			throw new PersistanceException(e);
		}
		logger.info("Loaded object by id:" + identifier);
		return obj;
	}

	/**
	 * @param query
	 * @return
	 */
	public <T> List<T> executeHQLQueryListResult(String queryStr,
			Map<String, QueryParam> parameters,
			List<QueryProjection> projections) throws PersistanceException
			{
		Session session = getCurrentSession();
		List<T> tList;
		try
		{
			Query query = session.createQuery(queryStr);
			query = queryParamDecorator.decorateQuery(query, parameters);
			query = queryProjectionDecorator.decorateQuery(query, projections);
			tList = query.list();
		} catch (Exception e)
		{
			logger.error("Error while getting object by id", e);
			throw new PersistanceException(e);
		}
		return tList;
			}

	/**
	 * @param query
	 * @return
	 */
	public <T> T executeHQLQueryUniqueResult(String queryStr,
			Map<String, QueryParam> parameters,
			List<QueryProjection> projections) throws PersistanceException
	{
		Session session = getCurrentSession();
		T t;
		try
		{
			Query query = session.createQuery(queryStr);
			query = queryParamDecorator.decorateQuery(query, parameters);
			query = queryProjectionDecorator.decorateQuery(query, projections);
			t = (T) query.uniqueResult();
		} catch (Exception e)
		{
			logger.error("Error while getting object by id", e);
			throw new PersistanceException(e);
		}
		return t;
	}

	/**
	 * @param query
	 * @return
	 */
	public int executeHQLQueryUpdateRecords(String queryStr,
			Map<String, QueryParam> parameters,
			List<QueryProjection> projections) throws PersistanceException
	{
		Session session = getCurrentSession();
		int recordsUpdated;
		try
		{
			Query query = session.createQuery(queryStr);
			query = queryParamDecorator.decorateQuery(query, parameters);
			query = queryProjectionDecorator.decorateQuery(query, projections);
			recordsUpdated = query.executeUpdate();
		} catch (Exception e)
		{
			logger.error("Error while getting object by id", e);
			throw new PersistanceException(e);
		}
		return recordsUpdated;
	}

	/**
	 * @param query
	 * @return
	 */
	public <T> List<T> executeHQLQueryListResult(String queryStr,
			Map<String, QueryParam> parameters) throws PersistanceException
			{
		Session session = getCurrentSession();
		List<T> tList;
		try
		{
			Query query = session.createQuery(queryStr);
			query = queryParamDecorator.decorateQuery(query, parameters);
			tList = query.list();
		} catch (Exception e)
		{
			logger.error("Error while getting object by id", e);
			throw new PersistanceException(e);
		}
		return tList;
			}

	/**
	 * @param query
	 * @return
	 */
	public <T> T executeHQLQueryUniqueResult(String queryStr,
			Map<String, QueryParam> parameters) throws PersistanceException
	{
		Session session = getCurrentSession();
		T t;
		try
		{
			Query query = session.createQuery(queryStr);
			query = queryParamDecorator.decorateQuery(query, parameters);
			t = (T) query.uniqueResult();
		} catch (Exception e)
		{
			logger.error("Error while getting object by id", e);
			throw new PersistanceException(e);
		}
		return t;
	}

	/**
	 * @param query
	 * @return
	 */
	public int executeHQLQueryUpdateRecords(String queryStr,
			Map<String, QueryParam> parameters) throws PersistanceException
	{
		Session session = getCurrentSession();
		int recordsUpdated;
		try
		{
			Query query = session.createQuery(queryStr);
			query = queryParamDecorator.decorateQuery(query, parameters);
			recordsUpdated = query.executeUpdate();
		} catch (Exception e)
		{
			logger.error("Error while getting object by id", e);
			throw new PersistanceException(e);
		}
		return recordsUpdated;
	}
}
