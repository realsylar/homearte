package com.homearte.commons.core.manager;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.homearte.commons.aws.entity.S3AccessCredentialParams;
import com.homearte.commons.aws.entity.S3MultipartFileParams;
import com.homearte.commons.core.config.ApplicationProperties;

@Service
public class S3BucketManager
{

	private S3AccessCredentialParams	credentialParams;
	private ApplicationProperties		properties;
	private static final String			BUCKET_NAME			= "s3_bucket_name_project_data";
	private static final String			ACCESS_KEY			= "s3_access_key";
	private static final String			ACCESS_SECRET_KEY	= "s3_access_secret_key";

	private void init()
	{

		this.credentialParams = new S3AccessCredentialParams(
				properties.getProperty(BUCKET_NAME),
				properties.getProperty(ACCESS_KEY),
				properties.getProperty(ACCESS_SECRET_KEY));
	}

	/**
	 * @param credentialParams
	 * @param mailingConfiguration
	 */
	@Autowired
	public S3BucketManager(ApplicationProperties properties)
	{
		super();
		this.properties = properties;
		init();
	}

	public void uploadFile(S3MultipartFileParams fileParams)
	{
		AWSCredentials credentials = new BasicAWSCredentials(
				credentialParams.getAccessKey(),
				credentialParams.getAccessSecretKey());
		AmazonS3 s3Client = new AmazonS3Client(credentials);
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(fileParams.getFileSize());
		s3Client.putObject(credentialParams.getBucketName(),
				fileParams.getFileKeyName(),
				fileParams.getMultipartFileInputStream(), metadata);
	}

	public S3MultipartFileParams recieveFile(S3MultipartFileParams fileParams)
	{
		AWSCredentials credentials = new BasicAWSCredentials(
				credentialParams.getAccessKey(),
				credentialParams.getAccessSecretKey());
		AmazonS3 s3Client = new AmazonS3Client(credentials);
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(fileParams.getFileSize());
		S3Object data = s3Client.getObject(credentialParams.getBucketName(),
				fileParams.getFileKeyName());
		fileParams.setFileData(data);
		fileParams.setFileSize(data.getObjectMetadata().getContentLength());
		return fileParams;
	}

	public void handlePostDataRead(S3MultipartFileParams fileParams)
			throws IOException
	{
		fileParams.getFileData().getObjectContent().close();
		fileParams.getFileData().close();
	}

	/**
	 * @param s3MultipartFileParams
	 */
	public void deleteFile(S3MultipartFileParams s3MultipartFileParams)
	{
		AWSCredentials credentials = new BasicAWSCredentials(
				credentialParams.getAccessKey(),
				credentialParams.getAccessSecretKey());
		AmazonS3 s3Client = new AmazonS3Client(credentials);
		s3Client.deleteObject(credentialParams.getBucketName(),
				s3MultipartFileParams.getFileKeyName());
	}

}