/**
 *
 */
package com.homearte.commons.core.cache;

/**
 * @author Anurag Agrawal
 *
 */
public class TestObject
{

	private int		z;
	private String	y;

	/**
	 * @return the z
	 */
	public int getZ()
	{
		return z;
	}

	/**
	 * @param z
	 *            the z to set
	 */
	public void setZ(int z)
	{
		this.z = z;
	}

	/**
	 * @return the y
	 */
	public String getY()
	{
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(String y)
	{
		this.y = y;
	}

	/**
	 * @param z
	 * @param y
	 */
	public TestObject(int z, String y)
	{
		super();
		this.z = z;
		this.y = y;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "TestObject [z=" + z + ", y=" + y + "]";
	}

}
