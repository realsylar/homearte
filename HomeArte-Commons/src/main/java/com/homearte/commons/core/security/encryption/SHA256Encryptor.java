/**
 *
 */
package com.homearte.commons.core.security.encryption;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 *
 * @author Anurag Agrawal
 *
 */
@Service
public class SHA256Encryptor
{

	private static final String	ALGO				= "SHA-256";
	private static final String	RANDOM_ALGO			= "SHA1PRNG";
	private static final int	LEFT_PAD_LENGTH		= 17;
	private static final String	LEFT_PAD_STRING		= "lpintek";
	private static final int	RIGHT_PAD_LENGTH	= 17;
	private static final String	RIGHT_PAD_STRING	= "rpintek";

	public SHAEncryptedValue encryptValue(String message)
			throws NoSuchAlgorithmException, InvalidKeyException
	{
		byte[] salt = new byte[32];
		SecureRandom.getInstance(RANDOM_ALGO).nextBytes(salt);
		String saltStr = Arrays.toString(salt);
		String encryptedMessage = encryptValue(message, saltStr);
		return new SHAEncryptedValue(encryptedMessage, saltStr);
	}

	public String encryptValue(String message, String salt)
			throws NoSuchAlgorithmException, InvalidKeyException
	{
		MessageDigest digest = MessageDigest.getInstance(ALGO);
		StringBuffer digestibleMessage = new StringBuffer();
		digestibleMessage.append(Arrays.toString(getPaddedValue(message)));
		digestibleMessage.append(salt);
		byte[] digestedData = digest.digest(digestibleMessage.toString()
				.getBytes());
		String encryptedMessage = Base64.encodeBase64String(Hex
				.encodeHexString(digestedData).getBytes());
		return encryptedMessage;
	}

	/**
	 * @param message
	 * @return
	 */
	private byte[] getPaddedValue(String message)
	{
		message = StringUtils.leftPad(message, message.length()
				+ LEFT_PAD_LENGTH, LEFT_PAD_STRING);
		message = StringUtils.rightPad(message, message.length()
				+ RIGHT_PAD_LENGTH, RIGHT_PAD_STRING);
		return message.getBytes();
	}

}
