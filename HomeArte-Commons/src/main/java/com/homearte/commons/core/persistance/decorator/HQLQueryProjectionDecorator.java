/**
 *
 */
package com.homearte.commons.core.persistance.decorator;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;

import com.homearte.commons.core.persistance.QueryProjection;
import com.homearte.commons.core.persistance.enums.QueryProjectionTypes;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class HQLQueryProjectionDecorator
{

	public Query decorateQuery(Query query, List<QueryProjection> projections)
	{
		if (projections != null)
			for (QueryProjection projection : projections)
			{
				if (projection.getType().getValue() == QueryProjectionTypes.START_LIMIT
						.getValue())
				{
					query.setFirstResult((Integer) projection.getValue());
				}
				else if (projection.getType().getValue() == QueryProjectionTypes.END_LIMIT
						.getValue())
				{
					query.setMaxResults((Integer) projection.getValue());
				}
			}
		return query;
	}
}
