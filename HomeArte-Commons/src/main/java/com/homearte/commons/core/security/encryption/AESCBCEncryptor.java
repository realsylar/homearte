/**
 *
 */
package com.homearte.commons.core.security.encryption;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homearte.commons.core.config.ApplicationProperties;
import com.homearte.commons.core.security.encryption.exception.EncryptorException;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class AESCBCEncryptor
{

	private ApplicationProperties	props;
	private String					mainKey;
	private String					secondKey;
	private static final String		AES_ENCRYPT_MAIN_KEY	= "aes_encrypt_main_key";
	private static final String		AES_ENCRYPT_2ND_KEY		= "aes_encrypt_2nd_key";
	private static final Logger		logger					= Logger.getLogger(AESCBCEncryptor.class);

	/**
	 * @param props
	 * @param mainKey
	 */
	@Autowired
	public AESCBCEncryptor(ApplicationProperties props)
	{
		super();
		this.props = props;
		this.mainKey = null;
		this.secondKey = null;
	}

	/**
	 *
	 */
	private void init()
	{
		if (mainKey == null || "".equals(mainKey.trim()))
			mainKey = props.getProperty(AES_ENCRYPT_MAIN_KEY);
		if (secondKey == null || "".equals(secondKey.trim()))
			secondKey = props.getProperty(AES_ENCRYPT_2ND_KEY);
	}

	public String encrypt(String plainMessage) throws EncryptorException
	{
		try
		{
			init();
			SecretKeySpec key = new SecretKeySpec(mainKey.getBytes("UTF-8"),
					"AES");
			IvParameterSpec iv = new IvParameterSpec(
					secondKey.getBytes("UTF-8"));
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key, iv);
			byte[] cipherText = new byte[cipher.getOutputSize(plainMessage
					.length())];
			cipherText = cipher.doFinal(plainMessage.getBytes());
			String cipherTextStr = new String(
					Base64.encodeBase64String(cipherText));
			return cipherTextStr;
		} catch (UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | NoSuchAlgorithmException
				| NoSuchPaddingException e)
		{
			logger.error("Exception occured while encrypting value : "
					+ e.getMessage());
			throw new EncryptorException(e);
		}

	}

	public String decrypt(String encMessage) throws EncryptorException
	{
		try
		{
			init();
			SecretKeySpec key = new SecretKeySpec(mainKey.getBytes("UTF-8"),
					"AES");
			IvParameterSpec iv = new IvParameterSpec(
					secondKey.getBytes("UTF-8"));
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key, iv);
			byte[] encMessageWithoutBase64 = encMessage.getBytes("UTF-8");
			encMessageWithoutBase64 = Base64
					.decodeBase64(encMessageWithoutBase64);
			byte[] plainText = cipher.update(encMessageWithoutBase64);
			plainText = cipher.doFinal();
			return new String(plainText, "UTF-8");
		} catch (UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | NoSuchAlgorithmException
				| NoSuchPaddingException e)
		{
			logger.error("Exception occured while decrypting value : "
					+ e.getMessage());
			throw new EncryptorException(e);
		}
	}
}
