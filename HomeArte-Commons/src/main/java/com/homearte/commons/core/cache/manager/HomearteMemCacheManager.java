/**
 *
 */
package com.homearte.commons.core.cache.manager;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homearte.commons.core.cache.MemoryCacheOps;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class HomearteMemCacheManager
{

	private MemoryCacheOps	cacheOps;

	/**
	 * @param cacheOps
	 */
	@Autowired
	public HomearteMemCacheManager(MemoryCacheOps cacheOps)
	{
		super();
		this.cacheOps = cacheOps;
	}

	public void addItemWithKey(String key, Object t)
	{
		cacheOps.addItemWithKey(key, t);
	}

	public void removeItemWithKey(String key)
	{
		cacheOps.removeItemWithKey(key);
	}

	public Object getItemWithKey(String key) throws ExecutionException
	{
		return cacheOps.getItemWithKey(key);
	}

	public void updateItemWithKey(String key, Object t)
	{
		cacheOps.updateItemWithKey(key, t);
	}
}
