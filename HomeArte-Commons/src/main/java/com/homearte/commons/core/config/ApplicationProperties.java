/**
 *
 */
package com.homearte.commons.core.config;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.homearte.commons.core.persistance.dao.PropertiesDAO;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ApplicationProperties
{

	private static final Logger	logger			= Logger.getLogger(ApplicationProperties.class);

	private Map<String, String>	keyValuePair	= Maps.newHashMap();
	private PropertiesDAO		propertiesDAO;

	@Autowired
	public ApplicationProperties(PropertiesDAO propertiesDAO)
	{
		this.propertiesDAO = propertiesDAO;
	}

	@PostConstruct
	@Transactional(propagation = Propagation.REQUIRED)
	public void loadProperties()
	{
		List<Object[]> list = propertiesDAO.loadPropertiesFromDB();
		Map<String, String> propertiesFromDB = Maps.newHashMap();
		for (Object[] keyValue : list)
		{
			propertiesFromDB.put((String) keyValue[0], (String) keyValue[1]);
		}
		keyValuePair.putAll(propertiesFromDB);
		logger.info("The map loaded from DB is : " + keyValuePair);
		propertiesFromDB = null;
	}

	public String getProperty(String key)
	{
		return keyValuePair.get(key);
	}
}
