/**
 *
 */
package com.homearte.commons.core.security.encryption;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.springframework.stereotype.Service;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class RandomSecureTokenGenerator
{

	private SecureRandom	random	= new SecureRandom();

	public String generateToken()
	{
		return new BigInteger(128, random).toString();
	}
}
