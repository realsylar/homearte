/**
 *
 */
package com.homearte.commons.core.persistance.decorator;

import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Component;

import com.homearte.commons.core.persistance.QueryParam;
import com.homearte.commons.core.persistance.enums.QueryParamTypes;

/**
 * @author Anurag Agrawal
 *
 */
@Component
public class HQLQueryParameterDecorator
{

	public Query decorateQuery(Query query, Map<String, QueryParam> params)
	{
		if (params != null)
			for (String paramName : params.keySet())
			{
				QueryParam param = params.get(paramName);
				if (param.getType().getValue() == QueryParamTypes.INT
						.getValue())
				{
					query.setInteger(paramName, (Integer) param.getValue());
				}
				else if (param.getType().getValue() == QueryParamTypes.STRING
						.getValue())
				{
					query.setString(paramName, (String) param.getValue());
				}
				else if (param.getType().getValue() == QueryParamTypes.BOOLEAN
						.getValue())
				{
					query.setBoolean(paramName, (Boolean) param.getValue());
				}
			}
		return query;
	}
}
