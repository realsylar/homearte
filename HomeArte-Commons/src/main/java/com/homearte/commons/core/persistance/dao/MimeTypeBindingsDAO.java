/**
 *
 */
package com.homearte.commons.core.persistance.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class MimeTypeBindingsDAO
{

	private static final String	SELECT_ALL_MIME_TYPE_BINDINGS_QUERY	= "select * from mime_type_mapping";
	private SessionFactory		sessionFactory;

	@Autowired
	public MimeTypeBindingsDAO(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public List<Object[]> loadBindingsFromDB()
	{
		List<Object[]> list = sessionFactory.getCurrentSession()
				.createSQLQuery(SELECT_ALL_MIME_TYPE_BINDINGS_QUERY).list();
		return list;
	}
}
