/**
 *
 */
package com.homearte.commons.core.persistance;

import com.homearte.commons.core.persistance.enums.QueryParamTypes;

/**
 * @author Anurag Agrawal
 *
 */
public class QueryParam
{

	private Object			value;
	private QueryParamTypes	type;

	/**
	 * @param value
	 * @param type
	 */
	public QueryParam(Object value, QueryParamTypes type)
	{
		super();
		this.value = value;
		this.type = type;
	}

	/**
	 * @return the value
	 */
	public Object getValue()
	{
		return value;
	}

	/**
	 * @return the type
	 */
	public QueryParamTypes getType()
	{
		return type;
	}

}
