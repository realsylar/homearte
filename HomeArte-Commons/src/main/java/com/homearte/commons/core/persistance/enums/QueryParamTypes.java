/**
 *
 */
package com.homearte.commons.core.persistance.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum QueryParamTypes
{
	INT(0), STRING(1), BOOLEAN(2);

	private int	value;

	/**
	 * @param value
	 */
	private QueryParamTypes(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static QueryParamTypes getEnumForValue(int value)
	{
		for (QueryParamTypes type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

}
