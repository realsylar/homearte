/**
 *
 */
package com.homearte.commons.core.persistance.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class PropertiesDAO
{

	private static final String	SELECT_ALL_APPLICATION_PROPERTIES_QUERY	= "select * from basic_property";
	private SessionFactory		sessionFactory;

	@Autowired
	public PropertiesDAO(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public List<Object[]> loadPropertiesFromDB()
	{
		List<Object[]> list = sessionFactory.getCurrentSession()
				.createSQLQuery(SELECT_ALL_APPLICATION_PROPERTIES_QUERY).list();
		return list;
	}
}
