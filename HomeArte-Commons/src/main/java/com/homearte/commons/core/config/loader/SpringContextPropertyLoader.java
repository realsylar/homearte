/**
 *
 */
package com.homearte.commons.core.config.loader;

import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class SpringContextPropertyLoader implements ApplicationContextAware
{

	private static ApplicationContext	applicationContext;
	private static final Logger			logger	= Logger.getLogger(SpringContextPropertyLoader.class);

	public static String getPropertyForKey(String key)
	{
		if (applicationContext != null)
		{
			return applicationContext.getMessage(key, null, Locale.US);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.context.ApplicationContextAware#setApplicationContext
	 * (org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext appContext)
			throws BeansException
	{
		if (appContext != null)
		{
			applicationContext = appContext;
			logger.debug("Application context is successfully loaded and set.");
		}
		else
		{
			logger.error("Application context could not be loaded.");
		}
	}
}
