/**
 *
 */
package com.homearte.commons.core.persistance.exception;

/**
 * @author Anurag Agrawal
 *
 */
public class PersistanceException extends Exception
{

	private Exception	originalException;

	public PersistanceException(Exception e)
	{
		originalException = e;
	}

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getCause()
	 */
	@Override
	public synchronized Throwable getCause()
	{
		return originalException;
	}

}
