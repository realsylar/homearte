/**
 *
 */
package com.homearte.commons.core.security.encryption;

/**
 * @author Anurag Agrawal
 *
 */
public class SHAEncryptedValue
{

	private String	encryptedValue;
	private String	salt;

	/**
	 * @param encryptedValue
	 * @param salt
	 */
	public SHAEncryptedValue(String encryptedValue, String salt)
	{
		super();
		this.encryptedValue = encryptedValue;
		this.salt = salt;
	}

	/**
	 * @return the encryptedValue
	 */
	public String getEncryptedValue()
	{
		return encryptedValue;
	}

	/**
	 * @return the salt
	 */
	public String getSalt()
	{
		return salt;
	}

}
