/**
 *
 */
package com.homearte.commons.core.cache;

import java.util.concurrent.ExecutionException;

/**
 * @author Anurag Agrawal
 *
 */
public interface MemoryCacheOps
{

	public void addItemWithKey(String key, Object t);

	public void removeItemWithKey(String key);

	public Object getItemWithKey(String key) throws ExecutionException;

	public void updateItemWithKey(String key, Object t);
}
