/**
 *
 */
package com.homearte.commons.core.cache;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class HomearteCache
{

	private static final long		CACHE_SIZE	= 500L;
	private Cache<String, Object>	cache;

	@PostConstruct
	private void init()
	{
		cache = CacheBuilder.newBuilder().maximumSize(CACHE_SIZE).build();
	}

	/**
	 * @return the cache
	 */
	public Cache<String, Object> getCache()
	{
		return cache;
	}

	/**
	 * @param cache
	 *            the cache to set
	 */
	public void setCache(Cache<String, Object> cache)
	{
		this.cache = cache;
	}

}
