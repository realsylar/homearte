/**
 *
 */
package com.homearte.commons.core.manager;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homearte.commons.aws.entity.SESMailingConfiguration;
import com.homearte.commons.aws.entity.SESMailingCredentialParams;
import com.homearte.commons.aws.entity.SESMailingMessageParams;
import com.homearte.commons.aws.enums.EmailBodyType;
import com.homearte.commons.aws.main.SESEmailSender;
import com.homearte.commons.core.config.ApplicationProperties;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class EmailSenderManager
{

	private SESMailingCredentialParams	credentialParams;
	private SESMailingConfiguration		mailingConfiguration;
	private ApplicationProperties		properties;
	private static final String			HOST					= "ses_host";
	private static final String			SMTP_USERNAME			= "ses_smtp_username";
	private static final String			SMTP_PASSWORD			= "ses_smtp_password";
	private static final String			PROTOCOL				= "ses_protocol";
	private static final String			AUTHENTICATION_REQUIRED	= "ses_auth_required";
	private static final String			STARTTLS_ENABLE			= "ses_starttls_enable";
	private static final String			STARTTLS_REQUIRED		= "ses_starttls_required";
	private static final String			PORT					= "ses_port";

	private void init()
	{

		this.credentialParams = new SESMailingCredentialParams(
				properties.getProperty(HOST),
				properties.getProperty(SMTP_USERNAME),
				properties.getProperty(SMTP_PASSWORD));
		this.mailingConfiguration = new SESMailingConfiguration(
				properties.getProperty(PROTOCOL),
				properties.getProperty(AUTHENTICATION_REQUIRED),
				properties.getProperty(STARTTLS_ENABLE),
				properties.getProperty(STARTTLS_REQUIRED),
				Integer.valueOf(properties.getProperty(PORT)));
	}

	/**
	 * @param credentialParams
	 * @param mailingConfiguration
	 */
	@Autowired
	public EmailSenderManager(ApplicationProperties properties)
	{
		super();
		this.properties = properties;
		init();
	}

	/**
	 *
	 * @param messageParams
	 * @throws MessagingException
	 */
	public void sendMail(SESMailingMessageParams messageParams)
			throws MessagingException
	{
		SESEmailSender emailSender = new SESEmailSender(mailingConfiguration);
		emailSender.sendEmail(messageParams, credentialParams);
	}

	/**
	 *
	 * @param fromAddress
	 * @param toAddresses
	 * @param ccAddresses
	 * @param bccAddresses
	 * @param subject
	 * @param body
	 * @param bodyType
	 * @throws MessagingException
	 */
	public void sendMail(String fromAddress, List<String> toAddresses,
			List<String> ccAddresses, List<String> bccAddresses,
			String subject, String body, EmailBodyType bodyType)
			throws MessagingException
	{
		sendMail(new SESMailingMessageParams(fromAddress, toAddresses,
				ccAddresses, bccAddresses, subject, body, bodyType));
	}

	/*
	 * private manageTheSESlimits() {
	 * 
	 * }
	 */
}
