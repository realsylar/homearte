/**
 *
 */
package com.homearte.commons.core.security.encryption.exception;

/**
 * @author Anurag Agrawal
 *
 */
public class EncryptorException extends Exception
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;
	private Exception			internalException;

	/**
	 * @param internalException
	 */
	public EncryptorException(Exception internalException)
	{
		super();
		this.internalException = internalException;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage()
	{
		return internalException.getMessage();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Throwable#getStackTrace()
	 */
	@Override
	public StackTraceElement[] getStackTrace()
	{
		return internalException.getStackTrace();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Throwable#printStackTrace()
	 */
	@Override
	public void printStackTrace()
	{
		internalException.printStackTrace();
	}

}
