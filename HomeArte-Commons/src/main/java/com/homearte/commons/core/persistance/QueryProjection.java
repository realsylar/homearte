/**
 *
 */
package com.homearte.commons.core.persistance;

import com.homearte.commons.core.persistance.enums.QueryProjectionTypes;

/**
 * @author Anurag Agrawal
 *
 */
public class QueryProjection
{

	private Object					value;
	private QueryProjectionTypes	type;

	/**
	 * @param value
	 * @param type
	 */
	public QueryProjection(Object value, QueryProjectionTypes type)
	{
		super();
		this.value = value;
		this.type = type;
	}

	/**
	 * @return the value
	 */
	public Object getValue()
	{
		return value;
	}

	/**
	 * @return the type
	 */
	public QueryProjectionTypes getType()
	{
		return type;
	}

}
