/**
 *
 */
package com.homearte.commons.validation.valiators;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Anurag Agrawal
 *
 */
public class PhoneNumberValidator
{

	private static final String	INDIAN_MOBILE_NUMBER_REGEX		= "^9\\d{9}$";
	private static final String	INDIAN_LANDLINE_NUMBER_REGEX	= "/^[0-9]\\d{2,4}-\\d{6,8}$/";

	public static boolean isValid(String phoneNumber)
	{
		if (!StringUtils.isBlank(phoneNumber))
		{
			if (phoneNumber.matches(INDIAN_MOBILE_NUMBER_REGEX)
					|| phoneNumber.matches(INDIAN_LANDLINE_NUMBER_REGEX))
				return true;
		}
		return false;
	}
}
